﻿
using QJY.Data;
using QjySaaSWeb.AppCode;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SZHL.UI.AppCode
{

    public partial class DownLoadFileN : Page
    {

        public string type
        {
            get { return Request.QueryString["type"] ?? "file"; }
        }

        public string MD5
        {
            get { return Request.QueryString["MD5"] ?? ""; }
        }
        public string Name
        {
            get { return Request.QueryString["Name"] ?? "测试文件"; }
        }
        public string FileId
        {
            get { return Request.QueryString["fileId"] ?? ""; }
        }
        public string ComId
        {
            get { return Request.QueryString["comId"] ?? ""; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string FileServerUrl = "";
                if (!string.IsNullOrEmpty(ComId))
                {
                    int cId = 0;
                    int.TryParse(ComId, out cId);
                    JH_Auth_QY qymodel = new JH_Auth_QYB().GetEntity(d => d.ComId == cId);
                    FileServerUrl = qymodel != null ? qymodel.FileServerUrl : "";
                }
                if (!string.IsNullOrEmpty(FileId) && FileServerUrl!="")
                { 
                    string filename = "";
                    int fileId = int.Parse(FileId.Split(',')[0]);
                    FT_File file = new FT_FileB().GetEntity(d => d.ID == fileId);
                    List<string> extends = new List<string>() { "jpg", "png", "gif", "jpeg" };
                    if (!extends.Contains(file.FileExtendName.ToLower()))//文件不是图片的不返回地址，此方法只用于图片查看
                    {
                        Response.AddHeader("Content-Disposition", "attachment;filename=" + Name);
                        Response.ContentType = "application/octet-stream";
                        filename = "/images/Vimg/qywd/"+file.FileExtendName+".png";
                    }
                    else {
                        Response.AddHeader("Content-Disposition", "attachment;filename=" + file.Name);
                        Response.ContentType = "application/octet-stream";
                        filename = FileServerUrl + file.FileMD5;
                    }
                   
                    Response.Redirect(filename);
                    return;
                }

                if (type == "file" && string.IsNullOrEmpty(FileId))
                {
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + Name);
                    Response.ContentType = "application/octet-stream";
                    string filename = FileServerUrl + MD5;
                    Response.Redirect(filename);
                }
                if (type == "folder")
                {

                    Response.AddHeader("Content-Disposition", "attachment;filename=" + Name);
                    Response.ContentType = "application/octet-stream";
                    string filename = FileServerUrl + "zipfile/" + MD5;
                    Response.Redirect(filename);

                }

            }
            catch (Exception ex) { }
            // Response.ContentType = "application/x-zip-compressed";
        }
    }
}