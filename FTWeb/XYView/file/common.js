$(function(){
    //设置按钮
    $(".setBtn").on("click",function(){
        $(".setCt").toggle();
    });

    //导航
    $(".nLink").on("click",function(){
        var $t = $(this),
            $p = $t.parent(),
            $next = $t.next(".subNavCt");
        if($next.html()){
            $next.slideToggle()
            $p.siblings(".navList").children(".subNavCt").hide()
        }
    });

    //移动版导航
    var bol = true,
        $nav = $("#nav"),
        $navBtn = $("#navBtn");
    $navBtn.on("click",function(){
        var $t = $(this);
        if($t.hasClass("active")){
            bol = false;
        }
        if(bol){
            bol = false;
            $t.addClass("active");
            $nav.show()
                .animate({
                    right:50
                },300)
        }else{
            $t.removeClass("active");
            hideNav();
        }
    });

    function hideNav(){
        bol = true;
        $navBtn.removeClass("active")
        $nav.animate({
            right:-200
        },300,function(){
            $nav.removeAttr("style");
        })
    }

    //导航移动
    function touch(obj){
        var t = {
            jqT : obj ,
            that : $(obj)[0] ,
            spirit : false ,
            tempX : 0,
            tempY : 0,
            touchStart : function(event){
                if (t.spirit ||! event.touches.length) return;
                var touch = event.touches[0];

                this.startX = touch.pageX;
                this.startY = touch.pageY;

                t.spirit = true;
            },
            touchMove : function(event){
                bol = false;
                event.preventDefault();
                if (!t.spirit || !event.touches.length) return;
                var touch = event.touches[0],
                    x = touch.pageX - this.startX,
                    y = touch.pageY - this.startY;

                if(this.tempX || this.tempY){
                    x += this.tempX;
                    y += this.tempY;
                }

                t.jqT.css({
                    "transform" : "translate("+ x +"px," + y +"px)",
                    "-moz-transform" : "translate("+ x +"px," + y +"px)",
                    "-webkit-transform" : "translate("+ x +"px," + y +"px)",
                    "-o-transform" : "translate("+ x +"px," + y +"px)",
                    "-ms-transform" : "translate("+ x +"px," + y +"px)"
                })

                this.x = x ;
                this.y = y ;
                
            },
            touchEnd : function(){
                if (!t.spirit) return;
                t.spirit = null;
                this.tempX = this.x;
                this.tempY = this.y;
                setTimeout(function(){
                    bol = true;
                },1)
            },
            init : function(){
                if(this.that){
                    this.that.addEventListener("touchstart", t.touchStart, false);
                    this.that.addEventListener("touchmove", t.touchMove, false);
                    this.that.addEventListener("touchend", t.touchEnd, false);

                }
            }
        }
        t.init();
    }
    touch($("#navBtn"))


    $(document).click(function(event){
        if($(event.target).parents('.settingBox').length == 0){
            $(".setCt").hide();
        }
        if($(event.target).parents('.nav').length == 0  && $(event.target).parents('.navBtn').length == 0 && !$(event.target).hasClass('navBtn')){
            hideNav();
        }
        if($(event.target).parents('.fSelect').length == 0){
            $(".fSltCt").hide();
        }

    });

    //下拉按钮
    $(".fSltNow").on("click",function(){
        $(this).next(".fSltCt").toggle();
    });
    $(".fSltCt span").on("click",function(){
        var $t = $(this);
            txt = $t.text();
            $tNow = $t.parent().prev(".fSltNow");
        $(".fSltCt").hide();
        $tNow.text(txt);
    });

    //字体选择
    $(".btnFont").on("click",function(){
        var $t = $(this),
            size = $t.attr("data-size");
        $t.addClass("btnFontAct")
            .siblings().removeClass("btnFontAct");
        $(".mCLassTb,.examBox").css("fontSize",size);
    })


    //sub nav
    $(".kbLink").on("click",function(){
        var $t = $(this),
            $tCt = $t.next(".subKbCt");
        if(!$tCt.text()) return;
        $t.toggleClass("active");
        $tCt.slideToggle();
        $t.siblings(".kbLink").removeClass("active");
        if($t.parent().hasClass("kbNav")){//如果是一级
            $t.siblings().find(".kbLink").removeClass("active");
            $tCt.siblings().find(".subKbCt").hide();
        }
        $tCt.siblings(".subKbCt").hide();
    })

    //sub nav
    $(".tagMoreBtn").on("click",function(){
        $(".tagMore").slideToggle();
    })

    //弹窗关闭
    $(".mTitClose").click(poWindows.close);



})


//窗口显示关闭
var poWindows = {
    setBody : function(t){
        this.b = $("body,html") ;
        if(t){
            this.b.removeClass("bHide")
        }else{
            this.b.addClass("bHide")
        }
    },
    close : function(){
        $(this).parents(".poWindows").fadeOut(300,function(){
            poWindows.setBody(true)
        });
    },
    show: function(obj){
        obj.fadeIn(300);
        poWindows.setBody(false)
    },
    closeAll : function(){
        $(".poWindows").fadeOut(300,function(){
            poWindows.setBody(true)
        });
    },
	reset:function(objId,title,height,width){
		$(objId+" .pCtBox").css("width",width?width:"80%");
		$(objId+" .iTouchBox").css("height",height?height:"auto");
		$(objId+" .mTitName").text(title);
		poWindows.show($(objId));
	},
	html:function(title,html,height,width){
		poWindows.reset("#sTouch",title,height,width);
		var htmlDiv = "<div style='padding:15px'>"+html+"</div>";
		$("#sTouch .iTouchBox").html(htmlDiv);
	    $("#iTouch").jScrollPane({
	        verticalDragMaxHeight: 68
	    })
	},
	ajax:function(title,url,params,height,width){
		poWindows.reset("#sTouch",title,height,width);
		$("#sTouch .iTouchBox").html("");
	    $.post(url,params?params:{},function(info){
			$("#sTouch .iTouchBox").html(info);
		});
	    $("#iTouch").jScrollPane({
	        verticalDragMaxHeight: 68
	    })
	},
	iframe:function(title,url,height,width){
		poWindows.reset("#sTouchIframe",title,height,width);
		$("#iTouchIframe").attr("src",url);
	}
}

//tab
var comp = {
    tab : function(tab,con,addClass,obj){
        var $_self = obj;
        var $_nav = $(tab);
        $_nav.removeClass(addClass),
            $_self.addClass(addClass);
        var $_index = $_nav.index($_self);
        var $_con = $(con);
        $_con.hide(),
            $_con.eq($_index).show();
    }
}

