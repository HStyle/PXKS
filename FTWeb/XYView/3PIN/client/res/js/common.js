var comp = {
    tab: function(tab, con, addClass, obj) { //tab
        var $_self = obj;
        var $_nav = $(tab);
        $_nav.removeClass(addClass);
        $_self.addClass(addClass);
        var $_index = $_nav.index($_self);
        var $_con = $(con);
        $_con.hide();
        $_con.eq($_index).show();
    }
};

var setHeight = {
    change: true, //是否改变位置
    full: false, //是否全屏
    subBox: $("#video"), //定义储存位置
    mBox: $("#mBox"), //定义储存位置2
    changeIng: true, //是否在调换位置中，
    frame: $(".frame"),
    pptMiddle: true,
    bind: function() {
        this.main = $("#main");
        this.lie = $("#lie");
        this.tit = $("#title");
        this.col = $(".col");
        this.lieCt = $("#tabCt");
        this.win = $(window);
        this.wHeight = this.win.height(); //可视高度
        this.wWidth = this.win.width() * .8; //可视宽度的80%
        this.maxHeight = this.wHeight - this.tit.height(); //除去头部剩下大小
        this.gap = 15; //间距大小

    },
    setPc: function() {
        //根据高度设置主题内容框架位置和大小
        var _h = this.maxHeight - 2 * this.gap,
                _w = _h * 1.33333;
        //如果主内容的宽度小于主宽度的50%或者大于70%，则总宽度设为可视窗口宽度，不按高度设置，按宽度60%设置，高度为4:3
        this.wWidth = this.win.width() * .8; //重设
        if (this.wWidth * .5 > _w || _w > this.wWidth * .75) {
            this.wWidth = this.wWidth * 1.25;
            _w = this.wWidth * .75;
            _h = _w * .75;
        }

//        var tempW = this.win.width();
//        if(tempW > 1000 && tempW < 1100){
//            _w = this.wWidth * .75
//            _h = _w * .75
//        }



        //设置主内容位置与大小
        this.mBox.css({
            right: this.gap,
            top: this.gap,
            height: _h,
            width: _w
        });

        $("#pptBox").unbind("dblclick");//取消绑定双击全屏

        if (!this.subBox.html()) {//如果是两分屏
            this.setSubPc(_w, _h);
            return false;
        }


        //设置video的位置宽高
        var _vW = this.wWidth - _w - 3 * this.gap,
                _vH = _vW * .75,
                _vR = _w + 2 * this.gap;
        this.subBox.css({
            right: _vR,
            top: this.gap,
            height: _vH,
            width: _vW
        });

        //设置目录
        var _lW = _vW,
                _lH = _h - _vH - this.gap;
        this.lie.removeAttr("style")
                .css({
                    right: _vR,
                    top: _vH + 2 * this.gap,
                    height: _lH,
                    width: _lW
                });

        this.lieCt.height(
                _lH - 39
                );

        this.setMainSize();

    },
    setSubPc: function(w, h) {
        var _vW = this.wWidth - w - 3 * this.gap,
                _vR = w + 2 * this.gap;
        $("#example_video_1").css({
            height: h
        });
        this.lie.css({
            right: _vR,
            top: this.gap,
            height: h,
            width: _vW
        });
        this.lieCt.height(
                h - 39
                );
        this.setMainSize();
    },
    setMainSize: function() {
        //最后给父级加上居中和宽度
        this.main.css({
            margin: "0 auto",
            width: this.wWidth
        });
    },
    setPhone: function() {
        //手机版宽度设置，宽度高度比为4:3(1:0.75)
        var _aw = this.win.width() - this.gap * 2,
                _ah = _aw * .75;
        this.col.removeAttr("style"); //如果设置了行内样式先清除
        //设置宽度自动
        this.main.css({
            width: "auto"
        });
        //设置高度
        this.subBox.css({
            height: _ah
        });
        this.mBox.css({
            height: _ah
        });
        $("#pptBox").bind("dblclick", function() {
            setHeight.setFull();
        });
    },
    exToggle: function(bool) { //tpggle函数
        if (bool) {
            bool = false;
        } else {
            bool = true;
        }
        return bool;
    },
    exchange: function(bool) {
        var videoBig = true;
        //根据传入的bool值判断改变位置
        if (bool) {
            this.subBox = $("#video");
            this.mBox = $("#mBox");
            $("#pptBox").removeClass("pptBoxMin");
            videoBig = false;
        } else {
            this.subBox = $("#mBox");
            this.mBox = $("#video");
            $("#pptBox").addClass("pptBoxMin");
        }

        var $v = this.subBox,
                $m = this.mBox,
                tempV = {right: $v.css("right"), width: $v.width(), height: $v.height()},
        that = this;

        //互换位置，先把其中之一的位置信息存入temp，再设置
        if (that.changeIng) {
            that.changeIng = false;
            $v.animate({
                width: $m.width(),
                height: $m.height(),
                right: $m.css("right")
            }, 500);

            $m.animate({
                width: tempV.width,
                height: tempV.height,
                right: tempV.right
            }, 500, function() {
                that.changeIng = true;
                that.frameType();
            });
        }



        if (videoBig) {
            this.videoSize($v);
        } else {
            this.videoSize($m);
        }




    },
    videoSize: function(obj) {
        $("#example_video_1").css({
            height: obj.height()
        });
    },
    set: function() {
        //如果当前是全屏则退出
        if (this.full) {
            this.fullSize(); //全屏的参数设置
            return false;
        }

        //判断宽度绑定设置方法
        if (this.win.width() < 769) {
            this.setPhone();
        } else {
            this.setPc();
        }

        var that = this;

        that.setFrame();


        var that = this;

        setTimeout(function() {
            if (!that.subBox.html()) {//如果是两分屏
                that.videoSize($("#mBox"));
                return false;
            }
            that.videoSize($("#video"));

        }, 1);
    },
    setFrame: function() {
        var that = this;

        that.frameType();

        this.frame.load(function() {
            that.frameType();
        });

        window.onresize = function() {
            that.frameType();
        };
        window.onorientationchange = function() {
            that.frameType()
        }

    },
    frameType: function() {

        var $f = this.frame,
                type = $f.attr("data-type");
        if (type === "ppt") { //用这个元素作为标识，如果有就表示为ppt
            var $ct = $f.contents().find("#SlideObj"),
                    $head = $f.contents().find("head"),
                    $body = $f.contents().find("body");
            $ct.css("width", "100%");
            $head.find("#resetCss").remove();
            $body.css({overflow: "hidden"});
            $head.append(
                    "<style id='resetCss'>*{margin:0;padding: 0}</style>"
                    );
            var h = $("#mBox").height();
            if (this.full) {
                h = $("#pptBox").height();
                $("#mBox").height(
                        this.win.height()
                        );
                if (this.pptMiddle) {
                    if (this.win.width() < 769) {
                        $(".pptBoxMiddle").removeClass("active")
                                .css({
                                    marginTop: -h / 2,
                                    marginLeft: -$("#pptBox").width() / 2
                                });

                    }
                }
            }
            $ct.css({
                height: h,
                fontSize: h / 28.5,
                left: 0
            });
        } else {//否则为word
            $(".pptBtnBox").hide();
            var $div = $f.contents().find("div").eq(0),
                    $div2 = $div.children("div"),
                    $tb = $div.find("table"),
                    $img = $f.contents().find("img"),
                    $head = $f.contents().find("head");
$body = $f.contents().find("body");
			/*
			$body.css("background","#ffffff");
            $head.find("#resetCss").remove();
            $head.append(
                    "<style id='resetCss'>*{margin:0;padding: 0}body,html{overflow-x:hidden;}html{background: #ffffff;}</style>"
                    );
            $div.css({
                width: "100%",
                overflowX: "hidden"
            });
            $div2.css("width", "80%");
            $tb.css("width", "100%");
            $img.removeAttr("height", "width")
                    .css("max-width", "100%");
			*/
            if (this.full) {
                var hhh = $f.contents().find("body").height();
                if (this.win.width() > 1400)
                    hhh += 200;
                $("#pptBox").css({
                    height: hhh
                });
                if (this.win.width() < 769) {
                    $("#pptBox").css({
                        width: this.win.width(),
                        marginTop: -10
                    });
                }
            }
			
			//------修复高清WORD样式
            $f.contents().find('.navbar').remove();
            $body.css("padding-top", 0);
            $f.contents().find(".word-page").css('margin-bottom', 0);
            $f.contents().find(".word-page").css('-moz-box-shadow', "0 0 0 0");
            $f.contents().find(".word-page").css('-webkit-box-shadow', "0 0 0 0");
            $f.contents().find(".word-page").css('box-shadow', "0 0 0 0");
            $f.contents().find(".word-page").css('max-width', "100%");

            var $svgs = $f.contents().find('embed');
            $svgs.each(function () {
                var theSvgElement = this.getSVGDocument().documentElement;
                theSvgElement.setAttribute('viewBox', 0 + " " + 0 + " " + 600 + " " + 800);
            });

            //---------------------

			
            return;
        }
    },
    setFull: function() {
        //如果全屏则把full设置为true
        this.full = true;
        //先把行内样式所设置的宽高定位等去掉，再添加所写好的全屏class
        $("#mBox").removeAttr("style")
                .addClass("mainFull");
        $("#main").width("auto");
		 $("#pptToolAll,#pptToolSingle").hide();
        this.fullSize();

    },
    fullSize: function() {
        var pptH = this.wHeight - 32,
                pptW = pptH * 1.3333;
        if (this.win.width() < 769) {
            this.wWidth = this.win.width();
        }
        if (this.wWidth < pptW) {
            pptW = this.wWidth - 16;
            pptH = pptW * .75;
        }
        this.tit.hide();

        var ppWidth = pptH * 1.3333;
        if (this.frame.attr("data-type") !== "ppt" && ppWidth > 850) {
            ppWidth = 850;
        }

        $("#pptBox").height(pptH)
                .width(ppWidth);

        $("#video").hide();
        this.setFrame();

        if (this.pptMiddle) {
            $(".pptBoxMiddle").removeClass("active")
                    .css({
                        marginTop: -pptH / 2,
                        marginLeft: -$("#pptBox").width() / 2
                    });
        }
    },
    closeFull: function() {
        //关闭全屏
        this.full = false;
        $("#mBox").removeClass("mainFull");
        $("#pptBox").removeAttr("style");
        $("#video").show();
        this.main.addClass("subMain");
        this.tit.show();
        //关闭后从新设置位置
        this.set();
        $("#pptToolAll,#pptToolSingle").hide();
        $(".subPptBox").remove();
        $("#navBox").removeClass("active");
        this.pptMiddle = true;
        $("#selectBox").show();
    },
    init: function() {

        this.bind(); //绑定参数获取数据
        this.set(); //绑定设置函数

    }
};


//导航移动
function touch(obj) {
    var t = {
        jqT: obj,
        that: $(obj)[0],
        spirit: false,
        tempX: 0,
        tempY: 0,
        touchStart: function(event) {
            if (t.spirit || !event.touches.length)
                return;
            var touch = event.touches[0];

            this.startX = touch.pageX;
            this.startY = touch.pageY;

            t.spirit = true;
        },
        touchMove: function(event) {
            bol = false;
            event.preventDefault();
            if (!t.spirit || !event.touches.length)
                return;
            var touch = event.touches[0],
                    x = touch.pageX - this.startX,
                    y = touch.pageY - this.startY;

            if (this.tempX || this.tempY) {
                x += this.tempX;
                y += this.tempY;
            }

            t.jqT.css({
                "transform": "translate(" + x + "px," + y + "px)",
                "-moz-transform": "translate(" + x + "px," + y + "px)",
                "-webkit-transform": "translate(" + x + "px," + y + "px)",
                "-o-transform": "translate(" + x + "px," + y + "px)",
                "-ms-transform": "translate(" + x + "px," + y + "px)"
            })

            this.x = x;
            this.y = y;

        },
        touchEnd: function() {
            if (!t.spirit)
                return;
            t.spirit = null;
            this.tempX = this.x;
            this.tempY = this.y;
            setTimeout(function() {
                bol = true;
            }, 1)
        },
        init: function() {
            if (this.that) {
                this.that.addEventListener("touchstart", t.touchStart, false);
                this.that.addEventListener("touchmove", t.touchMove, false);
                this.that.addEventListener("touchend", t.touchEnd, false);

            }
        }
    }
    t.init();
}


$(function() {

    //加载先执行
    setHeight.init();

    //切换位置函数绑定
    $("#btnQie").click(function() {
        setHeight.change = setHeight.exToggle(setHeight.change);
        setHeight.exchange(setHeight.change);
    });

    //全屏，关闭全屏
    $("#btnFull,#navBtnFull").click(function() {
        setHeight.setFull();
    });
    $("#btnCloseF").click(function() {
        setHeight.closeFull();
    });


    //显示关闭导航
    var lieShow = true;
    $("#navBtnLog").click(function() {
        var $lie = $("#lie"),
                lHeight;
        if (lieShow) {
            $lie.css("zIndex", "10000")
                    .show()
                    .animate({
                        right: 70
                    });
            lHeight = $lie.height() - 41;
            $("#tabCt").height(lHeight);
            lieShow = false;
        } else {
            $lie.animate({
                right: "-100%"
            }, function() {
                $lie.hide();
            });
            lieShow = true;
        }

    });

    $(document).on("click", function(event) {
        if ($(event.target).parents('#navBox').length === 0 && $(event.target).parents('#lie').length === 0 && !$(event.target).hasClass("btnAllFull")) {
            $("#navBox").removeClass("active");
            lieShow = true;
            if (setHeight.win.width() < 769) {
                $("#lie").hide();
            }
        }
    });


    //如果窗口改变则从新设置
    $(window).resize(function() {
        setHeight.init();
    });


    //tab
    $('.tabTitList').click(function() {
        comp.tab(".tabTitList", ".tabCtList", "active", $(this));
    }).eq(0).trigger("click");

    //菜单
    $('#btnShowNav').click(function() {
        $("#navBox").toggleClass("active");
    });


    /*=========================
     * 滑动方法示例
     * */

    $("#pptBox .pptCt>img").swipe({
        onswipeleft: function() {
            CW_Engine.prePage();
            //左滑动
        },
        onswiperight: function() {
            CW_Engine.nextPage();
            //右滑动
        }
    });



    //列表显示
    $("#pptToolAll").click(function() {
        setHeight.pptMiddle = false;
        $(".pptBtnBox,#pptToolAll").hide();
        $("#pptToolSingle").show();
        var $pptBox = $("#pptBox"),
                pptH = $pptBox.height(),
                pptW = $pptBox.width();
        //取得当前文档
        if (CW_Engine.type === 'ppt') {
            //插入之前的
            for (var i = CW_Engine.pos - 1; i >= 1; i--) {
                var url = CW_Engine.root + CW_Engine.dir + "/content.files/slide" + i + ".htm";
                $("#mBox .ct").prepend(
                        $("<div>").addClass("subPptBox")
                        .css({
                            width: pptW,
                            height: pptH
                        })
                        .append(
                                $("<div>", {class: "pptCt"}).append(
                                "<iframe src='" + url + "' data-type='ppt' class='frame'></iframe>"
                                )
                                )
                        );
            }
            //插入之后的
            for (var i = CW_Engine.pos + 1; i <= CW_Engine.pages; i++) {
                var url = CW_Engine.root + CW_Engine.dir + "/content.files/slide" + i + ".htm";
                $("#mBox .ct").append(
                        $("<div>").addClass("subPptBox")
                        .css({
                            width: pptW,
                            height: pptH
                        })
                        .append(
                                $("<div>", {class: "pptCt"}).append(
                                "<iframe src='" + url + "' data-type='ppt' class='frame'></iframe>"
                                )
                                )
                        );
            }
        }
        $(".pptBoxMiddle").addClass("active")
                .css({
                    margin: "0 auto"
                });
        setAddFrame();//设置文档后的内容
        //窗口可视大小改变
        $(window).resize(function() {
            $(".subPptBox").css({
                width: $pptBox.width(),
                height: $pptBox.height()
            });
            var h = $("#pptBox").height();
            $(".frame").contents().find("#SlideObj").css({
                height: h,
                fontSize: h / 28.5
            });
        });
    });

    //单页显示
    $("#pptToolSingle").click(function() {
        $(".subPptBox").remove();
        $(this).hide();
        $("#pptToolAll,.pptBtnBox").show();
        setHeight.pptMiddle = true;
        setHeight.frameType();
        var h = $("#pptBox").height();
        $(".pptBoxMiddle").removeClass("active")
                .css({
                    marginTop: -h / 2,
                    marginLeft: -$("#pptBox").width() / 2
                });
    });

    function setAddFrame() {
        var $fBox = $(".frame");
        $fBox.load(function() {
            $fBox.each(function() {
                var $f = $(this),
                        $ct = $f.contents().find("#SlideObj"),
                        $head = $f.contents().find("head"),
                        $body = $f.contents().find("body");
                $ct.css("width", "100%");
                $head.find("#resetCss").remove();
                $body.css({overflow: "hidden"});
                $head.append(
                        "<style id='resetCss'>*{margin:0;padding: 0}</style>"
                        );
                var h = $("#pptBox").height();
                $ct.css({
                    height: h,
                    fontSize: h / 28.5,
                    left: 0
                });
                $("#mBox").height("auto");

            });
        });

    }

    //手机版位置切换
    var tempQie = true;
    $("#navBtnQie").on("click", function() {
        if (tempQie) {//默认
            $("#video").before($("#mBox"))
            tempQie = false;
        } else {
            $("#mBox").before($("#video"))
            tempQie = true;
        }
    })


})
