function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]); return null;
}

function loadJS(url, callback, charset) {
	var script = document.createElement('script');
	script.onload = script.onreadystatechange = function() {
		if (script && script.readyState
				&& /^(?!(?:loaded|complete)$)/.test(script.readyState))
			return;
		script.onload = script.onreadystatechange = null;
		script.src = '';
		script.parentNode.removeChild(script);
		script = null;
		if (callback)
			callback();
	};
	script.charset = charset;
	script.src = url;
	try {
		document.getElementById("outOrder").appendChild(script);
	} catch (e) {
		console.log("加载js文件错误 ");
	}
}

/**
 * 
 * @returns {Number}
 */
function courseLength(){
	var l = 0;
	$.each(CW.videosAttr,function(index,elem){
		console.log("视频："+elem.vLength);
		l+=elem.vLength;
	});
	return l;
}
/**
 * 
 * @returns {Number}
 */
function playLength(){
	var vIndex = CW_Engine.LoadStatus.vinx;
	var l = 0;
	$.each(CW.videosAttr,function(index,elem){
		if(index<vIndex){
			l+=elem.vLength;
		}
		if(index==vIndex){
			l+=CW_Engine.Comp.video()[0].currentTime;
		}
	});
	return l;
}
/**
 * 直接设为预览状态
 * @returns {Boolean}
 */
CW_Engine.LoadStatus.preview = function() {
    return true;
};
/**
 * 初始化菜单点击事件
 * @returns {undefined}
 */
CW_Engine._initBtns = function() {
	var _busid = getQueryString("id");
	if(_busid !== undefined){
		CW_Engine._busid = _busid;
	}
	var _vinx = getQueryString("vinx");
	if(_vinx !== undefined){
		CW_Engine._vinx = _vinx;
	}
	var _vpos = getQueryString("vpos");
	if(_vpos !== undefined){
		CW_Engine._vpos = _vpos;
	}
	
	var _souref = getQueryString("souref");
	if(_souref !== undefined){
		
		
		var _busiId = getQueryString("busiId");
		var _trainid = getQueryString("trainid");
		if(_souref=='3'){//公司指定
			userTrainId = _busiId;
        	trainId = _trainid;
        	//标记，公司指定学习计
        	statType = '2'+_souref;
		}else if(_souref=='1'){//个人收藏
			userTrainId = _busiId;
        	trainId = _trainid;
        	//标记，公司指定学习计
        	statType = '2'+_souref;;
		}
		
		loadJS("/scripts/ajax.js",function(){},'utf-8');
		loadJS("/scripts/learningwatch.js",function(){
			userTrainId = _busiId;
        	trainId = _trainid;
        	//标记，公司指定学习计
        	statType = '2'+_souref;
			start();
			//重新设置页面页面关闭事件
			window.onbeforeunload = function(){
				
		    	saveSchedule(function(data, status) {
		            
		        });
		    	watch();
		  }

			
		},'utf-8');
		
	}

	//保存
    $("#saveBtn").click(function() {
    	saveSchedule(function(data, status) {
            try {
                var j = eval("(" + data.trim() + ")");
                if (j.status === '1') {
                    CW.save = j.save;
                    alert("保存成功");
                } else {
                    alert("保存失败：" + j.result);
                }
            } catch (e) {
                alert("保存失败，可能需要重新登陆系统！");
            }
            $("#saveBtn").show();
        });
		//alert("vinx=" + CW_Engine.LoadStatus.vinx + ",vpos=" +  CW_Engine.Comp.video()[0].currentTime +"save:"+CW.save);
        /*
    	$.post(
                "/cwnew/viewer_mark.jsp",
                {
                    "action":"mark",
                    "coursewareLength":courseLength(),
                    "playLength":playLength(),
					"vinx": CW_Engine.LoadStatus.vinx,
					"save": CW.save,
					"id": CW_Engine._busid,
					"vpos": CW_Engine.Comp.video()[0].currentTime
                },
        function(data, status) {
            try {
                var j = eval("(" + data.trim() + ")");
                if (j.status === '1') {
                    CW.save = j.save;
                    alert("保存成功");
                } else {
                    alert("保存失败：" + j.result);
                }
            } catch (e) {
                alert("保存失败，可能需要重新登陆系统！");
            }
            $("#saveBtn").show();
        }
        );
        */
    });
    
    window.onbeforeunload = function(){
    	saveSchedule(function(data, status) {
            
        });
  }

    //给菜单增加点击事件
    var $gtimes = $(".ctlSubTit").click(function() {
        var ix = $(this).parent().attr("id");
        var menu = CW_Engine.getMenu(ix);
        var sec = menu.time;
        if (sec !== undefined) {
            CW_Engine.goMenu(menu.video, sec);
        }
    });
    //上翻页
    $("#pptBtnPrev").click(function() {
        CW_Engine.prePage();
    });
    //下翻页
    $("#pptBtnNext").click(function() {
        CW_Engine.nextPage();
    });
};


//保存进度或书签
function saveSchedule(callBack){
	$.post("/cwnew/viewer_mark.jsp",
            {
                "action":"mark",
                "coursewareLength":courseLength(),
                "playLength":playLength(),
				"vinx": CW_Engine.LoadStatus.vinx,
				"save": CW.save,
				"id": CW_Engine._busid,
				"vpos": CW_Engine.Comp.video()[0].currentTime
            },
            function(data, status) {
				callBack(data, status);
			}
    );
}

/**
 * 渲染树菜单
 * @param {type} parent
 * @param {type} menu
 * @param {type} level
 * @returns {undefined}
 */
CW_Engine._initTree = function(parent, menu, level) {
    if (!level) {
        level = 0;
    }
    $.each(menu, function(index, element) {
        if (element.rended) {
            return;
        }
        element.rended = true;
        var $li;
        //每个菜单li都有唯一的ID为 ix
        //
        var subMenu = new Array();
        $.each(CW.menu, function(_index, _element) {
            if (_element.ix.length > element.ix.length && _element.ix.indexOf(element.ix) === 0) {
                subMenu.push(_element);
            }
        });

        if (subMenu.length > 0) {
            //说明有子菜单
            if (level === 0) {
                //从第二级目录递归渲染
                return CW_Engine._initTree(parent, subMenu, ++level);
            }
            if (level > 2) {
                $li = $("<li id=\"" + element.ix + "\"></li>");
                var $a = $("<a class=\"ctlSubTit\">" + element.name + "</a>");
                var $ol = $("<ul class=\"catalogCt\"></ul>");
                $li.append($a);
                $li.append($ol);
            } else {
                //第二级目录
                $li = $("<div></div>");
                var $a = $("<div class=\"catalogTit pIco active\">" + element.name + "</div>");
                var $ol = $("<ul class=\"catalogCt\"></ul>");
                $li.append($a);
                $li.append($ol);
            }
            //递归渲染
            CW_Engine._initTree($ol, subMenu, ++level);
        } else {
            //没有子菜单
            $li = $("<li style='cursor:pointer;' class=\"gtime\" id=\"" + element.ix + "\"></li>");
            var $a = $("<a class=\"ctlSubTit\">" + element.name + "</a>");
            $li.append($a);
        }
        parent.append($li);
    });
};

