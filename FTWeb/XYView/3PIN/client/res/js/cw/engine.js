/**
 * 课件引擎
 * @type type
 */
var CW_Engine = {
    _vinx: 0,
    _vpos: 0,
    /**
     * 初始化树（由viewer或maker实现）
     * @param {type} parent
     * @param {type} menu
     * @param {type} level
     * @returns {undefined}
     */
    _initTree: function (parent, menu, level) {
    },
    /**
     * 初始化按扭事件（由viewer或maker实现）
     * @returns {undefined}
     */
    _initBtns: function () {
    }
};

/**
 * 初始化
 * @returns {undefined}
 */
CW_Engine.init = function () {
    console.log("课件引擎初始化");
    CW_Engine.loadCW();
    //载入视频
    CW_Engine.loadVideos();
    //载入文档
    CW_Engine.loadDocs();
    //载入菜单
    CW_Engine.loadMenus();
};

/**
 * 加载状态
 * @type type
 */
CW_Engine.LoadStatus = {
    c: 0, //全部另载检测定时器
    vinx: -1, //当前视频文件
    dinx: -1, //当前文档文件
    cwLoaded: true, //课件JSON是否加载完成
    videoLoaded: false, //视频是否载入完成
    docLoaded: false, //文档是否加载文成
    menuLoaded: false, //菜单是否加载完成
    /**
     * 所有加载项是否加载完成
     * @returns {Boolean}
     */
    loaded: function () {
        return CW_Engine.LoadStatus.cwLoaded
                //&& CW_Engine.LoadStatus.videoLoaded
                && CW_Engine.LoadStatus.docLoaded
                && CW_Engine.LoadStatus.menuLoaded;
    },
    /**
     * 是否处于预览状态
     * @returns {unresolved}
     */
    preview: function () {
        return $("#previewBtn")[0].checked;
    }
};

/**
 * 注册文件
 * @param {type} menu
 * @param {type} file
 * @param {type} type
 * @returns {undefined}
 */
CW_Engine.regFile = function (menu, file, type, dir) {
    console.log("注册文档，文件=" + file + "，类型=" + type + "，菜单项数=" + menu.length);
    CW.files.push({
        "file": file,
        "dn": dir,
        "type": type,
        "pages": menu.length
    });
    CW.menu.push({
        ix: 'root_' + file + "_",
        name: file
    });
    if (menu.length === 0) {
        menu.push({
            ix: 'root_' + file + "_0",
            name: '请输入标题'
        });
    }
    $.each(menu, function (index, el) {
        el.ix = 'root_' + file + "_" + el.ix + "_";
        el.type = type;
        el.file = {
            "file": file,
            "type": type,
            "dn": dir
        };
    });
    $.merge(CW.menu, menu);
};

/**
 * 获取组件
 * @type type
 */
CW_Engine.Comp = {
    /**
     * 取得视频组件
     * @returns {$}
     */
    video: function () {
        return $("#example_video_1");
    },
    /**
     * 取得文档组件
     * @returns {$}
     */
    doc: function () {
        return $("#frame");
    },
    /**
     * 取得菜单组件
     * @returns {$}
     */
    menu: function () {
        return $("#catalog");
    },
    /**
     * 取得画布组件
     * @returns {$}
     */
    cav: function () {
        return $("#canvasComp");
    }
};

/**
 * 
 * @returns {undefined}
 */
CW_Engine.loadCW = function () {
    console.log("载入课件");
    //设置标题
    $(document).attr("title", CW.title);
    $(".hName").text(CW.title);
    //设置简介
    $(".intro").text(CW.desc);
    //设置链接
    $(".link").html("<a href='" + CW.link + "' target=_blank>" + CW.link + "</a>");
    /**
     * 当前是否为PPT课件
     * @returns {Boolean}
     */
    CW.isPPT = function () {
        if (CW_Engine.LoadStatus.dinx >= 0)
            return CW.files[CW_Engine.LoadStatus.dinx].type === 'ppt';
        return false;
    };
    /**
     * 当前是否为word课件
     * @returns {Boolean}
     */
    CW.isDOC = function () {
        if (CW_Engine.LoadStatus.dinx >= 0)
            return CW.files[CW_Engine.LoadStatus.dinx].type === 'doc';
        return false;
    };
    /**
     * 记录视频文件的总播放长度
     */
    CW.recordVideoLength = function (vname, vLength) {
        var hasInput = false;
        $.each(CW.videosAttr, function (index, element) {
            var name = element.vName;
            if (vname === name) {
                hasInput = true;
                element.vLength = vLength;
            }
        });
        if (!hasInput) {
            CW.videosAttr.push({ "vName": vname, "vLength": vLength });
        }
    }

    /**
     * 更改视频顺序
     */
    CW.exchangeVideo = function (vname1, vname2) {
        var tempElement = "";
        var tempIndex = "";
        $.each(CW.videos, function (index, element) {
            if (vname1 === element) {
                if (tempElement === "") {
                    tempElement = element;
                    tempIndex = index;
                } else {
                    CW.videos[tempIndex] = element;
                    CW.videos[index] = tempElement;
                }
            }
            if (vname2 === element) {
                if (tempElement == "") {
                    tempElement = element;
                    tempIndex = index;
                } else {
                    CW.videos[tempIndex] = element;
                    CW.videos[index] = tempElement;
                }
            }
        });

    }
    /**
     * 删除视频
     */
    CW.removeVideo = function (vname1) {
        var index = jQuery.inArray(vname1, CW.videos);
        CW.videos.splice(index, 1);

    }



    /**
    * 删除文档
    */
    CW.removeDoc = function (docid, docname) {
        //删除文档记录
        var deletdocindex = '';
        $.each(CW.files, function (i, f) {
            if (f.dn == docid) {
                deletdocindex = i;
            }
        });
        CW.files.splice(deletdocindex, 1);

        //删除菜单记录
        var menuArray = new Array();
        $.each(CW.menu, function (i, m) {
            if (m.ix.indexOf("_" + docname + "_") == -1) {
                menuArray.push(m);
            }
        });
        CW.menu = menuArray;

    }

    /**
     * 更改文档顺序
     */
    CW.exchangeDoc = function (docid1, docname1, docid2, docname2) {
        var tempElement = "";
        var tempIndex = "";
        //更换文档顺序
        var option1Index;
        var option2Index;
        $.each(CW.files, function (i, e2) {
            if (e2.dn == docid1) {
                option1Index = i;
            }
            if (e2.dn == docid2) {
                option2Index = i;
            }
        });
        var tempOption = CW.files[option1Index];
        CW.files[option1Index] = CW.files[option2Index];
        CW.files[option2Index] = tempOption;

        //更换菜单顺序
        var mChangeIndex1 = '';
        var mChangeIndex2 = '';
        $.each(CW.menu, function (index, element) {
            if (element.name == docname1) {
                mChangeIndex1 = index;
            }
            if (element.name == docname2) {
                mChangeIndex2 = index;
            }
        });
        var menuChangeTemp = CW.menu[mChangeIndex1];
        CW.menu[mChangeIndex1] = CW.menu[mChangeIndex2];
        CW.menu[mChangeIndex2] = menuChangeTemp;

        //将调大的节点 再调小
        var toTopIndex = mChangeIndex2;
        if (mChangeIndex1 > mChangeIndex2) {
            toTopIndex = mChangeIndex1;
        }
        var toTopIx = CW.menu[toTopIndex].ix;
        console.log("toTopIx：" + toTopIx);
        var fristIndex;
        $.each(CW.menu, function (index, element) {
            console.log("将调大的再调小：" + element.ix);
            if (element.ix.indexOf(toTopIx) == 0) {
                fristIndex = index;
                return false;
            }
        });
        var menuChangeTemp2 = CW.menu[toTopIndex];
        CW.menu[toTopIndex] = CW.menu[fristIndex];
        CW.menu[fristIndex] = menuChangeTemp2;


    }

    //检查是否全部资源可用
    CW_Engine.LoadStatus.c = setInterval(CW_Engine.loadCompleted, 1000);
};

/**
 * 初始化按扭
 * @returns {undefined}
 */
CW_Engine.initBtns = function () {
    //视频列表选择
    $("#videoSelect").change(function () {
        console.log("chnage:" + $(this).val());
        var ix = parseInt($(this).val());
        CW_Engine.loadVideo(ix);
    });
    //文档列表选择
    $("#docSelect").change(function () {
        //CW_Engine.VideoHandler.currentPos = -1;
        //$("#mBox").css("border-top", "");
        var ix = parseInt($(this).val());
        CW_Engine.loadDoc(ix);

    });
    CW_Engine._initBtns();
};

/**
 * 是否可以开始制作
 * @returns {undefined}
 */
CW_Engine.loadCompleted = function () {
    if (CW_Engine.LoadStatus.loaded()) {
        //清除定时检测
        clearInterval(CW_Engine.LoadStatus.c);
        //如果全部加载完毕，初始相关按扭事件
        //初始化按扭
        CW_Engine.initBtns();
        //显示视频播放控制条
        CW_Engine.Comp.video().attr("controls", "controls");
        if (CW_Engine.LoadStatus.preview()) {
            console.log("自动播放");
            $("#videoSelect").val(CW_Engine._vinx).trigger("change");
            CW_Engine.VideoHandler.seek = CW_Engine._vpos;
            CW_Engine.Comp.video()[0].play();
        }
    }
};

/**
 * 载入视频列表
 * @returns {undefined}
 */
CW_Engine.loadVideos = function () {
    //原数据加载事件
    CW_Engine.Comp.video().on("loadedmetadata", CW_Engine.VideoHandler.loadedMetaData);
    //播放进度事件
    CW_Engine.Comp.video().on("timeupdate", CW_Engine.VideoHandler.timeUpdate);
    //播放完成事件
    CW_Engine.Comp.video().on("ended", CW_Engine.VideoHandler.ended);
    //禁止视频右键菜单
    CW_Engine.Comp.video().bind('contextmenu', function () {
        return false;
    });
    //监控可播放事件
    CW_Engine.Comp.video().on("canplay", function () {
        console.log("视频可播放");
        //视频加载完成
        CW_Engine.LoadStatus.videoLoaded = true;
    });
    //载入视频列表
    $.each(CW.videos, function (index, element) {
        var x = element.indexOf("/");
        if (x > -1) {
            element = element.substring(x + 1);
        }
        $op = $("<option value=\"" + index + "\">" + element + "</option>");
        $("#videoSelect").append($op);
    });
    //载入首个视频
    CW_Engine.loadVideo();
};

/**
 * 载入视频
 * @param {type} ix
 * @returns {undefined}
 */
CW_Engine.loadVideo = function (ix) {
    if (ix === undefined) {
        //计算当前视频索引
        if ((CW_Engine.LoadStatus.vinx + 1) === CW.videos.length) {
            CW_Engine.LoadStatus.vinx = 0;
        } else {
            CW_Engine.LoadStatus.vinx += 1;
        }
    } else {
        CW_Engine.LoadStatus.vinx = ix;
    }
    console.log("加载视频 " + CW_Engine.LoadStatus.vinx);
    $("#videoSelect")[0].selectedIndex = (CW_Engine.LoadStatus.vinx);
    //设置视频源
    var vu = CW.dir + CW.videos[CW_Engine.LoadStatus.vinx];
    console.log("视频源 " + vu);
    CW_Engine.Comp.video().attr("src", vu);
    CW_Engine.Comp.video().attr("lang", CW.videos[CW_Engine.LoadStatus.vinx]);
};

/**
 * 加载文档列表
 * @returns {undefined}
 */
CW_Engine.loadDocs = function () {
    if (CW.files.length === 0) {
        CW_Engine.LoadStatus.docLoaded = true;
        return;
    }
    //载入列表
    $.each(CW.files, function (index, element) {
        var f = element.file;
        var x = f.indexOf("/");
        if (x > -1) {
            f = f.substring(x + 1);
        }
        $op = $("<option class=\"" + element.dn + "\" value=\"" + index + "\">" + f + "</option>");
        $("#docSelect").append($op);
    });
    //载入首个文档
    CW_Engine.loadDoc();
    console.log("文档列表渲染完成");
};

/**
 * 载入文档
 * @param {type} ix
 * @returns {undefined}
 */
CW_Engine.loadDoc = function (ix) {
    console.log("开始加载文档");

    if (ix === undefined) {
        //计算当前视频索引
        if ((CW_Engine.LoadStatus.dinx + 1) === CW.videos.length) {
            CW_Engine.LoadStatus.dinx = 0;
        } else {
            CW_Engine.LoadStatus.dinx += 1;
        }
    } else {
        CW_Engine.LoadStatus.dinx = ix;
    }
    var docObj = CW.files[CW_Engine.LoadStatus.dinx];
    //设置文档源
    var url;
    if (docObj.type === 'doc') {
        //word 直接放
        url = CW.dir + docObj.dn + "/content.html";
        //如果为word，则隐藏翻页控件
        $("#docFz").hide();
        var explorer = navigator.userAgent;
        if (explorer.indexOf(".NET") >= 0) {
            $("#frame").attr("data-type", "doc");
            $("#frame").css("z-index", "0");
            $(".newSltBox").css("display", "block");
        }

    } else if (docObj.type === 'ppt') {
        //ppt 放第一页
        var u = CW.dir + docObj.dn + "/content.files/1.svg";
        url = "res/pt.html?p=" + u;
        //url = CW.dir + docObj.dn + "/content.files/slide1.htm";
        //如果为ppt，则显示翻页控件
        $("#docFz").show();
        var explorer = navigator.userAgent;
        if (explorer.indexOf(".NET") >= 0) {
            $("#frame").attr("data-type", "ppt");
            $("#frame").css("z-index", "-1");
            $(".newSltBox").css("display", "");
        }
    }


    console.log("前往 " + url);
    CW_Engine.Comp.doc().attr("src", url);
    //监控文档完成事件
    CW_Engine.Comp.doc().load(function () {
        //设置文档框架空隙
        try {
            $(CW_Engine.Comp.doc().get(0).contentWindow.document.body).css("margin", 0);
        } catch (e) {

        }
        $("#docSelect").val(CW_Engine.LoadStatus.dinx);

        CW_Engine.Comp.doc().attr("data-type", CW.isDOC() ? "doc" : "ppt");
        //        document.getElementById('frame').dataset.type = "doc";
        //文档加载完成
        console.log("文档加载完成");
        CW_Engine.LoadStatus.docLoaded = true;
    });
};

/**
 * 载入菜单
 * @returns {undefined}
 */
CW_Engine.loadMenus = function () {
    //渲染树
    $.each(CW.menu, function (index, element) {
        element.rended = false;
    });
    CW_Engine._initTree(CW_Engine.Comp.menu(), CW.menu, "", "");
    CW_Engine.LoadStatus.menuLoaded = true;
};

/**
 * 重新载入菜单
 * @returns {undefined}
 */
CW_Engine.reloadMenus = function () {
    CW_Engine.LoadStatus.menuLoaded = false;
    CW_Engine.Comp.menu().empty();
    CW_Engine.loadMenus();
    _initMenuBtns();
};
/**
 * 根据时间，查找菜单
 * @param {type} sec
 * @param {type} vo
 * @returns {unresolved}
 */
CW_Engine.searchMenu = function (sec, vo) {
    console.log("搜索菜单，秒=" + sec);
    //如果处于非预览状态，则不再查找
    if (!CW_Engine.LoadStatus.preview()) {
        return null;
    }
    //先找到该时间对应的菜单数据
    var el = null;
    //查找菜单数据
    $.each(CW.menu, function (index, element) {
        if (element.time === sec && element.video === vo) {
            el = element;
            console.log("根据视频 " + vo + ", 时间 " + sec + " 查找到文档位置 " + el.pos);
            return false;
        }
    });
    //找到
    return el;
};

/**
 * 查找画布动作
 * @param {type} sec
 * @returns {unresolved}
 */
CW_Engine.searchDrs = function (sec) {
    console.log("搜索画布，秒=" + sec);
    if (!CW_Engine.LoadStatus.preview()) {
        return null;
    }
    return null;
};

/**
 * 根据菜单ID，取得菜单数据项
 * @param {type} ix
 * @returns {unresolved}
 */
CW_Engine.getMenu = function (ix) {
    var el = null;
    $.each(CW.menu, function (index, element) {
        if (element.ix === ix) {
            el = element;
            return false;
        }
    });
    return el;
};

/**
 * 根据视频路径，取得视频索引
 * @param {type} vfile
 * @returns {element}
 */
CW_Engine.getVideo = function (vfile) {
    var inx = null;
    $.each(CW.videos, function (index, element) {
        if (element === vfile) {
            inx = index;
            return false;
        }
    });
    return inx;
};

/**
 * 从指定的时间开始播放视频
 * @param {type} video
 * @param {type} sec
 * @returns {undefined}
 */
CW_Engine.goMenu = function (video, sec) {
    console.log("跳转菜单，视频=" + video + "，秒=" + sec);
    var ix = null;
    $.each(CW.videos, function (index, element) {
        if (element === video) {
            ix = index;
            return false;
        }
    });
    if (ix !== null) {
        CW_Engine.VideoHandler.lastPos = CW_Engine.VideoHandler.currentPos = 1;
        $("#videoSelect").val(ix).trigger("change");
        CW_Engine.VideoHandler.seek = sec;
    }
};

/**
 * 将文档滚动到指定位置
 * @param {type} f
 * @returns {undefined}
 */
CW_Engine.goPosition = function (f) {
    if (f.file) {
        if (f.file.type === 'doc') {
            CW_Engine._goTop(f.pos, f.file.dn);
        } else if (f.file.type === 'ppt') {
            CW_Engine._goPage(f.pos, f.file.dn);
        }
        //$("#mBox").css("border-top", "5px solid green");
    }
};

/**
 * 将word文档滚动到指定位置
 * @param {type} pos
 * @param {type} file
 * @returns {undefined}
 */
CW_Engine._goTop = function (pos, dn) {
    console.log("滚动文档 " + dn + ", 位置 " + pos);
    if (dn) {
        var u = CW_Engine.Comp.doc().get(0).contentWindow.document.location.href;
        if (u.indexOf(dn + "/content.html") === -1) {
            $.each(CW.files, function (index, element) {
                if (element.dn === dn) {
                    CW_Engine.LoadStatus.dinx = index;
                    $("#docSelect").val(index);
                    CW_Engine.loadDoc(index);
                    return false;
                }
            });
        } else {
            $(CW_Engine.Comp.doc().get(0).contentWindow.document.body).animate({ "scrollTop": pos });
        }
    } else {
        $(CW_Engine.Comp.doc().get(0).contentWindow.document.body).animate({ "scrollTop": pos });
    }
};

/**
 * 将ppt翻到指定页
 * @param {type} pos
 * @param {type} file
 * @returns {undefined}
 */
CW_Engine._goPage = function (pos, file) {
    if (!file) {
        file = CW.files[CW_Engine.LoadStatus.dinx].dn;
    }
    var count = $("#docSelect").get(0).options.length;
    for (var i = 0; i < count; i++) {
        if ($("#docSelect").get(0).options[i].className === file) {
            CW_Engine.LoadStatus.dinx = i;
            $("#docSelect").get(0).options[i].selected = true;
            break;
        }
    }

    var u = CW.dir + file + "/content.files/" + pos + ".svg";
    var hf = "res/pt.html?p=" + u;
    //var hf = CW.dir + file + "/content.files/slide" + pos + ".htm";
    console.log("前往 " + hf);
    CW_Engine.VideoHandler.currentPos = pos;
    CW_Engine.Comp.doc().get(0).contentWindow.document.location.href = hf;
    //$("#mBox").css("border-top", "");
};

/**
 * 返回ppt当前页
 * @returns {unresolved}
 */
CW_Engine.currentPage = function () {
    return CW_Engine.VideoHandler.currentPos;
};

/**
 * ppt 上翻
 * @returns {undefined}
 */
CW_Engine.prePage = function () {
    var p = CW_Engine.currentPage();
    if (p > 1) {
        CW_Engine._goPage(--p);
    }
    CW_Engine.prePageHandler();
};

/**
 * ppt下翻
 * @returns {undefined}
 */
CW_Engine.nextPage = function () {
    var p = CW_Engine.currentPage();
    //if (p < CW.menu.length - 1) {
    if (p < CW.files[CW_Engine.LoadStatus.dinx].pages) {
        CW_Engine._goPage(++p);
    }
    CW_Engine.nextPageHandler();
};

CW_Engine.nextPageHandler = function () { };
CW_Engine.prePageHandler = function () { };