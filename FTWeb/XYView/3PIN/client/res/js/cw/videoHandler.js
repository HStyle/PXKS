/**
 * 视频播放事件处理
 * @type sec
 */
CW_Engine.VideoHandler = {
    second: 0, //当前秒数
    preMenu: null, //上一菜单项控件
    lastPos: 1, //最后的自动位置
    currentPos: 1, //最后的手动位置
    seek: -1, //加载后即跳转
    loadedMetaData: function(event) {
        console.log("视频元数据加载完毕");
        console.log("登记视频长度："+event.target.lang+"=>"+event.target.duration);
        CW.recordVideoLength(event.target.lang,event.target.duration);
        if (CW_Engine.VideoHandler.seek > -1) {
            console.log("自动切换，自动播放");
            event.target.currentTime = CW_Engine.VideoHandler.seek;
            event.target.play();
            CW_Engine.VideoHandler.seek = -1;
        }
    },
    timeUpdate: function(event) {
        var sec = parseInt(event.target.currentTime);
        var vo = event.target.lang;
        if (sec === CW_Engine.VideoHandler.second)
            return;
        CW_Engine.VideoHandler.second = sec;
        //get current menu
        var _me = CW_Engine.searchMenu(sec, vo);
        if (_me !== null) {
            console.log("找到时间 " + sec + " 对应的菜单节点 " + _me.ix + ", 位置为 " + _me.pos);
            if (CW_Engine.VideoHandler.preMenu !== null) {
                CW_Engine.VideoHandler.preMenu.removeClass("active").removeClass("sli");
            }
            CW_Engine.VideoHandler.preMenu = $(document.getElementById(_me.ix)).find("a:first");
            CW_Engine.VideoHandler.preMenu.addClass("active").addClass("sli");
            //只有未手工翻页的情况下
            if (CW_Engine.VideoHandler.lastPos === CW_Engine.VideoHandler.currentPos) {
                CW_Engine.VideoHandler.lastPos = CW_Engine.VideoHandler.currentPos = _me.pos;
                CW_Engine.goPosition(_me);
            }
        }
        //get current draws
        var _dr = CW_Engine.searchDrs(sec);
        if (_dr !== null) {
            var cvs = CW_Engine.Comp.cav().get(0);
            var ctx = cvs.getContext('2d');
            ctx.lineWidth = 10;
            ctx.globalAlpha = 0.5;
            ctx.beginPath();
            var ps = _dr.pos;
            var pses = ps.split(";");
            $.each(pses, function(index, element) {
                var p = element.split(",");
                var px = p[0];
                var py = p[1];
                if (index === 0) {
                    ctx.moveTo(px, py);
                } else {
                    ctx.lineTo(px, py);
                }
            });
            ctx.stroke();
        }
    },
    ended: function() {
        CW_Engine.VideoHandler.seek = 0;
        console.log("视频播放完毕,切换下一个视频");
        if(CW_Engine.LoadStatus.vinx + 1 < CW.videos.length){
			CW_Engine.loadVideo();
		}else{
			if (CW_Engine.LoadStatus.preview()) {
				alert("播放完毕！");
			}
		}
    }
};

