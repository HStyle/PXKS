/**
 * 
 * @returns {undefined}
 */
CW_Engine._initBtns = function() {

    //保存
    $("#saveBtn").click(function() {
        CW.link = $("#link").text();
        CW.desc = $("#desc").text();
        var _cw = $.toJSON(CW);
        $.base64.utf8encode = true;
        _cw = $.base64.btoa(_cw);
        $(this).hide();
        $.post(
                "/cwnew/save_han_v5.jsp",
                {
                    "CW": _cw
                },
        function(data, status) {
            try {
                var j = eval("(" + data.trim() + ")");
                if (j.status === '1') {
                    CW.save = j.save;
                    //alert("保存成功,访问地址：" + j.result);
                    alert("保存成功");
                } else {
                    alert("保存失败：" + j.result);
                }
            } catch (e) {
                alert("登陆后再制作保存！");
            }
            $("#saveBtn").show();
        }
        );
    });

    //上翻页
    $("#pptBtnPrev").click(function() {
        CW_Engine.prePage();
    });
    //下翻页
    $("#pptBtnNext").click(function() {
        CW_Engine.nextPage();
    });

    _initMenuBtns();
};


/**
 * 渲染树菜单
 * @param {type} parent
 * @param {type} menu
 * @param {type} level
 * @returns {undefined}
 */
CW_Engine._initTree = function(parent, menu, level) {
    $.each(menu, function(index, element) {
        if (element.rended) {
            return;
        }
        element.rended = true;
        var $li;
        var _f = (level === "" ? "folder" + index : "sub" + level + index);
        //每个菜单li都有唯一的ID为 'm' + inx
        //
        var subMenu = new Array();
        $.each(CW.menu, function(_index, _element) {
            if (_element.ix.length > element.ix.length && _element.ix.indexOf(element.ix) === 0) {
                subMenu.push(_element);
            }
        });

        if (subMenu.length > 0) {
            //说明有子菜单
            $li = $("<li id=\"" + element.ix + "\"></li>");
            var $label = $("<label><span class=\"del\" title=\"删除\">×</span><input value=\"" + element.name + "\"/></label>");
            var $input = $("<input type=\"checkbox\" checked=\"checked\">");
            var $ol = $("<ol></ol>");
            $li.append($label);
            $li.append($input);
            $li.append($ol);

            //递归渲染
            CW_Engine._initTree($ol, subMenu, level + "folder");
        } else {
            //没有子菜单
            $li = $("<li id=\"" + element.ix + "\" class=\"file\"></li>");
            var pos = '0';
            if (element.type === 'ppt') {
                //如果是PPT，默认菜单对应的文档就是菜单的ID
                var sp = element.ix.split("_");
                pos = sp[sp.length - 2];
            }
            element.pos = pos;
            var $a = CW_Engine._createMenu(element.name, element.video + " " + Util.formatSec(element.time), element.file.file + " " + pos);
            $li.append($a);
        }
        parent.append($li);
    });
    //菜单加载完成
    CW_Engine.LoadStatus.menuLoaded = true;
};

function _initMenuBtns() {

    //加入树组件中的文本框事件
    $("ol li :input").mousemove(function() {
        $(this).css("background-color", "#fc6");
    }).mouseout(function() {
        $(this).css("background-color", "transparent");
    });

    //点击时间图标跳到时间位置
    $(".gtime").unbind("click").click(function() {
        var ix = $(this).parents("li:first").attr("id");
        var menu = CW_Engine.getMenu(ix);
        var sec = menu.time;
        if (sec) {
            CW_Engine.goMenu(menu.video, sec);
        }
    });
    //点击文档图标走到指定位置
    $(".gdoc").unbind("click").click(function() {
        var ix = $(this).parents("li:first").attr("id");
        var menu = CW_Engine.getMenu(ix);
        var pos = menu.pos;
        if (menu.type === 'ppt') {
            CW_Engine._goPage(pos, menu.file);
        } else if (menu.type === 'doc') {
            CW_Engine._goTop(pos, menu.file);
        }
    });
    //给视频时间输入框加入点击事件
    $(".ctime").unbind("click").click(function() {
        //取得视频的当前时间
        var time = parseInt(CW_Engine.Comp.video()[0].currentTime);
        //填在输入框中
        $(this).html(CW.videos[CW_Engine.LoadStatus.vinx] + " " + Util.formatSec(time));
        //取得菜单ID
        var ix = $(this).parents("li:first").attr("id");
        //设置菜单时间
        var menu = CW_Engine.getMenu(ix);
        menu.video = CW.videos[CW_Engine.LoadStatus.vinx];
        menu.time = time;
    });
    //组文档位置输入框加入点击事件
    $(".cdoc").unbind("click").click(function() {
        var pos;
        if (CW.isPPT()) {
            //ppt 取得当前页
            pos = CW_Engine.currentPage();
        } else if (CW.isDOC()) {
            //word 取得当前滚动位置
            pos = CW_Engine.Comp.doc().get(0).contentWindow.document.body.scrollTop;
        }
        //填在输入框中
        $(this).text(CW.files[CW_Engine.LoadStatus.dinx].file + " " + pos);
        //取得菜单ID
        var ix = $(this).parents("li:first").attr("id");
        //设置菜单位置
        var menu = CW_Engine.getMenu(ix);
        menu.pos = pos;
        menu.file = CW.files[CW_Engine.LoadStatus.dinx];
    });
    //在上方加入菜单事件
    $(".uad").unbind("click").click(function() {
        var $cli = $(this).parents("li:first");
        var ix = $cli.parents("li:first").attr("id") + Util.random(1000, 9999) + "_";
        var name = "请输入标题";
        var $li = $("<li id=\"" + ix + "\" class=\"file\"></li>");
        var $a = CW_Engine._createMenu(name);
        $li.append($a);
        $cli.before($li);
        CW.menu.push({
            "ix": ix,
            "name": name
        });
        _initMenuBtns();
    });
    //在下方加入菜单事件
    $(".dad").unbind("click").click(function() {
        var $cli = $(this).parents("li:first");
        var ix = $cli.parents("li:first").attr("id") + Util.random(1000, 9999) + "_";
        var name = "请输入标题";
        var $li = $("<li id=\"" + ix + "\" class=\"file\"></li>");
        var $a = CW_Engine._createMenu(name);
        $li.append($a);
        $cli.after($li);
        CW.menu.push({
            "ix": ix,
            "name": name
        });
        _initMenuBtns();
    });
    //添加子菜单事件
    $(".sad").unbind("click").click(function() {
        var $cli = $(this).parents("li:first");
        var $label = $("<label><span class=\"del\" title=\"删除\">×</span><input value=\"" + $cli.find("input")[0].value + "\"/></label>");
        var $input = $("<input type=\"checkbox\" checked=\"checked\" id=\"\"/> ");
        var $ol = $("<ol></ol>");
        $cli.removeClass();
        $cli.empty();
        $cli.append($label);
        $cli.append($input);
        $cli.append($ol);
        var ix = $cli.attr("id") + "1_";
        var name = "请输入标题";
        var $li = $("<li id=\"" + ix + "\" class=\"file\"></li>");
        $ol.append($li);
        var $a = CW_Engine._createMenu(name);
        $li.append($a);
        CW.menu.push({
            "ix": ix,
            "name": name
        });
        _initMenuBtns();
    });
    //删除按扭事件
    $(".del").unbind("click").click(function() {
        if (confirm("确认要删除吗？")) {
            var $cli = $(this).parents("li:first");
            var ix = $cli.attr("id");
            for (var i = CW.menu.length - 1; i >= 0; i--) {
                if (CW.menu[i].ix.indexOf(ix) > -1) {
                    CW.menu.splice(i, 1);
                }
            }
            $cli.remove();
        }
    });
    //显示操作按扭事件
    $(".nw").hide();
    $(".ma").unbind("mouseover").unbind("mouseout").mouseenter(function() {
        $(this).children(".nw").show();
    }).mouseleave(function() {
        $(this).children(".nw").hide();
    });
}

/**
 * 
 * @param {type} name
 * @param {type} pos
 * @returns {$}
 */
CW_Engine._createMenu = function(name, time, pos) {
    if (!pos || pos.indexOf("undefined") > -1) {
        pos = '-';
    }
    if(!time || time.indexOf("undefined") > -1){
        time = "--:--:--";
    }
    return $("<a class=ma><div class=\"nws\"><img class=\"gtime\" src='res/img/time.png' title='视频时间'/><span class=\"ctime\" title=\"点此将当前时间写入\">" + time + "</span><img class=\"gdoc\" src='res/img/pos.png' title='文档位置'/><span class=\"cdoc\" title=\"点此将当前文档位置写入\">" + pos + "</span><input value=\"" + name + "\"/></div><div class=\"nw\"><span class=\"del\" title=\"删除\">×</span><span class=\"uad\" title=\"上方插入同级菜单\">↑+</span><span class=\"dad\" title=\"下方插入同级菜单\">↓+</span><span class=\"sad\" title=\"插入子菜单\">→+</span></div></a>");
};