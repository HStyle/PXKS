﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QjySaaSWeb.AppCode
{
    public class WFEventArgs : EventArgs
    {
        public string DataID;
        public WFEventArgs(string _DataID)
        {
            DataID = _DataID;
        }
    }
}