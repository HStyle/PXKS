﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;
using System.Data;
using QJY.Data;


namespace QjySaaSWeb.AppCode
{
    public class FormHelp
    {

        public JH_Auth_FormBase GetFormCodeByFormCode(string strFormCode)
        {
            return new JH_Auth_FormBaseB().GetEntity(d => d.FormCode == strFormCode);
        }

        /// <summary>
        /// 添加表单记录
        /// </summary>
        /// <param name="strFormTable"></param>
        /// <param name="strUserName"></param>
        /// <returns></returns>
        public int addForm(string strFormTable, JH_Auth_User userInfo)
        {
            DataTable dtReturn = new JH_Auth_FormBaseB().GetDTByCommand(" INSERT INTO " + strFormTable + " ( CRDate,CRUser)VALUES('" + DateTime.Now.ToString("yyyy-MM-dd HH:mm") + "','" + userInfo.UserName + "') select @@identity");
            if (dtReturn.Rows.Count > 0)
            {
                return int.Parse(dtReturn.Rows[0][0].ToString());
            }
            else
            {
                return 0;
            }
        }
        /// <summary>
        /// 删除表单数据
        /// </summary>
        /// <param name="intFormID"></param>
        /// <param name="strFormTable"></param>
        public void DelForm(int intFormID, string strFormTable)
        {
            try
            {
                new JH_Auth_FormBaseB().ExsSql(" DELETE " + strFormTable + "   where  ID=" + intFormID);

            }
            catch (Exception)
            {

            }
        }





        #region 流程使用

        /// <summary>
        /// 设定表单为已完成状态
        /// </summary>
        /// <param name="intFormID"></param>
        public void UpdateFormStatus(int intFormID, string strFormTable)
        {
            string strSQL = " Update " + strFormTable + " set  IsComPlete = 'Y' WHERE  ID = " + intFormID + "";
            new JH_Auth_FormBaseB().ExsSql(strSQL);
        }

        /// <summary>
        /// 添加表单记录（带流程的）
        /// </summary>
        /// <param name="strFormTable">表名</param>
        /// <param name="strUserName">用户名</param>
        /// <param name="intProcessDefinitionID">流程ID</param>
        /// <returns></returns>
        public int addForm(string strFormTable, JH_Auth_User userInfo, int intProcessStanceid)
        {
            DataTable dtReturn = new JH_Auth_FormBaseB().GetDTByCommand(" INSERT INTO " + strFormTable + " ( CRDate,CRUser,intProcessStanceid,ComId)VALUES('" + DateTime.Now + "','" + userInfo.UserName + "'," + intProcessStanceid + ",'" + userInfo.ComId + "') select @@identity");
            if (dtReturn.Rows.Count > 0)
            {
                return int.Parse(dtReturn.Rows[0][0].ToString());
            }
            else
            {
                return 0;
            }
        }





        /// <summary>
        /// 通过流程ID找到对应的表单记录
        /// </summary>
        /// <param name="strFormTable">表单表名</param>
        /// <param name="FormColumnName">列名</param>
        /// <param name="intProcessInstanceID">流程实例ID</param>
        /// <returns></returns>
        public int GetFormIDByIntPRCID(string strFormTable, string FormColumnName, int intProcessInstanceID)
        {
            DataTable dtReturn = new JH_Auth_FormBaseB().GetDTByCommand("SELECT " + FormColumnName + "  FROM " + strFormTable + " WHERE intProcessStanceid=" + intProcessInstanceID);
            if (dtReturn.Rows.Count > 0)
            {
                return int.Parse(dtReturn.Rows[0][0].ToString());
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// 通过表单记录找到对应的流程ID
        /// </summary>
        /// <param name="strFormTable">表单表名</param>
        /// <param name="FormColumnName">列名</param>
        /// <param name="intFormID">流程实例ID</param>
        /// <returns></returns>
        public int GetIntPRCIDByFormID(string strFormTable, int intFormID)
        {
            DataTable dtReturn = new JH_Auth_FormBaseB().GetDTByCommand("SELECT intProcessStanceid  FROM " + strFormTable + " WHERE ID=" + intFormID);
            if (dtReturn.Rows.Count > 0)
            {
                return int.Parse(dtReturn.Rows[0][0].ToString());
            }
            else
            {
                return 0;
            }
        }


        /// <summary>
        /// 获取表单创建者
        /// </summary>
        /// <param name="strFormTable"></param>
        /// <param name="intProcessInstanceID"></param>
        /// <returns></returns>
        public string GetUserByIntPRCID(string strFormTable, int intProcessInstanceID)
        {
            DataTable dtReturn = new JH_Auth_FormBaseB().GetDTByCommand("SELECT  CRUser FROM " + strFormTable + " WHERE intProcessStanceid=" + intProcessInstanceID);
            if (dtReturn.Rows.Count > 0)
            {
                return dtReturn.Rows[0][0].ToString();
            }
            else
            {
                return "0";
            }
        }


        /// <summary>
        /// 根据流程号获取该流程号对应表单的类型
        /// </summary>
        /// <param name="intProcessDefinitionID"></param>
        /// <returns></returns>
        public JH_Auth_FormBase GetFormCodeByPDID(int intProcessDefinitionID)
        {
            JH_Auth_FormBase mode = new JH_Auth_FormBase();
            if (intProcessDefinitionID != 0)
            {
                mode = new JH_Auth_FormBaseB().GetEntities(d => d.FormProcessDefinitionID == intProcessDefinitionID).FirstOrDefault();
            }
            return mode;
        }






        /// <summary>
        /// 更新表单流程号,重新发起流程时使用
        /// </summary>
        /// <param name="strFormTable"></param>
        /// <param name="strUserName"></param>
        /// <param name="intProcessDefinitionID"></param>
        /// <returns></returns>
        public void updateFormProcessId(int intFormID, string strFormTable, int intProcessDefinitionID)
        {
            new JH_Auth_FormBaseB().GetDTByCommand("  update  " + strFormTable + "  set intProcessStanceid=" + intProcessDefinitionID + " where ID=" + intFormID);

        }








        /// <summary>
        /// 删除流程的全部通知
        /// </summary>
        /// <param name="intProcessDefinitionID"></param>
        public void delFormConfirm(string ProcessStanceCode)
        {
            new JH_Auth_ConFirmDataB().Delete(d => d.intProcessStanceid == ProcessStanceCode);
        }


        /// <summary>
        /// 获取需要接收的单据
        /// </summary>
        /// <param name="strUserCode"></param>
        /// <returns></returns>

        public List<JH_Auth_ConFirmData> GetConFirmListByUser(string strUserCode)
        {
            return new JH_Auth_ConFirmDataB().GetEntities(d => d.usercode == strUserCode && d.ConfirmDate == null && d.isSend == "Y").OrderByDescending(d => d.CRDate).ToList();
        }

        /// <summary>
        /// 获取已接收的数据
        /// </summary>
        /// <param name="strUserCode"></param>
        /// <returns></returns>

        public List<JH_Auth_ConFirmData> GetOldConFirmListByUser(string strUserCode)
        {
            return new JH_Auth_ConFirmDataB().GetEntities(d => d.usercode == strUserCode && d.ConfirmDate != null && d.isSend == "Y").ToList();
        }


        /// <summary>
        /// 获取表单接收人
        /// </summary>
        /// <param name="intProcessDefinitionID"></param>
        /// <returns></returns>

        public string GetConfirmUsers(string ProcessStanceCode)
        {
            string strReturn = "";
            List<string> ListUsers = new JH_Auth_ConFirmDataB().GetEntities(" intProcessStanceid ='" + ProcessStanceCode + "'").Select(d => d.usercode).ToList();
            strReturn = ListUsers.ListTOString(',');
            return strReturn;
        }

        /// <summary>
        /// 设定表单通知
        /// </summary>
        /// <param name="ProcessStanceCode"></param>
        /// <param name="hidConfirmUsers"></param>
        /// <param name="FormCode"></param>
        /// <param name="FormDec"></param>
        public void SaveFormConfirms(string ProcessStanceCode, string hidConfirmUsers, string FormCode, int FormID, string FormDec, JH_Auth_User userInfo, string strIsSend = "N")
        {
            JH_Auth_ConFirmData model = new JH_Auth_ConFirmDataB().GetEntities(" intProcessStanceid ='" + ProcessStanceCode + "' and ComId='" + userInfo.ComId + "'").FirstOrDefault();
            string UserName = model == null ? userInfo.UserName : model.CRUser;
            List<string> ListUser = hidConfirmUsers.SplitTOList(',').Distinct().ToList();
            foreach (string item in ListUser)
            {
                if (item != "" && new JH_Auth_ConFirmDataB().GetEntities(" intProcessStanceid ='" + ProcessStanceCode + "' AND usercode= '" + item + "' and ComId='" + userInfo.ComId + "'").Count() == 0)
                {
                    JH_Auth_ConFirmData temp = new JH_Auth_ConFirmData { isSend = strIsSend, FormCode = FormCode, intProcessStanceid = ProcessStanceCode, usercode = item, ConfirmType = 1, CRUser = UserName, PrcName = FormDec, FormID = FormID, CRDate = DateTime.Now, ComId = model.ComId };
                    new JH_Auth_ConFirmDataB().Insert(temp);
                }
            }
        }
        /// <summary>
        /// 批量发送表单通知
        /// </summary>
        /// <param name="ProcessStanceCode"></param>
        public void SendFormConfirm(string ProcessStanceCode)
        {
            string strSQL = " Update JH_Auth_ConFirmData set  isSend = 'Y' WHERE  intProcessStanceid = '" + ProcessStanceCode + "'";
            new JH_Auth_ConFirmDataB().ExsSql(strSQL);
        }

        /// <summary>
        /// 确认通知
        /// </summary>
        /// <param name="intProcessStanceid"></param>
        /// <param name="strUserName"></param>

        public void UpdateFormConfirmStust(string ProcessStanceCode, string strUserName)
        {
            string strSQL = " Update JH_Auth_ConFirmData set  ConfirmDate = '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " ' WHERE usercode='" + strUserName + "' AND intProcessStanceid = '" + ProcessStanceCode + "'";
            new JH_Auth_ConFirmDataB().ExsSql(strSQL);
        }


        /// <summary>
        /// 确认指定用户的全部通知
        /// </summary>
        /// <param name="strUserName"></param>
        public void UpdateFormConfirmStust(string strUserName)
        {
            string strSQL = " Update JH_Auth_ConFirmData set  ConfirmDate = '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " ' WHERE usercode='" + strUserName + "' AND  ConfirmDate is NULL";
            new JH_Auth_ConFirmDataB().ExsSql(strSQL);
        }
        #endregion

    }
}