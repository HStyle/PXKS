﻿using QJY.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.UI;


namespace QjySaaSWeb.AppCode
{
    /// <summary>
    ///BasePage 的摘要说明
    /// </summary>
    public class BasePage : Page
    {

        public JH_Auth_UserB.UserInfo UserInfo;

        public BasePage()
        {
            //
            //TODO: 在此处添加构造函数逻辑
            //
        }
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e); 
            if (Request.Cookies["szhlcode"] != null)
            {
                UserInfo = new JH_Auth_UserB().GetUserInfo(Request.Cookies["szhlcode"].Value.ToString());
            }
            if (Request.Cookies["openid"] != null)
            {
                UserInfo = new JH_Auth_UserB().GetUserInfoBYopenid(Request.Cookies["openid"].Value.ToString());
            }
        }

        /// <summary>
        /// BMK如果Session失效，重新登录
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)  //重写Page类中的OnLoad方法
        {
            //if (Request.Cookies["szhlcode"] == null && Request.Cookies["wxszhlcode"] == null)
            if (Request.Cookies["szhlcode"] == null && Request.Cookies["openid"]==null)
            {
               // string strUrl = System.Configuration.ConfigurationManager.AppSettings["WebLogin"].ToString();
                string strUrl = "/login.html";
                Response.Write("<script type='text/javascript'>window.parent.location='" + strUrl + "'</script>");
                Response.End();
            }
            else
            {



                //string strMenuCode = Request.QueryString["MenuCode"] ?? "-100";
                //if (strMenuCode != "")
                //{
                //    if (!new JH_Auth_MenuB().IsHasMenuAuth(UserInfo.User.UserName, int.Parse(strMenuCode)))
                //    {
                //        //不具有访问页面的权限
                //        Response.Redirect("~/View/Admin/ErrWeb.aspx");
                //        Response.End();
                //    }
                //    else
                //    {
                        
                //    }
                //}
            }
            base.OnLoad(e); //执行父类的OnLoad方法
        }

        /// <summary>
        /// 弹出信息提示
        /// </summary>
        /// <param name="strMsgType">0：成功，1：警告 2:提示</param>
        /// <param name="strMsg">信息内容</param>
        public void AlertMSG(string strMsgType, string strMsg)
        {
            switch (strMsgType)
            {
                case "0":
                    ClientScript.RegisterStartupScript(ClientScript.GetType(), "myscript", "<script>parent.MyDialog.success('" + strMsg + "');</script>");
                    break;
                case "1":
                    ClientScript.RegisterStartupScript(ClientScript.GetType(), "myscript", "<script>parent.MyDialog.warning('" + strMsg + "');</script>");
                    break;
                case "2":
                    ClientScript.RegisterStartupScript(ClientScript.GetType(), "myscript", "<script>parent.MyDialog.tips('" + strMsg + "');</script>");
                    break;
                case "3":
                    ClientScript.RegisterStartupScript(ClientScript.GetType(), "myscript", "<script>parent.MyDialog.success('" + strMsg + "');</script>");
                    break;
                case "4":
                    ClientScript.RegisterStartupScript(ClientScript.GetType(), "myscript", "<script>parent.MyDialog.warning('" + strMsg + "');</script>");
                    break;
                default:
                    ClientScript.RegisterStartupScript(ClientScript.GetType(), "myscript", "<script>parent.MyDialog.success('" + strMsg + "');</script>");
                    break;


            }
        }











    }
}