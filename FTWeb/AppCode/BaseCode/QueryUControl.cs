﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;

namespace QjySaaSWeb.AppCode
{
    [SerializableAttribute]
    public class QueryUControl : System.Web.UI.UserControl
    {


        /// <summary>
        /// 初始化表单的查询条件（一直存在于查询条件中）
        /// </summary>
        public virtual string SetinitQuery() { return ""; }



        /// <summary>
        /// 初始化表单的查询条件（只在首次查询时有效）
        /// </summary>
        public virtual string SetDefaultQuery() { return ""; }

        public virtual void FormQueryData() { }

        //获取模糊搜索的SQL

        public virtual string GetQuerydata() { return ""; }



    }

  
}