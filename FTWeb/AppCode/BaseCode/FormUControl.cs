﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using QJY.Data;

namespace QjySaaSWeb.AppCode
{
    /// <summary>
    /// 一般的父类用户控件
    /// </summary>
    [SerializableAttribute]
    public class FormUControl : System.Web.UI.UserControl
    {


        public JH_Auth_UserB.UserInfo UserInfo;

        public string strERROR = "ERROR";
        public string strSUCCESS = "SUCCESS";

        public virtual void initALLForm(string strFormCode) { }
        public virtual void initAddForm(string strFormCode) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="intFormID"></param>
        /// <param name="strActionType">VIEW:查看页面,UPDATE：编辑页面</param>
        public virtual void initMangerForm(int intFormID,string strActionType) { }
        /// <summary>
        ///  //验证表单(通过传入是否暂存,来确定验证是用于暂存还是送审 Y：暂存,N：送审)
        /// </summary>
        /// <param name="strActionType"></param>
        /// <returns></returns>
        public virtual string VerifyAddForm(string strAddType = "N")
        {
            return "";
        }
        public virtual string VerifyUpdateForm(int intFormID)
        {
            return "";
        }
        /// <summary>
        /// 保存表单数据
        /// </summary>
        public virtual bool SaveForm(int intFormID, string strActionType, int intProcessInstanceID = 0) { return true; }


        public virtual string SPForm(int intFormID,string strSPYJ)
        {
            return "";
        }

        public virtual string RebackForm(int intFormID, string strSPYJ)
        {
            return "";
        }


    }



    /// <summary>
    /// 带流程的父类用户控件
    /// </summary>
    [SerializableAttribute]
    public class WFFormUControl : FormUControl
    {
        /// <summary>
        /// 保存表单数据
        /// </summary>


        /// <summary>
        /// 流程开始前执行的事件
        /// </summary>
        /// <param name="strTaskInstanceID"></param>
        public virtual void StartedProcessBefore() { }

        /// <summary>
        /// 流程开始后执行的事件
        /// </summary>
        /// <param name="strTaskInstanceID"></param>
        public virtual void StartedProcessEnd(string strTaskInstanceID) { }




        /// <summary>
        /// 初始化审批表单
        /// </summary>
        /// <param name="intFormID">表单号</param>
        public virtual void initManageForm(int intFormID, string strTaskInstanceID) { }


        /// <summary>
        /// 验证审批界面的表单
        /// </summary>
        /// <param name="intFormID"></param>
        /// <param name="strTaskInstanceID"></param>
        /// <returns></returns>

        public virtual string VerifyMangeForm(int intFormID, string strTaskInstanceID)
        {
            return "";
        }



        /// <summary>
        /// 处理单据后调用的方法
        /// </summary>
        /// <returns></returns>
        public virtual void ManageForm(int intFormID, string strTaskInstanceID)
        {

        }

        /// <summary>
        /// 退回流程
        /// </summary>
        public virtual void RebackForm(int intFormID)
        {

        }



        /// <summary>
        /// 结束流程后调用的方法
        /// </summary>
        public virtual void ComPletetForm(JH_Auth_FormBase FormBaseData, int intFormID, int intProcessInstanceID)
        {

        }

        /// <summary>
        /// 取消流程执行的方法
        /// </summary>
        /// <param name="intProcessInstanceID">表单号</param>
        /// <returns></returns>
        public virtual bool CancelForm(int FormID)
        {
            return true;
        }
    }

}