﻿using QJYCUSTOM.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.Web.UI.WebControls;
using System.Text;

namespace QjySaaSWeb.AppCode
{
    public class CUS_SWRWGLB : BaseEFDao<CUS_SWRWGL>
    {


    }
    public class CUS_SWRWGL_ITEMB : BaseEFDao<CUS_SWRWGL_ITEM>
    {
        public string strGetDBCount(string strGroupCode)
        {
            int Count = new CUS_SWRWGL_ITEMB().GetEntities(d => d.GroupCode == strGroupCode).Count();
            int DBCount = new CUS_SWRWGL_ITEMB().GetEntities(d => d.GroupCode == strGroupCode&&d.RWFS==null).Count();
            return DBCount.ToString() + "/" + Count.ToString();
        }
    }
}