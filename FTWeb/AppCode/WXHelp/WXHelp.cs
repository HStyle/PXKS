﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using System.Data;
using Senparc.Weixin;
using Senparc.Weixin.Entities;
using Senparc.Weixin.QY;
using Senparc.Weixin.QY.Entities;
using Senparc.Weixin.QY.CommonAPIs;
using Senparc.Weixin.QY.AdvancedAPIs;
using Senparc.Weixin.QY.AdvancedAPIs.App;

using Senparc.Weixin.QY.AdvancedAPIs.OAuth2;
using Senparc.Weixin.Helpers;
using Senparc.Weixin.HttpUtility;
using System.IO;
using System.Text;
using System.Threading;
using Senparc.Weixin.QY.AdvancedAPIs.Mass;
using QJY.Data;
using Senparc.Weixin.QY.AdvancedAPIs.MailList;
using Senparc.Weixin.QY.AdvancedAPIs.Chat;
using Senparc.Weixin.QY.AdvancedAPIs.ShakeAround;
using System.Net;

namespace QjySaaSWeb.AppCode
{
    public class WXHelp
    {

        private string WXUrl = "";//  
        public string corpId = "";//"wx102a8aac04b33c39";// 
        private string corpSecret = "";//"gtXlCYi2NHCGXG9R3Yc_VD9q3Gcujw39BV7U84nQKhyxs4OmpNyS_S5VSwS1YzjR";//
        private string appTZGGID = "";// GetWXInfo("appTZGGID");//通知公告应用ID
        private string appGRXXID = "";// GetWXInfo("appGRXXID");//个人消息ID
        public string FileServerUrl = "";
        public string FileServerUrlUpload = "";
        public string isUseWX = "N";//"Y";//是否使用企业号的开关
        public string IMToken = "";
        public string IMAesKey = "";
        public string isWXSQ = "N";//"Y";//是否需要授权
        public string suite_id = "";//应用套件id
        public string suite_secret = "";//应用套件secret
        public string suite_ticket = "";//微信后台推送的ticket
        public string permanent_code = "";//企业号永久授权码
        public string authCorpID = "";//授权方corpId
        public string authAppID = "";//授权方应用Id ,非授权时发消息的app的ID
        public string ModelCode = "";//应用Id
        public string appType = "1";  //1.消息型，2.主页型

        public WXHelp()
        { }
        public WXHelp(JH_Auth_QY Qyinfo, string strCode)
        {
            //获取企业信息

            string strurl = Qyinfo.WXUrl;
            if (strurl.Substring(strurl.Length - 1, 1) == "/")
            {
                WXUrl = strurl;
            }
            else
            {
                WXUrl = strurl + "/";
            }
            corpId = Qyinfo.corpId;
            corpSecret = Qyinfo.corpSecret;
            isUseWX = Qyinfo.IsUseWX;
            FileServerUrl = Qyinfo.FileServerUrl;
            if (FileServerUrl.Substring(FileServerUrl.Length - 1, 1) != "/")
            {
                FileServerUrl = FileServerUrl + "/";
            }
            FileServerUrlUpload = FileServerUrl + "fileupload?qycode=" + Qyinfo.QYCode;

            IMToken = Qyinfo.Token;
            IMAesKey = Qyinfo.EncodingAESKey;

            if (isUseWX == "Y")
            {
                //企业model信息
                if (strCode == "")
                {
                    var qy1 = new JH_Auth_QY_ModelB().GetEntities(" ComId ='" + Qyinfo.ComId + "' and QYModelCode='QYTX' and isnull(AgentId,'')!='' ").FirstOrDefault();
                    if (qy1 != null) { strCode = "QYTX"; }
                    else
                    {
                        var qy = new JH_Auth_QY_ModelB().GetEntities(" ComId ='" + Qyinfo.ComId + "' and isnull(AgentId,'')!='' ").FirstOrDefault();
                        if (qy != null)
                        {
                            var mod = new JH_Auth_ModelB().GetEntity(p => p.ID == qy.ModelID);
                            if (mod != null)
                            {
                                strCode = mod.ModelCode;
                            }
                        }
                    }
                }

                ModelCode = strCode;

                var zds = new JH_Auth_ZiDianB().GetEntities(" class=4 and comid=" + Qyinfo.ComId + " and ','+Remark+',' like '%," + strCode + ",%'");
                if (zds.Count() > 0)
                {
                    var zd = zds.FirstOrDefault();
                    if (!string.IsNullOrEmpty(zd.TypeName))
                    {
                        strCode = zd.TypeName;
                    }
                }
                if (strCode != "")
                {
                    var app = new JH_Auth_ModelB().GetEntity(p => p.ModelCode == strCode);
                    if (app != null)
                    {
                        authAppID = app.AppID;
                        appType = app.AppType;
                        if (app.IsSQ == "0")
                        {
                            isWXSQ = "N";
                        }
                        else
                        {

                            var tj = new JH_Auth_QY_TJB().GetEntity(p => p.TJID == app.TJId && p.ComId == Qyinfo.ComId);
                            if (tj != null)
                            {
                                permanent_code = tj.PermanentCode;
                                authCorpID = tj.CorpID;
                            }

                            var pj = new JH_Auth_WXPJB().GetEntity(p => p.TJID == app.TJId);
                            if (pj != null)
                            {
                                suite_id = pj.TJID;
                                suite_secret = pj.TJSecret;
                                suite_ticket = pj.Ticket;
                            }

                            var yy = new JH_Auth_QY_ModelB().GetEntity(p => p.ModelID == app.ID && p.ComId == Qyinfo.ComId);
                            if (yy != null)
                            {
                                authAppID = yy.AgentId;
                            }

                            //isUseWX = "Y";
                            isWXSQ = "Y";
                        }

                    }
                }
            }
        }

        public string GetToken()
        {
            if (isWXSQ == "Y")
            {
                string SuiteToken = ThirdPartyAuthApi.GetSuiteToken(suite_id, suite_secret, suite_ticket).suite_access_token;
                return ThirdPartyAuthApi.GetCorpToken(SuiteToken, suite_id, authCorpID, permanent_code).access_token;
            }
            else
            {
                if (isUseWX == "Y")
                {
                    AccessTokenResult Token = CommonApi.GetToken(corpId.Trim(), corpSecret.Trim());
                    return Token.access_token;
                }
                else
                {
                    return "";
                }
            }
        }

        public JsApiTicketResult GetTicket()
        {
            if (isWXSQ == "Y")
            {
                string access_token = GetToken();
                var url = string.Format("https://qyapi.weixin.qq.com/cgi-bin/get_jsapi_ticket?access_token={0}",
                   access_token);

                JsApiTicketResult result = Get.GetJson<JsApiTicketResult>(url);

                return result;
            }
            else if (isUseWX == "Y")
            {
                JsApiTicketResult js = CommonApi.GetTicket(corpId, corpSecret);
                return js;
            }
            return null;
        }

        public JsonGroupTicket GetGroup_Ticket()
        {
            string access_token = GetToken();
            var url = string.Format("https://qyapi.weixin.qq.com/cgi-bin/ticket/get?access_token={0}&type=contact",
               access_token);

            JsonGroupTicket js = Get.GetJson<JsonGroupTicket>(url);
            //new QjySaaSWeb.AppCode.JH_Auth_LogB().Insert(new QJY.Data.JH_Auth_Log() { CRDate = DateTime.Now, LogContent = "接口调用，action：" +new JavaScriptSerializer().Serialize(js) });
            return js;
        }

        #region 消息相关
        public void SendTH(List<Article> MODEL, string ModelCode, string type, string strUserS = "@all")
        {
            try
            {
                if (strUserS == "")
                {
                    return;
                }
                thModel th = new thModel();
                th.MODEL = MODEL;
                //th.ID = ID;
                th.authAppID = authAppID;
                th.UserS = string.IsNullOrEmpty(strUserS) ? "@all" : strUserS;
                if (isUseWX == "Y")
                {
                    th.MODEL.ForEach(d => d.Url = WXUrl + "View_Mobile/UI/UI_COMMON.html?funcode=" + ModelCode + "_" + type + (d.Url == "" ? "" : "_" + d.Url) + "&corpid=" + corpId);

                    th.MODEL.ForEach(d => d.PicUrl = (string.IsNullOrEmpty(d.PicUrl) ? "" : FileServerUrl + "image/" + new FT_FileB().ExsSclarSql("select FileMD5 from FT_File where ID='" + d.PicUrl + "'").ToString()));


                    //System.Threading.ThreadPool.QueueUserWorkItem(new System.Threading.WaitCallback(SendCommonMSG), th);
                    if (appType == "1")
                    {
                        MassApi.SendNews(GetToken(), th.UserS.Replace(',', '|'), "", "", authAppID, th.MODEL);
                    }
                    else
                    {
                        MassApi.SendText(GetToken(), th.UserS.Replace(',', '|'), "", "", authAppID, th.MODEL[0].Title);
                    }
                }

                //System.Threading.ThreadPool.QueueUserWorkItem(new System.Threading.WaitCallback(SendCommonMSG), th);
            }
            catch (Exception ex)
            {
                CommonHelp.WriteLOG(ex.ToString());
            }
        }

        /// <summary>
        /// 发送通知消息
        /// </summary>
        /// <param name="MODEL"></param>
        /// <param name="flag"></param>
        /// <param name="ID"></param>
        /// <param name="strUserS"></param>
        private void SendCommonMSG(object o)//Article MODEL, string flag, int ID, string strUserS = "@all")
        {
            try
            {
                thModel th = (thModel)o;
                MassApi.SendNews(GetToken(), th.UserS.Replace(',', '|'), "", "", th.authAppID, th.MODEL);

            }
            catch { }
        }



        /// <summary>
        /// 图文消息
        /// </summary>
        /// <param name="Msgs"></param>
        /// <param name="strAPPID"></param>
        /// <param name="strUserS"></param>
        public void SendWXMsg(List<Article> Msgs, string strAPPID, string strUserS = "@all")
        {
            try
            {
                if (strUserS == "")
                {
                    return;
                }
                if (isUseWX == "Y")
                {
                    MassApi.SendNews(GetToken(), strUserS, "", "", strAPPID, Msgs);
                }
            }
            catch { }
        }

        /// <summary>
        /// 文字消息
        /// </summary>
        /// <param name="MsgText"></param>
        /// <param name="strAPPID"></param>
        /// <param name="strUserS"></param>
        public void SendWXRText(string MsgText, string strAPPID, string strUserS = "@all")
        {
            try
            {
                if (strUserS == "")
                {
                    return;
                }
                if (isUseWX == "Y")
                {
                    if (strUserS != "")
                    {
                        MassApi.SendText(GetToken(), strUserS, "", "", strAPPID, MsgText);
                    }
                }
            }
            catch { }
        }
        /// <summary>
        /// 图片消息
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="strAPPID"></param>
        /// <param name="strUserS"></param>
        public void SendImage(string filePath, string strAPPID, string strUserS = "@all")
        {
            try
            {
                if (strUserS == "")
                {
                    return;
                }
                if (isUseWX == "Y")
                {
                    Senparc.Weixin.QY.AdvancedAPIs.Media.UploadTemporaryResultJson md = MediaApi.Upload(GetToken(), Senparc.Weixin.QY.UploadMediaFileType.image, filePath);
                    if (md.media_id != "")
                    {
                        MassApi.SendImage(GetToken(), strUserS, "", "", strAPPID, md.media_id);
                    }
                }
            }
            catch { }
        }
        //文件消息
        public void SendFile(string filePath, string strAPPID, string strUserS = "@all")
        {
            try
            {
                if (strUserS == "")
                {
                    return;
                }
                if (isUseWX == "Y")
                {
                    Senparc.Weixin.QY.AdvancedAPIs.Media.UploadTemporaryResultJson md = MediaApi.Upload(GetToken(), Senparc.Weixin.QY.UploadMediaFileType.file, filePath);
                    if (md.media_id != "")
                    {
                        MassApi.SendFile(GetToken(), strUserS, "", "", strAPPID, md.media_id);
                    }

                }
            }
            catch { }
        }

        #endregion


        #region 组织机构相关
        public string GetCodeURL(string Redurl)
        {
            string url = "";
            if (isUseWX == "Y")
            {
                url = OAuth2Api.GetCode(corpId, Redurl, "");
            }
            return url;
        }
        public string GetUserDataByCode(string strCode)
        {
            string UserCode = "";

            try
            {
                if (isUseWX == "Y")
                {

                    GetUserInfoResult OBJ = OAuth2Api.GetUserId(GetToken(), strCode);
                    UserCode = OBJ.UserId;

                }
            }
            catch (Exception EX)
            {
                new JH_Auth_LogB().Insert(new JH_Auth_Log() { CRDate = DateTime.Now, LogContent = "获取用户代码GetUserDataByCode" + EX.Message.ToString() });

            }

            return UserCode;
        }
        public int WX_CreateBranch(JH_Auth_Branch Model)
        {

            int pid = 0;
            var bm = new JH_Auth_BranchB().GetEntity(p => p.DeptCode == Model.DeptRoot && p.ComId == Model.ComId);
            if (bm != null)
            {
                pid = Int32.Parse(bm.WXBMCode.ToString());
            }
            return MailListApi.CreateDepartment(GetToken(), Model.DeptName, pid, Model.DeptShort, Model.WXBMCode).id;

        }
        //同步部门使用
        public int WX_CreateBranchTB(JH_Auth_Branch Model)
        {

            int pid = 0;
            var bm = new JH_Auth_BranchB().GetEntity(p => p.DeptCode == Model.DeptRoot && p.ComId == Model.ComId);
            if (bm != null)
            {
                pid = Int32.Parse(bm.WXBMCode.ToString());
            }
            return MailListApi.CreateDepartment(GetToken(), Model.DeptName, pid, Model.DeptShort).id;

        }
        public QyJsonResult WX_UpdateBranch(JH_Auth_Branch Model)
        {
            QyJsonResult Ret = new QyJsonResult();
            if (isUseWX == "Y")
            {
                int pid = 0;
                var bm = new JH_Auth_BranchB().GetEntity(p => p.DeptCode == Model.DeptRoot && p.ComId == Model.ComId);
                if (bm != null)
                {
                    pid = Int32.Parse(bm.WXBMCode.ToString());
                }
                Ret = MailListApi.UpdateDepartment(GetToken(), Model.WXBMCode.ToString(), Model.DeptName, pid, Model.DeptShort);
            }
            return Ret;
        }

        public QyJsonResult WX_DelBranch(string strDeptCode)
        {
            QyJsonResult Ret = new QyJsonResult();
            if (isUseWX == "Y")
            {
                Ret = MailListApi.DeleteDepartment(GetToken(), strDeptCode);
            }
            return Ret;
        }
        public GetDepartmentListResult WX_GetBranchList(string strDeptCode)
        {
            GetDepartmentListResult Ret = new GetDepartmentListResult();
            int? id = null;
            if (!string.IsNullOrEmpty(strDeptCode))
            {
                id = Int32.Parse(strDeptCode);
            }
            if (isUseWX == "Y")
            {
                Ret = MailListApi.GetDepartmentList(GetToken(), id);
            }
            return Ret;
        }
        public QyJsonResult WX_CreateTag(JH_Auth_Role Model)
        {
            QyJsonResult Ret = new QyJsonResult();
            if (isUseWX == "Y")
            {
                Ret = MailListApi.CreateTag(GetToken(), Model.RoleName, Model.WXBQCode);
            }
            return Ret;
        }
        public QyJsonResult WX_UpdateTag(JH_Auth_Role Model)
        {
            QyJsonResult Ret = new QyJsonResult();
            if (isUseWX == "Y")
            {
                int bqid = Int32.Parse(Model.WXBQCode.ToString());
                Ret = MailListApi.UpdateTag(GetToken(), bqid, Model.RoleName);
            }
            return Ret;
        }
        public QyJsonResult WX_DelTag(int strBQCode)
        {
            QyJsonResult Ret = new QyJsonResult();
            if (isUseWX == "Y")
            {
                Ret = MailListApi.DeleteTag(GetToken(), strBQCode);
            }
            return Ret;
        }
        public QyJsonResult WX_AddTagMember(JH_Auth_UserRole Model)
        {
            var role = new JH_Auth_RoleB().GetEntity(p => p.RoleCode == Model.RoleCode);
            QyJsonResult Ret = new QyJsonResult();
            if (isUseWX == "Y")
            {
                string[] userList = { Model.UserName };
                int bqid = Int32.Parse(role.WXBQCode.ToString());
                Ret = MailListApi.AddTagMember(GetToken(), bqid, userList, null);
            }
            return Ret;
        }
        public QyJsonResult WX_DelTagMember(int strBQCode, string[] userList)
        {
            QyJsonResult Ret = new QyJsonResult();
            if (isUseWX == "Y")
            {
                Ret = MailListApi.DelTagMember(GetToken(), strBQCode, userList);
            }
            return Ret;
        }
        public GetTagListResult WX_GetTagList()
        {
            GetTagListResult Ret = new GetTagListResult();

            if (isUseWX == "Y")
            {
                Ret = MailListApi.GetTagList(GetToken());
            }
            return Ret;
        }
        public GetTagMemberResult WX_GetTagMember(int tagid)
        {
            GetTagMemberResult Ret = new GetTagMemberResult();
            if (isUseWX == "Y")
            {
                Ret = MailListApi.GetTagMember(GetToken(), tagid);
            }
            return Ret;
        }
        public GetMemberResult WX_GetUser(string username)
        {
            GetMemberResult Ret = new GetMemberResult();
            try
            {
                if (isUseWX == "Y")
                {
                    Ret = MailListApi.GetMember(GetToken(), username);
                }
            }
            catch
            {
            }
            return Ret;
        }
        public QyJsonResult WX_CreateUser(JH_Auth_User Model)
        {
            try
            {
                QyJsonResult Ret = new QyJsonResult();
                if (isUseWX == "Y")
                {
                    int[] Branch = { new JH_Auth_BranchB().GetEntity(d => d.DeptCode == Model.BranchCode).WXBMCode.Value };
                    Ret = MailListApi.CreateMember(GetToken(), Model.UserName, Model.UserRealName, Branch, Model.zhiwu, Model.mobphone, Model.mailbox, Model.weixinnum);
                }
                return Ret;
            }
            catch (Exception ex)
            {
                QyJsonResult Ret = new QyJsonResult();
                new QjySaaSWeb.AppCode.JH_Auth_LogB().Insert(new QJY.Data.JH_Auth_Log() { CRDate = DateTime.Now, LogContent = Model.UserName + "新增错误：" + ex.ToString() });
                return Ret;
            }
        }
        /// <summary>
        /// 更新用户包括状态
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        public QyJsonResult WX_UpdateUser(JH_Auth_User Model)
        {
            try
            {
                //  CommonHelp.WriteLog("Token:" +  GetToken() + " UserName" + Model.UserName + "UserRealName:" + Model.UserRealName + "Branch:" + Model.BranchCode + "zhiwu:" + Model.zhiwu + "mobphone：" + Model.mobphone + "mailbox：" + Model.mailbox + "weixinNum：" + Model.weixinnum);
                QyJsonResult Ret = new QyJsonResult();
                if (isUseWX == "Y")
                {

                    int[] Branch = { new JH_Auth_BranchB().GetEntity(d => d.DeptCode == Model.BranchCode).WXBMCode.Value };
                    Ret = MailListApi.UpdateMember(GetToken(), Model.UserName, Model.UserRealName, Branch, Model.zhiwu, Model.mobphone, Model.mailbox, Model.weixinnum, Model.IsUse == "Y" ? 1 : 0);
                }
                return Ret;
            }
            catch (Exception ex)
            {
                QyJsonResult Ret = new QyJsonResult();
                new QjySaaSWeb.AppCode.JH_Auth_LogB().Insert(new QJY.Data.JH_Auth_Log() { CRDate = DateTime.Now, LogContent = Model.UserName + "更新错误：" + ex.ToString() });
                return Ret;
            }
        }

        public QyJsonResult WX_DelUser(string strUserName)
        {
            QyJsonResult Ret = new QyJsonResult();
            if (isUseWX == "Y")
            {
                Ret = MailListApi.DeleteMember(GetToken(), strUserName);
            }
            return Ret;
        }

        public QyJsonResult WX_GetDepartmentList()
        {
            QyJsonResult Ret = new QyJsonResult();
            if (isUseWX == "Y")
            {
                Ret = MailListApi.GetDepartmentList(GetToken());
            }
            return Ret;
        }
        public QyJsonResult WX_GetDepartmentMember(int depid)
        {
            QyJsonResult Ret = new QyJsonResult();
            if (isUseWX == "Y")
            {
                Ret = MailListApi.GetDepartmentMember(GetToken(), depid, 1, 0);
            }
            return Ret;
        }
        public GetDepartmentMemberInfoResult WX_GetDepartmentMemberInfo(int depid)
        {
            GetDepartmentMemberInfoResult Ret = new GetDepartmentMemberInfoResult();
            if (isUseWX == "Y")
            {
                Ret = MailListApi.GetDepartmentMemberInfo(GetToken(), depid, 1, 0);
            }
            return Ret;
        }
        #endregion

        #region 企业会话
        /// <summary>
        /// 创建会话
        /// </summary>
        /// <param name="chatid"></param>
        /// <param name="name"></param>
        /// <param name="owner"></param>
        /// <param name="userlist"></param>
        /// <returns></returns>
        public QyJsonResult WX_CreateChat(string chatid, string name, string owner, string[] userlist)
        {
            QyJsonResult Ret = new QyJsonResult();
            if (isUseWX == "Y")
            {
                Ret = ChatApi.CreateChat(GetToken(), chatid, name, owner, userlist);
            }
            return Ret;
        }
        /// <summary>
        /// 会话变更
        /// </summary>
        /// <param name="chatid"></param>
        /// <param name="opUser"></param>
        /// <param name="name"></param>
        /// <param name="owner"></param>
        /// <param name="addUserList"></param>
        /// <param name="delUserList"></param>
        /// <returns></returns>
        public QyJsonResult WX_UpdateChat(string chatid, string opUser, string name = null, string owner = null, string[] addUserList = null, string[] delUserList = null)
        {
            QyJsonResult Ret = new QyJsonResult();
            if (isUseWX == "Y")
            {
                Ret = ChatApi.UpdateChat(GetToken(), chatid, opUser, name, owner, addUserList, delUserList);
            }
            return Ret;
        }
        /// <summary>
        /// 退出会话
        /// </summary>
        /// <param name="chatid"></param>
        /// <param name="owner"></param>
        /// <returns></returns>
        public QyJsonResult WX_QuitChat(string chatid, string opUser)
        {
            QyJsonResult Ret = new QyJsonResult();
            if (isUseWX == "Y")
            {
                Ret = ChatApi.QuitChat(GetToken(), chatid, opUser);
            }
            return Ret;
        }
        /// <summary>
        /// 获取会话
        /// </summary>
        /// <param name="chatid"></param>
        /// <returns></returns>
        public GetChatResult WX_GetChat(string chatid)
        {
            GetChatResult Ret = new GetChatResult();
            if (isUseWX == "Y")
            {
                Ret = ChatApi.GetChat(GetToken(), chatid);
            }
            return Ret;
        }
        /// <summary>
        /// 清除消息未读状态
        /// </summary>
        /// <param name="chatid"></param>
        /// <param name="type"></param>
        /// <param name="chatIdOrUserId"></param>
        /// <returns></returns>
        public QyJsonResult WX_ClearNotify(string chatid, Chat_Type type, string chatIdOrUserId)
        {
            QyJsonResult Ret = new QyJsonResult();
            if (isUseWX == "Y")
            {
                Ret = ChatApi.ClearNotify(GetToken(), chatid, type, chatIdOrUserId);
            }
            return Ret;
        }
        /// <summary>
        /// 发消息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="type"></param>
        /// <param name="msgType"></param>
        /// <param name="chatIdOrUserId"></param>
        /// <param name="contentOrMediaId"></param>
        /// <returns></returns>
        public QyJsonResult WX_SendChatMessage(string sender, Chat_Type type, ChatMsgType msgType, string chatIdOrUserId, string contentOrMediaId)
        {
            QyJsonResult Ret = new QyJsonResult();
            if (isUseWX == "Y")
            {
                Ret = ChatApi.SendChatMessage(GetToken(), sender, type, msgType, chatIdOrUserId, contentOrMediaId);
            }
            return Ret;
        }
        public SetMuteResult WX_SetMute(List<UserMute> userMuteList)
        {
            SetMuteResult Ret = new SetMuteResult();
            if (isUseWX == "Y")
            {
                Ret = ChatApi.SetMute(GetToken(), userMuteList);
            }
            return Ret;
        }
        #endregion


        #region 应用相关
        public QyJsonResult GetAPPinfo(int agentId)
        {
            GetAppInfoResult Ret = new GetAppInfoResult();
            if (isUseWX == "Y")
            {
                Ret = AppApi.GetAppInfo(GetToken(), agentId);
            }
            return Ret;
        }
        public QyJsonResult SetAPPinfo(SetAppPostData data)
        {
            QyJsonResult Ret = new QyJsonResult();
            if (isUseWX == "Y")
            {
                Ret = AppApi.SetApp(GetToken(), data);
            }
            return Ret;
        }
        public GetAppListResult GetAppList(int agentId, string accesstoken)
        {
            GetAppListResult Ret = new GetAppListResult();
            if (isUseWX == "Y")
            {
                Ret = AppApi.GetAppList(GetToken(), agentId);
            }
            return Ret;
        }
        #endregion

        #region 菜单相关
        public QyJsonResult WX_WxCreateMenu(int agentId, string accesstoken)
        {
            QyJsonResult Ret = new QyJsonResult();
            if (isUseWX == "Y")
            {
                List<Senparc.Weixin.QY.Entities.Menu.BaseButton> lm = new List<Senparc.Weixin.QY.Entities.Menu.BaseButton>();
                var list = new JH_Auth_CommonB().GetEntities(p => p.ModelCode == ModelCode && p.TopID == 0 && p.Type == "1");

                foreach (var l in list)
                {
                    var list1 = new JH_Auth_CommonB().GetEntities(p => p.ModelCode == ModelCode && p.TopID == l.ID && p.Type == "1");
                    if (list1.Count() == 0)
                    {
                        Senparc.Weixin.QY.Entities.Menu.SingleViewButton svb = new Senparc.Weixin.QY.Entities.Menu.SingleViewButton();
                        svb.name = l.MenuName;
                        svb.type = "view";
                        if (string.IsNullOrEmpty(l.MenuCode))
                        {
                            svb.url = l.Url + "?funcode=" + l.ModelCode + "&corpId=" + authCorpID;
                        }
                        else
                        {
                            svb.url = l.Url + "?funcode=" + l.ModelCode + "_" + l.MenuCode + "&corpId=" + authCorpID;
                        }

                        lm.Add(svb);
                    }
                    else
                    {
                        Senparc.Weixin.QY.Entities.Menu.SubButton scb = new Senparc.Weixin.QY.Entities.Menu.SubButton();
                        scb.name = l.MenuName;

                        foreach (var l1 in list1)
                        {
                            Senparc.Weixin.QY.Entities.Menu.SingleViewButton svb1 = new Senparc.Weixin.QY.Entities.Menu.SingleViewButton();
                            svb1.name = l1.MenuName;
                            svb1.type = "view";
                            if (string.IsNullOrEmpty(l.MenuCode))
                            {
                                svb1.url = l1.Url + "?funcode=" + l1.ModelCode + "&corpId=" + authCorpID;
                            }
                            else
                            {
                                svb1.url = l1.Url + "?funcode=" + l1.ModelCode + "_" + l1.MenuCode + "&corpId=" + authCorpID;
                            }

                            scb.sub_button.Add(svb1);
                        }

                        lm.Add(scb);
                    }
                }
                if (lm.Count > 0)
                {
                    Senparc.Weixin.QY.Entities.Menu.ButtonGroup buttonData = new Senparc.Weixin.QY.Entities.Menu.ButtonGroup();
                    buttonData.button = lm;

                    Ret = CommonApi.CreateMenu(accesstoken, agentId, buttonData);
                }
            }
            return Ret;
        }
        public QyJsonResult WX_WxCreateMenuNew(int agentId, string accesstoken)
        {
            QyJsonResult Ret = new QyJsonResult();
            if (isUseWX == "Y")
            {
                List<Senparc.Weixin.QY.Entities.Menu.BaseButton> lm = new List<Senparc.Weixin.QY.Entities.Menu.BaseButton>();

                var list = new JH_Auth_CommonB().GetEntities(p => p.ModelCode == ModelCode && p.TopID == 0).OrderBy(p => p.Sort);

                foreach (var l in list)
                {
                    string url = string.Empty;
                    string key = string.Empty;
                    if (string.IsNullOrEmpty(l.MenuCode))
                    {
                        url = l.Url + "?funcode=" + l.ModelCode + "&corpId=" + authCorpID;
                        key = l.ModelCode;
                    }
                    else
                    {
                        url = l.Url + "?funcode=" + l.ModelCode + "_" + l.MenuCode + "&corpId=" + authCorpID;
                        key = l.ModelCode + "_" + l.MenuCode;
                    }

                    var list1 = new JH_Auth_CommonB().GetEntities(p => p.ModelCode == ModelCode && p.TopID == l.ID).OrderBy(p => p.Sort);
                    if (list1.Count() == 0)
                    {
                        lm.Add(GetButton(l.Type, l.MenuName, url, key));
                    }
                    else
                    {
                        Senparc.Weixin.QY.Entities.Menu.SubButton scb = new Senparc.Weixin.QY.Entities.Menu.SubButton();
                        scb.name = l.MenuName;

                        foreach (var l1 in list1)
                        {
                            string url1 = string.Empty;
                            string key1 = string.Empty;
                            if (string.IsNullOrEmpty(l1.MenuCode))
                            {
                                url1 = l1.Url + "?funcode=" + l1.ModelCode + "&corpId=" + authCorpID;
                                key1 = l1.ModelCode;
                            }
                            else
                            {
                                url1 = l1.Url + "?funcode=" + l1.ModelCode + "_" + l1.MenuCode + "&corpId=" + authCorpID;
                                key1 = l1.ModelCode + "_" + l1.MenuCode;
                            }

                            switch (l1.Type)
                            {
                                case "1": //跳转URL
                                    Senparc.Weixin.QY.Entities.Menu.SingleViewButton svb = new Senparc.Weixin.QY.Entities.Menu.SingleViewButton();
                                    svb.name = l1.MenuName;
                                    svb.type = "view";
                                    svb.url = url1;

                                    scb.sub_button.Add(svb);
                                    break;
                                case "2": //点击推事件
                                    Senparc.Weixin.QY.Entities.Menu.SingleClickButton skb = new Senparc.Weixin.QY.Entities.Menu.SingleClickButton();
                                    skb.name = l1.MenuName;
                                    skb.type = "click";
                                    skb.key = key1;

                                    scb.sub_button.Add(skb);
                                    break;
                                case "3"://扫码推事件
                                    Senparc.Weixin.QY.Entities.Menu.SingleScancodePushButton spb = new Senparc.Weixin.QY.Entities.Menu.SingleScancodePushButton();
                                    spb.name = l1.MenuName;
                                    spb.type = "scancode_push";
                                    spb.key = key1;

                                    scb.sub_button.Add(spb);
                                    break;
                                case "4"://扫码推事件且弹出“消息接收中”提示框
                                    Senparc.Weixin.QY.Entities.Menu.SingleScancodeWaitmsgButton swb = new Senparc.Weixin.QY.Entities.Menu.SingleScancodeWaitmsgButton();
                                    swb.name = l1.MenuName;
                                    swb.type = "scancode_waitmsg";
                                    swb.key = key1;

                                    scb.sub_button.Add(swb);
                                    break;
                                case "5"://弹出系统拍照发图
                                    Senparc.Weixin.QY.Entities.Menu.SinglePicSysphotoButton ssb = new Senparc.Weixin.QY.Entities.Menu.SinglePicSysphotoButton();
                                    ssb.name = l1.MenuName;
                                    ssb.type = "pic_sysphoto";
                                    ssb.key = key1;

                                    scb.sub_button.Add(ssb);
                                    break;
                                case "6"://弹出拍照或者相册发图
                                    Senparc.Weixin.QY.Entities.Menu.SinglePicPhotoOrAlbumButton sab = new Senparc.Weixin.QY.Entities.Menu.SinglePicPhotoOrAlbumButton();
                                    sab.name = l1.MenuName;
                                    sab.type = "pic_photo_or_album";
                                    sab.key = key1;

                                    scb.sub_button.Add(sab);
                                    break;
                                case "7"://弹出微信相册发图器
                                    Senparc.Weixin.QY.Entities.Menu.SinglePicWeixinButton sxb = new Senparc.Weixin.QY.Entities.Menu.SinglePicWeixinButton();
                                    sxb.name = l1.MenuName;
                                    sxb.type = "pic_weixin";
                                    sxb.key = key1;

                                    scb.sub_button.Add(sxb);
                                    break;
                                case "8"://弹出地理位置选择器
                                    Senparc.Weixin.QY.Entities.Menu.SingleLocationSelectButton slb = new Senparc.Weixin.QY.Entities.Menu.SingleLocationSelectButton();
                                    slb.name = l1.MenuName;
                                    slb.type = "location_select";
                                    slb.key = key1;

                                    scb.sub_button.Add(slb);
                                    break;
                            }

                        }

                        lm.Add(scb);
                    }
                }
                if (lm.Count > 0)
                {
                    Senparc.Weixin.QY.Entities.Menu.ButtonGroup buttonData = new Senparc.Weixin.QY.Entities.Menu.ButtonGroup();
                    buttonData.button = lm;

                    Ret = CommonApi.CreateMenu(accesstoken, agentId, buttonData);
                }
            }
            return Ret;
        }
        public QyJsonResult WX_CreateMenu(int agentId, Senparc.Weixin.QY.Entities.Menu.ButtonGroup buttonData)
        {
            QyJsonResult Ret = new QyJsonResult();
            if (isUseWX == "Y")
            {
                Ret = CommonApi.CreateMenu(GetToken(), agentId, buttonData);
            }
            return Ret;
        }
        public GetMenuResult WX_GetMenu(int agentId)
        {
            GetMenuResult Ret = new GetMenuResult();
            if (isUseWX == "Y")
            {
                Ret = CommonApi.GetMenu(GetToken(), agentId);
            }
            return Ret;
        }
        #endregion



        public string GetMediaFile(string mediaId, string strType = ".jpg")
        {
            string path = HttpContext.Current.Server.MapPath("\\temp\\");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string mdfile = path + Guid.NewGuid().ToString() + strType;
            FileStream fs = new FileStream(mdfile, FileMode.Create);
            MediaApi.Get(GetToken(), mediaId, fs);
            fs.Close();
            return mdfile;
        }
        public Senparc.Weixin.QY.Entities.Menu.BaseButton GetButton(string type, string menuname, string url, string key)
        {
            Senparc.Weixin.QY.Entities.Menu.BaseButton bb = new Senparc.Weixin.QY.Entities.Menu.BaseButton();
            switch (type)
            {
                case "1": //跳转URL
                    Senparc.Weixin.QY.Entities.Menu.SingleViewButton svb = new Senparc.Weixin.QY.Entities.Menu.SingleViewButton();
                    svb.name = menuname;
                    svb.type = "view";
                    svb.url = url;

                    bb = svb;
                    break;
                case "2": //点击推事件
                    Senparc.Weixin.QY.Entities.Menu.SingleClickButton scb = new Senparc.Weixin.QY.Entities.Menu.SingleClickButton();
                    scb.name = menuname;
                    scb.type = "click";
                    scb.key = key;

                    bb = scb;
                    break;
                case "3"://扫码推事件
                    Senparc.Weixin.QY.Entities.Menu.SingleScancodePushButton spb = new Senparc.Weixin.QY.Entities.Menu.SingleScancodePushButton();
                    spb.name = menuname;
                    spb.type = "scancode_push";
                    spb.key = key;

                    bb = spb;
                    break;
                case "4"://扫码推事件且弹出“消息接收中”提示框
                    Senparc.Weixin.QY.Entities.Menu.SingleScancodeWaitmsgButton swb = new Senparc.Weixin.QY.Entities.Menu.SingleScancodeWaitmsgButton();
                    swb.name = menuname;
                    swb.type = "scancode_waitmsg";
                    swb.key = key;

                    bb = swb;
                    break;
                case "5"://弹出系统拍照发图
                    Senparc.Weixin.QY.Entities.Menu.SinglePicSysphotoButton ssb = new Senparc.Weixin.QY.Entities.Menu.SinglePicSysphotoButton();
                    ssb.name = menuname;
                    ssb.type = "pic_sysphoto";
                    ssb.key = key;

                    bb = ssb;
                    break;
                case "6"://弹出拍照或者相册发图
                    Senparc.Weixin.QY.Entities.Menu.SinglePicPhotoOrAlbumButton sab = new Senparc.Weixin.QY.Entities.Menu.SinglePicPhotoOrAlbumButton();
                    sab.name = menuname;
                    sab.type = "pic_photo_or_album";
                    sab.key = key;

                    bb = sab;
                    break;
                case "7"://弹出微信相册发图器
                    Senparc.Weixin.QY.Entities.Menu.SinglePicWeixinButton sxb = new Senparc.Weixin.QY.Entities.Menu.SinglePicWeixinButton();
                    sxb.name = menuname;
                    sxb.type = "pic_weixin";
                    sxb.key = key;

                    bb = sxb;
                    break;
                case "8"://弹出地理位置选择器
                    Senparc.Weixin.QY.Entities.Menu.SingleLocationSelectButton slb = new Senparc.Weixin.QY.Entities.Menu.SingleLocationSelectButton();
                    slb.name = menuname;
                    slb.type = "location_select";
                    slb.key = key;

                    bb = slb;
                    break;
            }
            return bb;
        }
    }

    public class thModel
    {
        public List<Article> MODEL { get; set; }
        public string authAppID { get; set; }
        public int ID { get; set; }
        public string UserS { get; set; }
    }

    public class JsonGroupTicket
    {
        public string errcode { get; set; }
        public string errmsg { get; set; }
        public string group_id { get; set; }
        public string ticket { get; set; }
        public string expires_in { get; set; }

    }
}