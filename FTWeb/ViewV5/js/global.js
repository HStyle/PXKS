if (typeof $ != "undefined") {
    var jq = $;
    ; $(function () {

        // flash
        if ($().slide) {
            $(".banner").slide({ mainCell: ".bd", titCell: ".hd", effect: "leftLoop", vis: "auto", autoPage: true, autoPlay: true, pnLoop: true, delayTime: 500, interTime: 5000 });
            //

        };
        // end flash

        // 图片经过

        $(".videolist li").live('mouseenter', function () {
            $(this).find(".bkcover").stop().fadeIn();
            $(this).find(".whplay").stop().animate({ top: "50%", "opacity": 1 }, 500);
        })
        $(".videolist li").live('mouseleave', function () {
            $(this).find(".bkcover").fadeOut();
            $(this).find(".whplay").stop().animate({ top: "100%", "opacity": 0 });
        })
        //


        //二级菜单
        $('.left_nav .show').addClass('dl_click');
        $('.left_nav .show dd').show('slow');
     
        $('.left_nav dl dt').live('click', function () {
            $(this).parent().toggleClass('dl_click').siblings().removeClass('dl_click');
            $(this).parent().children('dd').slideToggle().end().siblings().children('dd').slideUp();
        })
        //
        $('.left_nav_dl dd p').live('click', function () {
            $(this).addClass('active').siblings().removeClass('active');

        })
        //

        //点击高亮
        $('.click_a a').click(function () {
            $(this).addClass('active').siblings().removeClass('active');
        });
        $('.click_checkbox a').click(function () {
            $(this).toggleClass('active');
        });
        //



        //排行榜的数字高亮
        $('.tab_list li:eq(0)').addClass('orange_1');
        $('.tab_list li:eq(1)').addClass('orange_2');
        $('.tab_list li:eq(2)').addClass('orange_3');
        //







        //文本框
        function placeholder(input) {
            var placeholder = input.attr("placeholder"),
                defaultValue = input.defaultValue;
            if (!defaultValue) {
                input.val() == "" ? input.val(placeholder).addClass("phcolor") : 0;
            }
            input.focus(function () {
                input.val() == placeholder ? $(this).val("") : 0;
            });
            input.blur(function () {
                input.val() == "" ? $(this).val(placeholder).addClass("phcolor") : 0;
            });
            input.keydown(function () {
                $(this).removeClass("phcolor");
            });
        }
        ; $(function () {
            supportPlaceholder = "placeholder" in document.createElement("input");
            if (!supportPlaceholder) {
                $("input").each(function () {
                    var type = $(this).attr("type");
                    text = $(this).attr("placeholder");
                    if (type == "text" || type == "number" || type == "search" || type == "email" || type == "date" || type == "url") {
                        placeholder($(this));
                    }
                });
            }
        });
        //end文本框



    });
};
// end jq