﻿
$(document).ready(function () {
    //顶部导航切换
    $(".nav li").live('click', (function (e) {

        $("#leftdv").show();
        $("#dvContent").css({ left: '220px' });
        getchildmenu($(this));
        $(".nav li a.selected").removeClass("selected")
        $(this).find("a").addClass("selected");
        var href = $(this).find("a").attr("href");
        $(".placeul").empty().append("<li><a href='" + href + "' target='main'>" + $(this).find("a").text() + "</a></li>")

    }))

    $("#2").trigger("click");


    //tab处理
    $(".menutab li").live('click', function () {
        $(".tabContent").hide();
        $($(this).find('a').attr("href")).show();
        $(".menutab li a").removeClass("selected");
        $(this).find('a').addClass("selected");
    })
    //tab处理


    //导航切换
    $(".menuson li").live('click', function () {
        $(".menuson li.active").removeClass("active")
        $(this).addClass("active");

        var href = $(this).attr("hrefurl");
        addNewTab(href, $(this).attr("id"), $(this).find("a").text())
    })
    $('.title').live('click', function () {
        var $ul = $(this).next('ul');
        $('dd').find('ul').slideUp();
        if ($ul.is(':visible')) {
            $(this).next('ul').slideUp();
        } else {
            $(this).next('ul').slideDown();
        }
    });





    //首页点击刷新
    $(".menutab li").click(function () {
        frames["main"].location.reload();
    })

    //刷新
    $("#btsximg").click(function () {
        var framename = $("iframe:visible").attr('name');
        //var href = frames[framename].location.href;
        //frames[framename].location.replace(href);
        frames[framename].location.reload()
    })


    // 'url:' + pageurl
    $("#imgSet").click(function () {
        var username = $("#hidUserName").val()
        var pageurl = 'SetCenter.aspx?username=' + username;
        MyDialog.viewform(pageurl, '修改密码');
    })
    //获取收藏夹



    //获取新版本信息
    //$.ajax({
    //    type: "GET",
    //    url: "/View_Manage/Base/AdminHandle.ashx",
    //    dataType: "html",
    //    data: { "UserName": $("#hidUserName").val(), "Action": "GetVesionContent" },

    //    success: function (msg) {
    //        if (msg != "") {
    //            MyDialog.viewhtml(msg,"版本更新",'800','500')
    //        }
    //    }
    //});
    // getscjhtml();
});

function getchildmenu(e) {
    var strMenuID = e.attr('id');
    var initmenuid = e.attr('initmenuid');
    $.ajax({
        type: "GET",
        url: "/View_Manage/Base/AdminHandle.ashx",
        dataType: "html",
        data: { "UserName": $("#hidUserName").val(), "PMenuCode": strMenuID, "Action": "GetMainMenu" },

        success: function (msg) {

            if (msg == "") {
                // $.ligerDialog.question('您没有查看权限,请联系管理员');
            }
            else {
                $("#mastmenu dd").remove();
                $("#mastmenu").append(msg);
                if (initmenuid) {
                    $("#" + initmenuid).parent().parent().find(".title").trigger('click');
                    $("#" + initmenuid).find("a").trigger('click');
                    e.attr("initmenuid", "");
                }
            }
        }
    });
}

//保存收藏夹数据
function savescjhtml() {
    var scjhtml = $("#scjmenu").html();
    $.ajax({
        type: "POST",
        url: "Common/ComAjaxHandler.ashx",
        dataType: "html",
        data: { "scjhtml": scjhtml, "Action": "AddSCJHtml" },
        success: function (msg) {

        }
    });
}
//获取收藏夹数据
function getscjhtml() {
    var scjhtml = $("#scjmenu").html();
    $.ajax({
        type: "GET",
        url: "Common/ComAjaxHandler.ashx",
        dataType: "html",
        data: { "Action": "GetSCJHtml" },
        success: function (msg) {
            $("#scjmenu").append(msg);
            $("#scjmenu li").removeClass("active");
        }
    });
}


//添加Tab页
function addNewTab(url, menuid, title) {

    //如果已经存在该Tab，则转到该Tab
    if ($("#Tab" + menuid).length > 0) {
        $("a[href='#Tab" + menuid + "']").parent().trigger('click');
    }
    else {
        $(".menutab li a").removeClass("selected");
        $(".tabContent").hide();
        var tabli = $('<li><a href="#Tab' + menuid + '" class="selected">' + title + '<img class="deltab" src="/images/MVimg/Index/close.png" style="width:16px; height:16px;vertical-align:middle; margin-left:5px;visibility:hidden;"></a></li>');
        tabli.find(".deltab").click(function () {
            tabli.remove();
            $("#Tab" + menuid).remove();
            if ($(".tabiframe:visible").length === 0) {
                $(".menutab li").last().trigger('click');
            }
        })
        tabli.hover(function () {
            $(this).find(".deltab").css({ visibility: 'visible' });
        },
        function () {
            $(this).find(".deltab").css({ visibility: 'hidden' });
        })
        $(".menutab").append(tabli);
        var nframe = $("#tab1").find('iframe').clone();
        nframe.attr("id", "frame" + menuid).attr("name", "frame" + menuid).attr("src", url);
        var newcontent = $('<div id="Tab' + menuid + '" class="tabContent"></div>').append(nframe);
        $("#dvContent").append(newcontent);
        SetFrameHeight("frame" + menuid);

        //总Tab数不能超过5个,如果超过了,则自动删除最早打开的
        if ($(".menutab li").length > 5) {
            $(".menutab li").eq(1).remove();
            $(".tabContent").eq(1).remove();
        }
    }
}
//设置Frame高度
function SetFrameHeight(framename) {
    var oFrm = document.getElementById(framename);
    oFrm.onload = oFrm.onreadystatechange = function () {
        if (this.readyState && this.readyState != 'complete') return;
        else {
            var mainheight = $("#dvContent").height() - 45;
            $("#" + framename).height(mainheight);
            $("#mastmenu").height(mainheight);
            $(window.frames[framename].document).find(".maindiv").css({ height: mainheight + "px", overflow: "auto" });
        }
    }
}