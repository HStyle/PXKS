﻿
function MyDialog() { }
//成功提示
MyDialog.success = function (content) {

    parent.layer.msg(content || '操作成功。。', {
        icon: 6,
        offset: 200,
        shift: 6
    });

}

//警告提示
MyDialog.warning = function (content) {
    parent.layer.msg(content, {
        icon: 5,
        offset: 200,
        shift: 6
    });
}

//警告提示
MyDialog.tips = function (content, callback) {
    parent.layer.msg(content, {
        offset: 200,
        shift: 6
    });
}

//等待提示
MyDialog.Mytips = function () {
    parent.layer.load();
}

//关闭提示
MyDialog.CloseMytips = function () {

    parent.layer.closeAll('loading');
}

//确认提示
MyDialog.confirm = function (title, yes, no) {
    parent.layer.confirm(title, {
        btn: ['确认', '取消'], //按钮
        shade: false //不显示遮罩
    }, function () {
        parent.layer.closeAll('dialog');
        return yes.call(this);
    }, function () {
        return no && no.call(this);
    });
}

//添加表单，查看暂存及已审批表单的弹出窗口
//添加表单，查看暂存及已审批表单的弹出窗口
MyDialog.viewform = function (url, title, width, height, option) {
    var width = width || $("body").width() - 300;
    var height = height || $("#main").height();
    var optionwin = {
        type: 2,
        fix: false, //不固定
        area: [width + 'px', height + 'px'],
        maxmin: true,
        content: url,
        shadeClose: true,
        skin: 'layui-layer-rim' //加上边框
    }
    layer.open(optionwin);
}



//自定义按钮的弹出窗口
MyDialog.btnwin = function (url, width, height, option, btcallbact) {
    var width = width || $("body").width() - 300;
    var height = height || $("#main").height();
    var optionwin = {
        type: 2,
        fix: false, //不固定
        area: [width + 'px', height + 'px'],
        maxmin: true,
        content: url,
        shadeClose: true,
        success: function (layero, index) {
            if ($(layero).find('.qr').length == 0) {
                var footdv = $('<div class="layui-layer-title" style="border-bottom-width: 1px; margin-top: -3px;"></div>');
                var btnConfirm = $("<a href='#' style='float:right;min-width: 120px;color: whitesmoke;background-color: cornflowerblue;border-radius: 5px;text-align: center;' class='qr'>确   认</a>");
                btnConfirm.appendTo(footdv).bind('click', function () {
                    return btcallbact.call(this, layero, index);
                })
                $(layero).append(footdv);
            }
        }

    }
    layer.open(optionwin);
}



MyDialog.viewhtml = function (content, title, width, height, callbact) {
    var width = width || 600;
    var height = height || 400;
    layer.open({
        type: 1,
        title: title,
        skin: 'layui-layer-rim', //加上边框
        area: [width + 'px', height + 'px'],
        content: content,
        shadeClose: true,
        success: function (layero, index) {
            if (callbact) {
                return callbact.call(this, layero, index);
            }
        }

    });
}



//WelCome页面确认执行弹出窗口
MyDialog.exeform = function (ProcessInstanceID, url) {
    $.dialog({
        ok: function () {

        },
        okVal: '确认接收',
        cancelVal: '关闭',
        cancel: true,
        content: 'url:' + url,
        title: '处理任务',
        background: '#DCE2F1',
        opacity: 0.2,
        width: 1200,
        lock: true,

        height: 650
    });
}



MyDialog.openwinold = function (url) {
    var iWidth = 610;                          //弹出窗口的宽度;
    var iHeight = 600;                       //弹出窗口的高度;
    //获得窗口的垂直位置
    var iTop = (window.screen.availHeight - 30 - iHeight) / 2;
    //获得窗口的水平位置
    var iLeft = (window.screen.availWidth - 10 - iWidth) / 2;
    var params = 'width=' + iWidth
               + ',height=' + iHeight
               + ',top=' + iTop
               + ',left=' + iLeft
               + ',channelmode=yes'//是否使用剧院模式显示窗口。默认为 no
               + ',directories=yes'//是否添加目录按钮。默认为 yes
               + ',fullscreen=no' //是否使用全屏模式显示浏览器
               + ',location=no'//是否显示地址字段。默认是 yes
               + ',menubar=no'//是否显示菜单栏。默认是 yes
               + ',resizable=no'//窗口是否可调节尺寸。默认是 yes
               + ',scrollbars=yes'//是否显示滚动条。默认是 yes
               + ',status=yes'//是否添加状态栏。默认是 yes
               + ',titlebar=yes'//默认是 yes
               + ',toolbar=no'//默认是 yes
    ;
    window.open(url, name, params);
}