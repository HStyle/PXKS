﻿using QjySaaSWeb.AppCode;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace QjySaaSWeb
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {

            System.Timers.Timer t = new System.Timers.Timer();
            t.Interval = 3 * 1000;
            t.Elapsed += new System.Timers.ElapsedEventHandler(TimerNow);
            t.AutoReset = true;
            t.Enabled = true;
            t.Start();
        }
        public void TimerNow(object source, System.Timers.ElapsedEventArgs e)
        {
            string path = Environment.CurrentDirectory;
            try
            {
                Random rd = new Random();
                string strUrl = "http://www.ycpxxt.com/API/WXAPI.ashx?action=AUTOALERT&r=" + rd.Next();
                HttpWebResponse ResponseDataXS = CommonHelp.CreateHttpResponse(strUrl, null, 0, "", null, "GET");
                string Returndata = new StreamReader(ResponseDataXS.GetResponseStream(), Encoding.UTF8).ReadToEnd();

            }
            catch (Exception ex)
            {

                CommonHelp.WriteLOG(ex.Message.ToString());

            }
        }
        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}