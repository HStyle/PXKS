﻿function addTab(tabname, sdate,edate) {
    var tabid = getsjs();
    var tabli = $("<li tabid='" + tabid + "'><a href='#" + tabid + "' data-toggle='tab'>" + tabname + "&nbsp;&nbsp;&nbsp; </a></li>");
    addZXSET(tabli, tabname, tabid);
    tabli.insertBefore("#addChi");
    $(".tab-content").append("<div class='tab-pane fade' id='" + tabid + "'><div  id='GT" + tabid + "' class='divGant'></div></div>");
    $('#myTab a').not($("#addLink")).last().tab('show');
    addYanGant(tabname, sdate, edate, tabid);
  
    $("#addLink").popover('hide');
    return $("#" + tabid);
}

function addYanGant(tabname, sdate, edate, tabid) {
    var gantwidth = screen.width - 300;
    var gant = $("#GT" + tabid);
    gant.attr("zxname", tabname).attr("zxsdate", sdate).attr("zxedate", edate).YanGant({
        startdate: sdate,
        enddate: edate,
        gantWidth: gantwidth,
        tdclick: function () {
            $(".GantChatBody_TasktdC").not($(this)).popover('hide').removeClass("GantChatBody_TasktdC");
            $(".GantChatBody_Task").not($(this)).popover('hide');

        },
        inittd: function (td) {
            td.popover({
                content: function () {
                    return $(".Taskdv").clone(true).html();
                },
                container: "body",
                html: true,
                placement: 'bottom',
                showed: function (tip) {
                    //子项菜单Tab
                    $('.rwmenu li a').click(function (e) {
                        e.preventDefault();
                        $(".rwmenutab .tab-pane", tip.$tip).hide();
                        $($(this).attr("href"), tip.$tip).show();
                    })
                    $('.rwmenu li a', tip.$tip).eq(0).trigger('click');
                    //
                    //隐藏推迟任务,删除任务
                    $(".litc,.liderw", tip.$tip).hide();

                    $(".taskname", tip.$tip).change(function () {
                        if ($(this).val() === "互提资料") {
                            $(".szzynamedv", tip.$tip).show()
                        } else {
                            $(".szzynamedv", tip.$tip).hide()
                        }
                    });
                    $(".taskname", tip.$tip).trigger('change');
                    $(".zyname", tip.$tip).val(td.attr("zyname"));
                    $(".sdate", tip.$tip).val(td.attr("date"));
                    $(".edate", tip.$tip).val(td.attr("date"));

                    $(".qx", tip.$tip).click(function () {
                        td.popover('hide');
                        td.removeClass("GantChatBody_TasktdC");
                    })
                    $(".qd", tip.$tip).click(function () {
                        var task = gettaskdv(tip.$tip);
                        var newtd = $("td[zyname='" + task.zyname + "'][date='" + task.sdate + "']", gant);

                        taskdiv = addTask(newtd, task);
                        td.popover('hide');
                        td.removeClass("GantChatBody_TasktdC");
                    })
                    ComFunJS.initForm();
                }
            });
        },
        expandlev: 1,
        colmModel: [{
            display: '专业',
            name: 'taskname',
            width: 100
        }]
    });
}


function addTask(td, task) {
    var rowtab = td.closest("table");
    var taskdiv = $("<div class='GantChatBody_Task'  >  " + task.taskname + "<br/>" + ComFunJS.convertuser(parent.usersarr, task.taskuser) + "</div>").hide();
    var taskleft = td.attr("index") * 40 + td.attr("index") * 1;
    var taskheight = td.height();
    var taskwidth = (ComFunJS.daysBetween(task.sdate, task.edate) + 1) * td.width() + ComFunJS.daysBetween(task.sdate, task.edate);
    taskdiv.css({ "width": taskwidth + "px", "height": taskheight + "px", "left": taskleft + "px" });


    if (task.taskname === "互提资料" || task.taskname === "校对" || task.taskname === "审定") {
        taskdiv.addClass("evennum");
    }
    else {
        taskdiv.addClass("oldnum");
    }
    rowtab.before(taskdiv);

    //如果有实际时间
    if (task.asdate) {
        var Ataskdiv = $("<div class='GantChatBody_PlanTask'></div>");
        var Ataskheight = td.height() / 2;
        var Ataskleft;
        var Atd = td.parent().find("td[date='" + task.asdate + "']");
        if (Atd) {
            Ataskleft = Atd.attr("index") * 40 + Atd.attr("index") * 1
        }
        else {
            Ataskleft = 0
        }
        var Ataskwidth = (ComFunJS.daysBetween(task.asdate, task.aedate) + 1) * td.width() + ComFunJS.daysBetween(task.asdate, task.aedate);
        Ataskdiv.css({ "width": Ataskwidth + "px", "height": "6px", "left": Ataskleft + "px", "top": "17px" });
        rowtab.before(Ataskdiv);
    }



    taskdiv.show('normal').data("taskdata", task);
    taskdiv.click(function () {
        $(".GantChatBody_Task").not($(this)).popover('hide');
        $(".GantChatBody_TasktdC").not($(this)).popover('hide').removeClass("GantChatBody_TasktdC");

    })
    taskdiv.popover({
        content: function () {
            return $(".Taskdv").clone(true).html();
        },
        title:"完成任务",
        container: "body",
        html: true,
        placement: 'bottom',
        showed: function (tip) {
            inittaskdv(tip.$tip, task);
            

            //子项菜单Tab
            $('.rwmenu li a').click(function (e) {
                e.preventDefault();
                $(".rwmenutab .tab-pane", tip.$tip).hide();
                $($(this).attr("href"), tip.$tip).show();
            })
            $('.rwmenu li a', tip.$tip).eq(0).trigger('click');
            //


            ComFunJS.initForm();

            //取消操作
            $(".qx", tip.$tip).click(function () {
                taskdiv.popover('hide');
            })
            $(".qd", tip.$tip).click(function () {
                //添加新任务
                var task = gettaskdv(tip.$tip);
                var td = $("td[zyname='" + task.zyname + "'][date='" + task.sdate + "']", rowtab.parent().parent());
                addTask(td, task);
                td.popover('hide');
                td.removeClass("GantChatBody_TasktdC");
                //
                //删除原任务
                taskdiv.popover('hide');
                taskdiv.remove();
            });


            //删除任务
            $(".delrw", tip.$tip).click(function () {
                taskdiv.popover('hide');
                taskdiv.remove();
            })
            

            $(".qrtcrw", tip.$tip).click(function () {

                taskdiv.popover('hide');

                var tcsdate = $(".tcrwdate", tip.$tip).val();
                var oldsdate = $(".sdate", tip.$tip).val();
                var daycount = ComFunJS.daysBetween(oldsdate, tcsdate);
                //自动推迟后续任务
                var nexttasks = [];
                if (oldsdate != tcsdate) {
                    //先缓存所有任务，然后删除原有任务
                    taskdiv.nextAll(".GantChatBody_Task").andSelf().each(function myfunction() {
                        nexttasks.push($(this).data("taskdata"));
                        $(this).remove();
                    })
                    //重新添加后续所有任务
                    for (var i = 0; i < nexttasks.length; i++) {
                        nexttasks[i].sdate = ComFunJS.format(ComFunJS.DateAdd(ComFunJS.StringToDate(nexttasks[i].sdate), 'd', daycount), "YYYY-MM-DD");
                        nexttasks[i].edate = ComFunJS.format(ComFunJS.DateAdd(ComFunJS.StringToDate(nexttasks[i].edate), 'd', daycount), "YYYY-MM-DD");
                        var tdntask = $("td[zyname='" + nexttasks[i].zyname + "'][date='" + nexttasks[i].sdate + "']", rowtab.parent().parent());
                        addTask(tdntask, nexttasks[i]);
                        tdntask.popover('hide');
                        tdntask.removeClass("GantChatBody_TasktdC");

                    }
                }
                //
            })
          
        }
    });
    return taskdiv;
}
//初始化任务提示
function inittaskdv(tip, task) {
    $(".zyname", tip.$tip).val(task.zyname);
    $(".sdate", tip.$tip).val(task.sdate);
    $(".tcrwdate", tip.$tip).val(task.sdate);

    $(".edate", tip.$tip).val(task.edate);
    $(".taskname", tip.$tip).val(task.taskname);
    $(".taskuser", tip.$tip).val(task.taskuser);
    if (task.taskname === "互提资料") {
        $(".szzynamedv", tip.$tip).show();
        $(".szzyname", tip.$tip).val(task.szzyname);
    }
    else {
        $(".szzynamedv", tip.$tip).hide();
    }
    if (task.asdate) {
        $(".asdate", tip.$tip).val(task.asdate);
        $(".aedate", tip.$tip).val(task.aedate);
        $(".sjdate", tip.$tip).show();
    }

}
//获取任务提示中的值
function gettaskdv(tip) {
    var task = new Object();
    task.zyname = $(".zyname", tip).val();
    task.sdate = $(".sdate", tip).val();
    task.edate = $(".edate", tip).val();
    task.taskname = $(".taskname", tip).val();
    task.taskuser = $(".taskuser", tip).val();
    task.szzyname = $(".szzyname", tip).val();
    task.asdate = $(".asdate", tip).val();
    task.aedate = $(".aedate", tip).val();

    return task;
}

function getTaskData() {
    var taskdata = { data: [] };
    $(".divGant").each(function () {
        var zxname = $(this).attr("zxname");
        var zxsdate = $(this).attr("zxsdate");
        var zxedate = $(this).attr("zxedate");

        $(".GantChatBody_Task", $(this)).each(function () {
            var data = $(this).data("taskdata");
            data.zxname = zxname;
            data.zxsdate = zxsdate;
            data.zxedate = zxedate;
            taskdata.data.push(data);
        });
    })
    var datastr = JSON.stringify(taskdata);
    return datastr;
}
function delTab(tabname) {
    $("#myTab li a").each(function () {
        if ($.trim($(this).text()) === tabname) {
            var id = $(this).attr("href").substr(1, $(this).attr("href").length - 1);
            $("#" + id).remove();
            $(this).parent().remove();
        }
    })

}
function getsjs() {
    var returnnum = 0;
    var count = 1000;
    var original = new Array;//原始数组
    //给原始数组original赋值
    for (var i = 0; i < count; i++) {
        original[i] = i + 1;
    }
    for (var num, i = 0; i < count; i++) {
        do {
            num = Math.floor(Math.random() * count);
            returnnum = original[num];
        } while (original[num] == null);
        original[num] = null;
    }
    return returnnum;
}


function initgantdata(taskstr) {
    var taskdata = JSON.parse(taskstr);
    var zxs = Array();
    for (var i = 0; i < taskdata.data.length; i++) {

        var zxname = taskdata.data[i].zxname;
        if ($("div[zxname='" + zxname + "']").length === 0) {
            var tab = addTab(zxname, taskdata.data[i].zxsdate, taskdata.data[i].zxedate);
            zxs.push(tab.attr("zxname", zxname));
        }
    }
    for (var i = 0; i < zxs.length; i++) {
        for (var m = 0; m < taskdata.data.length; m++) {
            if (taskdata.data[m].zxname === zxs[i].attr("zxname")) {
                var td = $("td[zyname='" + taskdata.data[m].zyname + "'][date='" + taskdata.data[m].sdate + "']", zxs[i]);
                addTask(td, taskdata.data[m]);
            }
        }
    }
}
function addZXSET(tabli, tabname, tabid) {
    var iset = $("<i data-html='true' title='设置子项' class='icon-th-large'></i>");
    iset.popover({
        content: function () {
            return $(".delzx").clone(true).html();
        },
        container: "body",
        showed: function (tip) {
            var gant = $("#GT" + tabid);

            //隐藏pov
            $(".GantChatBody_Tasktd,.GantChatBody_Tasktd_weekend,.GantChatBody_Task", gant).popover('hide')
            //

            //子项菜单Tab
            $('.zxmenu li a').click(function (e) {
                e.preventDefault();
                $(".zxmenutab .tab-pane", tip.$tip).hide();
                $($(this).attr("href"), tip.$tip).show();
            })
            $('.zxmenu li a').eq(0).trigger('click');
            //

            $(".zxname,.newzxname", tip.$tip).val(tabname);
            $(".sdate,.tcdate", tip.$tip).val(gant.attr("zxsdate"));
            $(".edate", tip.$tip).val(gant.attr("zxedate"));


            //获取当前子项任务数据
            var oldtaskdata = { data: [] };
            $(".GantChatBody_Task", gant).each(function () {
                var data = $(this).data("taskdata");
                data.zxname = gant.attr("zxname");
                data.zxsdate = gant.attr("zxsdate");
                data.zxedate = gant.attr("zxedate");
                oldtaskdata.data.push(data);
            });
            //


            //更新子项
            $(".updatezx", tip.$tip).click(function () {
                iset.popover('hide');
                var newzxname = $(".zxname", tip.$tip).val();
                var newsdate = $(".sdate", tip.$tip).val();
                var newedate = $(".edate", tip.$tip).val();

                $("a", tabli).html(newzxname + "&nbsp;&nbsp;&nbsp;");
                addZXSET(tabli, newzxname, tabid);
                
                //重新生成子项甘特图
                gant.empty();
                addYanGant(newzxname, newsdate, newedate, tabid);
                for (var m = 0; m < oldtaskdata.data.length; m++) {
                    //替换原有子项名称,子项开始时间，子项结束时间
                    oldtaskdata.data[m].zxname = newzxname;
                    oldtaskdata.data[m].zxsdate = newsdate;
                    oldtaskdata.data[m].zxedate = newedate;

                    var td = $("td[zyname='" + oldtaskdata.data[m].zyname + "'][date='" + oldtaskdata.data[m].sdate + "']", gant);
                    addTask(td, oldtaskdata.data[m]);
                }
                ///
            })

            //复制子项
            $(".fzzx", tip.$tip).click(function () {
                var newzxname = $(".newzxname", tip.$tip).val();
                iset.popover('hide');
                var tab = addTab(newzxname, gant.attr("zxsdate"), gant.attr("zxedate"));
                for (var m = 0; m < oldtaskdata.data.length; m++) {
                    //替换原有子项名称
                    oldtaskdata.data[m].zxname = newzxname;
                    var td = $("td[zyname='" + oldtaskdata.data[m].zyname + "'][date='" + oldtaskdata.data[m].sdate + "']", tab);
                    addTask(td, oldtaskdata.data[m]);
                }
            })

            //推迟子项
            $(".qrtc", tip.$tip).click(function () {
                iset.popover('hide');
                var sdate = $(".sdate", tip.$tip).val();
                var edate = $(".edate", tip.$tip).val();
                var newtcsdate = $(".tcdate", tip.$tip).val();
                var daycount = ComFunJS.daysBetween(sdate, newtcsdate);

                var newtcedate = ComFunJS.format(ComFunJS.DateAdd(ComFunJS.StringToDate(edate), 'd', daycount), "YYYY-MM-DD");


                //重新生成子项甘特图
                 gant.empty();
                 addYanGant(tabname, newtcsdate, newtcedate, tabid);
                 for (var m = 0; m < oldtaskdata.data.length; m++) {
                     //替换原有子项名称,子项开始时间，子项结束时间
                     oldtaskdata.data[m].zxname = tabname;
                     oldtaskdata.data[m].zxsdate = newtcsdate;
                     oldtaskdata.data[m].zxedate = newtcedate;

                     var newtasksdate = ComFunJS.format(ComFunJS.DateAdd(ComFunJS.StringToDate(oldtaskdata.data[m].sdate), 'd', daycount), "YYYY-MM-DD");
                     var newtaskedate = ComFunJS.format(ComFunJS.DateAdd(ComFunJS.StringToDate(oldtaskdata.data[m].edate), 'd', daycount), "YYYY-MM-DD");

                     oldtaskdata.data[m].sdate = newtasksdate;
                     oldtaskdata.data[m].edate = newtaskedate;

                     var td = $("td[zyname='" + oldtaskdata.data[m].zyname + "'][date='" + oldtaskdata.data[m].sdate + "']", gant);
                     addTask(td, oldtaskdata.data[m]);
                 }
                ///

            })

            //删除子项
            $(".del", tip.$tip).click(function () {
                iset.popover('destroy');
                delTab(tabname);
            })
            
            ComFunJS.initForm();
        }
    });
    $("a", tabli).append(iset);
}
