﻿/*
* YanSelect 选择插件
* Copyright 2011, 刘玉宏
* 2011-05-10 v1.0	编写
*/
(function ($) {
    $.fn.YanTip = function (options) {
        var defaults = {
            place: "right",//right,top,left,bottom
            type: "click",//hover,click
            title: "提示",
            content: "没有内容",
            isonly: true,
            isshowfoot: true,
            showed: $.noop,
            confirm: $.noop,
            cancel: $.noop

        };
        var options = $.extend(defaults, options);
        var selector = this.selector;
        var eventtype = options.type == "click" ? "click" : "mouseenter";
        this.live(eventtype, function (event) {
            event.stopPropagation();

            var $dom = $(this);
            if (options.isonly) {
                $(".Yan_tip").remove();
            }
            var sclass = "Yan_tip-" + options.place;
            var $tip = $('<div class="Yan_tip"><div class="Yan_box"><s class="' + sclass + '"><i></i></s><div class="Yan_tip_Title" >' + options.title + '<span class="Yan_tip_Del" style="float:right;cursor:pointer">X</span></div><div class="Yan_box_Content"></div><div class="Yan_tip_Foot"><button class="Yan_tip_Cancel btn btn-default">取消</button><button class="Yan_tip_ConFirm btn btn-success">确定</button></div></div></div>');
            $tip.find(".Yan_box_Content").append(options.content).end().appendTo('body');
            var actualWidth = $tip.outerWidth();
            var actualHeight = $tip.outerHeight();
            if (options.place === "top") {
                $tip.css({ "top": (event.pageY - actualHeight - 10) + "px", "left": (event.pageX - actualWidth / 2) + "px" });
                $("s", $tip).css({ "top": actualHeight - 1 + "px", "left": (actualWidth / 2) - 10 + "px" })
            }
            if (options.place === "right") {
                $tip.css({ "top": (event.pageY - 50) + "px", "left": (event.pageX + 20) + "px" })
            }
            if (options.place === "bottom") {
                $tip.css({ "top": (event.pageY + 30) + "px", "left": (event.pageX - actualWidth / 2) + "px" });
                $("s", $tip).css({ "top": -20 + "px", "left": (actualWidth / 2) - 10 + "px" })
            }
            if (options.place === "left") {
                $tip.css({ "top": (event.pageY - 50) + "px", "left": event.pageX - actualWidth - 20 + "px" });
                $("s", $tip).css({ "left": actualWidth - 1 + "px" })

            }
            $tip.find(".Yan_tip_Del").click(function () {
                $tip.remove();
            })
            if (options.isshowfoot) {
                $tip.find(".Yan_tip_Foot").show();
                $tip.find(".Yan_tip_ConFirm").click(function () {
                    if ($.isFunction(options.confirm)) {
                        options.confirm.call(this, $tip, $dom);
                    }
                })
                $tip.find(".Yan_tip_Cancel").click(function () {
                    if ($.isFunction(options.cancel)) {
                        options.cancel.call(this, $tip);
                    }
                    $tip.remove();
                })

            }
            if ($.isFunction(options.showed)) {
                options.showed.call(this, $tip, $dom);
            }
            $tip.show();
        });
        if (options.type == "hover") {
            this.live('mouseleave', function (event) {
                event.stopPropagation();

                $(".Yan_tip").remove();
            })
        }
    };
})(jQuery);