var Util = {
    PX: 'px',
    fixPx: function (src) {
        if (src.contains(Util.PX)) {
            return src.substring(0, src.length - 2);
        }
        return parseInt(src);
    },
    caclPx: function (src, c) {
        var _src = parseInt(src);
        return (_src + c) + Util.PX;
    },
    formatSec: function (value, s, m, h) {
        if (s == undefined || s == null)
            s = 's';
        if (!m)
            m = 'm';
        if (!h)
            h = 'h';

        var theTime = parseInt(value);
        var theTime1 = 0;
        var theTime2 = 0;
        if (theTime > 60) {
            theTime1 = parseInt(theTime / 60);
            theTime = parseInt(theTime % 60);
            if (theTime1 > 60) {
                theTime2 = parseInt(theTime1 / 60);
                theTime1 = parseInt(theTime1 % 60);
            }
        }
        var result = "" + parseInt(theTime) + s;
        if (theTime1 > 0) {
            result = "" + parseInt(theTime1) + m + result;
        }
        if (theTime2 > 0) {
            result = "" + parseInt(theTime2) + h + result;
        }
        return result;
    },
    random: function (min, max) {
        var range = max - min;
        var rand = Math.random();
        return (min + Math.round(rand * range));
    },
    getQueryString: function (key) {
        //var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
        //var r = window.location.search.substr(1).match(reg);
        //if (r != null) return unescape(r[2]); return null;

        var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)", "i");
        var r = window.location.search.substr(1).match(reg);
        if (r != null) { return unescape(r[2]); }
        else {
            return "";
        }
    }
};

