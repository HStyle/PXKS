﻿var ismake = false;
var is3 = false;
var boxT;

function uploadFile() {
    //$("#saveBtn").click();
    openFileManager();
}

// 文档上传完成回掉
function uploadFileOver() {
    /*
	if (!isClose) {
		isclose = true;
		return;
	}
	var files = $.toJSON(uploadDocFileNames);
	files = "{\"root\":" + files + "}";
	*/
    // location.replace(encodeURI("/cwnew/maker/v5/edit_3.jsp?courseid=<%=courseid
    // %>&files="+files));


    if (ismake) {
        savekj(function (data, status) {
            if (is3) {
                location.replace(encodeURI("/cwnew/maker/v5/edit_3.jsp?courseid=" + courseid));
            } else {
                location.replace(encodeURI("/cwnew/maker/v5/edit_2.jsp?courseid=" + courseid));
            }

        });
    } else {
        savekj(function (data, status) {
        });
    }
}



function openFileManager() {
    boxT = $.weeboxs.open(
			'/kjzz/kjzz_file_manage.jsp?courseid=' + courseid + '&v='
					+ new Date().getTime(), {
					    title: '文件管理(同时添加文档和视频时,请先添加文档)',
					    draggable: true,
					    modal: true,
					    contentType: 'ajax',
					    width: 800,
					    height: 500,
					    onok: function (box) {

					        box.close();// 增加事件方法后需手动关闭弹窗
					    },
					    oncancel: function (box1) {
					        box1.close();// 增加事件方法后需手动关闭弹窗
					    },
					    onclose: function () {
					        uploadFileOver();
					    }
					});
}






/**
 * 交换视频顺序
 */
function exchangeV(vName1, vName2) {
    // 所有选项放到数组中
    var vArray = new Array();
    $.each($("#videoSelect").find("option"), function (i, e2) {
        vArray.push(e2);
    });

    // 在数组中，交换选项
    var option1Index;
    var option2Index;
    $.each(vArray, function (i, e2) {
        if (e2.text == vName1) {
            option1Index = i;
        }
        if (e2.text == vName2) {
            option2Index = i;
        }
    });
    var tempOption = vArray[option1Index];
    vArray[option1Index] = vArray[option2Index];
    vArray[option2Index] = tempOption;

    // 清空select，重新加入排序好的option
    $("#videoSelect").empty();
    $.each(vArray, function (i, e2) {
        console.log(e2.value + "==>" + e2.text + ":" + e2.selected + ":index="
				+ i);
        $("#videoSelect").append(
				"<option value=\"" + i + "\" selected=" + e2.selected + ">"
						+ e2.text + "</option>");

    });

    // 交换，视频顺序
    CW.exchangeVideo(vName1, vName2);
}

/**
 * 添加视频
 */
function addV(vname) {

    var isContain = false;
    $.each(CW.videos, function (i, elem) {
        if (elem === vname) {
            isContain = true;
        }
    });
    if (isContain) {
        return;
    }

    console.log("添加视频：" + vname);
    $op = $("<option value=\"" + CW.videos.length + "\">" + vname + "</option>");
    $("#videoSelect").append($op);

    CW.videos.push(vname);

    // 若为第一个视频，选中并加载。
    if (CW.videos.length == 1) {
        $("#videoSelect").val(0);
        // 载入首个视频
        CW_Engine.loadVideo();
    }

}

/**
 * 删除视频
 */
function deletev(vname) {
    console.log("在文档和视频排序界面 删除视频：" + vname);
    // 列表中 是否包括vname
    var isContaint = false;
    $.each($("#videoSelect").find("option"), function (i, e2) {
        if (e2.text == vname) {
            isContaint = true;
        }
    });
    if (!isContaint) {
        return;
    }

    // 选中视频文件名称
    var selectVname = $("#videoSelect").find("option:selected").text();
    ;

    // 所有选项放到数组中
    var vArray = new Array();
    $.each($("#videoSelect").find("option"), function (i, e2) {
        vArray.push(e2);
    });

    // 在数组中，删除选项
    var option1Index;
    $.each(vArray, function (i, e2) {
        if (e2.text == vname) {
            option1Index = i;
        }
    });
    vArray.splice(option1Index, 1);

    // 清空select，重新加入排序好的option
    $("#videoSelect").empty();
    $.each(vArray, function (i, e2) {
        console.log(e2.value + "==>" + e2.text + ":" + e2.selected + ":index="
				+ i);
        $("#videoSelect").append(
				"<option value=\"" + i + "\" selected=" + e2.selected + ">"
						+ e2.text + "</option>");

    });

    if (selectVname == vname) {
        $("#videoSelect").val(0);
        // 载入首个视频
        CW_Engine.loadVideo();
    }

    CW.removeVideo(vname);

}

/**
 * 交换文档顺序
 */
function exchangeD(docid1, docname1, docid2, docname2) {
    docname1 = docname1.substring(0, docname1.indexOf('.'));
    docname2 = docname2.substring(0, docname2.indexOf('.'));
    console.log("交换文档顺序：" + docid1 + "<=====>" + docid2)
    // 所有选项放到数组中
    var vArray = new Array();
    $.each($("#docSelect").find("option"), function (i, e2) {
        vArray.push(e2);
    });

    // 在数组中，交换选项
    var option1Index;
    var option2Index;
    $.each(vArray, function (i, e2) {
        if (e2.text == docname1) {
            option1Index = i;
        }
        if (e2.text == docname2) {
            option2Index = i;
        }
    });
    var tempOption = vArray[option1Index];
    vArray[option1Index] = vArray[option2Index];
    vArray[option2Index] = tempOption;

    // 清空select，重新加入排序好的option
    $("#docSelect").empty();
    $.each(vArray, function (i, e2) {
        console.log(e2.value + "==>" + e2.text + ":" + e2.selected + ":index="
				+ i);
        $("#docSelect").append(
				"<option class=\"" + e2.class + "\" value=\"" + i
						+ "\" selected=" + e2.selected + ">" + e2.text
						+ "</option>");

    });

    CW.exchangeDoc(docid1, docname1, docid2, docname2);

    // 重新加载左侧菜单
    CW_Engine.reloadMenus();
}

/**
 * 添加文档
 */
function addD(docs) {
    if (docs.length == 0) {
        return;
    }

    $.each(docs, function (i, docName) {
        console.log("添加文档：" + docName);
        $.post("/cwnew/loadDocJs.jsp?docName=" + docName, {
            "createUserN": createUserN,
            "CourseName": CourseName
        }, function (data) {
            var j = eval("(" + data.trim() + ")");
            if (j.suc) {
                console.log("加载js文件：" + j.jsPath);

                loadJS(j.jsPath, function () {
                    console.log("完成加载js文件：" + j.jsPath);
                    docName = docName.substring(0, docName.indexOf("."));
                    $op = $("<option class=\"" + j.dn + "\" value=\""
							+ (CW.files.length - 1) + "\">" + docName
							+ "</option>");
                    $("#docSelect").append($op);

                    if (CW.files.length == 1) {
                        $("#docSelect").val(0);
                        // 载入首个文档
                        CW_Engine.loadDoc();
                    }
                    // 重新加载左侧菜单
                    CW_Engine.reloadMenus();
                }, "gbk");
            }
        });
    });

}

/**
 * 删除文档
 */
function deleteD(docid, docname) {
    docname = docname.substring(0, docname.indexOf("."));
    console.log("删除文档：name=" + docname + ";id=" + docid);

    // 列表中 是否包括vname
    var isContaint = false;
    $.each($("#docSelect").find("option"), function (i, e2) {
        if (e2.text == docname) {
            isContaint = true;
        }
    });
    if (!isContaint) {
        return;
    }

    // 选中视频文件名称
    var selectVname = $("#docSelect").find("option:selected").text();
    ;

    // 所有选项放到数组中
    var vArray = new Array();
    $.each($("#docSelect").find("option"), function (i, e2) {
        vArray.push(e2);
    });

    // 在数组中，删除选项
    var option1Index;
    $.each(vArray, function (i, e2) {
        if (e2.text == docname) {
            option1Index = i;
        }
    });
    vArray.splice(option1Index, 1);

    // 清空select，重新加入排序好的option
    $("#docSelect").empty();
    $.each(vArray, function (i, e2) {
        console.log(e2.value + "==>" + e2.text + ":" + e2.selected + ":index="
				+ i);
        $("#docSelect").append(
				"<option value=\"" + i + "\" class=\"" + e2.class
						+ "\" selected=" + e2.selected + ">" + e2.text
						+ "</option>");

    });

    if (selectVname == docname) {
        $("#docSelect").val(0);
        // 载入首个文档
        CW_Engine.loadDoc();
    }

    CW.removeDoc(docid, docname);

    // 重新加载左侧菜单
    CW_Engine.reloadMenus();
}

function loadJS(url, callback, charset) {
    var script = document.createElement('script');
    script.onload = script.onreadystatechange = function () {
        if (script && script.readyState
				&& /^(?!(?:loaded|complete)$)/.test(script.readyState))
            return;
        script.onload = script.onreadystatechange = null;
        script.src = '';
        script.parentNode.removeChild(script);
        script = null;
        if (callback)
            callback();
    };
    script.charset = charset;
    script.src = url;
    try {
        document.getElementById("outOrder").appendChild(script);
    } catch (e) {
        console.log("加载js文件错误 ");
    }
}


$(document).ready(function () {
    $("body").append("<div id=\"outOrder\"></div>");
});
//===========================================================

//保存课件
function savekj(callbc) {
    //CW.link = $("#link").val();
    CW.desc = $("#desc").val();
    console.log(CW);
    var _cw = JSON.stringify(CW);
    console.log(_cw);
    //$.base64.utf8encode = true;
    //_cw = $.base64.btoa(_cw);
    $.post("/API/VIEWAPI.ashx?Action=PXGL_UPDATECW", {
        P1: kjId,
        P2: _cw
    }, function (data, status) {
        console.log(data);
        return callback.call(this, $.parseJSON(data));
    });
}
/**
 * 
 * @returns {undefined}
 */
CW_Engine._initBtns = function () {

    // 保存
    $("#saveBtn").click(function () {
        savekj(function (data, status) {
            try {
                if (data.ErrorMsg == "") {
                    alert("课件保存成功");
                }
                else {
                    alert("课件保存失败：" + data.ErrorMsg);
                }
            } catch (ex) { }
        });
    });

    // 上翻页
    $("#pptBtnPrev").click(function () {
        CW_Engine.prePage();
    });
    // 下翻页
    $("#pptBtnNext").click(function () {
        CW_Engine.nextPage();
    });

    _initMenuBtns();
};


/**
 * 渲染树菜单
 * 
 * @param {type}
 *            parent
 * @param {type}
 *            menu
 * @param {type}
 *            level
 * @returns {undefined}
 */
CW_Engine._initTree = function (parent, menu, level) {
    $.each(menu, function (index, element) {
        if (element.rended) {
            return;
        }
        element.rended = true;
        // 每个菜单li都有唯一的ID为 'm' + inx
        //
        __initTree(parent, element, level);
    });
    // 菜单加载完成
    CW_Engine.LoadStatus.menuLoaded = true;
};

function __initTree(parent, element, level) {
    var $li;
    // 子节点
    var subMenu = new Array();
    $.each(CW.menu, function (_index, _element) {
        if (_element.ix.length > element.ix.length && _element.ix.indexOf(element.ix) === 0) {
            console.log("为 " + element.name + " 添加子节点：" + _element.name);
            subMenu.push(_element);
            /*
			 * if(element.ix=="root_" && !(_element.file)){ console.log("为
			 * "+element.name+" 添加子节点："+_element.name); subMenu.push(_element);
			 * 
			 * }else{ console.log("为 "+element.name+" 添加子节点："+_element.name);
			 * subMenu.push(_element); }
			 */
        }
    });
    if (subMenu.length > 0 || (!(element.file) && CW.screen != 2)) {
        //说明有子菜单
        $li = $("<li id=\"" + element.ix + "\"></li>");
        var $label = $("<label>" + (level === "" ? element.name : "<span class=\"del\" title=\"删除\">×</span><input value=\"" + element.name + "\"/>") + "</label>");
        var $input = $("<input type=\"checkbox\" checked=\"checked\">");
        var $ol = $("<ol></ol>");
        $li.append($label);
        $li.append($input);
        $li.append($ol);

        //递归渲染
        CW_Engine._initTree($ol, subMenu, level + "folder");
    } else {
        if (CW.screen == 2) {
            element.file = {};
        }
        //没有子菜单
        $li = $("<li id=\"" + element.ix + "\" class=\"file\"></li>");
        var pos = '0';
        if (element.file.type === 'ppt') {
            //如果是PPT，默认菜单对应的文档就是菜单的ID
            var sp = element.ix.split("_");
            pos = sp[sp.length - 2];
        } else {
            pos = element.pos;
        }
        element.pos = pos;
        var $a = CW_Engine._createMenu(element.name, element.video + " <font color=yellow>" + Util.formatSec(element.time) + "</font>", element.file.file + " <font color=yellow>" + pos + "</font>");
        $li.append($a);
    }
    parent.append($li);
}

function _initMenuBtns() {

    //加入树组件中的文本框事件
    $("ol li :input").mousemove(function () {
        $(this).css("background-color", "#fc6");
    }).mouseout(function () {
        $(this).css("background-color", "transparent");
    }).blur(function () {
        var na = $(this).val();
        var ix = $(this).parents("li:first").attr("id");
        var menu = CW_Engine.getMenu(ix);
        menu.name = na;
    });

    //点击时间图标跳到时间位置
    $(".gtime").unbind("click").click(function () {
        var ix = $(this).parents("li:first").attr("id");
        var menu = CW_Engine.getMenu(ix);
        var sec = menu.time;
        if (sec) {
            CW_Engine.goMenu(menu.video, sec);
        }
    });
    //点击文档图标走到指定位置
    $(".gdoc").unbind("click").click(function () {
        var ix = $(this).parents("li:first").attr("id");
        var menu = CW_Engine.getMenu(ix);
        var pos = menu.pos;
        if (menu.type === 'ppt') {
            CW_Engine._goPage(pos, menu.file);
        } else if (menu.type === 'doc') {
            CW_Engine._goTop(pos, menu.file.dn);
        }
    });
    //给视频时间输入框加入点击事件
    $(".ctime").unbind("click").click(function () {
        //取得视频的当前时间        
        var time;
        if (isBlws) {
            time = blwsPlayer.j2s_getCurrentTime();
        }
        else {
            time = parseInt(CW_Engine.Comp.video()[0].currentTime);
        }
        if (isNaN(time)) {
            time = parseInt($("#videoTime").text());
        }
        //填在输入框中
        $(this).html(CW.videos[CW_Engine.LoadStatus.vinx].name + " <font color=yellow>" + Util.formatSec(time) + "</font>");
        //取得菜单ID
        var ix = $(this).parents("li:first").attr("id");
        //设置菜单时间
        var menu = CW_Engine.getMenu(ix);

        menu.video = CW.videos[CW_Engine.LoadStatus.vinx].name;
        menu.time = time;
    });
    //组文档位置输入框加入点击事件
    $(".cdoc").unbind("click").click(function () {
        var pos;
        if (CW.isPPT()) {
            //ppt 取得当前页
            pos = CW_Engine.currentPage();
        } else if (CW.isDOC()) {
            //word 取得当前滚动位置
            pos = CW_Engine.Comp.doc().get(0).contentWindow.document.body.scrollTop;
        }
        //填在输入框中
        $(this).html(CW.files[CW_Engine.LoadStatus.dinx].file + " <font color=yellow>" + pos + "</font>");
        //取得菜单ID
        var ix = $(this).parents("li:first").attr("id");
        //设置菜单位置
        var menu = CW_Engine.getMenu(ix);
        menu.pos = pos;
        menu.file = CW.files[CW_Engine.LoadStatus.dinx];
    });
    //在上方加入菜单事件
    $(".uad").unbind("click").click(function () {
        var $cli = $(this).parents("li:first");
        var ix = $cli.parents("li:first").attr("id") + Util.random(1000, 9999) + "_";
        var name = "请输入标题";
        var $li = $("<li id=\"" + ix + "\" class=\"file\"></li>");
        var $a = CW_Engine._createMenu(name);
        $li.append($a);
        $cli.before($li);
        CW.menu.push({
            "ix": ix,
            "name": name
        });
        _initMenuBtns();
    });
    //在下方加入菜单事件
    $(".dad").unbind("click").click(function () {
        var $cli = $(this).parents("li:first");
        var ix = $cli.parents("li:first").attr("id") + Util.random(1000, 9999) + "_";
        var name = "请输入标题";
        var $li = $("<li id=\"" + ix + "\" class=\"file\"></li>");
        var $a = CW_Engine._createMenu(name);
        $li.append($a);
        $cli.after($li);
        CW.menu.push({
            "ix": ix,
            "name": name
        });
        _initMenuBtns();
    });
    //添加子菜单事件
    $(".sad").unbind("click").click(function () {
        var $cli = $(this).parents("li:first");
        var $label = $("<label><span class=\"del\" title=\"删除\">×</span><input value=\"" + $cli.find("input")[0].value + "\"/></label>");
        var $input = $("<input type=\"checkbox\" checked=\"checked\" id=\"\"/> ");
        var $ol = $("<ol></ol>");
        $cli.removeClass();
        $cli.empty();
        $cli.append($label);
        $cli.append($input);
        $cli.append($ol);
        var ix = $cli.attr("id") + "1_";
        var name = "请输入标题";
        var $li = $("<li id=\"" + ix + "\" class=\"file\"></li>");
        $ol.append($li);
        var $a = CW_Engine._createMenu(name);
        $li.append($a);
        CW.menu.push({
            "ix": ix,
            "name": name
        });
        _initMenuBtns();
    });
    //删除按扭事件
    $(".del").unbind("click").click(function () {
        if (confirm("确认要删除吗？")) {
            var $cli = $(this).parents("li:first");
            var ix = $cli.attr("id");
            for (var i = CW.menu.length - 1; i >= 0; i--) {
                if (CW.menu[i].ix.indexOf(ix) > -1) {
                    CW.menu.splice(i, 1);
                }
            }
            $cli.remove();
        }
    });
    //显示操作按扭事件
    $(".nw").hide();
    $(".ins").unbind("mouseover").unbind("mouseout").mouseenter(function () {
        $(this).children(".nw").show();
    }).mouseleave(function () {
        $(this).children(".nw").hide();
    });
}

/**
 * 
 * @param {type} name
 * @param {type} pos
 * @returns {$}
 */
CW_Engine._createMenu = function (name, time, pos) {
    if (!pos || pos.indexOf("undefined") > -1) {
        pos = '-';
    }
    if (!time || time.indexOf("undefined") > -1) {
        time = "--:--:--";
    }
    var s = "<a class=ma><div class=\"ins\"><input value=\"" + name + "\"/><sp class=\"nw\"><span class=\"del\" title=\"删除\">×</span><span class=\"uad\" title=\"上方插入同级菜单\">↑+</span><span class=\"dad\" title=\"下方插入同级菜单\">↓+</span><span class=\"sad\" title=\"插入子菜单\">→+</span></sp></div><div class=\"nws\"><img class=\"gtime\" src='res/img/time.png' title='视频时间'/><span class=\"ctime\" title=\"点此将当前时间写入\">" + time + "</span>";
    if (CW.files.length > 0) {
        s += "<img class=\"gdoc\" src='res/img/pos.png' title='文档位置'/><span class=\"cdoc\" title=\"点此将当前文档位置写入\">" + pos + "</span>";
    }
    s += "</div></a>";
    return $(s);
};