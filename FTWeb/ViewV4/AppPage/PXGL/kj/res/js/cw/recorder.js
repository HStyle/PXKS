CW_Engine.nextPageHandler = function () {
    var _file = CW.files[CW_Engine.LoadStatus.dinx].file;
    var _pos = CW_Engine.currentPage() + "";
    var _me;
    $.each(CW.menu, function (index, element) {
        if (element.file !== undefined) {
            if (element.file.file === _file && element.pos === _pos) {
                _me = element;
                return;
            }
        }
    });
    if (_me && _me.time === undefined) {
        var time = parseInt(videoTime.innerText);
        if (isNaN(time)) {
            return;
        }
        _me.video = CW.videos[CW_Engine.LoadStatus.vinx];
        _me.time = time;
        //填在输入框中
        var $ctime = $(document.getElementById(_me.ix)).find(".ctime");
        $ctime.html(_me.video + " <font color=yellow>" + Util.formatSec(time) + "</font>");
    }
};


CW_Engine.prePageHandler = function () {

};

/**
 * 
 * @returns {undefined}
 */
CW_Engine._initBtns = function () {

    //保存
    $("#saveBtn").click(function () {
        CW.link = $("#link").text();
        CW.desc = $("#desc").text();
        var _cw = $.toJSON(CW);
        $.base64.utf8encode = true;
        _cw = $.base64.btoa(_cw);
        $(this).hide();
        $.post(
                "/cwnew/save_han_v5.jsp",
                {
                    "CW": _cw
                    //"action":"1"
                },
                function (data, status) {
                    try {
                        var j = eval("(" + data.trim() + ")");
                        if (j.status === '1') {
                            CW.save = j.save;
                            //alert("课件数据保存成功,请切换到'高速模式'上传视频文件。");
                            //location.href = "/cwnew/maker/v5/record_3_2.jsp?courseid="+courseid;
                            //alert("保存成功");

                            // alert("课件数据保存成功");
                            //location.href = "publish.jsp?video=" + CW.videos[0];

                        } else {
                            alert("保存失败：" + j.result);
                        }
                    } catch (e) {
                        alert("保存失败，可能需要重新登陆系统！");
                    }
                    //$("#saveBtn").show();
                }
        );





    });

    //上翻页
    $("#pptBtnPrev").click(function () {
        CW_Engine.prePage();
    });
    //下翻页
    $("#pptBtnNext").click(function () {
        CW_Engine.nextPage();
    });

    _initMenuBtns();
};


/**
 * 渲染树菜单
 * @param {type} parent
 * @param {type} menu
 * @param {type} level
 * @returns {undefined}
 */
CW_Engine._initTree = function (parent, menu, level) {
    $.each(menu, function (index, element) {
        if (element.rended) {
            return;
        }
        element.rended = true;
        /*
        if (element.type === 'doc') {
            return;
        }
        */
        __initTree(parent, element, level);
    });
    //菜单加载完成
    CW_Engine.LoadStatus.menuLoaded = true;
};
