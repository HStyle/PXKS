﻿$(function () {
    var html = "";
    for (var i = 0; i < paramConfig.length; i++) {
        var paramJson = paramConfig[i];
        html += '<div class="table-responsive" style="border:solid 1px;margin-top:20px;padding:10px" id="divGroup' + i + '">';
        html += '<div style="border-bottom:dashed 1px;padding-bottom:2px"><input type="text" value="' + paramJson.groupName + '" style="margin-right:10px" placeholder="参数组名称" class="groupName" /><input type="button" onclick="delGroup(this)" value="删除该参数组" /><input type="button" style="margin-left:20px" onclick="addParam(this)" value="添加参数" /></div>';
        html += ' <table class="table table-striped">';
        html += ' <thead>';
        html += '<tr>';
        html += '<th>名称</th>';
        html += '<th>标识</th>';
        html += '<th>操作</th>';
        html += '</tr>';
        html += ' </thead>';
        html += ' <tbody>';
        for (var chlid = 0; chlid < paramJson.groupParam.length; chlid++) {
            var groupParam = paramJson.groupParam[chlid];
            html += '<tr>';
            html += '<td><input type="text" value="' + groupParam.name + '" class="groupChildName" placeholder="参数名称"/></td>';
            html += ' <td><input type="text" value="' + groupParam.tag + '" class="groupChildTag" placeholder="参数标识"/></td>';
            html += ' <td><input type="button" value="删除" onclick="del(this)" /></td>';
            html += '</tr>';
        }
        html += ' </tbody>';
        html += '</table>';
        html += '</div>';
    }
    $("#group").html(html);
})
function addParam(a) {
    if (!a) {
        var len = $(".table-responsive").length;
        var divGroup = document.createElement("div");
        divGroup.id = 'divGroup' + len;
        divGroup.style = "border:solid 1px;margin-top:20px;padding:10px";
        divGroup.className = "table-responsive";
        var html = '<div style="border-bottom:dashed 1px;padding-bottom:2px"><input type="text" value="" style="margin-right:10px" placeholder="参数组名称" class="groupName" /><input type="button" onclick="delGroup(this)" value="删除该参数组" /><input type="button" style="margin-left:20px" onclick="addParam(this)" value="添加参数" /></div>';
        html += ' <table class="table table-striped">';
        html += ' <thead>';
        html += '<tr>';
        html += '<th>名称</th>';
        html += '<th>标识</th>';
        html += '<th>操作</th>';
        html += '</tr>';
        html += ' </thead>';
        html += ' <tbody>';
        html += '<tr>';
        html += '<td><input type="text" value="" class="groupChildName" placeholder="参数名称"/></td>';
        html += ' <td><input type="text" value="" class="groupChildTag" placeholder="参数标识"/></td>';
        html += ' <td><input type="button" value="删除" onclick="del(this)" /></td>';
        html += '</tr>';
        html += ' </tbody>';
        html += '</table>';
        divGroup.innerHTML = html;
        $("#group").append(divGroup);
    }
    else {
        var trElement = document.createElement("tr");
        var tr = '<td><input type="text" value="" class="groupChildName" placeholder="参数名称"/></td>';
        tr += '<td><input type="text" value="" class="groupChildTag" placeholder="参数标识"/></td>';
        tr += '<td><input type="button" value="删除" onclick="del(this)" /></td>';
        trElement.innerHTML = tr;
        var tbody = $(a).closest(".table-responsive").find("tbody").append(trElement);
    }
}
function del(a) {
    layer.confirm("确定删除该参数？",
         function (index) {
             $(a).closest("tr").remove();
             layer.close(index);
         }
    )
}
function delGroup(a) {
    layer.confirm("确定删除该参数组？",
         function (index) {
             $(a).closest(".table-responsive").remove();
             layer.close(index);
         }
    )
}

function saveConfig(webapi) {
    var divGroup = $(".table-responsive");
    var tags = '0,0';
    var configJson = '[';
    for (var i = 0; i < divGroup.length; i++) {
        var groupName = $(divGroup[i]).find(".groupName").val();
        if (!groupName) {
            layer.msg("第" + (i + 1) + "个参数组的名称不能为空");
            return false;
        }

        var groupChildName = $(divGroup[i]).find(".groupChildName");
        var groupChildTag = $(divGroup[i]).find(".groupChildTag");
        var groupChildNameToTag = "[";
        for (var child = 0; child < groupChildName.length; child++) {
            var groupChildNameVal = $(groupChildName[child]).val();
            var groupChildTagVal = $(groupChildTag[child]).val();
            if (!groupChildNameVal) {
                layer.msg("第" + (i + 1) + "个参数组的第" + (child + 1) + "个参数名称不能为空");
                return false;
            }
            if (!groupChildTagVal) {
                layer.msg("第" + (i + 1) + "个参数组的第" + (child + 1) + "个参数标识不能为空");
                return false;
            }
            var reg = /^[a-z]{1}[a-z|A-Z|0-9]*$/
            if (!groupChildTagVal) {
                layer.msg("第" + (i + 1) + "个参数组的第" + (child + 1) + "个参数标识不能为空");
                return false;
            }
            if (!reg.test(groupChildTagVal)) {
                layer.msg("第" + (i + 1) + "个参数组的第" + (child + 1) + "个参数标识需要由小写字母a-z开头的字母数字组成");
                return false;
            }
            if (tags.indexOf(',' + groupChildTagVal + ',') > -1) {
                layer.msg("第" + (i + 1) + "个参数组的第" + (child + 1) + "个参数标识与其它参数的标识重复");
                return false;
            }
            groupChildNameToTag += '{';
            groupChildNameToTag += '"name":"' + groupChildNameVal + '"';
            groupChildNameToTag += ',';
            groupChildNameToTag += '"tag":"' + groupChildTagVal + '"';
            groupChildNameToTag += '}';
            if (child < (groupChildName.length - 1)) {
                groupChildNameToTag += ',';
            }
        }
        groupChildNameToTag += "]";
        configJson += '{';
        configJson += '"groupTag":"divGroup' + i + '"';
        configJson += ',';
        configJson += '"groupName":"' + groupName + '"';
        configJson += ',';
        configJson += '"groupParam":' + groupChildNameToTag;
        configJson += '}';
        if (i < (divGroup.length - 1)) {
            configJson += ',';
        }
    }
    configJson += ']';
    configJson = 'var paramConfig=' + configJson;
    $.post(webapi, { data: configJson }, function (result) {
        if (result.title) {
            layer.msg("保存成功");
        }
        else {
            layer.msg(result.message);
        }
    })
}