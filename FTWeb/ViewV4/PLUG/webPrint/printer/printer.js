
var printObjects = [];
var expressCompnany_datas = [];
function initPage(companys)
{
    expressCompnany_datas = companys;
    initParamInfo();
    bindCheckedToParamInfo();
    initExpressCompnany();
    initPageUi();
}
///初始化内容参数列表
function initParamInfo() {
    var html = "";
    for (var i = 0; i < paramConfig.length; i++) {
        var paramJson = paramConfig[i];
        html += '<div class="accordion-group">';
        html += '<div class="accordion-heading">';
        html += '<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse' + (i + 1) + '">';
        html += paramJson.groupName;
        html += '</a>';
        html += '</div>';
        html += '<div id="collapse' + (i + 1) + '" class="accordion-body collapse in">';
        html += '<div class="accordion-inner" id="addressorInfo">';
        for (var chlid = 0; chlid < paramJson.groupParam.length; chlid++) {
            var groupParam = paramJson.groupParam[chlid];
            html += '<label class="checkbox"><input type="checkbox" class="infoCheck" id="' + groupParam.tag + '">' + groupParam.name + '</label>';
        }
        html += '</div>';
        html += '</div>';
        html += '</div>';
    }
    $("#accordion2").html(html);
}
///给内容参数绑定选中事件
function bindCheckedToParamInfo() {
    $('.infoCheck').change(function () {
        var a = $(this).is(':checked');
        var id = $(this).attr('id');
        var text = $(this).parent().text();
        var cb = $(this);
        if (a) {
            var data = {};
            var contentTop = $('#content').offset().top;
            var contentLeft = $('#content').offset().left;
            addPrintObjects(data,id,text);
        }
        else {
            if (printObjects != null && printObjects.length > 0) {
                for (var i = 0; i < printObjects.length; i++) {
                    if (printObjects[i].id == id) {
                        var tempObj = printObjects[i];
                        tempObj.remove();
                        printObjects.splice(i, 1);
                        tempObj = null;
                        break;
                    }
                }
            }
        }
    })
}
///给打印内容对象添加参数
function addPrintObjects(data,id,text) {
    if (data.w == null) {
        data.w = 'auto';
    }
    if (data.h == null) {
        data.h = 'auto';
    }
    if (data.top == null) {
        data.top = 0;
    }
    if (data.left == null) {
        data.left = 0;
    }
    if (data.name == null) {
        data.name = id;
    }
    if (data.text == null) {
        data.text = text;
    }
    if (data.font_size == null) {
        data.font_size = $('#fontSize').val();
    }
    if (data.font_family == null) {
        data.font_family = $('#fontStyle').val();
    }
    var object = new PrintObject(data, $('#content'));
    printObjects.push(object);
}

///初始化快递公司下拉列表
function initExpressCompnany()
{
    //绑定快递公司列表开始
    for (var i = 0; i < expressCompnany_datas.length; i++) {
        $('#expressCompnany').append('<option value="' + expressCompnany_datas[i].KDID + '" >' + expressCompnany_datas[i].KDName + '</option>');
    }
    //更换快递公司事件
    $('#expressCompnany').change(function () {
        printObjects = [];
        $('.infoCheck').attr('checked', false);
        $('.object').remove();
        $('#content').html('');
        var expressCompnany = $(this).val();
        //initContent(expressCompnany);
        getUserSetting(expressCompnany);
    });
}
///初始化基本参数
function initPageUi() {
    ///字体
    for (var b = 8; b <= 25; b++) {
        $('.fontSizes').append('<option value="' + b + 'px" ' + (b == 16 ? 'selected' : '') + '>' + b + '</option>')
    }
    $('.aboutObject').hide();
    ///选择字体样式事件
    $('.styleSelector').change(function () {
        var val = $(this).val();
        var styleName = $(this).attr('data_styleName');
        for (var i = 0; i < printObjects.length; i++) {
            if ('all' == $(this).attr('data_scope') || printObjects[i].selected) {
                var data = {};
                data[styleName] = val;
                printObjects[i].setStyle(data);
            }
        }
    })
    ///修改宽度
    $('#imgWidth').change(function () {
        var v = $(this).val();
        if (v != '' && isNaN(v) == false && $('#expressImg').length > 0) {
            $('#expressImg').css('width', (v * 3.78) + 'px');
            $('#expressImg').find('img').css('width', (v * 3.78) + 'px');
        }

    })
    ///修改高度
    $('#imgHeight').change(function () {
        var v = $(this).val();
        if (v != '' && isNaN(v) == false && $('#expressImg').length > 0) {
            $('#expressImg').css('height', (v * 3.78) + 'px');
            $('#expressImg').find('img').css('height', (v * 3.78) + 'px');
        }

    })
    ///内容点击宽度
    $('#content').click(function (e) {
        $('#objectSetting').hide();
    })
}
///初始化用户默认快递
function initUserSetting(userOptions) {
    initContent(userOptions);
}
///获取用户快递配置
function getUserSetting(KDID) {
    var url="http://www.qijieyun.com/API/VIEWAPI.ashx?action=CRM_GETKDSETTING";
    if (KDID != "") {
        url += "&p1=" + KDID;
    }
    else {
        url += "&p2=1";
    }
    ajaxJsonp(url, function (result) {
        layer.closeLoad();
        if (result.ErrorMsg == '') {
            if (result.Result.length > 0) {
                initUserSetting(result.Result[0]);
            }
            else {
                getCurrentExpressCompanyData(KDID);
            }
        }
        else {
            layer.msg(result.ErrorMsg);
        }
    })
}
///jsonp提交
function ajaxJsonp(url,success) {
    layer.load({ shade: [0.3, '#000'], time: 30000 });
    $.ajax({
        url: url,
        async: true,
        type: "post",
        dataType: "jsonp",
        jsonp: "jsonpcallback",
        success:success,
        error: function (res) {
            layer.closeLoad();
            layer.msg("请求错误");
        }
    })
}

///保存个人配置
function saveConfig(webapi) {
    if ($('#expressCompnany').val() == '') {
        layer.alert('请选择快递公司', 9);
        return;
    }
    var url = "/API/VIEWAPI.ashx?action=CRM_SETKDSETTING";
    var paramJson = buildTemplate();
    //url += "&P1=" + paramJson;

    //layer.load({ shade: [0.3, '#000'], time: 30000 });
    $.post(url, { P1: paramJson }, function (result) {
        var r = JSON.parse(result);
        if (r.ErrorMsg == "") {
            layer.msg("保存成功");
        } else {
            layer.msg(result.ErrorMsg);
        }
    })


    //ajaxJsonp(url, function (result) {
    //    layer.closeLoad();
    //    if (result.ErrorMsg == '') {
    //        layer.msg("保存成功");
    //    }
    //    else {
    //        layer.msg(result.ErrorMsg);
    //    }
    //})
}

///设置当前快递单配置数据
function getCurrentExpressCompanyData(expressCompnany) {
    var currentExpressCompanyData = null;
    for (var i = 0; i < expressCompnany_datas.length; i++) {
        if (expressCompnany_datas[i].KDID == expressCompnany) {
            currentExpressCompanyData = expressCompnany_datas[i];
            break;
        }
    }
    initContent(currentExpressCompanyData);
}

//加载快递公司配置开始
function initContent(templateData) {
    if (templateData != null) {
        $('#content').append('<div id="expressImg"><img src="printer/images/' + templateData.KDID + '.jpg"/></div>');
        if (templateData.objects&&templateData.objects.length > 0) {
            var objects = JSON.parse(templateData.objects);
            if (objects != null && objects.length > 0) {
                for (var k = 0; k < objects.length; k++) {
                    $('#' + objects[k].name).removeAttr("checked").click();
                    if (printObjects != null && printObjects.length > 0) {
                        for (var i = 0; i < printObjects.length; i++) {
                            if (printObjects[i].id == objects[k].name) {
                                var tempObj = printObjects[i];
                                tempObj.remove();
                                printObjects.splice(i, 1);
                                tempObj = null;
                                break;
                            }
                        }
                    }
                    addPrintObjects(objects[k], objects[k].name, objects[k].text);
                }
            }
        }
        if (templateData.IsDefault=="True") {
            $("#isDefault").removeAttr("checked");
            $("#isDefault").click();
        }
        else {
            $("#isDefault").removeAttr("checked");

        }
        if ($('#expressImg').find('img').length > 0) {
            $('#expressImg').find('img').load(function () {
                if (templateData != null && templateData.imageWidth != null && templateData.imageWidth != '' && isNaN(templateData.imageWidth) == false) {
                    $('#imgWidth').val(templateData.imageWidth);
                    $('#expressImg').css('width', (templateData.imageWidth * 3.78) + 'px');
                    $('#expressImg').find('img').css('width', (templateData.imageWidth * 3.78) + 'px');
                } else {
                    $('#imgWidth').val(Math.round($('#expressImg').find('img').width() / 3.78));
                }
                if (templateData != null && templateData.imgHeight != null && templateData.imgHeight != '' && isNaN(templateData.imgHeight) == false) {
                    $('#imgHeight').val(templateData.imgHeight);
                    $('#expressImg').css('height', (templateData.imgHeight * 3.78) + 'px');
                    $('#expressImg').find('img').css('height', (templateData.imgHeight * 3.78) + 'px');
                } else {
                    $('#imgHeight').val(Math.round($('#expressImg').find('img').height() / 3.78));
                }

                templateData = null;
            })
        } else {
            templateData = null;
        }

    } else {
        templateData = null;
    }
}

//清除打印内容参数选中状态
function clearPrintObjectSelected(){
	for(var i=0;i<printObjects.length;i++){
		if(printObjects[i].selected){
			printObjects[i].unselected();
		}
	}
}

function buildTemplate() {
    var str = "";
    var config = {};
    config.KDID = $('#expressCompnany').val();
    config.KDName = $('#expressCompnany').find("option:selected").text();
    config.imageWidth = $('#imgWidth').val();
    config.imageHeight = $('#imgHeight').val();
    if ($('#isDefault').is(":checked")) {
        config.IsDefault = true;
    }
    else {
        config.IsDefault = false;
    }
    var params = [];
    var checkParam = $("#accordion2").find("input[type='checkbox']:checked");
    for (var param = 0; param < checkParam.length; param++) {
        var currentParam = $(checkParam[param]);
        var id = currentParam.attr("id");
        var text = currentParam.parent().text();
        var paramObj = {};
        paramObj.name = id;
        paramObj.text = text;

        var obj = $("#" + id + "_obj");
        if (obj) {
            paramObj.w = obj.css("width").replace('px', '');
            paramObj.h = obj.css("height").replace('px', '');
            paramObj.top = obj.css("top").replace('px', '');
            paramObj.left = obj.css("left").replace('px', '');
            paramObj.font_size = obj.css("font-size");
            paramObj.font_weight = obj.css("font-weight");
            paramObj.font_family = obj.css("font-family");
        }
        params[param] = paramObj;
    }
    config.objects =JSON.stringify(params);
    str = JSON.stringify(config);
    return str;
}
