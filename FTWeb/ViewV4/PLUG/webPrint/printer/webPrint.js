﻿///jsonp提交
function ajaxJsonp(url, success) {
    layer.load({ shade: [0.3, '#000'], time: 30000 });
    $.ajax({
        url: url,
        async: true,
        type: "post",
        dataType: "jsonp",
        jsonp: "jsonpcallback",
        success: success,
        error: function (res) {
            layer.closeLoad();
            layer.msg("请求错误");
        }
    })
}
function initExpressCompnany() {
    //绑定快递公司列表开始
    for (var i = 0; i < expressCompnany_datas_default.length; i++) {
        $('#expressCompnany').append('<option value="' + expressCompnany_datas_default[i].KDID + '" >' + expressCompnany_datas_default[i].KDName + '</option>');
    }
    $('#expressCompnany').change(function () {
        $("#content").html('');
        setPrintOptions();
    });
}
function setPrintOptions() {
    var KDID = $("#expressCompnany").val();
    ///获取用户快递配置
    var url = "/API/VIEWAPI.ashx?action=CRM_GETKDSETTING";
    if (KDID) {
        url += "&p1=" + KDID;
    }
    else {
        url += "&p2=1";
    }
    $.getJSON(url, function (result) {
        layer.closeLoad();
        if (result.ErrorMsg == '') {
            if (result.Result.length > 0) {
                var objects = [];
                var options = {}
                var currentCompany = result.Result[0];
                options.expressCompnany = currentCompany.KDID;
                options.imageWidth = currentCompany.imageWidth * 3.78;
                options.imgHeight = currentCompany.imgHeight * 3.78;
                if (currentCompany.objects.length<1) {
                    layer.msg("对不起，该快递单模板没有配置，请配置后再使用");
                    return;
                }
                objects =JSON.parse(currentCompany.objects);
                for (var i = 0; i < objects.length; i++) {
                    if (printContent[objects[i].name] != null) {
                        objects[i].text = printContent[objects[i].name];
                    }
                }
                options.objects = objects;
                startPrint(options);

            }
            else {
                layer.msg("对不起，该快递单模板没有配置，请配置后再使用")
            }
        }
        else {
            layer.msg(result.ErrorMsg);
        }
    })
}
function startPrint(option) {
    $("#divImg").css({
        "background-image": "url(printer/images/" + option.expressCompnany + ".jpg)",
        "width": option.imageWidth||800,
        "height": option.imgHeight||560
    });
    var html = '';
    for (var i = 0; i < option.objects.length; i++) {
        var item = option.objects[i];
        html += '<div id="' + item.name + '_obj" class="object ui-draggable ui-draggable-handle ui-resizable selected"';
        html += 'style="width:' + item.w + 'px;height:' + item.h + 'px;top:' + item.top + 'px;left:' + item.left + 'px;position:absolute;';
        html += ' font-size:' + item.font_size + ';font-weight:' + item.font_weight + ';font-family:' + item.font_family + ';text-align:left;">' + item.text + '</div>';
    }
    $("#content").append(html);
    $("#expressCompnany").val(option.expressCompnany);
}
function webPrint(id)
{
    window.print();
    return false;
}
