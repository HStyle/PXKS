﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using FastReflectionLib;
using QjySaaSWeb.AppCode;
using Newtonsoft.Json;
using QJY.Data;
using System.Data;
using Newtonsoft.Json.Linq;
using Senparc.Weixin.QY.Entities;
using System.Net;
using System.Configuration;
using System.IO;
using Senparc.Weixin.HttpUtility;
using Senparc.Weixin.MP.AdvancedAPIs;
using Senparc.Weixin.MP.AdvancedAPIs.OAuth;
using Senparc.Weixin.MP.CommonAPIs;
using Senparc.Weixin.MP;
using System.Xml;
using System.Threading.Tasks;
using Senparc.Weixin;

namespace QjySaaSWeb.API
{
    public class Commanage : IWsService
    {
        public void ProcessRequest(HttpContext context, ref Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            MethodInfo methodInfo = typeof(Commanage).GetMethod(msg.Action.ToUpper());
            Commanage model = new Commanage();
            methodInfo.FastInvoke(model, new object[] { context, msg, P1, P2, UserInfo });
        }


        #region 官网登录和注册
        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1">用户名</param>
        /// <param name="P2">密码（未解码的）</param>
        /// <param name="UserInfo"></param>
        public void LOGIN(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string password = context.Request["password"];
            string username = context.Request["UserName"];
            string ComCode = context.Request["ComId"] ?? "";
            JH_Auth_QY qyModel = new JH_Auth_QY();
            int intComID = 0;
            int.TryParse(ComCode, out intComID);
            password = CommonHelp.GetMD5(password);
            JH_Auth_User userInfo = new JH_Auth_User();
            if (intComID == 0)
            {
                List<JH_Auth_User> userList = new JH_Auth_UserB().GetEntities(d => (d.UserName == username || d.mobphone == username) && d.UserPass == password && d.IsUse == "Y").ToList();
                if (userList.Count() == 0)
                {
                    msg.ErrorMsg = "用户名或密码不正确";
                    return;
                }
                else if (userList.Count() > 1)
                {
                    msg.ErrorMsg = "-1";//用户名密码获取用户不止一个时，提示输入域名
                    return;
                }
                else
                    userInfo = userList[0];
            }
            else
            {
                List<JH_Auth_User> userList = new JH_Auth_UserB().GetEntities(d => (d.UserName == username || d.mobphone == username) && d.UserPass == password && d.IsUse == "Y" && d.ComId == intComID).ToList();
                if (userList.Count() == 0)
                {
                    msg.ErrorMsg = "用户名或密码不正确";
                    return;
                }
                else
                {
                    userInfo = userList[0];
                }
            }
            if (userInfo == null)
            {
                msg.ErrorMsg = "用户名或密码不正确";
            }
            else
            {

                if (string.IsNullOrEmpty(userInfo.pccode))
                {
                    userInfo.pccode = System.Guid.NewGuid().ToString();
                }
                userInfo.logindate = DateTime.Now;
                new JH_Auth_UserB().Update(userInfo);
                msg.Result = userInfo.pccode;
                msg.Result1 = userInfo.UserName;

                //LOG异步
                try
                {
                    Task.Factory.StartNew<string>(() =>
                    {
                        new JH_Auth_LogB().Insert(new JH_Auth_Log()
                        {
                            ComId = userInfo.ComId.Value.ToString(),
                            LogType = "PCLOGIN",
                            LogContent = "用户" + userInfo.UserRealName + "登录，登录时间:" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " 登录地点:" + CommonHelp.getIpAddr(),
                            CRUser = userInfo.UserName,
                            CRDate = DateTime.Now
                        });
                        return "";
                    });


                }
                catch { }

            }
        }
        /// <summary>
        /// 注册
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void REGISTER(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            JObject tt = (JObject)JsonConvert.DeserializeObject(P1);
            string qyCode = tt["QYCode"].ToString();
            var qy = new JH_Auth_QYB().GetEntity(p => p.QYCode == qyCode);
            if (qy != null)
            {
                msg.ErrorMsg = "企业名称已存在!";
            }
            string mobile = tt["QYCode"].ToString();
            var qy2 = new JH_Auth_QYB().GetEntities(p => p.Mobile == mobile);
            if (qy2.Count() > 0)
            {
                msg.ErrorMsg = "此手机已注册企业，请更换手机号继续注册";
            }
            if (string.IsNullOrEmpty(msg.ErrorMsg))
            {
                string QyScape = ConfigurationManager.AppSettings["initSpace"].ToString(); //公司初始控件 
                string password = CommonHelp.GetMD5(tt["UserPass"].ToString());
                string fileUrl = new FileHelp().GetFileServerUrl(qyCode);
                string userName = System.Guid.NewGuid().ToString();
                if (tt["QYCode"].ToString().ToLower() == "fullteem" || tt["QYCode"].ToString().ToLower() == "qijieyun")
                {
                    new JH_Auth_UserB().ExsSclarSql("exec RegistCompanyTest '" + tt["QYName"].ToString() + "','" + tt["mobphone"].ToString() + "','" + userName + "','" + password + "','" + tt["QYCode"].ToString() + "','" + fileUrl + "'," + QyScape);
                }
                else
                {
                    new JH_Auth_UserB().ExsSclarSql("exec RegistCompany '" + tt["QYName"].ToString() + "','" + tt["mobphone"].ToString() + "','" + userName + "','" + password + "','" + tt["QYCode"].ToString() + "','" + fileUrl + "'," + QyScape);
                }

                new FileHelp().AddQycode(qyCode, tt["QYName"].ToString());
                string content = "您的[" + tt["QYName"].ToString() + "]公司账号已经注册成功：\r\n登录网站：" + tt["QYCode"].ToString() + ".qijieyun.com \r\n管理员账号：" + tt["mobphone"].ToString() + "\r\n管理员密码：" + tt["UserPass"].ToString() + "\r\n如有疑问，请访问www.qijieyun.com联系在线客服";
                CommonHelp.SendSMS(tt["mobphone"].ToString(), content, 0);
                string user = context.Request["ID"] ?? "";
            }
        }
        /// <summary>
        /// 注册
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void REGISTERNEW(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            JObject tt = (JObject)JsonConvert.DeserializeObject(P1);
            string qyName = tt["QYName"].ToString();
            var qy = new JH_Auth_QYB().GetEntity(p => p.QYName == qyName);
            if (qy != null)
            {
                msg.ErrorMsg = "企业名称已存在!";
            }
            if (tt["xm"].ToString() == "")
            {
                msg.ErrorMsg = "姓名不能为空!";
            }
            string mobile = tt["mobphone"].ToString();
            var qy2 = new JH_Auth_QYB().GetEntities(p => p.Mobile == mobile);
            if (qy2.Count() > 0)
            {
                msg.ErrorMsg = "此手机已注册企业，请更换手机号继续注册";
            }
            if (string.IsNullOrEmpty(msg.ErrorMsg))
            {
                string QyScape = ConfigurationManager.AppSettings["initSpace"].ToString(); //公司初始控件 
                string password = CommonHelp.GetMD5(tt["UserPass"].ToString());

                #region 判断获取不重复的qycode随机code，如果表中存在重复code，while继续获取，否则直接执行下一步
                bool flag = true;
                string qyCode = "";
                while (flag == true)
                {
                    //生成code随机数
                    Random ran = new Random((int)DateTime.Now.Ticks);
                    qyCode = ran.Next().ToString();
                    if (new JH_Auth_QYB().GetEntities(p => p.QYCode == qyCode).Count() == 0)
                    {
                        flag = false;
                        continue;
                    }
                }
                #endregion

                string fileUrl = new FileHelp().GetFileServerUrl(qyCode);
                // string userName = System.Guid.NewGuid().ToString();
                string userName = mobile;
                if (qyCode.ToLower() == "fullteem" || qyCode.ToLower() == "qijieyun")
                {
                    new JH_Auth_UserB().ExsSclarSql("exec RegistCompanyTest '" + tt["QYName"].ToString() + "','" + tt["mobphone"].ToString() + "','" + userName + "','" + password + "','" + tt["QYCode"].ToString() + "','" + fileUrl + "'," + QyScape);
                }
                else
                {
                    new JH_Auth_UserB().ExsSclarSql("exec RegistCompany '" + tt["QYName"].ToString() + "','" + tt["mobphone"].ToString() + "','" + userName + "','" + password + "','" + qyCode + "','" + fileUrl + "','" + tt["xm"].ToString() + "'," + QyScape);
                }

                new FileHelp().AddQycode(qyCode, tt["QYName"].ToString());
                string content = "您的[" + tt["QYName"].ToString() + "]公司账号已经注册成功：\r\n登录网站：www.qijieyun.com \r\n管理员账号：" + tt["mobphone"].ToString() + "\r\n管理员密码：" + tt["UserPass"].ToString() + "\r\n如有疑问，请访问www.qijieyun.com联系在线客服";
                CommonHelp.SendSMS(tt["mobphone"].ToString(), content, 0);
                string user = context.Request["ID"] ?? "";
            }
        }

        public void CHECKREGISTERPHONE(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            var qy2 = new JH_Auth_QYB().GetEntities(p => p.Mobile == P1.Trim());
            if (qy2.Count() > 0)
            {
                msg.ErrorMsg = "此手机已注册企业，请更换手机号继续注册";
            }
        }
        /// <summary>
        /// 发送手机验证码
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void SENDCHKMSG(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            if (!string.IsNullOrEmpty(P1))
            {
                string code = CommonHelp.numcode(4);
                try
                {
                    string type = context.Request["type"] ?? "";
                    string content = "";
                    switch (type)
                    {
                        case "changeadmin":
                            content = "您更换超级管理员的验证码为：" + code + "，如非本人操作，请忽略本短信";
                            break;
                        default:
                            content = "验证码：" + code + "，如非本人操作，请忽略本短信";
                            break;

                    }

                    CommonHelp.SendSMS(P1, content, 0);
                    msg.Result = code;
                }
                catch
                {
                    msg.ErrorMsg = "发送验证码失败";
                }
            }
        }
        /// <summary>
        /// 验证企业名称
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void YZQYMC(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            if (!string.IsNullOrEmpty(P1))
            {
                var qy = new JH_Auth_QYB().GetEntity(p => p.QYName == P1);
                if (qy != null)
                {
                    msg.ErrorMsg = "企业名称已存在";
                }
            }
        }

        //验证二级域名是否存在
        public void YZQYYM(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            if (!string.IsNullOrEmpty(P1))
            {
                if (P1.ToLower() == "www" || P1.ToLower() == "saas") //www及saas不让用户注册
                {
                    msg.ErrorMsg = "二级域名已存在";
                    return;
                }
                var qy = new JH_Auth_QYB().GetEntity(p => p.QYCode == P1);
                if (qy != null)
                {
                    msg.ErrorMsg = "二级域名已存在";
                }
            }
        }
        #endregion
        #region 用户名,手机号注册赋天账号
        //用户名,手机号注册赋天账号  测试职务/测试部门
        public void REGISTERFULLTEEM(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            JH_Auth_User user = new JH_Auth_User();
            user.UserName = P2; //用户名
            user.telphone = P2; //手机号
            user.mobphone = P2;
            user.IsUse = "Y";
            user.ComId = 10183;
            user.BranchCode = 793;//赋天的测试部门
            user.UserRealName = P1;
            user.UserGW = context.Request["qyname"];
            List<JH_Auth_User> user1 = new JH_Auth_UserB().GetEntities(d => (d.UserName == user.UserName || d.mobphone == user.UserName) && d.ComId == 10183).ToList();
            if (user1.Count > 0)
            {
                msg.ErrorMsg = "您的手机号已注册，请登录";
                return;
            }
            user.UserPass = CommonHelp.GetMD5("123456");
            JH_Auth_UserRole userRole = new JH_Auth_UserRole();
            userRole.UserName = user.UserName;
            userRole.RoleCode = 725;
            userRole.ComId = 10183;
            new JH_Auth_UserRoleB().Insert(userRole);
            if (!new JH_Auth_UserB().Insert(user))
            {
                msg.ErrorMsg = "添加用户失败";
            }
            else
            {
                try
                {

                    JH_Auth_QY QYinfo = new JH_Auth_QYB().GetQYByComID(10183);
                    WXHelp wx = new WXHelp(QYinfo, "");
                    wx.WX_CreateUser(user);
                    msg.Result = user;
                    //string strContent = "您的体验账号已经注册成功：登录网址：fullteem.qijieyun.com\r\n";
                    //strContent += "登录名：" + user.UserName + "\r\n";
                    //strContent += "密码：123456";
                    ////发送短信
                    //CommonHelp.SendSMS(user.UserName, strContent);
                }
                catch (Exception ex)
                {
                    new JH_Auth_UserB().Delete(user);
                    msg.ErrorMsg = "添加用户失败" + ex.Message;
                }
            }
        }
        #endregion

        #region 评论


        /// <summary>
        /// 获取企业信息
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETQYINFO(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string qycode = context.Request["qycode"];
            JH_Auth_QY Qyinfo = new JH_Auth_QYB().GetEntity(d => d.QYCode == qycode);
            if (Qyinfo == null)
            {
                msg.ErrorMsg = "没有找到该企业";
            }
            else
            {
                msg.Result = Qyinfo;
            }
        }
        #endregion

        #region 管理员登录
        public void ADMINLOGIN(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            try
            {
                string password = context.Request["password"];
                string username = context.Request["UserName"];
                password = CommonHelp.GetMD5(password);
                JH_Auth_User userInfo = new JH_Auth_UserB().GetEntity(d => (d.UserName == username || d.mobphone == username) && d.UserPass == password && d.IsUse == "Y" && d.isSupAdmin == "Y");
                if (userInfo == null)
                {
                    msg.ErrorMsg = "用户名或密码不正确";
                }
                else
                {
                    if (string.IsNullOrEmpty(userInfo.pccode))
                    {
                        userInfo.pccode = System.Guid.NewGuid().ToString();
                    }
                    userInfo.logindate = DateTime.Now;
                    new JH_Auth_UserB().Update(userInfo);
                    msg.Result = userInfo.pccode;
                    msg.Result1 = userInfo.UserName;


                    //LOG异步
                    try
                    {
                        Task.Factory.StartNew<string>(() =>
                        {
                            new JH_Auth_LogB().Insert(new JH_Auth_Log()
                            {
                                ComId = userInfo.ComId.ToString(),
                                LogType = "PCADMINLOGIN",
                                LogContent = "用户" + userInfo.UserRealName + "管理员登录，登录时间:" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " 登录地点:" + CommonHelp.getIpAddr(),
                                CRUser = userInfo.UserName,
                                CRDate = DateTime.Now
                            });
                            return "";
                        });


                    }
                    catch { }
                }
            }
            catch (Exception ex)
            {
                msg.ErrorMsg = "登录失败，请重试";
            }
        }
        #endregion

        #region 找回密码
        public void CHECKPHONE(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            string ComId = context.Request["comId"];
            int id = 0;
            int.TryParse(ComId, out id);
            JH_Auth_User user = new JH_Auth_UserB().GetEntity(d => d.mobphone == P2 && d.ComId == id);
            if (user == null)
            {
                msg.ErrorMsg = "此手机号无效";
            }
        }
        //找回密码验证二级域名

        public void FINDPASSWORD(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string userpass = P2;
            JH_Auth_User userInfo = new JH_Auth_UserB().GetEntities(d => d.mobphone == P1 && d.ComId == 10312).LastOrDefault();
            if (userInfo == null)
            {
                msg.ErrorMsg = "没有找到该用户";
            }
            else
            {
                userInfo.UserPass = CommonHelp.GetMD5(userpass);
                new JH_Auth_UserB().Update(userInfo);

            }


        }
        #endregion


        #region 国电培训登录多个验证码参数
        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1">用户名</param>
        /// <param name="P2">密码（未解码的）</param>
        /// <param name="UserInfo"></param>
        public void GDPXLOGIN(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            if (context.Session["loginchkcode"] != null)
            {
                string code = context.Session["loginchkcode"].ToString();
                if (code.ToLower() != P1.ToLower())
                {
                    msg.ErrorMsg = "5";
                    context.Session.Remove("loginchkcode");
                    return;
                }
            }
            else
            {
                msg.ErrorMsg = "5";
                return;
            }

            string password = context.Request["password"];
            string username = context.Request["UserName"];
            string ComCode = context.Request["ComId"] ?? "";
            JH_Auth_QY qyModel = new JH_Auth_QY();
            int intComID = 0;
            int.TryParse(ComCode, out intComID);
            password = CommonHelp.GetMD5(password);
            JH_Auth_User userInfo = new JH_Auth_User();
            if (intComID == 0)
            {
                List<JH_Auth_User> userList = new JH_Auth_UserB().GetEntities(d => (d.UserName == username || d.mobphone == username) && d.UserPass == password && d.IsUse == "Y" && d.isSupAdmin != "Y").ToList();
                if (userList.Count() == 0)
                {
                    msg.ErrorMsg = "用户名或密码不正确";
                    return;
                }
                else if (userList.Count() > 1)
                {
                    msg.ErrorMsg = "-1";//用户名密码获取用户不止一个时，提示输入域名
                    return;
                }
                else
                    userInfo = userList[0];
            }
            else
            {
                userInfo = new JH_Auth_UserB().GetEntity(d => (d.UserName == username || d.mobphone == username) && d.UserPass == password && d.IsUse == "Y" && d.ComId == intComID && d.isSupAdmin != "Y");
            }
            if (userInfo == null)
            {
                msg.ErrorMsg = "用户名或密码不正确";
            }
            else
            {
                if (string.IsNullOrEmpty(userInfo.pccode))
                {
                    userInfo.pccode = System.Guid.NewGuid().ToString();
                }
                userInfo.logindate = DateTime.Now;
                new JH_Auth_UserB().Update(userInfo);
                msg.Result = userInfo.pccode;
                msg.Result1 = userInfo.UserName;

                //LOG
                try
                {
                    new JH_Auth_LogB().Insert(new JH_Auth_Log()
                    {
                        ComId = intComID.ToString(),
                        LogType = "PCLOGIN",
                        LogContent = "用户" + userInfo.UserRealName + "登录，登录时间:" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                        CRUser = userInfo.UserName,
                        CRDate = DateTime.Now
                    });
                }
                catch { }

            }
        }
        #endregion


        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1">用户名</param>
        /// <param name="P2">密码（未解码的）</param>
        /// <param name="UserInfo"></param>
        public void GDPXLOGINNEW(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            if (context.Session["loginchkcode"] != null)
            {
                string code = context.Session["loginchkcode"].ToString();
                if (code.ToLower() != P1.ToLower())
                {
                    msg.ErrorMsg = "5";
                    context.Session.Remove("loginchkcode");
                    return;
                }
            }
            else
            {
                msg.ErrorMsg = "5";
                return;
            }

            string password = context.Request["password"];
            string username = context.Request["UserName"];
            string ComCode = context.Request["ComId"] ?? "";
            JH_Auth_QY qyModel = new JH_Auth_QY();
            int intComID = 0;
            int.TryParse(ComCode, out intComID);
            password = CommonHelp.GetMD5(password);
            JH_Auth_User userInfo = new JH_Auth_User();
            if (intComID == 0)
            {
                List<JH_Auth_User> userList = new JH_Auth_UserB().GetEntities(d => (d.UserName == username || d.mobphone == username) && d.UserPass == password && d.IsUse == "Y" && d.isSupAdmin != "Y").ToList();
                if (userList.Count() == 0)
                {
                    msg.ErrorMsg = "用户名或密码不正确";
                    return;
                }
                else if (userList.Count() > 1)
                {
                    msg.ErrorMsg = "-1";//用户名密码获取用户不止一个时，提示输入域名
                    return;
                }
                else
                    userInfo = userList[0];
            }
            else
            {
                userInfo = new JH_Auth_UserB().GetEntity(d => (d.UserName == username || d.mobphone == username) && d.UserPass == password && d.IsUse == "Y" && d.ComId == intComID && d.isSupAdmin != "Y");



            }
            if (userInfo == null)
            {
                msg.ErrorMsg = "用户名或密码不正确";
            }
            else
            {

                DataTable dt = new JH_Auth_BranchB().ISSUPADMIN(userInfo.UserName);
                string strRoleCode = new JH_Auth_UserRoleB().GetRoleCodeByUserName(userInfo.UserName, userInfo.ComId.Value);
                if (dt.Rows.Count == 0 && !strRoleCode.Split(',').Contains("1143"))
                {
                    msg.ErrorMsg = "您不是管理员或者阅卷老师,无法登陆后台";
                    return;
                }
                if (string.IsNullOrEmpty(userInfo.pccode))
                {
                    userInfo.pccode = System.Guid.NewGuid().ToString();
                }
                userInfo.logindate = DateTime.Now;
                new JH_Auth_UserB().Update(userInfo);
                msg.Result = userInfo.pccode;
                msg.Result1 = userInfo.UserName;

                //LOG
                try
                {
                    new JH_Auth_LogB().Insert(new JH_Auth_Log()
                    {
                        ComId = intComID.ToString(),
                        LogType = "PCLOGIN",
                        LogContent = "用户" + userInfo.UserRealName + "登录，登录时间:" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                        CRUser = userInfo.UserName,
                        CRDate = DateTime.Now
                    });
                }
                catch { }

            }
        }




        /// <summary>
        /// 获取转向URL
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETWXRETURNURL(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string wxappid = CommonHelp.GetConfig("wxappid");
            var state = "pxpt-" + DateTime.Now.Millisecond;//随机数，用于识别请求可靠性
            string strReturnUrl = "http://www.ycpxxt.com/login.html";
            string strUrl = OAuthApi.GetAuthorizeUrl(wxappid, strReturnUrl.UrlEncode(), state, OAuthScope.snsapi_userinfo);
            msg.Result = strUrl;
            CommonHelp.WriteLOG(strUrl);
        }

        public void GETWXUSERINFO(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string wxappid = CommonHelp.GetConfig("wxappid");
            string wxsecret = CommonHelp.GetConfig("wxappsecret");
            string code = context.Request["code"] ?? "";
            OAuthAccessTokenResult result = null;

            //通过，用code换取access_token
            try
            {
                result = OAuthApi.GetAccessToken(wxappid, wxsecret, code);
            }
            catch (Exception ex)
            {
                msg.ErrorMsg = ex.Message;
            }
            if (result.errcode != ReturnCode.请求成功)
            {
                msg.ErrorMsg = result.errmsg;
            }
            OAuthUserInfo userInfo = OAuthApi.GetUserInfo(result.access_token, result.openid);
            msg.Result = userInfo;

            var model = new JH_Auth_UserB().GetEntities(d => d.openid == userInfo.openid);
            msg.Result1 = model.Count() > 0 ? "Y" : "N";


        }

        /// <summary>
        /// 判断是否绑定用户
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void ISBDWX(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string wxid = CommonHelp.GetConfig("openid");

            var model = new JH_Auth_UserB().GetEntities(d => d.openid == wxid);
            if (model.Count() > 0)
            {
                msg.Result = model.FirstOrDefault();
            }
        }


        public void BDWX(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string openid = context.Request["openid"] ?? "";
            string password = CommonHelp.GetMD5(P2);

            JH_Auth_User userInfo = new JH_Auth_UserB().GetEntity(d => (d.UserName == P1 || d.mobphone == P1) && d.UserPass == password && d.IsUse == "Y" && d.ComId == 10312 && d.isSupAdmin != "Y");
            if (userInfo == null)
            {
                msg.ErrorMsg = "无法绑定用户,请检查用户名密码是否正确";
            }
            else
            {
                userInfo.openid = openid;
                new JH_Auth_UserB().Update(userInfo);
            }
        }
    }
}