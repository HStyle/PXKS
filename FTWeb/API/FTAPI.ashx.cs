﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Practices.Unity;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;
using System.Web.SessionState;
using QjySaaSWeb.AppCode;
using QJY.Data;

namespace QjySaaSWeb.API
{
    /// <summary>
    /// FTAPI 的摘要说明
    /// </summary>
    public class FTAPI : IHttpHandler, IRequiresSessionState
    {
        public string ComId { get; set; }

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            context.Response.AddHeader("pragma", "no-cache");
            context.Response.AddHeader("cache-control", "");
            context.Response.CacheControl = "no-cache";
            string strAction = context.Request["Action"] ?? "";
            string P1 = context.Request["P1"] ?? "";
            string P2 = context.Request["P2"] ?? "";
            string P3 = context.Request["P3"] ?? "";
            Msg_Result Model = new Msg_Result() { Action = strAction.ToUpper(), ErrorMsg = "" };
            if (!string.IsNullOrEmpty(strAction))
            {
                try
                {
                    //TODO: 未实现,分享页面的接口通过暂时将分享人的code传递给打开链接的人来调用接口(不安全)
                    #region 必须登录执行接口
                    Model.ErrorMsg = "";
                    if (context.Request.Cookies["szhlcode"] != null)//如果存在TOKEN,根据TOKEN找到用户信息，并根据权限执行具体ACTION
                    {
                        //TO DO  判断有没有管理员角色
                        string strSZHLCode = context.Request.Cookies["szhlcode"].Value;
                        //通过Code获取用户名，然后执行接口方法

                        var container = ServiceContainerV.Current().Resolve<IWsService>(strAction.ToUpper());
                        JH_Auth_UserB.UserInfo UserInfo = new JH_Auth_UserB().GetUserInfo(strSZHLCode);
                        if (UserInfo != null)
                        {
                            container.ProcessRequest(context, ref Model, P1, P2, UserInfo);
                        }
                        else
                        {
                            Model.ErrorMsg = "NOSESSIONCODE";
                        }

                    }

                    #endregion

                }
                catch (Exception ex)
                {
                    Model.ErrorMsg = "操作异常，请重试" + strAction;
                    Model.Result = ex.Message;
                }
            }
            IsoDateTimeConverter timeConverter = new IsoDateTimeConverter();
            timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";
            string Result = JsonConvert.SerializeObject(Model, Formatting.Indented, timeConverter).Replace("null", "\"\"");
            context.Response.Write(Result);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}