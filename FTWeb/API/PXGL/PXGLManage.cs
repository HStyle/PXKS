﻿using QjySaaSWeb.AppCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using FastReflectionLib;
using System.Web;
using QJY.Data;
using Newtonsoft.Json;
using System.Data;
using System.Net;
using System.Net.Sockets;
using System.Configuration;
using System.Text;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;

namespace QjySaaSWeb.API
{
    public class PXGLManage : IWsService
    {

        public void ProcessRequest(HttpContext context, ref Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            MethodInfo methodInfo = typeof(PXGLManage).GetMethod(msg.Action.ToUpper());
            PXGLManage model = new PXGLManage();
            methodInfo.FastInvoke(model, new object[] { context, msg, P1, P2, UserInfo });
        }

        #region 课程分类

        public void GETKCFLLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            msg.Result = new SZHL_PX_KCFLB().GetEntities(d => d.ComId == UserInfo.User.ComId);
        }
        /// <summary>
        /// 添加记事本
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="strUserName"></param>
        public void ADDKCFL(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            SZHL_PX_KCFL kcfl = JsonConvert.DeserializeObject<SZHL_PX_KCFL>(P1);

            if (kcfl.KCFLName == null)
            {
                msg.ErrorMsg = "课程分类内容不能为空";
                return;
            }


            if (kcfl.ID == 0)
            {
                kcfl.CRDate = DateTime.Now;
                kcfl.CRUser = UserInfo.User.UserName;
                kcfl.ComId = UserInfo.User.ComId;
                new SZHL_PX_KCFLB().Insert(kcfl);

            }
            else
            {
                new SZHL_PX_KCFLB().Update(kcfl);
            }

            msg.Result = kcfl;
        }
        //删除课程分类
        public void DELKCFLBYID(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            try
            {
                if (new SZHL_PX_KCFLB().Delete(d => d.ID.ToString() == P1))
                {
                    msg.ErrorMsg = "";
                }
            }
            catch (Exception ex)
            {
                msg.ErrorMsg = ex.Message;
            }
        }

        #endregion

        #region 课件制作
        public void GETPXKJLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            int page = 0;
            int.TryParse(context.Request.QueryString["p"] ?? "1", out page);//页码
            page = page == 0 ? 1 : page;
            int recordCount = 0;
            string strWhere = string.Format(" ComId={0} ", UserInfo.User.ComId);
            if (P1 != "")
            {
                strWhere += string.Format(" and KJZY={0}", P1);
            }
            if (P2 != "")
            {
                strWhere += string.Format(" and KJZZType={0}", P2);
            }
            string content = context.Request["Content"] ?? "";
            if (content != "")
            {
                strWhere += string.Format(" and KJName like '%{0}%'", content);
            }
            DataTable dt = new SZHL_GZBGB().GetDataPager(" SZHL_PX_KJGL", "*  ", 100, page, "CRDate desc", strWhere, ref recordCount);
            foreach (DataRow row in dt.Rows)
            {
                row["CW"] = row["CW"].ToString().Replace("null", "\"\"");
                if (row["KJZZType"].ToString() == "2")
                {
                    int fileId = int.Parse(row["Files"].ToString());
                    FT_File file = new FT_FileB().GetEntity(d => d.ID == fileId);
                    DateTime time = DateTime.Parse(row["CRDate"].ToString());
                    row["FilePath"] = "http://office.qijieyun.com/" + UserInfo.QYinfo.QYCode + "/" + time.Year + "/" + time.Month + "/" + file.FileMD5 + "/" + row["FilePath"];
                }
            }
            msg.Result = dt;
            msg.Result1 = recordCount;
        }
        //课程选择课件
        public void GETKCPXKJLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            msg.Result = new SZHL_PX_KJGLB().GetDTByCommand("SELECT  ID,KJName,KJDec from SZHL_PX_KJGL where ComId=" + UserInfo.User.ComId);

        }
        public void GETKJGLMODEL(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            try
            {
                SZHL_PX_KJGLB kjglb = new SZHL_PX_KJGLB();

                int Id = 0;
                int.TryParse(P1, out Id);
                SZHL_PX_KJGL kjglModel = kjglb.GetEntity(d => d.ID == Id && d.ComId == UserInfo.User.ComId);
                if (P2 == "Y")//编辑或查看课件
                {
                    if (kjglModel.CW == null || string.IsNullOrEmpty(kjglModel.CW))
                    {
                        kjglModel.CW = createCW(kjglModel, UserInfo);
                        kjglb.Update(kjglModel);
                    }
                }
                else
                {
                    kjglModel.CW = "Y";//前台转json问题
                }



                if (kjglModel != null && !string.IsNullOrEmpty(kjglModel.Files))
                {
                    List<FT_File> files = new FT_FileB().GetEntities(" ID in (" + kjglModel.Files + ")").ToList();
                    if (kjglModel.KJZZType == 2)
                    {
                        FT_File file = files[0];
                        DateTime time = kjglModel.CRDate.Value;
                        msg.Result4 = "http://office.qijieyun.com/" + UserInfo.QYinfo.QYCode + "/" + time.Year + "/" + time.Month + "/" + file.FileMD5 + "/" + kjglModel.FilePath;
                    }
                    msg.Result1 = files;

                    var p = files.Where(d => d.FileExtendName.ToLower() == "mp4");
                    FT_File filem = new FT_File();
                    if (p.Count() > 0)
                    {
                        //判断视频同步到保利威视的状态
                        filem = p.ToList()[0];
                        msg.Result3 = filem.TBStatus;
                    }
                    if (kjglModel.KJZZType == 1 && filem.TBStatus == "2") //自定义课件保利威视同步成功后才能制作课件及预览
                    {
                        msg.Result2 = "自定义";
                    }
                    else if (kjglModel.KJZZType == 2) //课件压缩包直接可以预览
                    {
                        msg.Result2 = "压缩包";
                    }
                }
                msg.Result = kjglModel;
                if (kjglModel.FolderID != 0)
                {
                    FT_Folder folder = new FT_FolderB().GetEntity(d => d.ID == kjglModel.FolderID);
                    msg.Result4 = folder != null ? folder.Name : "";
                }
            }
            catch (Exception ex)
            {
                msg.ErrorMsg = "请检查课件文件";
            }
        }
        public void ADDPXGL(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            SZHL_PX_KJGL kjgl = JsonConvert.DeserializeObject<SZHL_PX_KJGL>(P1);

            if (string.IsNullOrEmpty(kjgl.KJName))
            {
                msg.ErrorMsg = "课件名称不能为空";
                return;
            }
            if (string.IsNullOrEmpty(kjgl.Files))
            {
                msg.ErrorMsg = "课件必须有课件文件";
                return;
            }
            if (kjgl.KJZZType == 1)
            {
                int[] fileIds = kjgl.Files.SplitTOInt(',');
                string hzm = new FT_FileB().GetEntities(d => fileIds.Contains(d.ID)).Select(d => d.FileExtendName).ToList().ListTOString(',');
                if (kjgl.KJType == "1")
                {
                    if (hzm.ToUpper().IndexOf("MP4") < 0 || !(hzm.ToUpper().IndexOf("PPT") > -1 || hzm.ToUpper().IndexOf("PPTX") > -1))
                    {
                        msg.ErrorMsg = "三分屏课件必须有文档(PPT)和视频(MP4)";
                        return;
                    }
                }
                else if (kjgl.KJType == "2")
                {
                    if (hzm.ToUpper().IndexOf("MP4") < 0)
                    {
                        msg.ErrorMsg = "二分屏课件必须有视频(MP4)";
                        return;
                    }
                }
            }
            if (kjgl.ID == 0)
            {
                kjgl.CRDate = DateTime.Now;
                kjgl.CRUser = UserInfo.User.UserName;
                kjgl.ComId = UserInfo.User.ComId;

                new SZHL_PX_KJGLB().Insert(kjgl);
                //课件压缩包转移课件文件夹
                if (kjgl.KJZZType == 2)
                {
                    string strWDIP = ConfigurationManager.AppSettings["WDIP"] ?? "";
                    string strWDDK = ConfigurationManager.AppSettings["WDDK"] ?? "";
                    int fileId = 0;
                    int.TryParse(kjgl.Files.Split(',')[0], out fileId);
                    FT_File file = new FT_FileB().GetEntity(d => d.ID == fileId && d.ComId == UserInfo.User.ComId);
                    int dk = Int32.Parse(strWDDK);
                    IPAddress ip = IPAddress.Parse(strWDIP);
                    Socket clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    clientSocket.Connect(new IPEndPoint(ip, dk)); //配置服务器IP与端口 
                    clientSocket.Send(Encoding.ASCII.GetBytes(file.FileMD5 + "_" + UserInfo.QYinfo.QYCode + "_" + DateTime.Now.Year + "_" + DateTime.Now.Month + "_flash"));//发送请求到服务器
                }
            }
            else
            {
                SZHL_PX_KJGL kjgl1 = new SZHL_PX_KJGLB().GetEntity(d => d.ID == kjgl.ID);
                kjgl.CW = kjgl1.CW;
                new SZHL_PX_KJGLB().Update(kjgl);
                //自定义课件及三分屏课件同步保利威视
                if (kjgl.KJZZType == 1)
                {
                    P1 = kjgl.ID.ToString();
                    SYNCTOBLWS(context, msg, P1, "N", UserInfo);
                }
            }

            msg.Result = kjgl;
            kjgl.CW = "Y";//前台转json问题
        }
        public void UPDATECW(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = 0;
            int.TryParse(P1, out Id);
            SZHL_PX_KJGL kjgl = new SZHL_PX_KJGLB().GetEntity(d => d.ID == Id && d.ComId == UserInfo.User.ComId);

            kjgl.CW = P2;

            new SZHL_PX_KJGLB().Update(kjgl);

            msg.Result = kjgl;
        }
        //删除课件
        public void DELKJGLBYID(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            try
            {
                if (new SZHL_PX_KJGLB().Delete(d => d.ID.ToString() == P1))
                {
                    msg.ErrorMsg = "";
                }
            }
            catch (Exception ex)
            {
                msg.ErrorMsg = ex.Message;
            }
        }
        /// <summary>
        /// 生成课件信息
        /// </summary>
        /// <param name="kjgl"></param>
        /// <param name="UserInfo"></param>
        /// <returns></returns>
        private string createCW(SZHL_PX_KJGL kjgl, JH_Auth_UserB.UserInfo UserInfo)
        {
            if (kjgl.KJZZType == 1)
            {
                //获取视频和文件根目录
                string fileBasePath = ConfigurationManager.AppSettings["WDYM"];
                JH_Auth_QY qyinfo = new JH_Auth_QYB().GetEntity(d => d.ComId == UserInfo.QYinfo.ComId);
                string videoBasePath = qyinfo.FileServerUrl;


                //根据files查询上传文件内容并整合进cw
                List<CWVideoEntity> cwVideoList = new List<CWVideoEntity>();
                List<CWFileEntity> cwFileList = new List<CWFileEntity>();
                List<CWMenuEntity> cwMenuList = new List<CWMenuEntity>();
                //添加菜单根目录
                cwMenuList.Add(new CWMenuEntity
                {
                    ix = "root_",
                    name = "目录",
                    rended = true
                });

                List<string> docTypeList = new List<string> { "doc", "docx", "ppt", "pptx" };

                IEnumerable<FT_File> fileList = new FT_FileB().GetEntities(" ID in (" + kjgl.Files + ")");
                if (fileList != null && fileList.Count() > 0)
                {
                    foreach (FT_File file in fileList)
                    {
                        if (file.FileExtendName.ToLower() == "mp4") //视频
                        {
                            cwVideoList.Add(new CWVideoEntity
                            {
                                url = videoBasePath + file.FileMD5,
                                name = file.Name + "." + file.FileExtendName,
                                vid = file.YLCode
                            });
                        }
                        else if (docTypeList.Contains(file.FileExtendName.ToLower())) //文档
                        {
                            int pageCount = 0;
                            int.TryParse(file.YLCount, out pageCount);
                            CWFileEntity cwFile = new CWFileEntity
                            {
                                file = file.Name,
                                dn = (file.FileExtendName == "doc" || file.FileExtendName == "docx") ? file.YLCode : (fileBasePath + file.YLPath),
                                type = (file.FileExtendName == "doc" || file.FileExtendName == "docx") ? "doc" : "ppt",
                                pages = pageCount
                            };

                            //添加文件
                            cwFileList.Add(cwFile);

                            //添加菜单
                            cwMenuList.Add(new CWMenuEntity
                            {
                                ix = "root_" + file.Name + "_",
                                name = file.Name,
                                rended = true
                            });

                            //逐个添加下级菜单
                            for (int i = 1; i <= pageCount; i++)
                            {
                                cwMenuList.Add(new CWMenuEntity
                                {
                                    ix = "root_" + file.Name + "_" + i + "_",
                                    name = "第" + i + "页",
                                    type = cwFile.type,
                                    rended = true,
                                    file = cwFile,
                                    pos = i.ToString()
                                });
                            }
                        }
                    }
                }


                CWEntity cw = new CWEntity
                {
                    title = kjgl.KJName,
                    dir = string.Empty,
                    videosAttr = new List<CWVideoAttrEntity>(),
                    screen = kjgl.KJType == "1" ? 3 : 2,
                    link = "",
                    desc = "",
                    videos = cwVideoList,
                    files = cwFileList,
                    menu = cwMenuList
                };

                return JsonConvert.SerializeObject(cw);
            }

            return null;
        }

        /// <summary>
        /// 将课件中的视频文件同步到保利威视
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void SYNCTOBLWS(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            Task<string> taskSync = Task.Factory.StartNew<string>(() =>
            {
                try
                {
                    SZHL_PX_KJGLB kjglb = new SZHL_PX_KJGLB();
                    FT_FileB ftFileb = new FT_FileB();

                    //查询企业信息
                    JH_Auth_QY qyinfo = new JH_Auth_QYB().GetEntity(d => d.ComId == UserInfo.QYinfo.ComId);
                    string videoBasePath = qyinfo.FileServerUrl;

                    //查询课件信息
                    int Id = 0;
                    int.TryParse(P1, out Id);
                    SZHL_PX_KJGL kjgl = kjglb.GetEntity(d => d.ID == Id && d.ComId == UserInfo.User.ComId);

                    //查询文件信息
                    IEnumerable<FT_File> fileList = ftFileb.GetEntities(" ID in (" + kjgl.Files + ")");

                    //逐个上传
                    bool isSynctSuccess = true;

                    Dictionary<string, string> dictFileYLCode = new Dictionary<string, string>();
                    int totalTime = kjgl.TotalTime ?? 1;
                    if (P2 != "N")
                    {
                        foreach (FT_File file in fileList)
                        {
                            if (file.FileExtendName.ToLower() == "mp4")
                            {
                                if (string.IsNullOrEmpty(file.YLCode))
                                {
                                    int videoTime;
                                    string vid = synctFileToBlws(UserInfo, ftFileb, file, videoBasePath, out videoTime);

                                    if (vid == null)
                                    {
                                        isSynctSuccess = false;
                                        break;
                                    }
                                    dictFileYLCode.Add(videoBasePath + file.FileMD5, vid);
                                    totalTime += videoTime;
                                }
                                else
                                {
                                    dictFileYLCode.Add(videoBasePath + file.FileMD5, file.YLCode);
                                    if (file.TotalTime != null)
                                    {
                                        totalTime += file.TotalTime.Value;
                                    }
                                }
                            }
                        }
                    }


                    //如果上传成功
                    if (isSynctSuccess)
                    {
                        //如果没有CW信息，直接生成
                        if (string.IsNullOrEmpty(kjgl.CW))
                        {
                            kjgl.CW = createCW(kjgl, UserInfo);
                        }
                        else
                        {
                            //获取CW信息，更新video内容
                            CWEntity cwEntity = JsonConvert.DeserializeObject<CWEntity>(kjgl.CW);

                            foreach (CWVideoEntity videoEntity in cwEntity.videos)
                            {
                                videoEntity.vid = dictFileYLCode[videoEntity.url];
                            }

                            //更新课件信息到数据库
                            kjgl.CW = JsonConvert.SerializeObject(cwEntity);
                        }
                        kjgl.TotalTime = totalTime;
                        kjglb.Update(kjgl);
                    }

                    return null;
                }
                catch (Exception ex)
                {
                    CommonHelp.WriteLOG(ex.Message);
                    return ex.Message;
                }
            });

            taskSync.ContinueWith((task) =>
            {
                try
                {
                    if (taskSync.Result != null)
                    {
                        CommonHelp.WriteLOG("同步视频到保利威视失败：" + taskSync.Result);
                    }
                }
                catch (Exception ex)
                {
                    CommonHelp.WriteLOG(ex.Message);
                    CommonHelp.WriteLOG(ex.StackTrace);
                }
            });
        }

        /// <summary>
        /// 将远程文件同步到保利威视
        /// </summary>
        /// <param name="ftFileb"></param>
        /// <param name="file"></param>
        /// <param name="videoBasePath"></param>
        /// <returns></returns>
        private string synctFileToBlws(JH_Auth_UserB.UserInfo UserInfo, FT_FileB ftFileb, FT_File file, string videoBasePath, out int videoTime)
        {
            videoTime = 0;
            //标记为开始同步
            file.TBStatus = "1";
            ftFileb.Update(file);

            //视频地址
            string fileUrl = videoBasePath + file.FileMD5;
            string uploadUrlFileUrl = ConfigurationManager.AppSettings["blws-uploadUrlFile-url"];
            string blwsWritetoken = ConfigurationManager.AppSettings["blws-writetoken"];
            string blwsDir = ConfigurationManager.AppSettings["blws-dir"];

            //同步到保利威视
            string param = "?method=uploadUrlFile";
            param += "&title=" + file.Name;
            param += "&writetoken=" + blwsWritetoken;
            param += "&fileUrl=" + fileUrl;
            param += "&cataid=" + blwsDir;
            param += "&tag=" + UserInfo.QYinfo.QYCode;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uploadUrlFileUrl + param);
            request.Method = "GET";
            request.KeepAlive = true;
            request.Timeout = 6000000;
            request.ContentType = "application/json";

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            StreamReader streamReader = new StreamReader(response.GetResponseStream());
            string responseContent = streamReader.ReadToEnd();

            response.Close();
            streamReader.Close();

            //解析返回结果，获取vid
            JObject jo = JObject.Parse(responseContent);

            string vid;
            string duration;
            if (jo["error"].ToString() == "0")  //同步成功
            {
                JObject videoJson = (JObject)((JArray)jo["data"])[0];
                vid = videoJson["vid"].ToString();
                duration = videoJson["duration"].ToString();
            }
            else  //同步失败
            {
                file.TBStatus = "-1";
                ftFileb.Update(file);
                return null;
            }

            #region 结果
            /*
             * 
{
    "error": "0",
    "data": [
        {
            "images_b": [
                "http://img.videocc.net/uimage/3/33074c0252/2/33074c025293590ea91fac0efec3a2c2_0_b.jpg",
                "http://img.videocc.net/uimage/3/33074c0252/2/33074c025293590ea91fac0efec3a2c2_1_b.jpg",
                "http://img.videocc.net/uimage/3/33074c0252/2/33074c025293590ea91fac0efec3a2c2_2_b.jpg",
                "http://img.videocc.net/uimage/3/33074c0252/2/33074c025293590ea91fac0efec3a2c2_3_b.jpg",
                "http://img.videocc.net/uimage/3/33074c0252/2/33074c025293590ea91fac0efec3a2c2_4_b.jpg",
                "http://img.videocc.net/uimage/3/33074c0252/2/33074c025293590ea91fac0efec3a2c2_5_b.jpg"
            ],
            "md5checksum": "5c5b3c7c0643d5105efd1cc01eb48008",
            "tag": "",
            "mp4": "http://mpv.videocc.net/33074c0252/2/33074c025293590ea91fac0efec3a2c2_1.mp4",
            "title": "title",
            "df": 1,
            "times": "0",
            "mp4_1": "http://mpv.videocc.net/33074c0252/2/33074c025293590ea91fac0efec3a2c2_1.mp4",
            "vid": "33074c025293590ea91fac0efec3a2c2_3",
            "cataid": "1",
            "swf_link": "http://player.polyv.net/videos/33074c025293590ea91fac0efec3a2c2_3.swf",
            "source_filesize": 2461981,
            "status": "10",
            "seed": 0,
            "flv1": "http://plvod01.videocc.net/33074c0252/2/33074c025293590ea91fac0efec3a2c2_1.flv",
            "sourcefile": "",
            "playerwidth": "",
            "hls": [
                "http://hls.videocc.net/33074c0252/3/33074c025293590ea91fac0efec3a2c2_1.m3u8"
            ],
            "default_video": "http://plvod01.videocc.net/33074c0252/2/33074c025293590ea91fac0efec3a2c2_1.flv",
            "duration": "00:01:59",
            "filesize": [
                0
            ],
            "first_image": "http://img.videocc.net/uimage/3/33074c0252/2/33074c025293590ea91fac0efec3a2c2_0.jpg",
            "original_definition": "480x424",
            "context": "",
            "images": [
                "http://img.videocc.net/uimage/3/33074c0252/2/33074c025293590ea91fac0efec3a2c2_0.jpg",
                "http://img.videocc.net/uimage/3/33074c0252/2/33074c025293590ea91fac0efec3a2c2_1.jpg",
                "http://img.videocc.net/uimage/3/33074c0252/2/33074c025293590ea91fac0efec3a2c2_2.jpg",
                "http://img.videocc.net/uimage/3/33074c0252/2/33074c025293590ea91fac0efec3a2c2_3.jpg",
                "http://img.videocc.net/uimage/3/33074c0252/2/33074c025293590ea91fac0efec3a2c2_4.jpg",
                "http://img.videocc.net/uimage/3/33074c0252/2/33074c025293590ea91fac0efec3a2c2_5.jpg"
            ],
            "playerheight": "",
            "ptime": "2016-06-21 18:23:21"
        }
    ]
}
             */
            #endregion

            //更新YLCode
            file.TBStatus = "2";
            file.YLCode = vid;
            file.TotalTime = videoTime = getVideoTime(duration);
            ftFileb.Update(file);

            return vid;
        }

        /// <summary>
        /// 将保利威视返回的时间字符串转换成秒
        /// </summary>
        /// <param name="duration"></param>
        /// <returns></returns>
        private int getVideoTime(string duration)
        {
            int time = 0;

            if (string.IsNullOrEmpty(duration))
                return time;

            List<string> timeArray = duration.Split(':').ToList<string>();

            int tmp;
            if (timeArray.Count == 3)
            {
                if (int.TryParse(timeArray[0], out tmp))
                {
                    time += tmp * 60;
                }
                timeArray.RemoveAt(0);
            }
            if (timeArray.Count == 2)
            {
                if (int.TryParse(timeArray[0], out tmp))
                {
                    time += tmp * 60;
                }
                timeArray.RemoveAt(0);
            }
            if (timeArray.Count == 1)
            {
                if (int.TryParse(timeArray[0], out tmp))
                {
                    time += tmp;
                }
            }

            return time;
        }
        #endregion

        #region 课程管理
        public void GETKCGLLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int page = 0;
            int.TryParse(context.Request.QueryString["p"] ?? "1", out page);//页码
            page = page == 0 ? 1 : page;
            int recordCount = 0;
            string strWhere = string.Format(" kcgl.ComId={0}", UserInfo.User.ComId);
            if (P1 != "")
            {
                strWhere += string.Format(" and kcgl.KCTypeID={0}", P1);
            }
            if (P2 != "")
            {
                strWhere += string.Format(" and kcgl.ZSKID={0}", P2);
            }
            string content = context.Request["Content"] ?? "";
            if (content != "")
            {
                strWhere += string.Format(" and kcgl.KCName like '%{0}%'", content);
            }
            DataTable dt = new SZHL_GZBGB().GetDataPager("  SZHL_PX_KCGL kcgl inner join  SZHL_PX_KCFL kcfl on kcfl.ID=kcgl.KCTypeID LEFT JOIN  SZHL_KS_SJ sj on kcgl.ID=sj.kcId ", " kcgl.*,kcfl.KCFLName,sj.ID SJID,sj.Status SJStatus  ", 100, page, "kcgl.CRDate desc", strWhere, ref recordCount);

            dt.Columns.Add("fileurl");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["KCFM"] != null && dt.Rows[i]["KCFM"].ToString() != "")
                {
                    int FIELID = int.Parse(dt.Rows[i]["KCFM"].ToString().Split(',')[0]);
                    FT_File file = new FT_FileB().GetEntities(d => d.ID == FIELID).FirstOrDefault();
                    dt.Rows[i]["fileurl"] = UserInfo.QYinfo.FileServerUrl + file.FileMD5;
                }
                else
                {
                    dt.Rows[i]["fileurl"] = "images/a_27.jpg";
                }
            }

            msg.Result = dt;
            msg.Result1 = recordCount;
        }



        /// <summary>
        /// 前台专用
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETKCGLLISTQT(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int page = 0;
            int.TryParse(context.Request.QueryString["p"] ?? "1", out page);//页码
            page = page == 0 ? 1 : page;
            int recordCount = 0;
            string strWhere = string.Format(" kcgl.ComId={0}  and kcgl.SQType='公开' ", UserInfo.User.ComId);
            if (P1 != "")
            {
                strWhere += string.Format(" and kcgl.KCTypeID={0}", P1);
            }
            if (P2 != "")
            {
                strWhere += string.Format(" and kcgl.ZSKID={0}", P2);
            }
            string content = context.Request["Content"] ?? "";
            if (content != "")
            {
                strWhere += string.Format(" and kcgl.KCName like '%{0}%'", content);
            }
            DataTable dt = new SZHL_GZBGB().GetDataPager("  SZHL_PX_KCGL kcgl inner join  SZHL_PX_KCFL kcfl on kcfl.ID=kcgl.KCTypeID LEFT JOIN  SZHL_KS_SJ sj on kcgl.ID=sj.kcId ", " kcgl.*,kcfl.KCFLName,sj.ID SJID,sj.Status SJStatus  ", 100, page, "kcgl.CRDate desc", strWhere, ref recordCount);

            dt.Columns.Add("fileurl");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["KCFM"] != null && dt.Rows[i]["KCFM"].ToString() != "")
                {
                    int FIELID = int.Parse(dt.Rows[i]["KCFM"].ToString().Split(',')[0]);
                    FT_File file = new FT_FileB().GetEntities(d => d.ID == FIELID).FirstOrDefault();
                    dt.Rows[i]["fileurl"] = UserInfo.QYinfo.FileServerUrl + file.FileMD5;
                }
                else
                {
                    dt.Rows[i]["fileurl"] = "images/a_27.jpg";
                }
            }

            msg.Result = dt;
            msg.Result1 = recordCount;
        }


        public void GETKCGLMODEL(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = 0;
            int.TryParse(P1, out Id);
            SZHL_PX_KCGL kcglModel = new SZHL_PX_KCGLB().GetEntity(d => d.ID == Id && d.ComId == UserInfo.User.ComId);
            msg.Result = kcglModel;
            string fl = new SZHL_PX_KCFLB().GetEntity(d => d.ID == kcglModel.KCTypeID).KCFLName;
            if (!string.IsNullOrEmpty(kcglModel.KJID))
            {
                int[] kjIds = kcglModel.KJID.SplitTOInt(',');

                if (kjIds.Length > 0)
                {
                    List<SZHL_PX_KJGL> kjgl = new List<SZHL_PX_KJGL>();

                    foreach (int item in kjIds)
                    {
                        SZHL_PX_KJGL model = new SZHL_PX_KJGLB().GetEntity(d => d.ID == item && d.ComId == UserInfo.User.ComId);
                        if (model != null)
                        {
                            kjgl.Add(model);
                        }
                    }
                    if (kjgl != null)
                    {




                        foreach (SZHL_PX_KJGL kjglitem in kjgl)
                        {
                            kjglitem.CW = kjglitem.CW.Replace("null", "\"\"");
                        }
                        msg.Result1 = kjgl.Select(d => d.KJName).ToList().ListTOString(',');
                        msg.Result2 = kjgl;
                        int seeTime = new SZHL_PX_SeeTimeB().GetEntities(d => kjIds.Contains(d.KCID.Value) && d.CRUser == UserInfo.User.UserName).Sum(d => d.KCDuration.Value);
                        decimal a = decimal.Parse(seeTime.ToString()) / decimal.Parse(kjgl.Sum(d => d.TotalTime).ToString());
                        if (a > 1)
                        {
                            a = 1;
                        }
                        msg.Result4 = (a * 100) + "-" + fl;
                    }
                }
                List<SZHL_KS_SJ> sjgl = new SZHL_KS_SJB().GetEntities(d => d.ComId == UserInfo.User.ComId && d.kcId == kcglModel.ID && d.Status == 1).ToList();
                if (sjgl.Count > 0)
                {
                    msg.Result3 = sjgl[0];
                }
            }
            msg.Result4 = msg.Result4 + "-" + (new SZHL_PX_KCSCB().GetEntities(d => d.KCID == kcglModel.ID && d.CRUser == UserInfo.User.UserName).Count() > 0 ? 1 : 0);
            DataTable dtKCTK = new SZHL_PX_GWKCB().GetDTByCommand("SELECT TOP 1 [ID],[TKName],[TKTypeId] FROM [QJY_PXKS].[dbo].[SZHL_KS_TK] where KCID=" + kcglModel.ID);
            if (dtKCTK.Rows.Count > 0)
            {
                msg.Result5 = dtKCTK.Rows[0]["ID"].ToString();
                msg.ResultType = dtKCTK.Rows[0]["TKTypeId"].ToString();

            }
            string strSJSTID = new SZHL_KS_STB().GetSJSTID("", Id.ToString());

            msg.DataLength = strSJSTID.Split(',').Length;


        }

        public void ADDKCGL(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            SZHL_PX_KCGL kcgl = JsonConvert.DeserializeObject<SZHL_PX_KCGL>(P1);
            if (string.IsNullOrEmpty(kcgl.KCName))
            {
                msg.ErrorMsg = "课程名称不能为空";
                return;
            }
            if (kcgl.ID == 0)
            {
                kcgl.CRDate = DateTime.Now;
                kcgl.CRUser = UserInfo.User.UserName;
                kcgl.ComId = UserInfo.User.ComId;
                new SZHL_PX_KCGLB().Insert(kcgl);
            }
            else
            {
                new SZHL_PX_KCGLB().Update(kcgl);
            }
            if (!string.IsNullOrEmpty(kcgl.KJID))
            {
                new SZHL_PX_KJKCB().Delete(d => d.ComId == UserInfo.User.ComId && d.KCID == kcgl.ID);
                int[] kjIds = kcgl.KJID.SplitTOInt(',');
                foreach (int kjId in kjIds)
                {
                    SZHL_PX_KJKC kjkc = new SZHL_PX_KJKC();
                    kjkc.ComId = UserInfo.User.ComId;
                    kjkc.KCID = kcgl.ID;
                    kjkc.KJID = kjId;
                    new SZHL_PX_KJKCB().Insert(kjkc);
                }
            }

            msg.Result = kcgl;
        }
        //删除课程
        public void DELKCGLBYID(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            try
            {
                if (new SZHL_PX_KCGLB().Delete(d => d.ID.ToString() == P1))
                {
                    // new SZHL_PX_KJKCB().Delete(d => d.KCID.ToString() == P1);
                    msg.ErrorMsg = "";
                }
            }
            catch (Exception ex)
            {
                msg.ErrorMsg = ex.Message;
            }
        }

        public void GETALLKC(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            msg.Result = new SZHL_PX_KCGLB().GetEntities(d => d.ComId == UserInfo.User.ComId);
        }

        #endregion

        #region 岗位管理
        public void GETGWGLMODEL(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = 0;
            int.TryParse(P1, out Id);
            SZHL_PX_GWGL gwglModel = new SZHL_PX_GWGLB().GetEntity(d => d.ID == Id && d.ComId == UserInfo.User.ComId);
            msg.Result = gwglModel;
        }
        public void GETGWLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = 0;
            int.TryParse(P1, out Id);
            msg.Result = new SZHL_PX_GWGLB().GetDTByCommand("select ID,GWName,GWDec from SZHL_PX_GWGL where ComId =" + UserInfo.User.ComId + " order by id desc");
        }
        public void ADDGWGL(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            SZHL_PX_GWGL gwgl = JsonConvert.DeserializeObject<SZHL_PX_GWGL>(P1);
            if (string.IsNullOrEmpty(gwgl.GWName))
            {
                msg.ErrorMsg = "岗位名称不能为空";
                return;
            }

            if (gwgl.ID == 0)
            {
                List<SZHL_PX_GWGL> gwglModel = new SZHL_PX_GWGLB().GetEntities(d => d.GWName == gwgl.GWName).ToList();
                if (gwglModel.Count() > 0)
                {
                    msg.ErrorMsg = "岗位名称已存在";
                    return;

                }
                gwgl.CRDate = DateTime.Now;
                gwgl.CRUser = UserInfo.User.UserName;
                gwgl.ComId = UserInfo.User.ComId;
                new SZHL_PX_GWGLB().Insert(gwgl);
            }
            else
            {
                new SZHL_PX_GWGLB().Update(gwgl);
            }
        }
        public void GETGWGLLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int page = 0;
            int pagecount = 0;

            int.TryParse(context.Request.QueryString["p"] ?? "1", out page);//页码
            page = page == 0 ? 1 : page;
            int.TryParse(context.Request.QueryString["pagecount"] ?? "8", out pagecount);//页数

            int recordCount = 0;
            string strWhere = string.Format(" ComId={0} ", UserInfo.User.ComId);

            string content = context.Request["Content"] ?? "";
            if (content != "")
            {
                strWhere += string.Format(" and GWName like '%{0}%'", content);
            }
            DataTable dt = new SZHL_PX_GWGLB().GetDataPager("  SZHL_PX_GWGL ", " * ", pagecount, page, "CRDate desc", strWhere, ref recordCount);

            dt.Columns.Add("SJID", Type.GetType("System.String"));//获取组织考试的试卷ID

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int gwid = int.Parse(dt.Rows[i]["ID"].ToString());
                SZHL_KS_SJ sj = new SZHL_KS_SJB().GetEntities(d => d.gwid == gwid).LastOrDefault();
                if (sj != null)
                {
                    dt.Rows[i]["SJID"] = sj.ID;
                }
            }


            msg.Result = dt;
            msg.Result1 = recordCount;
        }
        //获取所有岗位
        public void GETGWGLALL(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strwhere = "";
            if (P1 != "")
            {
                strwhere = string.Format(" where ID={0} ", P1);
            }
            DataTable dt = new SZHL_PX_GWGLB().GetDTByCommand("select * from SZHL_PX_GWGL " + strwhere + " order by CRDate ");
            msg.Result = dt;
        }
        //删除课程
        public void DELGWGLBYID(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            try
            {
                if (new SZHL_PX_GWGLB().Delete(d => d.ID.ToString() == P1))
                {
                    msg.ErrorMsg = "";
                }
            }
            catch (Exception ex)
            {
                msg.ErrorMsg = ex.Message;
            }
        }

        public void GETGWZYLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int page = 0;
            int.TryParse(context.Request.QueryString["p"] ?? "1", out page);//页码
            page = page == 0 ? 1 : page;
            int recordCount = 0;
            string strWhere = string.Format(" ComId={0} ", UserInfo.User.ComId);

            string content = context.Request["Content"] ?? "";

            DataTable dt = new DataTable();
            dt.Columns.Add("GWNAME", Type.GetType("System.String"));
            dt.Columns.Add("ZYNAME", Type.GetType("System.String"));
            dt.Columns.Add("ID", Type.GetType("System.String"));
            dt.Columns.Add("GWID", Type.GetType("System.String"));

            dt.Columns.Add("SJID", Type.GetType("System.String"));//获取组织考试的试卷ID


            List<SZHL_PX_GWGL> ListGw = new SZHL_PX_GWGLB().GetEntities(d => d.ID != 0).ToList();

            if (content != "")
            {
                ListGw = ListGw.Where(d => d.GWName.Contains(content)).ToList();
            }
            List<SZHL_PX_KCFL> ListZY = new SZHL_PX_KCFLB().GetEntities(d => d.ID != 0).ToList();

            List<SZHL_PX_GWZY> ListGWZY = new SZHL_PX_GWZYB().GetEntities(d => d.ID != 0).ToList();

            for (int i = 0; i < ListGw.Count(); i++)
            {
                var gwname = ListGw[i].GWName;
                for (int m = 0; m < ListZY.Count(); m++)
                {
                    DataRow newRow;
                    newRow = dt.NewRow();
                    newRow["GWNAME"] = gwname;
                    newRow["ZYNAME"] = ListZY[m].KCFLName;

                    SZHL_PX_GWZY temp = ListGWZY.Where(d => d.GW == gwname && d.ZY == ListZY[m].KCFLName).FirstOrDefault();
                    if (temp != null)
                    {
                        newRow["ID"] = temp.ID;
                        newRow["GWID"] = new SZHL_PX_GWGLB().GetEntity(d => d.GWName == temp.GW).ID;

                        SZHL_KS_SJ sj = new SZHL_KS_SJB().GetEntities(d => d.gwzyid == temp.ID).LastOrDefault();
                        if (sj != null)
                        {
                            newRow["SJID"] = sj.ID;
                        }

                    }
                    dt.Rows.Add(newRow);
                }
            }
            msg.Result = dt;
        }


        public void GETGWKCGLLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strWhere = string.Format(" kcgl.ComId={0} and gwkc.GWID={1} ", UserInfo.User.ComId, P2);
            if (P1 != "")
            {
                strWhere += string.Format(" and kcgl.KCTypeID={0}", P1);
            }
            string content = context.Request["Content"] ?? "";
            if (content != "")
            {
                strWhere += string.Format(" and kcgl.KCName like '%{0}%'", content);
            }
            string gwpxyq = context.Request["gwpxyq"] ?? "";
            if (!string.IsNullOrEmpty(gwpxyq))
            {
                strWhere += " and gwkc.GWPXYQ=" + gwpxyq;
            }
            string strSql = string.Format(" select kcgl.*,kcfl.KCFLName,zidian.TypeName,gwkc.ID gwkcid  from  SZHL_PX_KCGL kcgl inner join  SZHL_PX_KCFL kcfl on kcfl.ID=kcgl.KCTypeID inner join SZHL_PX_GWKC gwkc on  kcgl.ID=gwkc.KCID inner join JH_Auth_ZiDian zidian on gwkc.GWPXYQ=zidian.ID and zidian.Class=21 Where {0} Order BY kcgl.CRDate desc", strWhere);
            DataTable dt = new SZHL_PX_GWKCB().GetDTByCommand(strSql);
            msg.Result = dt;

        }
        /// <summary>
        /// 国电岗位培训
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETGWPXLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int page = 0;
            int.TryParse(context.Request.QueryString["p"] ?? "1", out page);//页码
            page = page == 0 ? 1 : page;
            int recordCount = 0;
            string strWhere = string.Format("And kcgl.ComId={0} ", UserInfo.User.ComId);
            if (P1 != "")
            {
                strWhere += string.Format(" and kcgl.KCTypeID={0}", P1);
            }

            string content = context.Request["Content"] ?? "";
            if (content != "")
            {
                strWhere += string.Format(" and kcgl.KCName like '%{0}%'", content);
            }
            string gwpxyq = context.Request["gwpxyq"] ?? "";
            if (!string.IsNullOrEmpty(gwpxyq))
            {
                strWhere += " and gwkc.GWPXYQ=" + gwpxyq;
            }

            string strSql = string.Format(@"(SELECT  DISTINCT kcgl.KCName,kcgl.KSS,kcgl.KCZJR,kcgl.KCTypeID,kcgl.KCZJRZC,kcfl.KCFLName,kcgl.KCFM,kcgl.ID,kcgl.CRDate, kjgl.TotalTime ,SUM(ps.KCDuration) looktime from SZHL_PX_GWKC gwkc 
                                            inner JOIN SZHL_PX_KJGL kjgl on gwkc.KCId=kjgl.ID INNER join SZHL_PX_KJKC kjkc on kjgl.ID=kjkc.KJID
                                            inner join SZHL_PX_KCGL kcgl on kjkc.KCID=kcgl.ID INNER join SZHL_PX_KCFL kcfl on kcfl.ID=kcgl.KCTypeID LEFT JOIN SZHL_PX_SeeTime ps on ps.KCID=kjgl.ID and ps.CRUser='{0}' where gwkc.GWId={1} {2}
                                            GROUP by kcgl.KCName,kcgl.KSS,kcgl.KCZJR,kcgl.KCTypeID,kcgl.KCZJRZC,kcfl.KCFLName,kcgl.KCFM,kcgl.ID,kcgl.CRDate,kjgl.TotalTime) newTab", UserInfo.User.UserName, P2, strWhere);
            DataTable dt = new SZHL_PX_GWKCB().GetDataPager(strSql, "*", 8, page, " CRDate desc ", "", ref recordCount);

            msg.Result = dt;
            msg.Result1 = recordCount;
            int gwId = 0;
            int.TryParse(P2, out gwId);
            msg.Result2 = new SZHL_PX_GWGLB().GetEntity(d => d.ID == gwId);
        }

        public void ADDGWKC(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            if (!string.IsNullOrEmpty(P1))
            {
                int gwId = int.Parse(P2);
                List<SZHL_PX_GWKC> gwkcList = JsonConvert.DeserializeObject<List<SZHL_PX_GWKC>>(P1);
                new SZHL_PX_GWKCB().Delete(d => d.ComId == UserInfo.User.ComId && d.GWId == gwId);
                foreach (SZHL_PX_GWKC gwkc in gwkcList)
                {
                    gwkc.GWId = gwId;
                    gwkc.ComId = UserInfo.User.ComId;
                    new SZHL_PX_GWKCB().Insert(gwkc);
                }
                //int gwId = int.Parse(P2);
                //foreach (string str in P1.Split(','))
                //{
                //    int kcId = 0;
                //    int.TryParse(str, out kcId);
                //    List<SZHL_PX_GWKC> gwkcList = new SZHL_PX_GWKCB().GetEntities(d => d.ComId == UserInfo.User.ComId && d.GWId == gwId && d.KCId == kcId).ToList();
                //    if (gwkcList.Count() == 0)
                //    {
                //        SZHL_PX_GWKC gwkc = new SZHL_PX_GWKC();
                //        gwkc.GWId = gwId;
                //        gwkc.ComId = UserInfo.User.ComId;
                //        gwkc.KCId = kcId;
                //        gwkc.GWPXYQ = int.Parse(context.Request["YQ"]);
                //        new SZHL_PX_GWKCB().Insert(gwkc);
                //    }
                //    else
                //    {
                //        gwkcList[0].GWPXYQ = int.Parse(context.Request["YQ"]);
                //        new SZHL_PX_GWKCB().Update(gwkcList[0]);
                //    }
                //}
            }
        }

        public void DELGWKCBYID(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            try
            {
                int gwkcID = 0;
                int.TryParse(P1, out gwkcID);
                if (new SZHL_PX_GWKCB().Delete(d => d.ID == gwkcID))
                {
                    msg.ErrorMsg = "";
                }
            }
            catch (Exception ex)
            {
                msg.ErrorMsg = ex.Message;
            }
        }
        #endregion

        #region 培训班管理
        public void GETPXBGLLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int page = 0;
            int.TryParse(context.Request.QueryString["p"] ?? "1", out page);//页码
            page = page == 0 ? 1 : page;
            int recordCount = 0;
            string strWhere = string.Format(" pxbgl.ComId={0} ", UserInfo.User.ComId);
            if (P1 != "")
            {
                strWhere += string.Format(" and pxbgl.PXType={0}", P1);
            }
            if (P2 != "")
            {
                strWhere += string.Format(" and pxbgl.isBM={0} ", P2);
            }
            string content = context.Request["Content"] ?? "";
            if (content != "")
            {
                strWhere += string.Format(" and pxbgl.PXName like '%{0}%'", content);
            }
            string status = context.Request["status"] ?? "";
            if (status != "")
            {
                switch (status)
                {
                    case "1":
                        strWhere += " And PXEndDate > GETDATE() AND PXStartDate < GETDATE()";
                        break;
                    case "2":
                        strWhere += "And PXStartDate > GETDATE()";
                        break;
                    case "3":
                        strWhere += "And PXEndDate < GETDATE()";
                        break;

                }
            }
            DataTable dt = new SZHL_GZBGB().GetDataPager("  SZHL_PX_PXBGL pxbgl inner join  JH_Auth_ZiDian zd on zd.ID=pxbgl.PXType ", " pxbgl.*,zd.TypeName   ", 8, page, "pxbgl.CRDate desc", strWhere, ref recordCount);
            msg.Result = dt;
            msg.Result1 = recordCount;
        }

        //删除培训班
        public void DELPXBGLBYID(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            try
            {
                if (new SZHL_PX_PXBGLB().Delete(d => d.ID.ToString() == P1))
                {
                    new SZHL_PX_PXBKCB().Delete(d => d.PXBID.ToString() == P1);
                    msg.ErrorMsg = "";
                }
            }
            catch (Exception ex)
            {
                msg.ErrorMsg = ex.Message;
            }
        }

        public void GETPXBGLMODEL(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = 0;
            int.TryParse(P1, out Id);
            SZHL_PX_PXBGL pxglModel = new SZHL_PX_PXBGLB().GetEntity(d => d.ID == Id && d.ComId == UserInfo.User.ComId);
            msg.Result = pxglModel;
            msg.Result1 = new SZHL_PX_PXBGLB().GetDTByCommand(@"SELECT kcgl.*,kcfl.KCFLName from SZHL_PX_PXBKC pxbkc inner join  SZHL_PX_KCGL kcgl on pxbkc.KCID=kcgl.ID inner join  SZHL_PX_KCFL kcfl on kcfl.ID=kcgl.KCTypeID where pxbkc.PXBID=" + Id);
            if (pxglModel != null)
            {
                if (!string.IsNullOrEmpty(pxglModel.BMBranch))
                {
                    int[] branchs = pxglModel.BMBranch.SplitTOInt(',');
                    msg.Result2 = new JH_Auth_BranchB().GetEntities(d => d.ComId == UserInfo.User.ComId && branchs.Contains(d.DeptCode)).Select(d => d.DeptName).ToList().ListTOString(',');
                }
                if (pxglModel.isBM != null && pxglModel.isBM == 1)
                {
                    msg.Result3 = new SZHL_PX_BMB().GetEntities(d => d.PXBID == pxglModel.ID && d.SHState == 1 && d.ComId == UserInfo.User.ComId);
                }
                msg.Result4 = new JH_Auth_ZiDianB().GetEntity(d => d.ID == pxglModel.PXType);
            }
        }

        //添加培训班
        public void ADDPXBGL(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            SZHL_PX_PXBGL pxbgl = JsonConvert.DeserializeObject<SZHL_PX_PXBGL>(P1);
            if (pxbgl.isBM == 1)
            {
                if (string.IsNullOrEmpty(pxbgl.BMStartDate.ToString()))
                {
                    msg.ErrorMsg = "请输入报名开始时间";
                    return;
                }
                if (string.IsNullOrEmpty(pxbgl.BMEndDate.ToString()))
                {
                    msg.ErrorMsg = "请输入报名结束时间";
                    return;
                }
                //if (string.IsNullOrEmpty(pxbgl.BMBranch))
                //{
                //    msg.ErrorMsg = "请选择报名部门";
                //    return;
                //}
            }
            if (string.IsNullOrEmpty(P2))
            {
                msg.ErrorMsg = "请选择课程";
                return;
            }
            if (pxbgl.ID == 0)
            {
                pxbgl.CRDate = DateTime.Now;
                pxbgl.CRUser = UserInfo.User.UserName;
                pxbgl.ComId = UserInfo.User.ComId;
                new SZHL_PX_PXBGLB().Insert(pxbgl);
            }
            else
            {
                new SZHL_PX_PXBGLB().Update(pxbgl);
            }
            if (!string.IsNullOrEmpty(P2))
            {
                new SZHL_PX_PXBKCB().Delete(d => d.ComId == UserInfo.User.ComId && d.PXBID == pxbgl.ID);
                foreach (string str in P2.Split(','))
                {

                    SZHL_PX_PXBKC pxbkc = new SZHL_PX_PXBKC();
                    pxbkc.PXBID = pxbgl.ID;
                    int kcId = 0;
                    int.TryParse(str, out kcId);
                    pxbkc.KCID = kcId;
                    pxbkc.ComId = UserInfo.User.ComId;
                    new SZHL_PX_PXBKCB().Insert(pxbkc);
                }

            }
            msg.Result = pxbgl;
        }

        #endregion

        #region 报名管理
        public void GETBMGLLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int page = 0;
            int.TryParse(context.Request.QueryString["p"] ?? "1", out page);//页码
            page = page == 0 ? 1 : page;
            int recordCount = 0;
            string strWhere = string.Format(" bmgl.ComId={0} ", UserInfo.User.ComId);
            if (P1 != "")
            {
                strWhere += string.Format(" and bmgl.SHState={0}", P1);
            }
            string content = context.Request["Content"] ?? "";
            if (content != "")
            {
                strWhere += string.Format(" and pxbgl.PXName like '%{0}%'", content);
            }
            if (P2 != "")
            {
                strWhere += string.Format(" and pxbgl.ID={0} ", P2);
            }
            DataTable dt = new SZHL_GZBGB().GetDataPager("  SZHL_PX_BM bmgl inner join  SZHL_PX_PXBGL pxbgl on bmgl.PXBID=pxbgl.ID ", " bmgl.*,pxbgl.PXName   ", 8, page, "bmgl.CJDate desc", strWhere, ref recordCount);
            msg.Result = dt;
            msg.Result1 = recordCount;
        }

        public void GETBMGLMODEL(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            int Id = 0;

            string strWhere = string.Format(" bmgl.ComId={0} ", UserInfo.User.ComId);
            int.TryParse(P1, out Id);
            SZHL_PX_BM bmModel = new SZHL_PX_BMB().GetEntity(d => d.ID == Id && d.ComId == UserInfo.User.ComId);
            SZHL_PX_PXBGL pxbModel = new SZHL_PX_PXBGLB().GetEntity(d => d.ID == bmModel.PXBID);
            msg.Result = bmModel;
            msg.Result1 = pxbModel;

        }

        //审核报名状态
        public void SHBMGL(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int id = int.Parse(P1);
            SZHL_PX_BM bmgl = new SZHL_PX_BMB().GetEntity(d => d.ID == id);

            if (!string.IsNullOrEmpty(P2))
            {
                int state = int.Parse(P2);
                bmgl.SHState = state;
                bmgl.SHDate = DateTime.Now;
                bmgl.SHName = UserInfo.User.UserRealName;
                new SZHL_PX_BMB().Update(bmgl);
            }
        }

        #region 添加报名
        /// <summary>
        /// 添加报名
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void ADDPXBM(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int pxbID = 0;
            if (!int.TryParse(P1, out pxbID))
            {
                msg.ErrorMsg = "数据异常,请刷新重试";
                return;
            }
            SZHL_PX_PXBGL pxb = new SZHL_PX_PXBGLB().GetEntity(d => d.ID == pxbID);
            if (pxb == null)
            {
                msg.ErrorMsg = "数据异常,请刷新重试";
                return;
            }
            bool isBMBranch = pxb.BMBranch.Split(',').Contains(UserInfo.BranchInfo.DeptCode.ToString());
            if (pxb.isBM == 1 && isBMBranch && pxb.BMStartDate < DateTime.Now && pxb.BMEndDate > DateTime.Now)
            {
                List<SZHL_PX_BM> bmgl = new SZHL_PX_BMB().GetEntities(d => d.PXBID == pxb.ID && d.ComId == UserInfo.User.ComId && d.CJName == UserInfo.User.UserName).ToList();

                if (bmgl.Count == 0)
                {
                    SZHL_PX_BM bm = new SZHL_PX_BM();
                    bm.BMName = pxb.PXName;
                    bm.PXBID = pxb.ID;
                    bm.SHState = 0;
                    bm.CJDate = DateTime.Now;
                    bm.CJName = UserInfo.User.UserName;
                    bm.ComId = UserInfo.User.ComId;
                    new SZHL_PX_BMB().Insert(bm);
                }
                else
                {
                    msg.ErrorMsg = "已经报名过";
                }
            }
            else if (pxb.BMStartDate > DateTime.Now)
            {
                msg.ErrorMsg = "报名未开始";
            }
            else if (pxb.BMEndDate < DateTime.Now)
            {
                msg.ErrorMsg = "报名已结束";
            }
            else
            { msg.ErrorMsg = "不在报名范围内"; }
        }
        #endregion
        #endregion

        #region 课件实体类
        public class CWEntity
        {
            public string title { get; set; }
            public string dir { get; set; }
            public List<CWVideoEntity> videos { get; set; }
            public List<CWVideoAttrEntity> videosAttr { get; set; }
            public List<CWFileEntity> files { get; set; }
            public List<CWMenuEntity> menu { get; set; }
            public int screen { get; set; }
            public string link { get; set; }
            public string desc { get; set; }
        }

        public class CWVideoEntity
        {
            public string name { get; set; }
            public string url { get; set; }
            public string vid { get; set; }
        }

        public class CWVideoAttrEntity
        {
            public string vName { get; set; }
            public double vLength { get; set; }
        }

        public class CWFileEntity
        {
            public string file { get; set; }
            public string dn { get; set; }
            public string type { get; set; }
            public int pages { get; set; }
        }

        public class CWMenuEntity
        {
            public string ix { get; set; }
            public string name { get; set; }
            public string type { get; set; }
            public CWFileEntity file { get; set; }
            public bool rended { get; set; }
            public string pos { get; set; }
            public int time { get; set; }
            public string video { get; set; }
        }
        #endregion

        #region 我的培训班
        /// <summary>
        /// 我的培训班
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETMYPXB(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int page = 0;
            int.TryParse(context.Request.QueryString["p"] ?? "1", out page);//页码
            page = page == 0 ? 1 : page;
            int recordCount = 0;
            string userName = UserInfo.User.UserName;
            string user = context.Request["user"] ?? "";
            if (user != "")
            {
                userName = user;
            }
            string strWhere = string.Format(" pxbgl.ComId={0} and pxbgl.PXStartDate < GETDATE() and (',' + pxbgl.PXPeople + ',' LIKE '%{1}%' OR (isBM = 1 AND ',' + BMBranch + ',' LIKE '%,{1},%' AND bm.SHState=1))", UserInfo.User.ComId, userName);

            DataTable dt = new SZHL_PX_PXBGLB().GetDataPager("	SZHL_PX_PXBGL pxbgl LEFT JOIN SZHL_PX_BM bm ON pxbgl.ID=bm.PXBID AND CJName='" + UserInfo.User.UserName + "'", " pxbgl.PXName,pxbgl.PXStartDate,pxbgl.PXEndDate,CASE WHEN PXStartDate > GETDATE() THEN 2 WHEN PXEndDate > GETDATE() AND PXStartDate < GETDATE() THEN 1 WHEN PXEndDate < GETDATE() THEN 3 END AS pxbzt ", 8, page, " pxbgl.PXEndDate desc ", strWhere, ref recordCount);
            msg.Result = dt;
            msg.Result1 = recordCount;
        }

        /// <summary>
        /// 我的培训班列表分页
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETMYPXBPAGE(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int page = 0;
            int.TryParse(context.Request.QueryString["p"] ?? "1", out page);//页码
            page = page == 0 ? 1 : page;
            int recordCount = 0;
            string strWhere = string.Format(" pxbgl.ComId={0} ", UserInfo.User.ComId);
            if (P1 != "")
            {
                strWhere += string.Format(" and pxbgl.PXType={0}", P1);
            }
            string content = context.Request["Content"] ?? "";
            if (content != "")
            {
                strWhere += string.Format(" and pxbgl.PXName like '%{0}%'", content);
            }
            if (P2 != "")
            {
                strWhere += string.Format(" and pxbgl.pxbzt='{0}' ", P2);// 2 未开始 1进行中，3已结束
            }
            strWhere += string.Format(" AND ( (pxbgl.IsPublic=0 and ','+pxbgl.PXPeople+',' like '%{0}%') OR  IsPublic=1  OR (isBM=1 AND ','+BMBranch+',' Like '%,{1},%')) ", UserInfo.User.UserName, UserInfo.BranchInfo.DeptCode);
            DataTable dt = new SZHL_GZBGB().GetDataPager("(select pxbgl.*,CASE WHEN PXStartDate > GETDATE() THEN 2 WHEN PXEndDate > GETDATE() AND PXStartDate < GETDATE() THEN 1 WHEN PXEndDate < GETDATE() THEN 3 END AS pxbzt,zd.TypeName from SZHL_PX_PXBGL pxbgl inner join  JH_Auth_ZiDian zd on zd.ID=pxbgl.PXType ) pxbgl ", " * ", 10, page, " pxbgl.pxbzt,pxbgl.PXStartDate ", strWhere, ref recordCount);
            //dt.Columns.Add("PXISBM", Type.GetType("System.String"));
            //dt.Columns.Add("ISCY", Type.GetType("System.String"));
            //dt.Columns.Add("ISSHOWBTN", Type.GetType("System.String"));
            //dt.Columns.Add("BMZT", Type.GetType("System.String"));
            //if (dt != null && dt.Rows.Count > 0)
            //{
            //    foreach (DataRow dr in dt.Rows)
            //    {
            //        dr["ISCY"] = "0";
            //        if (dr["PXPeople"] != null)
            //        {
            //            string[] ksuser = dr["PXPeople"].ToString().Split(',');
            //            if (ksuser.Contains(UserInfo.User.UserName))
            //            {
            //                dr["ISCY"] = "1";
            //            }
            //        }
            //        dr["PXISBM"] = "0";
            //        if (dr["ID"] != null && dr["ID"].ToString() != "")
            //        {
            //            List<SZHL_PX_BM> bmlist = new SZHL_PX_BMB().GetEntities(" PXBID in (" + dr["ID"].ToString() + ") And CJName='" + UserInfo.User.UserName + "'").ToList();
            //            if (bmlist.Count > 0)
            //            {
            //                dr["PXISBM"] = "1";
            //                if (bmlist[0].SHState == 1)
            //                {
            //                    dr["PXISBM"] = "3";
            //                }
            //                else if (bmlist[0].SHState == -1)
            //                {
            //                    dr["PXISBM"] = "2";
            //                }
            //            }
            //        }
            //        dr["ISSHOWBTN"] = "0";
            //        dr["BMZT"] = "0";
            //        if (dr["pxbzt"].ToString() != "3" && dr["ISCY"].ToString() != "1" && dr["PXISBM"].ToString() == "0")
            //        {
            //            if (dr["isBM"].ToString() == "1")
            //            {
            //                if (dr["BMStartDate"].ToDateTime() < DateTime.Now && DateTime.Now < dr["BMEndDate"].ToDateTime())
            //                {
            //                    dr["ISSHOWBTN"] = "1";
            //                }
            //                if (dr["BMStartDate"].ToDateTime() > DateTime.Now)
            //                {
            //                    dr["BMZT"] = "1";
            //                }
            //                if (DateTime.Now > dr["BMEndDate"].ToDateTime())
            //                {
            //                    dr["BMZT"] = "2";
            //                }
            //            }
            //            else
            //            {
            //                dr["ISSHOWBTN"] = "1";
            //            }
            //        }
            //    }
            //}

            msg.Result = dt;
            msg.Result1 = recordCount;
        }
        #endregion

        #region 课件观看时间
        /// <summary>
        /// 添加课件观看时间
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void CREATESEETIME(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            SZHL_PX_SeeTime sps = JsonConvert.DeserializeObject<SZHL_PX_SeeTime>(P1);

            sps.UserName = UserInfo.User.UserName;
            sps.CRDate = DateTime.Now;
            sps.CRUser = UserInfo.User.UserName;
            sps.ComId = UserInfo.User.ComId;
            sps.StartTime = DateTime.Now;
            sps.KCDuration = 0;

            SZHL_PX_SeeTimeB spsb = new SZHL_PX_SeeTimeB();
            spsb.Insert(sps);
            sps = spsb.GetEntity(d => d.KCID == sps.KCID && d.UserName == sps.UserName && d.CRDate == sps.CRDate);

            msg.Result = sps;
        }
        /// <summary>
        /// 更新课件观看时间
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void UPDATESEETIME(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            SZHL_PX_SeeTime sps = JsonConvert.DeserializeObject<SZHL_PX_SeeTime>(P1);
            new SZHL_PX_SeeTimeB().Update(sps);



            //不计算超过课件时长的时间
            string strSQL = "UPDATE SZHL_PX_SeeTime SET   SZHL_PX_SeeTime.KCduration= SZHL_PX_KJGL.totaltime  from SZHL_PX_SeeTime  inner join SZHL_PX_KJGL  on SZHL_PX_SeeTime.KCID = SZHL_PX_KJGL.ID  where SZHL_PX_SeeTime.KCduration>SZHL_PX_KJGL.totaltime";
            new SZHL_PX_SeeTimeB().ExsSclarSql(strSQL);
        }

        /// <summary>
        /// 获取学员观看课件时间
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETKCSEETIME(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int page = 0;
            int.TryParse(context.Request.QueryString["p"] ?? "1", out page);//页码
            page = page == 0 ? 1 : page;
            int recordCount = 0;
            string strWhere = string.Format(" ps.ComId={0}  and ps.KCDuration>60", UserInfo.User.ComId);
            if (P1 != "")
            {
                strWhere += " AND kjkc.KCID=" + P1;
            }
            if (P2 != "")
            {
                strWhere += " AND ps.CRUser='" + P2 + "'";
            }
            string content = context.Request["Content"] ?? "";
            if (content != "")
            {
                strWhere += string.Format(" and (auser.UserRealName like '%{0}%')", content);
            }
            DataTable dt = new SZHL_PX_SeeTimeB().GetDataPager("  SZHL_PX_SeeTime ps INNER JOIN JH_Auth_User auser ON ps.UserName= auser.UserName  inner join SZHL_PX_KJKC kjkc on ps.KCID=kjkc.KJID", " ps.*,auser.UserRealName,kjkc.KCID AS KCID1 ", 8, page, " ps.CRDate DESC ", strWhere, ref recordCount);

            string strSql = string.Format("SELECT  PGG.ID,gwname,PS.UserName,'' as istg, SUM(KCDuration) as zsc FROM SZHL_PX_SeeTime PS INNER JOIN SZHL_PX_KJKC PKJ ON PS.KCID=PKJ.KJID  INNER JOIN SZHL_PX_GWKC PG ON PKJ.KCID=PG.KCID INNER JOIN SZHL_PX_GWGL PGG ON PG.GWID=PGG.ID WHERE UserName='{0}' AND KCDuration<>'0'  GROUP BY  PGG.ID,gwname,PS.UserName ORDER BY gwname", UserInfo.User.UserName);
            DataTable dtgw = new SZHL_PX_KJGLB().GetDTByCommand(strSql);
            for (int i = 0; i < dtgw.Rows.Count; i++)
            {
                dtgw.Rows[i]["istg"] = new SZHL_PX_GWKCB().isKSTG(dtgw.Rows[i]["ID"].ToString(), UserInfo.User.UserName);
            }

            dt.Columns.Add("istg");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["istg"] = new SZHL_PX_GWKCB().isKSTG(dt.Rows[i]["KCID1"].ToString(), UserInfo.User.UserName);
            }


            msg.Result = dt;

            string strSqlZSC = string.Format("SELECT   SUM(KCDuration) AS ZSC  FROM SZHL_PX_SeeTime WHERE UserName='{0}' AND KCDuration<>'0' ", UserInfo.User.UserName);
            DataTable dtZSC = new SZHL_PX_KJGLB().GetDTByCommand(strSqlZSC);



            msg.Result1 = recordCount;
            msg.Result2 = dtgw;
            msg.Result3 = dtZSC;

        }


        /// <summary>
        /// 岗位课程学习统计数据
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETGWTJ(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int page = 0;
            int.TryParse(context.Request.QueryString["p"] ?? "1", out page);//页码
            page = page == 0 ? 1 : page;
            int recordCount = 0;

            string gw = context.Request["Content"] ?? "";
            string strWhere = "";
            if (gw != "")
            {
                strWhere = "and gwname like '%" + gw + "%'";
            }


            DataTable dtuser = new JH_Auth_UserB().GetUserListbyBranch(UserInfo.BranchInfo.DeptCode, "", UserInfo.User.ComId.Value);
            string strUsers = "";
            for (int i = 0; i < dtuser.Rows.Count; i++)
            {
                strUsers = strUsers + dtuser.Rows[i]["UserName"].ToString() + ",";
            }
            string LikeString = strUsers.TrimEnd(',').ToFormatLike();
            if (LikeString != "")
            {
                strWhere += string.Format(" and  UserName in ('{0}')", LikeString);
            }

            string strSql = string.Format("SELECT  PGG.ID,gwname,PS.UserName,'' as istg, SUM(KCDuration) as zsc FROM SZHL_PX_SeeTime PS INNER JOIN SZHL_PX_KJKC PKJ ON PS.KCID=PKJ.KJID  INNER JOIN SZHL_PX_GWKC PG ON PKJ.KCID=PG.KCID INNER JOIN SZHL_PX_GWGL PGG ON PG.GWID=PGG.ID WHERE   KCDuration<>'0' {0}  GROUP BY  PGG.ID,gwname,PS.UserName ORDER BY gwname", strWhere);
            int pagecount = 0;
            int.TryParse(context.Request.QueryString["pagecount"] ?? "8", out pagecount);//页码
            pagecount = pagecount == 0 ? 10 : pagecount;
            DataTable dt = new SZHL_PX_GWKCB().GetDTByCommand(strSql);
            recordCount = dt.Rows.Count;
            //dt.Columns.Add("isgly");//去除管理员
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                // dt.Rows[i]["istg"] = new SZHL_PX_GWKCB().isKSTG(dt.Rows[i]["ID"].ToString(), UserInfo.User.UserName);
                // dt.Rows[i]["isgly"] = new JH_Auth_BranchB().ISSUPADMIN(dt.Rows[i]["UserName"].ToString()).Rows.Count > 0 ? "Y" : "N";
            }
            msg.Result = new CommonHelp().GetPagedTable(dt, page, pagecount);
            msg.Result1 = recordCount;

        }
        #endregion

        public void GETZSKLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int FolderID = int.Parse(P1);//
            msg.Result = new FT_FolderB().GetEntities(" ComId=" + UserInfo.User.ComId + " AND  PFolderID=" + FolderID);
            msg.Result1 = new FT_FileB().GetEntities("ComId=" + UserInfo.User.ComId + " AND  FolderID=" + FolderID);
            string strSql = string.Format("SELECT  ID,KJName,KJDec from  SZHL_PX_KJGL where ComId=" + UserInfo.User.ComId + " and FolderID=" + FolderID);
            msg.Result2 = new SZHL_PX_KJGLB().GetDTByCommand(strSql);
        }

        #region 我的收藏学习
        /// <summary>
        /// 获取我的收藏学习
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETMYSCXX(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int page = 0;
            int.TryParse(context.Request.QueryString["p"] ?? "1", out page);//页码
            page = page == 0 ? 1 : page;
            int pagesize = 10;
            int.TryParse(context.Request.QueryString["ps"] ?? "10", out pagesize);//页码
            pagesize = pagesize == 0 ? 10 : pagesize;
            int recordCount = 0;
            string where = string.Format(" pc.CRUser='{0}' ", UserInfo.User.UserName);
            DataTable dt = new SZHL_PX_CollectionB().GetDataPager("SZHL_PX_Collection pc LEFT JOIN FT_File f ON pc.TypeID=f.ID", " pc.*,f.YLUrl ", pagesize, page, "pc.CRDate desc", where, ref recordCount);
            dt.Columns.Add("url", Type.GetType("System.String"));
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    dr["url"] = "javascript:void(0)";
                    switch (dr["Type"].ToString().ToUpper())
                    {
                        case "WD":
                            if (dr["YLUrl"] != null && dr["YLUrl"].ToString() != "")
                            {
                                dr["url"] = dr["YLUrl"].ToString().Replace("http://gdpx.qijieyun.com", "");
                            }
                            break;
                        case "KJ":
                            dr["url"] = "/ViewV4/AppPage/PXGL/kj/index.html?id=" + dr["TypeID"];
                            break;
                    }
                }
            }
            msg.Result = dt;
            msg.Result1 = recordCount;
        }
        /// <summary>
        /// 我的收藏学习
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void MYSCXXADD(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            SZHL_PX_Collection pc = JsonConvert.DeserializeObject<SZHL_PX_Collection>(P1);
            if (pc == null)
            {
                msg.ErrorMsg = "数据错误";
                return;
            }
            if (pc.ID == 0)
            {
                pc.CRDate = DateTime.Now;
                pc.CRUser = UserInfo.User.UserName;
                pc.ComId = UserInfo.User.ComId;

                List<SZHL_PX_Collection> pxlist = new SZHL_PX_CollectionB().GetEntities(d => d.CRUser == pc.CRUser && d.ComId == pc.ComId && d.Type == pc.Type && d.TypeID == pc.TypeID && d.Name == pc.Name).ToList();
                if (pxlist != null && pxlist.Count == 0)
                {
                    new SZHL_PX_CollectionB().Insert(pc);
                }
                else
                {
                    msg.ErrorMsg = "已收藏";
                }
            }
            else
            {
                new SZHL_PX_CollectionB().Update(pc);
            }
            msg.Result = pc;
        }
        //删除我的收藏学习
        public void DELSCXX(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            try
            {
                int id = 0;
                int.TryParse(P1, out id);
                if (id > 0)
                {
                    if (new SZHL_PX_CollectionB().Delete(d => d.ID == id))
                    {
                        msg.ErrorMsg = "";
                    }
                }
            }
            catch (Exception)
            {
                msg.ErrorMsg = "删除失败";
            }
        }
        #endregion

        #region 书签管理
        /// <summary>
        /// 添加书签
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void SAVEMARKER(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            SZHL_PX_Marker marker = JsonConvert.DeserializeObject<SZHL_PX_Marker>(P1);

            marker.UserName = UserInfo.User.UserName;
            marker.CRDate = DateTime.Now;
            marker.CRUser = UserInfo.User.UserName;
            marker.ComId = UserInfo.User.ComId;

            new SZHL_PX_MarkerB().Insert(marker);
        }

        /// <summary>
        /// 删除书签
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void DELETEMARKER(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            try
            {
                if (new SZHL_PX_MarkerB().Delete(d => d.ID.ToString() == P1))
                {
                    msg.ErrorMsg = "";
                }
            }
            catch (Exception ex)
            {
                msg.ErrorMsg = ex.Message;
            }
        }

        /// <summary>
        /// 查询书签
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETMARKERLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int kcId = int.Parse(P1);
            msg.Result = new SZHL_PX_MarkerB().GetEntities(d => d.KCID == kcId && d.UserName == UserInfo.User.UserName && d.ComId == UserInfo.User.ComId);
        }
        #endregion

        #region 获取ztree课件
        public void GETKJZTREE(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            DataTable dt = new SZHL_PX_KCFLB().GetDTByCommand("SELECT ID, KCFLName name from SZHL_PX_KCFL");
            dt.Columns.Add("children", typeof(object));
            foreach (DataRow row in dt.Rows)
            {
                DataTable dtKC = new SZHL_PX_KCGLB().GetDTByCommand("SELECT ID,KCName name,KJID from SZHL_PX_KCGL where KCTypeID=" + row["ID"]);
                if (dtKC.Rows.Count > 0)
                {
                    dtKC.Columns.Add("children", typeof(object));
                    foreach (DataRow rowKC in dtKC.Rows)
                    {
                        if (rowKC["KJID"].ToString() != "")
                        {
                            rowKC["children"] = new SZHL_PX_KJGLB().GetDTByCommand("SELECT  kjgl.ID,KJName name, case when gwkc.ID is NOT NULL then 'true'ELSE '' END checked from SZHL_PX_KJGL kjgl  LEFT JOIN SZHL_PX_GWKC gwkc on  kjgl.ID=gwkc.KCId and gwkc.GWPXYQ=" + P1 + " and gwkc.GWId=" + P2 + "   where kjgl.ID in (" + rowKC["KJID"] + ")");
                        }
                    }
                    row["children"] = dtKC;
                }
                else
                {
                    row.Delete();
                }
            }
            dt.AcceptChanges();
            msg.Result = dt;
        }
        public void GETKJZTREENEW(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            DataTable dt = new SZHL_PX_KCFLB().GetDTByCommand("SELECT ID, KCFLName name from SZHL_PX_KCFL");
            dt.Columns.Add("children", typeof(object));
            foreach (DataRow row in dt.Rows)
            {
                DataTable dtKC = new SZHL_PX_KCGLB().GetDTByCommand("SELECT kcgl.ID,KCName name, case when gwkc.ID is NOT NULL then 'true'ELSE '' END checked  from SZHL_PX_KCGL kcgl LEFT JOIN SZHL_PX_GWKC gwkc on  kcgl.ID=gwkc.KCId  and gwkc.GWId=" + P2 + " where KCTypeID=" + row["ID"]);
                if (dtKC.Rows.Count > 0)
                {
                    dtKC.Columns.Add("children", typeof(object));
                    row["children"] = dtKC;
                }
                else
                {
                    row.Delete();
                }
            }
            dt.AcceptChanges();
            msg.Result = dt;
        }

        #endregion

        #region 获取首页课程

        #endregion

        #region 个人学习排行榜
        public void GETSTUDYORDER(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strSql = string.Format(@"SELECT  top 8 * from (
                                        SELECT   stime.UserName,usera.UserRealName,SUM(stime.KCDuration) seeTime,branch.DeptName from SZHL_PX_SeeTime stime  inner join JH_Auth_User usera on stime.UserName=usera.UserName
                                        inner join JH_Auth_Branch branch on usera.BranchCode=branch.DeptCode where branch.ComId={0}
                                        GROUP by stime.UserName,branch.DeptName,usera.UserRealName
                                        ) NewTab order by  seeTime DESC", UserInfo.User.ComId);
            msg.Result = new SZHL_PX_SeeTimeB().GetDTByCommand(strSql);
        }
        #endregion
        #region 部门学习排行榜
        public void GETSTUDYBRANCH(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strSql = string.Format(@"SELECT top 8 * from (
                                            SELECT branch.DeptName,SUM(stime.KCDuration) seeTime from JH_Auth_Branch branch 
                                            inner join JH_Auth_User usera on branch.DeptCode=usera.BranchCode 
                                            inner join SZHL_PX_SeeTime stime on usera.UserName=stime.UserName where usera.ComId={0}
                                            GROUP by  branch.DeptName
                                            ) newtab order by newtab.seeTime desc ", UserInfo.User.ComId);
            msg.Result = new SZHL_PX_SeeTimeB().GetDTByCommand(strSql);
        }
        #endregion
        #region 课程收藏
        public void KCSC(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int kcId = 0;
            int.TryParse(P1, out kcId);
            if (new SZHL_PX_KCSCB().GetEntities(d => d.KCID == kcId && d.CRUser == UserInfo.User.UserName).Count() == 0)
            {
                SZHL_PX_KCSC kcsc = new SZHL_PX_KCSC();
                kcsc.KCID = kcId;
                kcsc.CRUser = UserInfo.User.UserName;
                kcsc.CRDate = DateTime.Now;
                new SZHL_PX_KCSCB().Insert(kcsc);
            }
            else
            {
                msg.ErrorMsg = "您已收藏";
            }
        }
        #region 收藏课程
        public void GETSCKC(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strSql = string.Format(@" SELECT DISTINCT kcgl.KCName,kcgl.KSS,kcgl.KCZJR,kcgl.KCTypeID,kcgl.KCZJRZC,kcfl.KCFLName,kcgl.KCFM,kcgl.ID,SUM(kjgl.TotalTime) TotalTime ,SUM(st.KCDuration) looktime FROM SZHL_PX_KCGL kcgl INNER join SZHL_PX_KCFL kcfl on kcgl.KCTypeID=kcfl.ID 
INNER join SZHL_PX_KJKC kjkc on  kcgl.ID=kjkc.KCID inner join SZHL_PX_KJGL kjgl on kjkc.KJID=kjgl.ID
left join SZHL_PX_SeeTime st on kjgl.ID=st.KCID and st.UserName='{0}'
inner join  SZHL_PX_KCSC kcsc on  kcgl.ID=kcsc.KCID where kcsc.CRUser='{0}'
GROUP by kcgl.KCName,kcgl.KSS,kcgl.KCZJR,kcgl.KCTypeID,kcgl.KCZJRZC,kcfl.KCFLName,kcgl.KCFM,kcgl.ID", UserInfo.User.UserName);
            DataTable dt = new SZHL_PX_KCGLB().GetDTByCommand(strSql);

            dt.Columns.Add("fileurl");
            for (int i = 0; i < dt.Rows.Count; i++)
            {

                if (dt.Rows[i]["KCFM"] != null && dt.Rows[i]["KCFM"].ToString() != "")
                {
                    int FIELID = int.Parse(dt.Rows[i]["KCFM"].ToString().Split(',')[0]);
                    FT_File file = new FT_FileB().GetEntities(d => d.ID == FIELID).FirstOrDefault();
                    dt.Rows[i]["fileurl"] = UserInfo.QYinfo.FileServerUrl + file.FileMD5;
                }
                else
                {
                    dt.Rows[i]["fileurl"] = "images/a_27.jpg";
                }

            }

            msg.Result = dt;

        }
        #endregion

        public void DELKCSC(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int kcId = 0;
            int.TryParse(P1, out kcId);
            new SZHL_PX_KCSCB().Delete(d => d.CRUser == UserInfo.User.UserName && d.KCID == kcId);
        }
        #endregion
        #region 课程统计
        public void GETKCCKTJ(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int page = 0;
            int.TryParse(context.Request.QueryString["p"] ?? "1", out page);//页码
            page = page == 0 ? 1 : page;
            int recordCount = 0;
            string strWhere = " where kcgl.ComId=" + UserInfo.User.ComId;
            if (P1 != "")
            {
                strWhere += string.Format(" and kcgl.KCTypeID=" + P1);
            }
            string content = context.Request["Content"] ?? "";
            if (content != "")
            {
                strWhere += string.Format(" and  ( kcgl.KCName  like '%{0}%' OR see.UserRealName  like '%{0}%' )", content);
            }

            DataTable dtuser = new JH_Auth_UserB().GetUserListbyBranch(UserInfo.BranchInfo.DeptCode, "", UserInfo.User.ComId.Value);
            string strUsers = "";
            for (int i = 0; i < dtuser.Rows.Count; i++)
            {
                strUsers = strUsers + dtuser.Rows[i]["UserName"].ToString() + ",";
            }
            string LikeString = strUsers.TrimEnd(',').ToFormatLike();
            if (LikeString != "")
            {
                strWhere += string.Format(" and  see.CRUser in ('{0}')", LikeString);
            }
            string strSql = string.Format(@"(SELECT kcgl.ID,kcgl.KCName,see.CRUser,see.UserRealName,kcgl.KSS,zd.KCFLName,ltrim(SUM(see.KCDuration)/3600)+':'+ltrim(SUM(see.KCDuration)%3600/60)+':'+ltrim(SUM(see.KCDuration)%60) totalSeconds,SUM(see.KCDuration) totalS
                                            from SZHL_PX_KCGL kcgl inner join SZHL_PX_KCFL zd on kcgl.KCTypeID=zd.ID
                                            inner join SZHL_PX_KJKC kjkc on kcgl.ID=kjkc.KCID
                                            inner join SZHL_PX_KJGL kjgl on kjgl.ID=kjkc.KJID
                                            INNER join SZHL_PX_SeeTime see on kjgl.ID=see.KCID {0}
                                            GROUP by kcgl.ID,kcgl.KCName,kcgl.KSS,zd.KCFLName,see.CRUser,see.UserRealName) as newtab", strWhere);
            int pagecount = 0;
            int.TryParse(context.Request.QueryString["pagecount"] ?? "8", out pagecount);//页码
            pagecount = pagecount == 0 ? 10 : pagecount;
            DataTable dt = new SZHL_GZBGB().GetDataPager(strSql, "* ", pagecount, page, " KCName ", " totalS>60", ref recordCount);

            dt.Columns.Add("istg");//去除管理员
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["istg"] = new SZHL_PX_GWKCB().isKSTG(dt.Rows[i]["ID"].ToString(), UserInfo.User.UserName);
            }

            msg.Result = dt;
            msg.Result1 = recordCount;

        }

        public void GETKCYHTJ(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int page = 0;
            int.TryParse(context.Request.QueryString["p"] ?? "1", out page);//页码
            page = page == 0 ? 1 : page;
            int recordCount = 0;
            string strWhere = " AND JH_Auth_User.ComId=" + UserInfo.User.ComId;

            string content = context.Request["Content"] ?? "";
            if (content != "")
            {
                strWhere += string.Format(" and  ( JH_Auth_User.UserRealName  like '%{0}%' OR  JH_Auth_Branch.DeptName  like '%{0}%' )", content.Trim());
            }


            string strSql = string.Format(@"( SELECT see.CRUser,JH_Auth_User.UserRealName,JH_Auth_Branch.DeptName,ltrim(SUM(see.KCDuration)/3600)+':'+ltrim(SUM(see.KCDuration)%3600/60)+':'+ltrim(SUM(see.KCDuration)%60) totalSeconds,
                                            SUM(see.KCDuration) totalS FROM SZHL_PX_SeeTime see LEFT JOIN JH_Auth_User  on see.CRUser=JH_Auth_User.UserName 
                                            LEFT JOIN JH_Auth_Branch on JH_Auth_User.BranchCode=JH_Auth_Branch.DeptCode WHERE see.KCDuration<>0 AND  JH_Auth_User.UserRealName is not NULL {0}
                                            GROUP by see.CRUser,JH_Auth_User.UserRealName,JH_Auth_Branch.DeptName
                                              ) as newtab", strWhere);
            int pagecount = 0;
            int.TryParse(context.Request.QueryString["pagecount"] ?? "8", out pagecount);//页码
            pagecount = pagecount == 0 ? 10 : pagecount;
            DataTable dt = new SZHL_GZBGB().GetDataPager(strSql, "* ", pagecount, page, " totalS ", " totalS>60", ref recordCount);

            msg.Result = dt;
            msg.Result1 = recordCount;

        }

        /// <summary>
        /// 导出客户
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1">客户信息</param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void EXPORTKH(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            try
            {
                int page = 0;
                int.TryParse(context.Request.QueryString["p"] ?? "1", out page);//页码
                page = page == 0 ? 1 : page;
                int recordCount = 0;
                string strWhere = " AND JH_Auth_User.ComId=" + UserInfo.User.ComId;

                string content = context.Request["Content"] ?? "";
                if (content != "")
                {
                    strWhere += string.Format(" and  ( JH_Auth_User.UserRealName  like '%{0}%' OR  JH_Auth_Branch.DeptName  like '%{0}%' )", content.Trim());
                }
                string strSql = string.Format(@"( SELECT see.CRUser,JH_Auth_User.UserRealName,JH_Auth_Branch.DeptName,ltrim(SUM(see.KCDuration)/3600)+':'+ltrim(SUM(see.KCDuration)%3600/60)+':'+ltrim(SUM(see.KCDuration)%60) totalSeconds,
                                            SUM(see.KCDuration) totalS FROM SZHL_PX_SeeTime see LEFT JOIN JH_Auth_User  on see.CRUser=JH_Auth_User.UserName 
                                            LEFT JOIN JH_Auth_Branch on JH_Auth_User.BranchCode=JH_Auth_Branch.DeptCode WHERE see.KCDuration<>0 AND  JH_Auth_User.UserRealName is not NULL {0}
                                            GROUP by see.CRUser,JH_Auth_User.UserRealName,JH_Auth_Branch.DeptName
                                              ) as newtab", strWhere);
                int pagecount = 0;
                int.TryParse(context.Request.QueryString["pagecount"] ?? "8", out pagecount);//页码
                pagecount = pagecount == 0 ? 10 : pagecount;
                DataTable dt = new SZHL_GZBGB().GetDataPager(strSql, "* ", 1000, page, " totalS ", " totalS>60", ref recordCount);
                msg.Result = dt;
                msg.Result1 = recordCount;

                CommonHelp ch = new CommonHelp();
                msg.ErrorMsg = ch.ExportToExcel("客户", dt);

            }
            catch
            {
                msg.ErrorMsg = "导出失败！";
            }
        }
        #endregion


        #region 新首页

        public void GETGWKCLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strZYSql = string.Format("SELECT DISTINCT KCTypeID, KCFLName  FROM SZHL_PX_GWKC INNER JOIN SZHL_PX_KCGL ON SZHL_PX_GWKC.KCId = SZHL_PX_KCGL.ID LEFT  JOIN SZHL_PX_KCFL ON SZHL_PX_KCGL.KCTypeID = SZHL_PX_KCFL.ID WHERE GWID={0} ", P1);
            DataTable dtZY = new SZHL_PX_GWKCB().GetDTByCommand(strZYSql);
            dtZY.Columns.Add("gwzyid", typeof(string));
            dtZY.Columns.Add("zykcdata", typeof(object));
            dtZY.Columns.Add("zyistg", typeof(object));

            for (int i = 0; i < dtZY.Rows.Count; i++)
            {
                string strKCName = dtZY.Rows[i]["KCFLName"].ToString();
                SZHL_PX_GWZY temp = new SZHL_PX_GWZYB().GetEntities(d => d.GW == P2 && d.ZY == strKCName).FirstOrDefault();
                if (temp != null)
                {
                    dtZY.Rows[i]["gwzyid"] = temp.ID;
                    dtZY.Rows[i]["zyistg"] = new SZHL_PX_GWKCB().isKSTG(temp.ID.ToString(), UserInfo.User.UserName);

                }
                else
                {
                    dtZY.Rows[i]["zyistg"] = "N";
                }
                DataTable dt = new SZHL_PX_GWKCB().GetDTByCommand(" SELECT SZHL_PX_GWKC.KCId AS ID,GWId,KCName,KCZJR,KSS,KCFM,SQType, KCTypeID, KCFLName,SZHL_PX_KCGL.CRDate,SZHL_KS_TK.KCID,SZHL_PX_KCGL.KJID,FF.FileMD5,'' AS looktime,'' as TotalTime,'' as isKS,'' as isKSTG,MAX(SZHL_KS_TK.ID) AS LXID FROM SZHL_PX_GWKC INNER JOIN SZHL_PX_KCGL ON SZHL_PX_GWKC.KCId = SZHL_PX_KCGL.ID LEFT  JOIN SZHL_PX_KCFL ON SZHL_PX_KCGL.KCTypeID = SZHL_PX_KCFL.ID LEFT  JOIN SZHL_KS_TK ON SZHL_PX_KCGL.ID = SZHL_KS_TK.KCID LEFT  JOIN FT_File FF ON SZHL_PX_KCGL.KCFM = FF.ID WHERE GWID=" + P1 + "  AND SQType='公开' AND KCTypeID=" + dtZY.Rows[i]["KCTypeID"].ToString() + " GROUP BY SZHL_PX_GWKC.KCId ,GWId,KCName,KCZJR,KSS,KCFM, SQType, KCTypeID, KCFLName,SZHL_PX_KCGL.CRDate,SZHL_KS_TK.KCID,SZHL_PX_KCGL.KJID , FF.FileMD5 ORDER BY SZHL_PX_KCGL.CRDate");
                dt.Columns.Add("fileurl");

                for (int m = 0; m < dt.Rows.Count; m++)
                {
                    dt.Rows[m]["looktime"] = new SZHL_PX_GWKCB().GetSeetime(dt.Rows[m]["ID"].ToString(), UserInfo.User.UserName);
                    dt.Rows[m]["TotalTime"] = new SZHL_PX_GWKCB().GetKCTotleTime(dt.Rows[m]["ID"].ToString());
                    string strSJSTID = new SZHL_KS_STB().GetSJSTID("", dt.Rows[m]["ID"].ToString());
                    dt.Rows[m]["isKS"] = strSJSTID.Split(',').Length == 20 ? "Y" : "N";
                    dt.Rows[m]["isKSTG"] = new SZHL_PX_GWKCB().isKSTG(dt.Rows[m]["ID"].ToString(), UserInfo.User.UserName);
                    if (dt.Rows[m]["FileMD5"] != null && dt.Rows[m]["FileMD5"].ToString() != "")
                    {
                        dt.Rows[m]["fileurl"] = UserInfo.QYinfo.FileServerUrl + dt.Rows[m]["FileMD5"].ToString();
                    }
                    else
                    {
                        dt.Rows[m]["fileurl"] = "images/a_27.jpg";
                    }


                }
                dtZY.Rows[i]["zykcdata"] = dt;



            }

            string isgwks = "0";
            DataTable dtList = new DataTable();
            dtList = new JH_Auth_TLB().GetDTByCommand(" SELECT * FROM SZHL_KS_SJ WHERE gwid='" + P1 + "' ");
            if (dtList.Rows.Count > 0)
            {
                isgwks = "1";
                string strGWTG = new SZHL_PX_GWKCB().isKSTG(P1, UserInfo.User.UserName);
                if (strGWTG == "Y")
                {
                    isgwks = "2";
                }
            }
            msg.Result1 = dtZY;
            msg.Result2 = isgwks;




        }

        public void GETGWZYKSLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strZYSql = string.Format("SELECT DISTINCT KCTypeID, KCFLName  FROM SZHL_PX_GWKC INNER JOIN SZHL_PX_KCGL ON SZHL_PX_GWKC.KCId = SZHL_PX_KCGL.ID LEFT  JOIN SZHL_PX_KCFL ON SZHL_PX_KCGL.KCTypeID = SZHL_PX_KCFL.ID WHERE GWID={0} ", P1);
            DataTable dtZY = new SZHL_PX_GWKCB().GetDTByCommand(strZYSql);
            dtZY.Columns.Add("gwzyid", typeof(string));
            dtZY.Columns.Add("gwzyksdata", typeof(string));//岗位专业组织考试数据
            dtZY.Columns.Add("iscanzzks");

            for (int i = 0; i < dtZY.Rows.Count; i++)
            {
                string strKCName = dtZY.Rows[i]["KCFLName"].ToString();
                SZHL_PX_GWZY temp = new SZHL_PX_GWZYB().GetEntities(d => d.GW == P2 && d.ZY == strKCName).FirstOrDefault();
                if (temp != null)
                {
                    dtZY.Rows[i]["gwzyid"] = temp.ID;
                    DataTable dtSJ = new SZHL_KS_SJB().GetDTByCommand("SELECT SZHL_KS_KSAP.*,SZHL_PX_GWZY.id FROM SZHL_KS_SJ INNER JOIN SZHL_PX_GWZY ON SZHL_KS_SJ.gwzyid=SZHL_PX_GWZY.ID INNER JOIN SZHL_KS_KSAP ON SZHL_KS_SJ.ID=SZHL_KS_KSAP.SJID WHERE  SZHL_PX_GWZY.ID='" + temp.ID + "' ");
                    dtZY.Rows[i]["gwzyksdata"] = dtSJ;
                }
                string strSJID = new SZHL_KS_STB().GetSJSTID(dtZY.Rows[i]["KCTypeID"].ToString(), "");
                dtZY.Rows[i]["iscanzzks"] = strSJID.Split(',').Length == 20 ? "Y" : "N";
            }
            int gwid = int.Parse(P1);
            DataTable dtgwSJ = new SZHL_KS_SJB().GetDTByCommand("SELECT SZHL_KS_KSAP.* FROM SZHL_KS_SJ INNER JOIN SZHL_PX_GWGL ON SZHL_KS_SJ.gwid=SZHL_PX_GWGL.ID INNER JOIN SZHL_KS_KSAP ON SZHL_KS_SJ.ID=SZHL_KS_KSAP.SJID  WHERE  SZHL_PX_GWGL.ID='" + gwid + "' ");
            msg.Result = dtgwSJ;
            msg.Result1 = dtZY;



        }
        public void JCBD(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string stropenid = UserInfo.User.openid;
            UserInfo.User.openid = "";
            new JH_Auth_UserB().Update(UserInfo.User);
            msg.Result = stropenid;
        }





        #endregion

    }

}