﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Practices.Unity;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;
using System.Web.SessionState;
using QjySaaSWeb.AppCode;
using QJY.Data;
using System.Diagnostics;

namespace QjySaaSWeb.API
{
    /// <summary>
    /// VIEWAPI 的摘要说明
    /// </summary>
    public class VIEWAPI : IHttpHandler, IRequiresSessionState
    {
        public string ComId { get; set; }

        public void ProcessRequest(HttpContext context)
        {
            Debug.WriteLine("START" + DateTime.Now);

            context.Response.ContentType = "text/plain";
            context.Response.AddHeader("pragma", "no-cache");
            context.Response.AddHeader("cache-control", "");
            context.Response.CacheControl = "no-cache";
            string strAction = context.Request["Action"] ?? "";
            string P1 = context.Request["P1"] ?? "";
            string P2 = context.Request["P2"] ?? "";
            string P3 = context.Request["P3"] ?? "";
            Msg_Result Model = new Msg_Result() { Action = strAction.ToUpper(), ErrorMsg = "" };
            if (!string.IsNullOrEmpty(strAction))
            {
                try
                {
                    //TODO: 未实现,分享页面的接口通过暂时将分享人的code传递给打开链接的人来调用接口(不安全)
                    #region 必须登录执行接口
                    Model.ErrorMsg = "";

                    var bl = true;
                    var acs = Model.Action.Split('_');
                    if (Model.Action.IndexOf("_") > 0)
                    {
                        if (acs[0].ToUpper() == "Commanage".ToUpper())
                        {
                            bl = false;
                            var container = ServiceContainerV.Current().Resolve<IWsService>(acs[0].ToUpper());
                            Model.Action = acs[1];
                            container.ProcessRequest(context, ref Model, P1, P2, new JH_Auth_UserB.UserInfo());
                        }
                    }
                    if (bl)
                    {
                        if (context.Request.Cookies["szhlcode"] != null && context.Request.Cookies["szhlcode"].ToString() != "")//如果存在TOKEN,根据TOKEN找到用户信息，并根据权限执行具体ACTION
                        {
                            string strSZHLCode = context.Request.Cookies["szhlcode"].Value;
                            //通过Code获取用户名，然后执行接口方法

                            var container = ServiceContainerV.Current().Resolve<IWsService>(acs[0].ToUpper());
                            JH_Auth_UserB.UserInfo UserInfo = new JH_Auth_UserB().GetUserInfo(strSZHLCode);
                            if (UserInfo != null)
                            {
                                new JH_Auth_LogB().InsertLog(Model.Action, "调用接口", context.Request.Url.AbsoluteUri, UserInfo.User.UserName, UserInfo.QYinfo.ComId);
                                Model.Action = Model.Action.Substring(acs[0].Length + 1);
                                container.ProcessRequest(context, ref Model, P1, P2, UserInfo);
                            }
                            else
                            {
                                Model.ErrorMsg = "NOSESSIONCODE";
                            }

                        }

                        if (context.Request.Cookies["openid"] != null && context.Request.Cookies["openid"].ToString() != "")//如果存在OPENID,根据OPENID找到用户信息，并根据权限执行具体ACTION
                        {
                            string stropenid = context.Request.Cookies["openid"].Value;
                            //通过openid获取用户名，然后执行接口方法

                            var container = ServiceContainerV.Current().Resolve<IWsService>(acs[0].ToUpper());
                            new JH_Auth_LogB().InsertLog(Model.Action, stropenid + "微信端调用接口", context.Request.Url.AbsoluteUri, "", 0);

                            var User = new JH_Auth_UserB().GetEntities(d => d.openid == stropenid);
                            if (User.Count() > 0)
                            {
                                JH_Auth_UserB.UserInfo UserInfo = new JH_Auth_UserB().GetUserInfoBYopenid(stropenid);
                                Model.Action = Model.Action.Substring(acs[0].Length + 1);
                                container.ProcessRequest(context, ref Model, P1, P2, UserInfo);
                                new JH_Auth_LogB().InsertLog(Model.Action, stropenid + "微信端调用接口", context.Request.Url.AbsoluteUri, UserInfo.User.UserName, UserInfo.QYinfo.ComId);
                            }
                            else
                            {
                                Model.ErrorMsg = "NOWXOPENID";
                            }


                        }
                    }
                    #endregion

                }
                catch (Exception ex)
                {
                    Model.ErrorMsg = "操作异常，请重试" + strAction;

                    Model.Result = ex.Message;
                    new JH_Auth_LogB().InsertLog(strAction, ex.Message.ToString() + ex.StackTrace, "", "", 0);

                }
            }
            string jsonpcallback = context.Request["jsonpcallback"] ?? "";

            IsoDateTimeConverter timeConverter = new IsoDateTimeConverter();
            timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";
            string Result = JsonConvert.SerializeObject(Model, Formatting.Indented, timeConverter).Replace("null", "\"\"");
            if (jsonpcallback != "")
            {
                Result = jsonpcallback + "(" + Result + ")";//支持跨域
            }
            context.Response.Write(Result);
            Debug.WriteLine("END" + DateTime.Now);

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}