﻿using QjySaaSWeb.AppCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using FastReflectionLib;
using System.Data;
using QJY.Data;
using Newtonsoft.Json;
using Senparc.Weixin.QY.Entities;
using Newtonsoft.Json.Linq;
using NPOI.SS.UserModel;
using System.Text;
using System.IO;
using NPOI.XSSF.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.SS.Formula.Eval;

namespace QjySaaSWeb.API
{
    public class XZGLManage : IWsService
    {
        public void ProcessRequest(HttpContext context, ref Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            MethodInfo methodInfo = typeof(XZGLManage).GetMethod(msg.Action.ToUpper());
            XZGLManage model = new XZGLManage();
            methodInfo.FastInvoke(model, new object[] { context, msg, P1, P2, UserInfo });
        }

        #region excel转换为table

        /// <summary>
        /// excel转换为table
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void EXCELTOTABLE(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            try
            {
                string str2 = "";
                DataTable dt = new DataTable();
                HttpPostedFile _upfile = context.Request.Files["upFile"];
                string headrow = context.Request["headrow"] ?? "0";//头部开始行下标
                if (_upfile == null)
                {
                    msg.ErrorMsg = "请选择要上传的文件 ";
                }
                else
                {
                    string fileName = _upfile.FileName;/*获取文件名： C:\Documents and Settings\Administrator\桌面\123.jpg*/
                    string suffix = fileName.Substring(fileName.LastIndexOf(".") + 1).ToLower();/*获取后缀名并转为小写： jpg*/
                    int bytes = _upfile.ContentLength;//获取文件的字节大小   
                    if (suffix == "xls" || suffix == "xlsx")
                    {
                        IWorkbook workbook = null;

                        Stream stream = _upfile.InputStream;

                        if (suffix == "xlsx") // 2007版本
                        {
                            workbook = new XSSFWorkbook(stream);
                        }
                        else if (suffix == "xls") // 2003版本
                        {
                            workbook = new HSSFWorkbook(stream);
                        }

                        //获取excel的第一个sheet
                        ISheet sheet = workbook.GetSheetAt(0);

                        //获取sheet的第一行
                        IRow headerRow = sheet.GetRow(int.Parse(headrow));

                        //一行最后一个方格的编号 即总的列数
                        int cellCount = headerRow.LastCellNum;
                        //最后一列的标号  即总的行数
                        int rowCount = sheet.LastRowNum;
                        if (rowCount <= int.Parse(headrow))
                        {
                            msg.ErrorMsg = "文件中无数据! ";
                        }
                        else
                        {
                            CommonHelp ch = new CommonHelp();
                            string[] yz = { "姓名", "手机" };
                            //列名
                            for (int i = 0; i < cellCount; i++)
                            {
                                string strlm = headerRow.GetCell(i).ToString().Trim();
                                if (string.IsNullOrWhiteSpace(strlm)) strlm = "第" + (i + 1) + "列";
                                dt.Columns.Add(strlm);//添加列名
                            }

                            #region 必填字段在文件中存不存在验证
                            foreach (var v in yz)
                            {
                                if (!dt.Columns.Contains(v))
                                {
                                    if (string.IsNullOrEmpty(str2))
                                    {
                                        str2 = "当前导入的必填字段：【" + v + "】";
                                    }
                                    else
                                    {
                                        str2 = str2 + "、【" + v + "】";
                                    }
                                }
                            }

                            if (!string.IsNullOrEmpty(str2))
                            {
                                str2 = str2 + " 在文件中不存在!<br>";
                            }
                            #endregion

                            for (int i = (sheet.FirstRowNum + int.Parse(headrow) + 1); i <= sheet.LastRowNum; i++)
                            {
                                DataRow dr = dt.NewRow();
                                bool bl = false;
                                IRow row = sheet.GetRow(i);
                                for (int j = row.FirstCellNum; j < cellCount; j++)
                                {
                                    string strsj = exportsheet(row.GetCell(j)).Trim();
                                    if (strsj != "")
                                    {
                                        bl = true;
                                    }
                                    dr[j] = strsj;
                                }
                                if (bl)
                                {
                                    dt.Rows.Add(dr);
                                }
                            }
                            msg.Result = dt; 
                            msg.ErrorMsg = str2;
                        }

                        sheet = null;
                        workbook = null;
                    }
                    else
                    {
                        msg.ErrorMsg = "请上传excel文件 ";
                    }
                }
            }
            catch (Exception)
            {
                msg.ErrorMsg = "导入失败！";
            }
        }

        private string exportsheet(ICell rowCell)
        {
            if (rowCell == null) return "";

            object shstring = "";
            switch (rowCell.CellType)
            {
                case CellType.Boolean:
                    shstring = Convert.ToString(rowCell.BooleanCellValue);
                    break;
                case CellType.Error:
                    shstring = ErrorEval.GetText(rowCell.ErrorCellValue);
                    break;
                case CellType.Formula:
                    switch (rowCell.CachedFormulaResultType)
                    {
                        case CellType.Boolean:
                            shstring = Convert.ToString(rowCell.BooleanCellValue);
                            break;
                        case CellType.Error:
                            shstring = ErrorEval.GetText(rowCell.ErrorCellValue);
                            break;
                        case CellType.Numeric:
                            shstring = Convert.ToString(rowCell.NumericCellValue);
                            break;
                        case CellType.String:
                            string strFORMULA = rowCell.StringCellValue;
                            if (strFORMULA != null && strFORMULA.Length > 0)
                            {
                                shstring = strFORMULA.ToString();
                            }
                            else
                            {
                                shstring = "";
                            }
                            break;
                        default:
                            shstring = "";
                            break;
                    }
                    break;
                case CellType.Numeric:
                    if (DateUtil.IsCellDateFormatted(rowCell))
                    {
                        shstring = DateTime.FromOADate(rowCell.NumericCellValue);
                    }
                    else
                    {
                        shstring = Convert.ToDouble(rowCell.NumericCellValue);
                    }
                    break;
                case CellType.String:
                    string str = rowCell.StringCellValue;
                    if (!string.IsNullOrEmpty(str))
                    {
                        shstring = Convert.ToString(str);
                    }
                    else
                    {
                        shstring = null;
                    }
                    break;
                default:
                    shstring = "";
                    break;
            }
            shstring = shstring == null ? "" : shstring;
            return shstring.ToString();
        }
        #endregion

 
        public void SENDZANMSG(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            var tx = JsonConvert.DeserializeObject<SZHL_TXSX>(P1);
            int msgid = Int32.Parse(tx.MsgID);

            UserInfo = new JH_Auth_UserB().GetUserInfo(tx.ComId.Value, tx.CRUser);

            var model = new SZHL_TSSQB().GetEntity(p => p.ID == msgid && p.ComId == UserInfo.User.ComId);
            if (model != null)
            {
                Article ar0 = new Article();
                ar0.Title = "";
                ar0.Description = "";
                ar0.Url = model.ID.ToString();
                List<Article> al = new List<Article>();
                al.Add(ar0);

                string jsr = string.Empty;
                jsr = model.CRUser;
                //发送消息
                if (!string.IsNullOrEmpty(jsr))
                {
                    WXHelp wx = new WXHelp(UserInfo.QYinfo, "TSSQ");
                    wx.SendTH(al, "TSSQ", "A", jsr);
                }
            }
        } 
 
    }
}