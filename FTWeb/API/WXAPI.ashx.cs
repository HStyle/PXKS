﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Practices.Unity;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;
using System.Web.SessionState;
using System.IO;
using System.Xml;
using Senparc.Weixin;
using Senparc.Weixin.Entities;
using Senparc.Weixin.QY;
using Senparc.Weixin.QY.AdvancedAPIs;
using QjySaaSWeb.AppCode;
using QJY.Data;
using Senparc.Weixin.QY.AdvancedAPIs.ThirdPartyAuth;
using System.Data;
using Senparc.Weixin.Helpers;
using Senparc.Weixin.HttpUtility;
using System.Net;
using System.Text;
using System.Configuration;
namespace QjySaaSWeb.API
{
    /// <summary>
    /// WXAPI 的摘要说明
    /// </summary>
    public class WXAPI : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            context.Response.AddHeader("pragma", "no-cache");
            context.Response.AddHeader("cache-control", "");
            context.Response.CacheControl = "no-cache";
            string strAction = context.Request["Action"] ?? "";

            Msg_Result Model = new Msg_Result() { Action = strAction.ToUpper(), ErrorMsg = "" };

            if (!string.IsNullOrEmpty(strAction))
            {
                #region 获取预授权码
                if (strAction == "GETYSQM")
                {
                    string strTJID = context.Request["TJID"] ?? "";
                    try
                    {
                        int id = Int32.Parse(strTJID);
                        var pj = new JH_Auth_WXPJB().GetEntity(p => p.ID == id);

                        string SuiteToken = ThirdPartyAuthApi.GetSuiteToken(pj.TJID, pj.TJSecret, pj.Ticket).suite_access_token;

                        int[] appid = { };
                        string PreAuthCode = ThirdPartyAuthApi.GetPreAuthCode(SuiteToken, pj.TJID, appid).pre_auth_code;

                        Model.Result = PreAuthCode;
                        Model.Result1 = pj;
                    }
                    catch (Exception ex)
                    {
                        Model.ErrorMsg = ex.ToString();
                    }
                }
                #endregion
                #region 获取永久授权码
                else if (strAction == "GETYJSQ")
                {
                    string strauth_code = context.Request["auth_code"] ?? "";
                    string strTJID = context.Request["TJID"] ?? "";
                    string strComID = context.Request["ComID"] ?? "";
                    var pj = new JH_Auth_WXPJB().GetEntity(p => p.TJID == strTJID);

                    try
                    {
                        string SuiteToken = ThirdPartyAuthApi.GetSuiteToken(pj.TJID, pj.TJSecret, pj.Ticket).suite_access_token;

                        //Senparc.Weixin.QY.AdvancedAPIs.ThirdPartyAuth.GetPermanentCodeResult gpcr = ThirdPartyAuthApi.GetPermanentCode(SuiteToken, strTJID, strauth_code);

                        //GetAuthInfoResult gair = ThirdPartyAuthApi.GetAuthInfo(SuiteToken, strTJID, gpcr.auth_corp_info.corpid, gpcr.permanent_code);

                        var url = string.Format("https://qyapi.weixin.qq.com/cgi-bin/service/get_permanent_code?suite_access_token={0}", SuiteToken);

                        var data = new
                        {
                            suite_id = strTJID,
                            auth_code = strauth_code
                        };

                        QjySaaSWeb.API.GetPermanentCodeResult gpcr = Senparc.Weixin.CommonAPIs.CommonJsonSend.Send<QjySaaSWeb.API.GetPermanentCodeResult>(null, url, data, CommonJsonSendType.POST);


                        int cid = Int32.Parse(strComID);

                        bool bl = true;
                        JH_Auth_QY jaq1 = new JH_Auth_QYB().GetEntity(p => p.corpId == gpcr.auth_corp_info.corpid);
                        if (jaq1 != null)
                        {
                            if (jaq1.ComId != cid)
                            {
                                bl = false;
                            }

                        }
                        if (bl)
                        {
                            JH_Auth_QY jaq = new JH_Auth_QYB().GetQYByComID(cid);
                            jaq.corpId = gpcr.auth_corp_info.corpid;
                            jaq.wxqrcode = gpcr.auth_corp_info.corp_wxqrcode;
                            jaq.IsUseWX = "Y";//安装应用成功更新是否启用微信状态
                            new JH_Auth_QYB().Update(jaq);

                            JH_Auth_QY_TJ jaqt1 = new JH_Auth_QY_TJB().GetEntity(p => p.ComId == cid && p.CorpID == gpcr.auth_corp_info.corpid && p.TJID == pj.TJID);
                            if (jaqt1 == null)
                            {
                                JH_Auth_QY_TJ jaqt = new JH_Auth_QY_TJ();
                                jaqt.ComId = cid;
                                jaqt.CorpID = gpcr.auth_corp_info.corpid;
                                jaqt.PermanentCode = gpcr.permanent_code;
                                jaqt.TJID = pj.TJID;

                                new JH_Auth_QY_TJB().Insert(jaqt);
                            }
                            else
                            {
                                jaqt1.PermanentCode = gpcr.permanent_code;
                                new JH_Auth_QY_TJB().Update(jaqt1);
                            }

                            if (strTJID == "tj7882b1f8bc56f05f")
                            {
                                var jaqm = new JH_Auth_QY_ModelB().GetEntities(" ComId='" + jaq.ComId + "' and QYModelCode ='QYIM'").FirstOrDefault();
                                if (jaqm != null)
                                {
                                    jaqm.AgentId = "QYIM";
                                    new JH_Auth_QY_ModelB().Update(jaqm);
                                }
                            }

                            //foreach (var model in gair.auth_info.agent)
                            foreach (var model in gpcr.auth_info.agent)
                            {

                                var m = new JH_Auth_ModelB().GetEntity(p => p.AppID == model.appid && p.TJId == pj.TJID);
                                var qm = new JH_Auth_QY_ModelB().GetEntity(p => p.ComId == cid && p.ModelID == m.ID);
                                if (qm == null)
                                {
                                    JH_Auth_QY_Model jaqm = new JH_Auth_QY_Model();
                                    jaqm.ComId = cid;
                                    jaqm.ModelID = m.ID;
                                    jaqm.Status = "1";
                                    jaqm.AgentId = model.agentid;
                                    //jaqm.CRUser = UserInfo.User.UserName;
                                    jaqm.CRDate = DateTime.Now;
                                    jaqm.PDID = 0;
                                    new JH_Auth_QY_ModelB().Insert(jaqm);
                                }
                                else
                                {
                                    qm.Status = "1";
                                    qm.AgentId = model.agentid;
                                    new JH_Auth_QY_ModelB().Update(qm);
                                }
                                if (m.AppType == "1")
                                {
                                    WXHelp wx = new WXHelp(jaq, m.ModelCode);
                                    int agentId = Int32.Parse(model.agentid);
                                    wx.WX_WxCreateMenuNew(agentId, gpcr.access_token);

                                    var url1 = string.Format("https://qyapi.weixin.qq.com/cgi-bin/agent/set?access_token={0}", wx.GetToken());
                                    var data1 = new
                                    {
                                        agentid = model.agentid,
                                        isreportenter = 1
                                    };

                                    QyJsonResult ret = Senparc.Weixin.CommonAPIs.CommonJsonSend.Send<QyJsonResult>(null, url1, data1, CommonJsonSendType.POST);
                                }
                            }
                        }
                        else
                        {
                            Model.ErrorMsg = "此企业号已经绑定到其他企业！";
                        }
                        //}
                        //else
                        //{
                        //    Model.ErrorMsg = "登陆超时,需要重新登陆";
                        //}

                        //}




                        //Model.Result = gpcr;
                        //Model.Result1 = gair;
                    }
                    catch (Exception ex)
                    {
                        Model.ErrorMsg = ex.ToString();

                        try
                        {
                            new JH_Auth_LogB().Insert(new JH_Auth_Log()
                            {
                                LogType = Model.Action,
                                LogContent = ex.ToString(),
                                Remark = context.Request.Url.AbsoluteUri,
                                //CRUser = UserInfo.User.UserName,
                                CRDate = DateTime.Now
                            });
                        }
                        catch { }

                    }
                }
                #endregion
                #region 企业号应用callback
                else if (strAction == "XXJS")
                {
                    String strCorpID = context.Request["corpid"] ?? "";
                    string strCode = context.Request["Code"] ?? "";
                    try
                    {
                        JH_Auth_QY jaq = new JH_Auth_QYB().GetEntity(p => p.corpId == strCorpID);
                        JH_Auth_Model jam = new JH_Auth_ModelB().GetEntity(p => p.ModelCode == strCode);
                        if (jaq != null && jam != null && !string.IsNullOrEmpty(jam.TJId))
                        {
                            if (HttpContext.Current.Request.HttpMethod.ToUpper() == "POST")
                            {
                                //string corpId = WXHelp.corpId;//"wxf285796b249c0cf6";

                                string signature = HttpContext.Current.Request.QueryString["msg_signature"];//企业号的 msg_signature
                                string timestamp = HttpContext.Current.Request.QueryString["timestamp"];
                                string nonce = HttpContext.Current.Request.QueryString["nonce"];

                                // 获得客户端RAW HttpRequest  
                                StreamReader srResult = new StreamReader(context.Request.InputStream);
                                string str = srResult.ReadToEnd();
                                XmlDocument XmlDocument = new XmlDocument();
                                XmlDocument.LoadXml(HttpContext.Current.Server.UrlDecode(str));
                                string ToUserName = string.Empty;
                                string strde = string.Empty;
                                string msgtype = string.Empty;//微信响应类型
                                foreach (XmlNode xn in XmlDocument.ChildNodes[0].ChildNodes)
                                {
                                    if (xn.Name == "ToUserName")
                                    {
                                        ToUserName = xn.InnerText;
                                    }
                                }
                                var pj = new JH_Auth_WXPJB().GetEntity(p => p.TJID == jam.TJId);
                                Tencent.WXBizMsgCrypt wxcpt = new Tencent.WXBizMsgCrypt(pj.Token, pj.EncodingAESKey, ToUserName);
                                int n = wxcpt.DecryptMsg(signature, timestamp, nonce, str, ref strde);
                                XmlDocument XmlDocument1 = new XmlDocument();
                                XmlDocument1.LoadXml(HttpContext.Current.Server.UrlDecode(strde));
                                foreach (XmlNode xn1 in XmlDocument1.ChildNodes[0].ChildNodes)
                                {
                                    if (xn1.Name == "MsgType")
                                    {
                                        msgtype = xn1.InnerText;
                                    }
                                    //CommonHelp.WriteLOG(XmlDocument1.OuterXml);

                                }
                                if (msgtype == "event")//处理事件
                                {
                                    //需要处理进入应用的菜单更改事件
                                    string strEvent = XmlDocument1.ChildNodes[0]["Event"].InnerText;
                                    string strUserName = XmlDocument1.ChildNodes[0]["FromUserName"].InnerText;
                                    string strAgentID = XmlDocument1.ChildNodes[0]["AgentID"].InnerText;
                                    string strEventKey = XmlDocument1.ChildNodes[0]["EventKey"].InnerText;

                                    if (strEvent.ToLower() == "enter_agent" || strEvent.ToLower() == "view")
                                    {
                                        JH_Auth_User jau = new JH_Auth_UserB().GetEntity(p => p.ComId == jaq.ComId && p.UserName == strUserName);
                                        JH_Auth_QY_Model jhqm = new JH_Auth_QY_ModelB().GetEntity(p => p.ComId == jaq.ComId && p.AgentId == strAgentID);
                                        if (jau != null && jhqm != null)
                                        {
                                            JH_Auth_YYLog jay = new JH_Auth_YYLog();
                                            jay.ComId = jaq.ComId;
                                            jay.AgentID = strAgentID;
                                            jay.CorpID = strCorpID;
                                            jay.CRDate = DateTime.Now;
                                            jay.CRUser = strUserName;
                                            jay.Event = strEvent;
                                            jay.EventKey = strEventKey;
                                            jay.ModelCode = strCode;
                                            jay.ModelID = jhqm.ModelID;
                                            jay.QYName = jaq.QYName;
                                            jay.TJID = jam.TJId;
                                            jay.Type = msgtype;
                                            jay.UserName = strUserName;
                                            jay.UserRealName = jau.UserRealName;

                                            new JH_Auth_YYLogB().Insert(jay);

                                            if (strEvent.ToLower() == "enter_agent")
                                            {
                                                //if (!string.IsNullOrEmpty(jhqm.AgentId))
                                                //{
                                                //    WXHelp wx = new WXHelp(jaq, strCode);
                                                //    int agentId = Int32.Parse(jhqm.AgentId);
                                                //    wx.WX_WxCreateMenuNew(agentId, wx.GetToken());
                                                //}
                                                //首次进入应用事件
                                                var jays = new JH_Auth_YYLogB().GetEntities(p => p.ComId == jaq.ComId && p.Event == "enter_agent" && p.AgentID == strAgentID && p.CRUser == strUserName);
                                                if (jays.Count() <= 1)
                                                { 
                                                }
                                            }
                                        }
                                        
                                    }
                                    
                                }
                                if (new List<string> { "text", "image", "voice", "video", "shortvideo", "link" }.Contains(msgtype))//处理消息事件
                                {

                                    if (XmlDocument1.ChildNodes.Count > 0)
                                    {
                                        JH_Auth_WXMSG wxmsgModel = new JH_Auth_WXMSG();
                                        wxmsgModel.AgentID = int.Parse(XmlDocument1.ChildNodes[0]["AgentID"].InnerText);
                                        wxmsgModel.ComId = jaq.ComId;
                                        wxmsgModel.ToUserName = XmlDocument1.ChildNodes[0]["ToUserName"].InnerText;
                                        wxmsgModel.FromUserName = XmlDocument1.ChildNodes[0]["FromUserName"].InnerText;
                                        wxmsgModel.CRDate = DateTime.Now;
                                        wxmsgModel.CRUser = XmlDocument1.ChildNodes[0]["FromUserName"].InnerText;
                                        wxmsgModel.MsgId = XmlDocument1.ChildNodes[0]["MsgId"].InnerText;
                                        wxmsgModel.MsgType = msgtype;
                                        wxmsgModel.ModeCode = strCode;
                                        wxmsgModel.Tags = "微信收藏";

                                        switch (msgtype)
                                        {
                                            case "text":
                                                wxmsgModel.MsgContent = XmlDocument1.ChildNodes[0]["Content"].InnerText;
                                                break;
                                            case "image":
                                                wxmsgModel.PicUrl = XmlDocument1.ChildNodes[0]["PicUrl"].InnerText;
                                                wxmsgModel.MediaId = XmlDocument1.ChildNodes[0]["MediaId"].InnerText;
                                                break;
                                            case "voice":
                                                wxmsgModel.MediaId = XmlDocument1.ChildNodes[0]["MediaId"].InnerText;
                                                wxmsgModel.Format = XmlDocument1.ChildNodes[0]["Format"].InnerText;
                                                break;
                                            case "video":
                                                wxmsgModel.MediaId = XmlDocument1.ChildNodes[0]["MediaId"].InnerText;
                                                wxmsgModel.ThumbMediaId = XmlDocument1.ChildNodes[0]["ThumbMediaId"].InnerText;
                                                break;
                                            case "shortvideo":
                                                wxmsgModel.MediaId = XmlDocument1.ChildNodes[0]["MediaId"].InnerText;
                                                wxmsgModel.ThumbMediaId = XmlDocument1.ChildNodes[0]["ThumbMediaId"].InnerText;
                                                break;
                                            case "link":
                                                wxmsgModel.Description = XmlDocument1.ChildNodes[0]["Description"].InnerText;
                                                wxmsgModel.Title = XmlDocument1.ChildNodes[0]["Title"].InnerText;
                                                wxmsgModel.URL = XmlDocument1.ChildNodes[0]["Url"].InnerText;
                                                wxmsgModel.PicUrl = XmlDocument1.ChildNodes[0]["PicUrl"].InnerText;
                                                break;
                                        }
                                        if (new List<string>() { "link", "text" }.Contains(msgtype))
                                        {
                                            if (msgtype == "link")
                                            {
                                                var jaw = new JH_Auth_WXMSGB().GetEntity(p => p.ComId == jaq.ComId && p.MsgId == wxmsgModel.MsgId);
                                                if (jaw == null)
                                                {
                                                    string strMedType = ".jpg";
                                                    JH_Auth_UserB.UserInfo UserInfo = new JH_Auth_UserB.UserInfo();
                                                    UserInfo = new JH_Auth_UserB().GetUserInfo(jaq.ComId, wxmsgModel.FromUserName);
                                                    string fileID = CommonHelp.ProcessWxIMGUrl(wxmsgModel.PicUrl, UserInfo, strMedType);

                                                    wxmsgModel.FileId = fileID;
                                                    new JH_Auth_WXMSGB().Insert(wxmsgModel);

                                                    if (strCode == "TSSQ")
                                                    {
                                                        SZHL_TXSX tx1 = new SZHL_TXSX();
                                                        tx1.ComId = jaq.ComId;
                                                        tx1.APIName = "TSSQ";
                                                        tx1.MsgID = wxmsgModel.ID.ToString();
                                                        tx1.FunName = "SENDWXMSG";
                                                        tx1.Date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                                        tx1.CRUser = wxmsgModel.CRUser;
                                                        tx1.CRDate = DateTime.Now;
                                                        TXSX.TXSXAPI.AddALERT(tx1); //时间为发送时间
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                new JH_Auth_WXMSGB().Insert(wxmsgModel);
                                            }
                                        }
                                        if (!string.IsNullOrEmpty(wxmsgModel.MediaId))
                                        {
                                            var jaw = new JH_Auth_WXMSGB().GetEntity(p => p.ComId == jaq.ComId && p.MediaId == wxmsgModel.MediaId);
                                            if (jaw == null)
                                            {
                                                string strMedType = ".jpg";
                                                if (strCode == "QYWD" || strCode == "CRM")//判断模块
                                                {
                                                    if (msgtype == "shortvideo" || msgtype == "video")//视频,小视频
                                                    {
                                                        strMedType = ".mp4";
                                                    }
                                                    if (new List<string>() { "image", "shortvideo", "video", "voice" }.Contains(msgtype))//下载到本地服务器
                                                    {
                                                        JH_Auth_UserB.UserInfo UserInfo = new JH_Auth_UserB.UserInfo();
                                                        UserInfo = new JH_Auth_UserB().GetUserInfo(jaq.ComId, wxmsgModel.FromUserName);
                                                        string fileID = CommonHelp.ProcessWxIMG(wxmsgModel.MediaId, strCode, UserInfo, strMedType);
                                                        wxmsgModel.FileId = fileID;
                                                        new JH_Auth_WXMSGB().Insert(wxmsgModel);

                                                    }
                                                   
                                                }
                                                //CommonHelp.WriteLOG("1");
                                                #region CRM
                                                if (strCode == "CRM")
                                                {
                                                    //名片识别
                                                    if (!string.IsNullOrEmpty(wxmsgModel.PicUrl))
                                                    {
                                                        try
                                                        {
                                                            string url = ConfigurationManager.AppSettings["CADEAPI"] ?? "";
                                                            if (url != "")
                                                            {
                                                                string verb = "POST";
                                                                HttpWebRequest req = WebRequest.Create(url) as HttpWebRequest;

                                                                req.Timeout = 60 * 1000;
                                                                //req.Headers.Add("Accept-Encoding", "gzip,deflate");
                                                                req.UserAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.110 Safari/537.36";

                                                                req.Method = verb;

                                                                string strData = "image=" + wxmsgModel.PicUrl;
                                                                //string strData = "[{\"image\":"+wxmsgModel.PicUrl+"}]" ;


                                                                try
                                                                {
                                                                    //CommonHelp.WriteLOG("2");
                                                                    if (verb == "POST")
                                                                    {
                                                                        byte[] data = Encoding.UTF8.GetBytes(strData);

                                                                        //req.ContentType = "application/text; charset=utf-8";
                                                                        req.ContentType = "application/x-www-form-urlencoded";
                                                                        req.ContentLength = data.Length;

                                                                        Stream requestStream = req.GetRequestStream();
                                                                        requestStream.Write(data, 0, data.Length);
                                                                        requestStream.Close();
                                                                    }


                                                                    using (var res = req.GetResponse())
                                                                    {
                                                                        using (var stream = res.GetResponseStream())
                                                                        {
                                                                            StreamReader sr = new StreamReader(stream, Encoding.UTF8);
                                                                            string response = sr.ReadToEnd();

                                                                            sr.Close();

                                                                            //CommonHelp.WriteLOG("3");

                                                                            SZHL_CRM_CARD scc = JsonConvert.DeserializeObject<SZHL_CRM_CARD>(response);
                                                                            scc.ComId = jaq.ComId;
                                                                            scc.Files = wxmsgModel.FileId;
                                                                            scc.Status = "0";
                                                                            scc.Del = 0;
                                                                            scc.CRDate = DateTime.Now;
                                                                            scc.CRUser = wxmsgModel.CRUser;
                                                                            new SZHL_CRM_CARDB().Insert(scc);
                                                                            //CommonHelp.WriteLOG(response);
                                                                            //return response;

                                                                            SZHL_TXSX tx1 = new SZHL_TXSX();
                                                                            tx1.ComId = jaq.ComId;
                                                                            tx1.APIName = "CRM";
                                                                            tx1.MsgID = scc.ID.ToString();
                                                                            tx1.FunName = "SENDWXMSG_MP";
                                                                            tx1.Date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                                                            tx1.CRUser = scc.CRUser;
                                                                            tx1.CRDate = DateTime.Now;
                                                                            TXSX.TXSXAPI.AddALERT(tx1); //时间为发送时间
                                                                        }
                                                                    }
                                                                }
                                                                catch (Exception ex)
                                                                {
                                                                    CommonHelp.WriteLOG(ex.ToString());
                                                                }
                                                                //return null;
                                                            }
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            CommonHelp.WriteLOG(ex.ToString());
                                                        }
                                                    }
                                                } 
                                                #endregion
                                            }
                                        }
                                    }
                                }
                            }

                            if (HttpContext.Current.Request.HttpMethod.ToUpper() == "GET")
                            {
                                var pj = new JH_Auth_WXPJB().GetEntity(p => p.TJID == jam.TJId);
                                Auth(pj.Token, pj.EncodingAESKey, "wx102a8aac04b33c39");
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        Model.ErrorMsg = ex.ToString();
                        CommonHelp.WriteLOG(ex.ToString());
                    }
                    
                }
                #endregion

                #region 微信扫码登录
                else if (strAction == "GETWXSMDL")
                {
                    string strCID = context.Request["corp_id"] ?? "";
                    string strAID = context.Request["auth_code"] ?? "";
                    string strPsc = ConfigurationManager.AppSettings["providersecret"] ?? "";
                    try
                    {
                        var url = "https://qyapi.weixin.qq.com/cgi-bin/service/get_provider_token";

                        var data = new
                        {
                            corpid = strCID,
                            provider_secret = strPsc
                        };

                        QjySaaSWeb.API.GetProviderToken Token = Senparc.Weixin.CommonAPIs.CommonJsonSend.Send<QjySaaSWeb.API.GetProviderToken>(null, url, data, CommonJsonSendType.POST); ;

                        Senparc.Weixin.QY.AdvancedAPIs.LoginAuth.GetLoginInfoResult user = LoginAuthApi.GetLoginInfo(Token.provider_access_token, strAID);

                        JH_Auth_QY jaq = new JH_Auth_QYB().GetEntity(p=>p.corpId==user.corp_info.corpid);
                        JH_Auth_User jau = new JH_Auth_UserB().GetEntity(p => p.ComId == jaq.ComId && p.UserName == user.user_info.userid);
                        if (jau != null)
                        {
                            if (string.IsNullOrEmpty(jau.pccode))
                            {
                                jau.pccode = System.Guid.NewGuid().ToString();
                            }

                            jau.logindate = DateTime.Now;
                            new JH_Auth_UserB().Update(jau);
                            Model.Result = jau.pccode;
                            Model.Result1 = jau.UserName;

                            try
                            {
                                new JH_Auth_LogB().Insert(new JH_Auth_Log()
                                {
                                    ComId = jaq.ComId.ToString(),
                                    LogType = "PCLOGIN",
                                    LogContent = "用户" + jau.UserRealName + "登录，登录时间:" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " 登录地点:" + CommonHelp.getIpAddr(),
                                    CRUser = jau.UserName,
                                    CRDate = DateTime.Now
                                });
                            }
                            catch { }
                        }
                    }
                    catch (Exception ex)
                    {
                        Model.ErrorMsg = ex.ToString();
                    }
                }
                #endregion

                #region 企业会话
                else if (strAction == "QYIM")
                {
                    if (HttpContext.Current.Request.HttpMethod.ToUpper() == "POST")
                    {
                        string corpId = context.Request["corpid"] ?? "";

                        try
                        {
                            JH_Auth_QY jaq = new JH_Auth_QYB().GetEntity(p => p.corpId == corpId);
                            if (jaq != null)
                            {
                                string signature = HttpContext.Current.Request.QueryString["msg_signature"];//企业号的 msg_signature
                                string timestamp = HttpContext.Current.Request.QueryString["timestamp"];
                                string nonce = HttpContext.Current.Request.QueryString["nonce"];

                                // 获得客户端RAW HttpRequest  
                                StreamReader srResult = new StreamReader(context.Request.InputStream);
                                string str = srResult.ReadToEnd();

                                string strde = string.Empty;

                                var pj = new JH_Auth_WXPJB().GetEntity(p => p.TJID == "tj7882b1f8bc56f05f");

                                Tencent.WXBizMsgCrypt wxcpt = new Tencent.WXBizMsgCrypt(pj.Token, pj.EncodingAESKey, corpId);

                                wxcpt.DecryptMsg(signature, timestamp, nonce, str, ref strde);

                                //string strde = HttpContext.Current.Request.QueryString[0];

                                XmlDocument XmlDocument = new XmlDocument();
                                XmlDocument.LoadXml(HttpContext.Current.Server.UrlDecode(strde));

                                string AgentType = string.Empty;
                                string ToUserName = string.Empty;
                                string ItemCount = string.Empty;
                                string PackageId = string.Empty;
                                string Item = string.Empty;

                                #region XML文档处理
                                foreach (XmlNode xn in XmlDocument.ChildNodes[0].ChildNodes)
                                {
                                    if (xn.Name == "AgentType")
                                    {
                                        AgentType = xn.InnerText;
                                    }
                                    if (xn.Name == "ToUserName")
                                    {
                                        ToUserName = xn.InnerText;
                                    }
                                    if (xn.Name == "ItemCount")
                                    {
                                        ItemCount = xn.InnerText;
                                    }
                                    if (xn.Name == "PackageId")
                                    {
                                        PackageId = xn.InnerText;
                                    }
                                    if (xn.Name == "Item")
                                    {
                                        Item += xn.InnerXml;

                                        string MsgType = xn.ChildNodes[2].InnerText;

                                        if (MsgType == "event")
                                        {
                                            #region event处理
                                            SZHL_QYIM zj = new SZHL_QYIM();
                                            
                                            //try
                                            //{
                                            //    zj.FromUserName = xn.ChildNodes[0]["FromUserName"].InnerText;
                                            //    zj.MsgType = xn.ChildNodes[0]["MsgType"].InnerText;
                                            //    zj.Event = xn.ChildNodes[0]["Event"].InnerText;
                                            //    zj.ChatId = xn.ChildNodes[0]["ChatId"].InnerText;
                                            //    zj.Name = xn.ChildNodes[0]["Name"].InnerText;
                                            //    zj.Owner = xn.ChildNodes[0]["Owner"].InnerText;
                                            //    zj.UserList = xn.ChildNodes[0]["UserList"].InnerText;
                                            //    zj.AddUserList = xn.ChildNodes[0]["AddUserList"].InnerText;
                                            //    zj.DelUserList = xn.ChildNodes[0]["DelUserList"].InnerText;

                                            //    DateTime time = DateTime.MinValue;
                                            //    DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1));
                                            //    time = startTime.AddSeconds(int.Parse(xn.ChildNodes[0]["CreateTime"].InnerText));

                                            //    zj.CreateTime = time;
                                            //}
                                            //catch { }

                                            
                                            foreach (XmlNode xnc in xn.ChildNodes)
                                            {
                                                if (xnc.Name == "FromUserName")
                                                {
                                                    zj.FromUserName = xnc.InnerText;
                                                }
                                                if (xnc.Name == "MsgType")
                                                {
                                                    zj.MsgType = xnc.InnerText;
                                                }
                                                if (xnc.Name == "Event")
                                                {
                                                    zj.Event = xnc.InnerText;
                                                }
                                                if (xnc.Name == "ChatId")
                                                {
                                                    zj.ChatId = xnc.InnerText;
                                                }
                                                if (xnc.Name == "Name")
                                                {
                                                    zj.Name = xnc.InnerText;
                                                }
                                                if (xnc.Name == "Owner")
                                                {
                                                    zj.Owner = xnc.InnerText;
                                                }
                                                if (xnc.Name == "UserList")
                                                {
                                                    zj.UserList = xnc.InnerText;
                                                }
                                                if (xnc.Name == "AddUserList")
                                                {
                                                    zj.AddUserList = xnc.InnerText;
                                                }
                                                if (xnc.Name == "DelUserList")
                                                {
                                                    zj.DelUserList = xnc.InnerText;
                                                }
                                                if (xnc.Name == "CreateTime")
                                                {
                                                    DateTime time = DateTime.MinValue;
                                                    DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1));
                                                    time = startTime.AddSeconds(int.Parse(xnc.InnerText));

                                                    zj.CreateTime = time;
                                                }
                                                if (xnc.ChildNodes.Count > 1)
                                                {
                                                    foreach (XmlNode xncn in xnc.ChildNodes)
                                                    {
                                                        if (xncn.Name == "ChatId")
                                                        {
                                                            zj.ChatId = xncn.InnerText;
                                                        }
                                                        if (xncn.Name == "Name")
                                                        {
                                                            zj.Name = xncn.InnerText;
                                                        }
                                                        if (xncn.Name == "Owner")
                                                        {
                                                            zj.Owner = xncn.InnerText;
                                                        }
                                                        if (xncn.Name == "UserList")
                                                        {
                                                            zj.UserList = xncn.InnerText;
                                                        }
                                                    }
                                                }
                                            }
                                            zj.Sourse = "1";
                                            zj.Status = "0";
                                            zj.ComId = jaq.ComId;
                                            new SZHL_QYIMB().Insert(zj);
                                            if (zj.Event == "create_chat")
                                            {
                                                SZHL_QYIM_LIST sql = new SZHL_QYIM_LIST();
                                                sql.ChatId = zj.ChatId;
                                                sql.FromUserName = zj.FromUserName;
                                                sql.MsgType = "group";
                                                sql.Name = zj.Name;
                                                sql.Owner = zj.Owner;
                                                sql.Sourse = "1";
                                                sql.Status = "0";
                                                sql.UserList = zj.UserList;
                                                sql.ComId = jaq.ComId;
                                                new SZHL_QYIM_LISTB().Insert(sql);
                                            }
                                            if (zj.Event == "update_chat")
                                            {
                                                SZHL_QYIM_LIST sql1 = new SZHL_QYIM_LISTB().GetEntities(p => p.ComId == jaq.ComId && p.ChatId == zj.ChatId && p.Status == "0").FirstOrDefault();
                                                if (sql1 != null)
                                                {
                                                    if (!string.IsNullOrEmpty(zj.Name))
                                                    {
                                                        sql1.Name = zj.Name;
                                                    }
                                                    if (!string.IsNullOrEmpty(zj.Owner))
                                                    {
                                                        sql1.Owner = zj.Owner;
                                                    }
                                                    if (!string.IsNullOrEmpty(zj.AddUserList))
                                                    {
                                                        sql1.UserList = sql1.UserList + "|" + zj.AddUserList;
                                                    }
                                                    if (!string.IsNullOrEmpty(zj.DelUserList))
                                                    {
                                                        string[] dul=zj.DelUserList.Split('|');
                                                        if (!string.IsNullOrEmpty(sql1.UserList))
                                                        {
                                                            string uis=string.Empty;
                                                            string[] strs = sql1.UserList.Split('|');
                                                            foreach (string s in strs)
                                                            {
                                                                bool bl = true;
                                                                foreach (string d in dul)
                                                                {
                                                                    if (s == d)
                                                                    {
                                                                        bl = false;
                                                                    }
                                                                }
                                                                if (bl)
                                                                {
                                                                    if (string.IsNullOrEmpty(uis))
                                                                    {
                                                                        uis = s;
                                                                    }
                                                                    else {
                                                                        uis = uis + "|" + s;
                                                                    }
                                                                }
                                                            }
                                                            sql1.UserList = uis;
                                                        }
                                                    }
                                                    sql1.ComId = jaq.ComId;
                                                    new SZHL_QYIM_LISTB().Update(sql1);
                                                }
                                            }
                                            if (zj.Event == "quit_chat")
                                            {
                                                SZHL_QYIM_LIST sql1 = new SZHL_QYIM_LISTB().GetEntities(p => p.ComId == jaq.ComId && p.ChatId == zj.ChatId && p.Status == "0").FirstOrDefault();
                                                if (sql1 != null)
                                                {
                                                    if (!string.IsNullOrEmpty(sql1.UserList))
                                                    {
                                                        string uis = string.Empty;
                                                        string[] strs = sql1.UserList.Split('|');
                                                        foreach (string s in strs)
                                                        {
                                                            bool bl = true;

                                                            if (s == zj.FromUserName)
                                                            {
                                                                bl = false;
                                                            }
                                                            if (bl)
                                                            {
                                                                if (string.IsNullOrEmpty(uis))
                                                                {
                                                                    uis = s;
                                                                }
                                                                else
                                                                {
                                                                    uis = uis + "|" + s;
                                                                }
                                                            }
                                                        }
                                                        sql1.UserList = uis;
                                                        sql1.ComId = jaq.ComId;
                                                        new SZHL_QYIM_LISTB().Update(sql1);
                                                    }
                                                }
                                            }
                                            #endregion
                                        }
                                        else if (new List<string> { "text", "image", "voice", "file", "link" }.Contains(MsgType))
                                        {
                                            #region 内容处理

                                            SZHL_QYIM_ITEM zj = new SZHL_QYIM_ITEM();

                                            //zj.FromUserName = xn.ChildNodes[0]["FromUserName"].InnerText;
                                            //zj.MsgType = xn.ChildNodes[0]["MsgType"].InnerText;
                                            //zj.Event = xn.ChildNodes[0]["Event"].InnerText;
                                            //zj.MsgId = xn.ChildNodes[0]["MsgId"].InnerText;

                                            //string strMedType = ".jpg";
                                            //if (new List<string>() { "image", "voice" }.Contains(MsgType))//下载到本地服务器
                                            //{
                                            //    if (MsgType == "voice")//视频,小视频
                                            //    {
                                            //        strMedType = ".mp3";
                                            //    }
                                            //    JH_Auth_UserB.UserInfo UserInfo = new JH_Auth_UserB.UserInfo();
                                            //    UserInfo = new JH_Auth_UserB().GetUserInfo(jaq.ComId, zj.FromUserName);
                                            //    string fileID = CommonHelp.ProcessWxIMG(xn.ChildNodes[0]["MediaId"].InnerText, "QYIM", UserInfo, strMedType);
                                            //    zj.FileID = Int32.Parse( fileID);
                                            //}

                                            //DateTime time = DateTime.MinValue;
                                            //DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1));
                                            //time = startTime.AddSeconds(int.Parse(xn.ChildNodes[0]["CreateTime"].InnerText));
                                            //zj.CreateTime = time;

                                            foreach (XmlNode xnc in xn.ChildNodes)
                                            {
                                                if (xnc.Name == "FromUserName")
                                                {
                                                    zj.FromUserName = xnc.InnerText;
                                                }
                                                if (xnc.Name == "MsgType")
                                                {
                                                    zj.MsgType = xnc.InnerText;
                                                }
                                                if (xnc.Name == "Event")
                                                {
                                                    zj.Event = xnc.InnerText;
                                                }
                                                if (xnc.Name == "Content")
                                                {
                                                    zj.Content = xnc.InnerText;
                                                }
                                                if (xnc.Name == "MsgId")
                                                {
                                                    zj.MsgId = xnc.InnerText;
                                                }

                                                if (xnc.Name == "PicUrl")
                                                {
                                                    zj.PicUrl = xnc.InnerText;
                                                }
                                                if (xnc.Name == "MediaId")
                                                {
                                                    zj.MediaId = xnc.InnerText;

                                                    string strMedType = ".jpg";
                                                    if (new List<string>() { "image", "voice" }.Contains(MsgType))//下载到本地服务器
                                                    {
                                                        if (MsgType == "voice")//视频,小视频
                                                        {
                                                            strMedType = ".amr";
                                                        }
                                                        JH_Auth_UserB.UserInfo UserInfo = new JH_Auth_UserB.UserInfo();
                                                        UserInfo = new JH_Auth_UserB().GetUserInfo(jaq.ComId, zj.FromUserName);
                                                        string fileID = CommonHelp.ProcessWxIMG(xnc.InnerText, "QYIM", UserInfo, strMedType);
                                                        zj.FileID = Int32.Parse(fileID);
                                                    }
                                                }
                                                if (xnc.Name == "CreateTime")
                                                {
                                                    DateTime time = DateTime.MinValue;
                                                    DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1));
                                                    time = startTime.AddSeconds(int.Parse(xnc.InnerText));
                                                    zj.CreateTime = time;
                                                }
                                                if (xnc.Name == "Content")
                                                {
                                                    zj.Content = xnc.InnerText;
                                                }
                                                if (xnc.Name == "Title")
                                                {
                                                    zj.Title = xnc.InnerText;
                                                }
                                                if (xnc.Name == "Description")
                                                {
                                                    zj.Description = xnc.InnerText;
                                                }
                                                if (xnc.Name == "Url")
                                                {
                                                    zj.Url = xnc.InnerText;
                                                }

                                                if (xnc.ChildNodes.Count > 1)
                                                {
                                                    foreach (XmlNode xncn in xnc.ChildNodes)
                                                    {
                                                        if (xncn.Name == "Type")
                                                        {
                                                            zj.Type = xncn.InnerText;
                                                        }
                                                        if (xncn.Name == "Id")
                                                        {
                                                            zj.UID = xncn.InnerText;
                                                        }
                                                    }
                                                }
                                            }
                                            zj.Sourse = "1";
                                            zj.Status = "0";
                                            zj.ComId = jaq.ComId;
                                            new SZHL_QYIM_ITEMB().Insert(zj);

                                            if (zj.Type == "single")
                                            {
                                                SZHL_QYIM_LIST sql1 = new SZHL_QYIM_LISTB().GetEntities(p => p.ComId == jaq.ComId && p.FromUserName == zj.FromUserName && p.UserList == zj.UID && p.Status=="0").FirstOrDefault();
                                                if (sql1 == null)
                                                {
                                                    SZHL_QYIM_LIST sql = new SZHL_QYIM_LIST();
                                                    sql.FromUserName = zj.FromUserName;
                                                    sql.MsgType = "single";
                                                    sql.Sourse = "1";
                                                    sql.Status = "0";
                                                    sql.UserList = zj.UID;
                                                    sql.ComId = jaq.ComId;
                                                    new SZHL_QYIM_LISTB().Insert(sql);
                                                }
                                            }
                                            #endregion
                                        }
                                    }
                                }
                                #endregion

                                HttpContext.Current.Response.Write(PackageId);
                                HttpContext.Current.Response.End();
                            }
                        }
                        catch (Exception ex)
                        {
                            CommonHelp.WriteLOG("QYIM:" + ex.ToString() + "\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                        }
                    }
                } 
                #endregion

                #region 获取唯一code
                else if (strAction.ToUpper() == "GetUserCodeByCode".ToUpper())
                {
                    //new SZHL.UI.AppCode.JH_Auth_LogB().Insert(new SZHL.Data.JH_Auth_Log() { CRDate = DateTime.Now, LogContent = "接口调用，action：" + Model.Action   });

                    #region 获取Code
                    Model.ErrorMsg = "获取Code错误，请重试";

                    string strCode = context.Request["code"] ?? "";
                    string strCorpID = context.Request["corpid"] ?? "";
                    string strModelCode = context.Request["funcode"] ?? "";

                    if (!string.IsNullOrEmpty(strCode))
                    {

                        var qy = new JH_Auth_QYB().GetEntity(p => p.corpId == strCorpID);
                        if (qy != null)
                        {
                            try
                            {
                                //if (strModelCode.Length > 4)
                                //{
                                //    strModelCode = strModelCode.Substring(0, 4);
                                //}
                                //获取用户名
                                WXHelp wx = new WXHelp(qy, strModelCode.Split('_')[0]);
                                //string username = new SZHL.UI.AppCode.WXHelp().GetUserDataByCode(strCode, Int32.Parse(SZHL.UI.AppCode.WXHelp.GetWXInfo("appCCQJID")));
                                string username = wx.GetUserDataByCode(strCode);

                                if (!string.IsNullOrEmpty(username))
                                {
                                    //根据用户名更新Code和时间
                                    string strGuid = System.Guid.NewGuid().ToString();
                                    var jau = new JH_Auth_UserB().GetUserByUserName(qy.ComId, username);
                                    if (jau != null)
                                    {
                                        if (string.IsNullOrEmpty(jau.pccode))
                                        {
                                            jau.pccode = strGuid;
                                            new JH_Auth_UserB().Update(jau);
                                        }
                                        Model.ErrorMsg = "";
                                        Model.Result = jau.pccode;
                                        Model.Result1 = jau.UserName;
                                    }

                                }
                                else
                                {
                                    Model.ErrorMsg = "当前用户名不存在";
                                }
                            }
                            catch (Exception ex)
                            {
                                Model.ErrorMsg = ex.ToString();
                            }
                        }
                        else
                        {
                            Model.ErrorMsg = "当前企业号未在电脑端注册";
                        }

                    }
                    else
                    {
                        Model.ErrorMsg = "Code为空";
                    }
                    #endregion
                }
                #endregion
                #region 是否存在
                else if (strAction.ToUpper() == "isexist".ToUpper())
                {
                    string strcorpid = context.Request["corpid"] ?? "";
                    if (strcorpid != "")
                    {
                        var qy = new JH_Auth_QYB().GetEntity(p => p.corpId == strcorpid);
                        if (qy == null)
                        {
                            Model.ErrorMsg = "当前企业号未注册此平台";
                        }
                        else
                        {
                            if (context.Request.Cookies["szhlcode"] != null)
                            {
                                //通过Cookies获取Code
                                //string szhlcode = "5ab470be-4988-4bb3-9658-050481b98fca"; 
                                string szhlcode = context.Request.Cookies["szhlcode"].Value.ToString();
                                //通过Code获取用户名，然后执行接口方法
                                var jau = new JH_Auth_UserB().GetUserByPCCode(szhlcode);
                                if (jau == null)
                                {
                                    Model.ErrorMsg = "用户Code不存在";
                                }
                                else
                                {
                                    if (new JH_Auth_QYB().GetQYByComID(jau.ComId.Value).corpId != strcorpid)
                                    {
                                        Model.ErrorMsg = "企业需要重新选择";
                                    }
                                    //重写CODE


                                }
                            }
                        }
                    }
                    else
                    {
                        Model.ErrorMsg = "企业号连接有误，请重新连接";
                    }

                }
                #endregion
                #region 获取企业信息
                else if (strAction.ToUpper() == "GETQYINFO".ToUpper())
                {
                    string strCorpID = context.Request["CorpID"] ?? "";

                    JH_Auth_QY jaq = new JH_Auth_QYB().GetEntity(p => p.corpId == strCorpID);
                    if (jaq != null)
                    {
                        Model.Result = jaq;
                    }
                    else
                    {
                        Model.ErrorMsg = "当前企业号未在电脑端注册";
                    }
                }
                #endregion
                #region 发送提醒
                else if (strAction.ToUpper() == "AUTOALERT")
                {
                    TXSX.TXSXAPI.AUTOALERT();

                }
                #endregion
                #region 必须登录执行接口
                else
                {
                    string P1 = context.Request["P1"] ?? "";
                    string P2 = context.Request["P2"] ?? "";

                    var acs = Model.Action.Split('_');

                    string strUserName = string.Empty;

                    if (context.Request.Cookies["szhlcode"] != null)
                    {
                        //通过Cookies获取Code
                        //string szhlcode = "5ab470be-4988-4bb3-9658-050481b98fca"; 
                        string szhlcode = context.Request.Cookies["szhlcode"].Value.ToString();
                        //通过Code获取用户名，然后执行接口方法
                        var jau = new JH_Auth_UserB().GetUserByPCCode(szhlcode);
                        if (jau != null)
                        {
                            var container = ServiceContainerV.Current().Resolve<IWsService>(acs[0].ToUpper());
                            JH_Auth_UserB.UserInfo UserInfo = new JH_Auth_UserB().GetUserInfo(szhlcode);

                            Model.Action = Model.Action.Substring(acs[0].Length + 1);
                            container.ProcessRequest(context, ref Model, P1, P2, UserInfo);
                        }
                    }
                    else
                    {
                        Model.ErrorMsg = "您未登录!";
                    }
                }
                #endregion

            }
            else
            {
                #region 获取SuiteTicket
                if (HttpContext.Current.Request.HttpMethod.ToUpper() == "POST")
                {
                    //string corpId = WXHelp.corpId;//"wxf285796b249c0cf6";

                    string signature = HttpContext.Current.Request.QueryString["msg_signature"];//企业号的 msg_signature
                    string timestamp = HttpContext.Current.Request.QueryString["timestamp"];
                    string nonce = HttpContext.Current.Request.QueryString["nonce"];

                    // 获得客户端RAW HttpRequest  
                    StreamReader srResult = new StreamReader(context.Request.InputStream);
                    string str = srResult.ReadToEnd();

                    XmlDocument XmlDocument = new XmlDocument();
                    XmlDocument.LoadXml(HttpContext.Current.Server.UrlDecode(str));

                    string ToUserName = string.Empty;
                    string Encrypt = string.Empty;

                    string strde = string.Empty;
                    string strinfotype = string.Empty;


                    foreach (XmlNode xn in XmlDocument.ChildNodes[0].ChildNodes)
                    {
                        if (xn.Name == "ToUserName")
                        {
                            ToUserName = xn.InnerText;
                        }
                        if (xn.Name == "Encrypt")
                        {
                            Encrypt = xn.InnerText;
                        }
                    }

                    var pj = new JH_Auth_WXPJB().GetEntity(p => p.TJID == ToUserName);

                    Tencent.WXBizMsgCrypt wxcpt = new Tencent.WXBizMsgCrypt(pj.Token, pj.EncodingAESKey, ToUserName);
                    int n = wxcpt.DecryptMsg(signature, timestamp, nonce, str, ref strde);

                    string strtct = string.Empty;
                    string strSuiteId = string.Empty;
                    string strtAuthCorpId = string.Empty;

                    XmlDocument XmlDocument1 = new XmlDocument();
                    XmlDocument1.LoadXml(HttpContext.Current.Server.UrlDecode(strde));

                    foreach (XmlNode xn1 in XmlDocument1.ChildNodes[0].ChildNodes)
                    {
                        if (xn1.Name == "SuiteId")
                        {
                            strSuiteId = xn1.InnerText;
                        }
                        if (xn1.Name == "SuiteTicket")
                        {
                            strtct = xn1.InnerText;
                        }
                        if (xn1.Name == "InfoType")
                        {
                            strinfotype = xn1.InnerText;
                        }
                        if (xn1.Name == "AuthCorpId")
                        {
                            strtAuthCorpId = xn1.InnerText;
                        }
                    }
                    if (strinfotype == "suite_ticket")
                    {
                        pj.Ticket = strtct;

                        new JH_Auth_WXPJB().Update(pj);
                    }
                    if (strinfotype == "change_auth"||strinfotype == "cancel_auth")
                    {
                        try
                        {
                            JH_Auth_QY jaq = new JH_Auth_QYB().GetEntity(p => p.corpId == strtAuthCorpId);
                            var jaqts = new JH_Auth_QY_TJB().GetEntities(p => p.ComId == jaq.ComId && p.TJID == strSuiteId);
                            foreach (var jaqt in jaqts)
                            {
                                if (jaqt != null)
                                {
                                    JH_Auth_WXPJ jaw = new JH_Auth_WXPJB().GetEntity(p => p.TJID == jaqt.TJID);
                                    if (jaw != null)
                                    {
                                        string SuiteToken = ThirdPartyAuthApi.GetSuiteToken(jaw.TJID, jaw.TJSecret, jaw.Ticket).suite_access_token;

                                        var url = string.Format("https://qyapi.weixin.qq.com/cgi-bin/service/get_auth_info?suite_access_token={0}", SuiteToken);

                                        var data = new
                                        {
                                            suite_id = jaqt.TJID,
                                            auth_corpid = jaqt.CorpID,
                                            permanent_code = jaqt.PermanentCode
                                        };
                                        try
                                        {
                                            QjySaaSWeb.API.GetAuthInfoResult gair = Senparc.Weixin.CommonAPIs.CommonJsonSend.Send<QjySaaSWeb.API.GetAuthInfoResult>(null, url, data, CommonJsonSendType.POST);
                                            //CommonHelp.WriteLOG(JsonConvert.SerializeObject(gair.auth_info.agent));
                                            //if (jaqt.TJID == "tj7882b1f8bc56f05f" && gair == null)
                                            //{
                                            //    var jaqm = new JH_Auth_QY_ModelB().GetEntities(" ComId='" + jaq.ComId + "' and QYModelCode ='QYIM' and isnull(AgentId,'')!=''").FirstOrDefault();
                                            //    if (jaqm != null)
                                            //    {
                                            //        jaqm.AgentId = "";
                                            //        new JH_Auth_QY_ModelB().Update(jaqm);
                                            //    }
                                            //}
                                            //else
                                            //{
                                                string modelids = string.Empty;
                                                var jams = new JH_Auth_ModelB().GetEntities(p => p.TJId == jaqt.TJID && p.IsSQ == "1");
                                                foreach (var jam in jams)
                                                {
                                                    if (string.IsNullOrEmpty(modelids))
                                                    {
                                                        modelids = jam.ID.ToString();
                                                    }
                                                    else
                                                    {
                                                        modelids = modelids + "," + jam.ID.ToString();
                                                    }
                                                }
                                                if (string.IsNullOrEmpty(modelids))
                                                {
                                                    modelids = "0";
                                                }
                                                //CommonHelp.WriteLOG(modelids);
                                                var jaqms = new JH_Auth_QY_ModelB().GetEntities(" ComId='" + jaq.ComId + "' and ModelID in (" + modelids + ") and isnull(AgentId,'')!=''");
                                                foreach (var jaqm in jaqms)
                                                {
                                                    bool bl = true;
                                                    foreach (var yy in gair.auth_info.agent)
                                                    {
                                                        if (yy.agentid == jaqm.AgentId)
                                                        {
                                                            bl = false;
                                                        }
                                                    }
                                                    if (bl)
                                                    {
                                                        //CommonHelp.WriteLOG(jaqm.ModelID.ToString());
                                                        jaqm.AgentId = "";
                                                        new JH_Auth_QY_ModelB().Update(jaqm);
                                                    }
                                                }
                                            //}
                                        }
                                        catch (Exception ex)
                                        {
                                            if (ex.ToString().IndexOf("48004") > 0 ||ex.ToString().IndexOf("40084") > 0)
                                            { 
                                                string modelids = string.Empty;
                                                var jams = new JH_Auth_ModelB().GetEntities(p => p.TJId == jaqt.TJID && p.IsSQ == "1");
                                                foreach (var jam in jams)
                                                {
                                                    if (string.IsNullOrEmpty(modelids))
                                                    {
                                                        modelids = jam.ID.ToString();
                                                    }
                                                    else
                                                    {
                                                        modelids = modelids + "," + jam.ID.ToString();
                                                    }
                                                }
                                                if (string.IsNullOrEmpty(modelids))
                                                {
                                                    modelids = "0";
                                                }
                                                //CommonHelp.WriteLOG(modelids);
                                                var jaqms = new JH_Auth_QY_ModelB().GetEntities(" ComId='" + jaq.ComId + "' and ModelID in (" + modelids + ") and isnull(AgentId,'')!=''");
                                                foreach (var jaqm in jaqms)
                                                {
                                                    jaqm.AgentId = "";
                                                    new JH_Auth_QY_ModelB().Update(jaqm);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            CommonHelp.WriteLOG(strinfotype+" "+ex.ToString());
                        }
                    }


                    HttpContext.Current.Response.Write("success");
                    HttpContext.Current.Response.End();
                }
                
                #endregion
            }

            IsoDateTimeConverter timeConverter = new IsoDateTimeConverter();
            timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";
            string Result = JsonConvert.SerializeObject(Model, Newtonsoft.Json.Formatting.Indented, timeConverter).Replace("null", "\"\"");
            context.Response.Write(Result);
        }

        /// <summary>
        /// 成为开发者的第一步，验证并相应服务器的数据
        /// </summary>
        private void Auth(string token, string encodingAESKey, string corpId)
        {
            //string token = "9f02rOXenCEWIsnMYd";//从配置文件获取Token

            //string encodingAESKey = "M7gwtCtryDvzzsq86LbIx2K1AlWYG1LoM76UMtUM2yu";//从配置文件获取EncodingAESKey

            //string corpId = "wxf285796b249c0cf6";//从配置文件获取corpId

            string echoString = HttpContext.Current.Request.QueryString["echoStr"];
            string signature = HttpContext.Current.Request.QueryString["msg_signature"];//企业号的 msg_signature
            string timestamp = HttpContext.Current.Request.QueryString["timestamp"];
            string nonce = HttpContext.Current.Request.QueryString["nonce"];

            string decryptEchoString = "";
            if (CheckSignature(token, signature, timestamp, nonce, corpId, encodingAESKey, echoString, ref decryptEchoString))
            {
                if (!string.IsNullOrEmpty(decryptEchoString))
                {
                    Int64 v = Convert.ToInt64(decryptEchoString);
                    HttpContext.Current.Response.Clear();
                    HttpContext.Current.Response.Write(v);
                    HttpContext.Current.Response.End();
                }
            }
        }

        #region 验证企业号签名
        /// <summary>
        /// 验证企业号签名
        /// </summary>
        /// <param name="token">企业号配置的Token</param>
        /// <param name="signature">签名内容</param>
        /// <param name="timestamp">时间戳</param>
        /// <param name="nonce">nonce参数</param>
        /// <param name="corpId">企业号ID标识</param>
        /// <param name="encodingAESKey">加密键</param>
        /// <param name="echostr">内容字符串</param>
        /// <param name="retEchostr">返回的字符串</param>
        /// <returns></returns>
        public bool CheckSignature(string token, string signature, string timestamp, string nonce, string corpId, string encodingAESKey, string echostr, ref string retEchostr)
        {
            Tencent.WXBizMsgCrypt wxcpt = new Tencent.WXBizMsgCrypt(token, encodingAESKey, corpId);
            int result = wxcpt.VerifyURL(signature, timestamp, nonce, echostr, ref retEchostr);
            if (result != 0)
            {
                //LogTextHelper.Error("ERR: VerifyURL fail, ret: " + result);
                return false;
            }

            return true;

            //ret==0表示验证成功，retEchostr参数表示明文，用户需要将retEchostr作为get请求的返回参数，返回给企业号。
            // HttpUtils.SetResponse(retEchostr);
        }
        
        #endregion
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        private DayOfWeek getWkDays(string wk)
        {
            DayOfWeek dw = new DayOfWeek();
            switch (wk)
            {
                case "周一":
                    dw = DayOfWeek.Monday;
                    break;
                case "周二":
                    dw = DayOfWeek.Tuesday;
                    break;
                case "周三":
                    dw = DayOfWeek.Wednesday;
                    break;
                case "周四":
                    dw = DayOfWeek.Thursday;
                    break;
                case "周五":
                    dw = DayOfWeek.Friday;
                    break;
                case "周六":
                    dw = DayOfWeek.Saturday;
                    break;
                case "周日":
                    dw = DayOfWeek.Sunday;
                    break;
            }

            return dw;
        }

    }



    #region 获取永久授权码所需的类
    public class GetPermanentCodeResult
    {
        /// <summary>
        /// 授权方（企业）access_token
        /// </summary>
        public string access_token { get; set; }

        /// <summary>
        /// 授权方（企业）access_token超时时间
        /// </summary>
        public int expires_in { get; set; }

        /// <summary>
        /// 企业号永久授权码
        /// </summary>
        public string permanent_code { get; set; }

        /// <summary>
        /// 授权方企业信息
        /// </summary>
        public ThirdParty_AuthCorpInfo auth_corp_info { get; set; }

        /// <summary>
        /// 授权信息
        /// </summary>
        public ThirdParty_AuthInfo auth_info { get; set; }
    }

    public class ThirdParty_AuthCorpInfo
    {
        /// <summary>
        /// 授权方企业号id
        /// </summary>
        public string corpid { get; set; }

        /// <summary>
        /// 授权方企业号名称
        /// </summary>
        public string corp_name { get; set; }

        /// <summary>
        /// 授权方企业号类型，认证号：verified, 注册号：unverified，体验号：test
        /// </summary>
        public string corp_type { get; set; }

        /// <summary>
        /// 授权方企业号圆形头像
        /// </summary>
        public string corp_round_logo_url { get; set; }

        /// <summary>
        /// 授权方企业号方形头像
        /// </summary>
        public string corp_square_logo_url { get; set; }

        /// <summary>
        /// 授权方企业号用户规模
        /// </summary>
        public string corp_user_max { get; set; }

        /// <summary>
        /// 授权方企业号应用规模
        /// </summary>
        public string corp_agent_max { get; set; }
        /// <summary>
        /// 二维码
        /// </summary>
        public string corp_wxqrcode { get; set; }

    }

    public class ThirdParty_AuthInfo
    {
        /// <summary>
        /// 授权的应用信息
        /// </summary>
        public List<ThirdParty_Agent> agent { get; set; }

        /// <summary>
        /// 授权的通讯录部门
        /// </summary>
        public List<ThirdParty_Department> department { get; set; }
    }

    public class ThirdParty_Agent
    {
        /// <summary>
        /// 授权方应用id
        /// </summary>
        public string agentid { get; set; }

        /// <summary>
        /// 授权方应用名字
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 授权方应用方形头像
        /// </summary>
        public string square_logo_url { get; set; }

        /// <summary>
        /// 授权方应用圆形头像
        /// </summary>
        public string round_logo_url { get; set; }

        /// <summary>
        /// 服务商套件中的对应应用id
        /// </summary>
        public string appid { get; set; }

        /// <summary>
        /// 授权方应用敏感权限组，目前仅有get_location，表示是否有权限设置应用获取地理位置的开关
        /// </summary>
        public string[] api_group { get; set; }
    }

    public class ThirdParty_Department
    {
        /// <summary>
        /// 部门id
        /// </summary>
        public string id { get; set; }

        /// <summary>
        /// 部门名称
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 父部门id
        /// </summary>
        public string parentid { get; set; }

        /// <summary>
        /// 是否具有该部门的写权限
        /// </summary>
        public string writable { get; set; }
    }

    /// <summary>
    /// 获取企业号的授权信息返回结果
    /// </summary>
    public class GetAuthInfoResult
    {
        /// <summary>
        /// 授权方企业信息
        /// </summary>
        public ThirdParty_AuthCorpInfo auth_corp_info { get; set; }

        /// <summary>
        /// 授权信息
        /// </summary>
        public ThirdParty_AuthInfo auth_info { get; set; }
    }

    public class GetAgentResult : QyJsonResult
    {
        /// <summary>
        /// 授权方企业应用id
        /// </summary>
        public string agentid { get; set; }

        /// <summary>
        /// 授权方企业应用名称
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 授权方企业应用方形头像
        /// </summary>
        public string square_logo_url { get; set; }

        /// <summary>
        /// 授权方企业应用圆形头像
        /// </summary>
        public string round_logo_url { get; set; }

        /// <summary>
        /// 授权方企业应用详情
        /// </summary>
        public string description { get; set; }

        /// <summary>
        /// 授权方企业应用可见范围（人员），其中包括userid和关注状态state
        /// </summary>
        public ThirdParty_AllowUserinfos allow_userinfos { get; set; }

        /// <summary>
        /// 授权方企业应用可见范围（部门）
        /// </summary>
        public ThirdParty_AllowPartys allow_partys { get; set; }

        /// <summary>
        /// 授权方企业应用可见范围（标签）
        /// </summary>
        public ThirdParty_AllowTags allow_tags { get; set; }

        /// <summary>
        /// 授权方企业应用是否被禁用
        /// </summary>
        public int close { get; set; }

        /// <summary>
        /// 授权方企业应用可信域名
        /// </summary>
        public string redirect_domain { get; set; }

        /// <summary>
        /// 授权方企业应用是否打开地理位置上报 0：不上报；1：进入会话上报；2：持续上报
        /// </summary>
        public int report_location_flag { get; set; }

        /// <summary>
        /// 是否接收用户变更通知。0：不接收；1：接收
        /// </summary>
        public int isreportuser { get; set; }
    }

    public class ThirdParty_AllowUserinfos
    {
        public List<ThirdParty_User> user { get; set; }
    }

    public class ThirdParty_User
    {
        public string userid { get; set; }
        public string status { get; set; }
    }

    public class ThirdParty_AllowPartys
    {
        public int[] partyid { get; set; }
    }

    public class ThirdParty_AllowTags
    {
        public int[] tagid { get; set; }
    }

    /// <summary>
    /// 获取企业号access_token返回结果
    /// </summary>
    public class GetCorpTokenResult
    {
        /// <summary>
        /// 授权方（企业）access_token
        /// </summary>
        public string access_token { get; set; }
        /// <summary>
        /// 授权方（企业）access_token超时时间
        /// </summary>
        public int expires_in { get; set; }
    }

    #endregion


    #region 微信扫码登录所需的类
    public class GetProviderToken
    {
        /// <summary>
        /// 服务提供商的accesstoken
        /// </summary>
        public string provider_access_token { get; set; }

        /// <summary>
        /// access_token超时时间
        /// </summary>
        public int expires_in { get; set; }
    } 
    #endregion
}