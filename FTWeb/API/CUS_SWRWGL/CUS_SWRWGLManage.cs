﻿using QjySaaSWeb.AppCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using FastReflectionLib;
using QJY.Data;
using Newtonsoft.Json;
using System.Data;
using Senparc.Weixin.QY.Entities;
using QJYCUSTOM.Data;
using NPOI.SS.UserModel;
using System.IO;
using NPOI.XSSF.UserModel;
using NPOI.HSSF.UserModel;

namespace QjySaaSWeb.API
{
    public class CUS_SWRWGLManage : IWsService
    {
        public void ProcessRequest(HttpContext context, ref Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            MethodInfo methodInfo = typeof(CUS_SWRWGLManage).GetMethod(msg.Action.ToUpper());
            CUS_SWRWGLManage model = new CUS_SWRWGLManage();
            methodInfo.FastInvoke(model, new object[] { context, msg, P1, P2, UserInfo });
        }
        //添加任务管理
        public void ADDCUS_SWRWGL(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            CUS_SWRWGL rwgl = JsonConvert.DeserializeObject<CUS_SWRWGL>(P1);
            if (rwgl.ID == 0)
            {
                List<CUS_SWRWGL_ITEM> items = JsonConvert.DeserializeObject<List<CUS_SWRWGL_ITEM>>(P2);
                if (items.Count() > 0)
                {
                    rwgl.CRDate = DateTime.Now;
                    rwgl.ComId = UserInfo.User.ComId;
                    rwgl.CRUser = UserInfo.User.UserName;
                    new CUS_SWRWGLB().Insert(rwgl);
                    foreach (CUS_SWRWGL_ITEM item in items)
                    {
                        if (!string.IsNullOrEmpty(item.RWContent))
                        {
                            item.Status = 0;
                            item.CRDate = DateTime.Now;
                            item.CRUser = UserInfo.User.UserName;
                            item.ComId = UserInfo.User.ComId;
                            item.RWID = rwgl.ID;
                            item.RWTitle = rwgl.RWTitle;
                            item.RWTYPE = rwgl.LeiBie;
                            item.RWDBR = rwgl.RWFZR;
                            item.IsTX = rwgl.IsTX;
                            item.RWBM = new JH_Auth_UserB().GetUserByUserName(UserInfo.QYinfo.ComId, item.RWUSER).BranchCode.ToString();
                            new CUS_SWRWGL_ITEMB().Insert(item);
                            //发送消息给审核人员

                            string jsr = item.RWUSER;
                            if (!string.IsNullOrEmpty(jsr))
                            {
                                SZHL_TXSX TX = new SZHL_TXSX();
                                TX.Date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                TX.APIName = "DBRW";
                                TX.ComId = UserInfo.User.ComId;
                                TX.FunName = "CUS_SWRWGLMSG";
                                TX.TXMode = "DBRW";
                                TX.CRUserRealName = UserInfo.User.UserRealName;
                                TX.MsgID = item.ID.ToString();
                                TX.TXContent = UserInfo.User.UserRealName + "发起了一个任务，等待您处理";
                                TX.TXUser = jsr;
                                TX.CRUser = UserInfo.User.UserName;
                                TXSX.TXSXAPI.AddALERT(TX); //时间为发送时间
                            }
                            //发送消息给传送人 
                            if (!string.IsNullOrEmpty(item.RWXBUSER))
                            {
                                SZHL_TXSX CSTX = new SZHL_TXSX();
                                CSTX.Date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                CSTX.APIName = "DBRW";
                                CSTX.ComId = UserInfo.User.ComId;
                                CSTX.FunName = "CUS_SWRWGLMSG";
                                CSTX.CRUserRealName = UserInfo.User.UserRealName;
                                CSTX.MsgID = item.ID.ToString();
                                CSTX.TXContent = UserInfo.User.UserRealName + "抄送一个督办任务，请您查阅";
                                CSTX.ISCS = "Y";
                                CSTX.TXUser = item.RWXBUSER;
                                CSTX.TXMode = "DBRW";
                                CSTX.CRUser = UserInfo.User.UserName;
                                TXSX.TXSXAPI.AddALERT(CSTX); //时间为发送时间
                            }
                            if (item.IsTX != null && item.IsTX.ToLower() == "true")
                            {
                                SZHL_TXSX TX = new SZHL_TXSX();
                                TX.Date = DateTime.Now.ToString("yyyy-MM-dd") + " 8:00";
                                TX.APIName = "DBRW";
                                TX.ComId = UserInfo.User.ComId;
                                TX.FunName = "CUS_SWRWGLMSG";
                                TX.TXMode = "DBRW";
                                TX.CRUserRealName = UserInfo.User.UserRealName;
                                TX.MsgID = item.ID.ToString();
                                TX.TXContent = UserInfo.User.UserRealName + "发起的任务即将到期";
                                TX.TXUser = jsr;
                                TX.CRUser = UserInfo.User.UserName;
                                TXSX.TXSXAPI.AddALERT(TX); //时间为发送时间
                            }
                        }
                    }

                }
                else
                {
                    msg.ErrorMsg = "请添加任务列表";
                }
            }
            else
            {
                List<CUS_SWRWGL_ITEM> items = JsonConvert.DeserializeObject<List<CUS_SWRWGL_ITEM>>(P2);
                if (items.Count() > 0)
                {
                    new CUS_SWRWGLB().Update(rwgl);
                    new CUS_SWRWGL_ITEMB().Delete(d => d.RWID == rwgl.ID);
                    foreach (CUS_SWRWGL_ITEM item in items)
                    {
                        if (!string.IsNullOrEmpty(item.RWContent))
                        {
                            item.Status = 0;
                            item.CRDate = DateTime.Now;
                            item.CRUser = UserInfo.User.UserName;
                            item.ComId = UserInfo.User.ComId;
                            item.RWTYPE = rwgl.LeiBie;
                            item.RWID = rwgl.ID;
                            item.RWTitle = rwgl.RWTitle;
                            item.RWDBR = rwgl.RWFZR;
                            item.RWBM = new JH_Auth_UserB().GetUserByUserName(UserInfo.QYinfo.ComId, item.RWUSER).BranchCode.ToString();
                            new CUS_SWRWGL_ITEMB().Insert(item);
                        }
                    }
                }
                else
                {
                    msg.ErrorMsg = "请添加任务列表";
                }
            }
        }
        //获取任务列表
        public void GETCUS_SWRWGLLISTWX(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strWhere = string.Format(" (RWUSER='{0}' or RWDBR='{0}' or SHUser='{0}') and item.ComId={1}", UserInfo.User.UserName, UserInfo.User.ComId);

            int page = 0;
            int.TryParse(context.Request.QueryString["p"] ?? "1", out page);
            page = page == 0 ? 1 : page;
            int total = 0;
            DataTable dt = new CUS_SWRWGLB().GetDataPager("CUS_SWRWGL_ITEM item  inner join JH_Auth_ZiDian zd on item.RWTYPE=zd.ID and zd.Class=2 ", "item.*,zd.TypeName", 8, page, "item.Status asc,item.CRDate desc", strWhere, ref total);
            msg.Result = dt;
            msg.Result1 = Math.Ceiling(total * 1.0 / 8);
        }


        //首页代办任务接口,已办任务接口
        public void GETCUS_SWRWGLLISTINDEX(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //待完成
            string strSql = string.Format(@"select item.*,zd.TypeName  from CUS_SWRWGL_ITEM item  inner join JH_Auth_ZiDian zd on item.RWTYPE=zd.ID and zd.Class=2 
                                            where  (RWWCQK is null or RWWCQK='') and RWUser='{0}' and item.ComId={1}", UserInfo.User.UserName, UserInfo.User.ComId);
            DataTable dt = new CUS_SWRWGL_ITEMB().GetDTByCommand(strSql);
            msg.Result = dt;
            //待评价
            string strPJSql = string.Format(@"select DISTINCT item.GroupTitle,item.GroupCode,item.CRUser from CUS_SWRWGL_ITEM item 
                                              where    SHUser ='{0}' ANd (RWFS is  null or RWFS=0) and item.Status=1 and item.ComId={1} ", UserInfo.User.UserName, UserInfo.User.ComId);
            DataTable dtPJ = new CUS_SWRWGL_ITEMB().GetDTByCommand(strPJSql);
            msg.Result1 = dtPJ;
            //已完成 已评价
            string strSql1 = string.Format(@"select  top 8 item.*,zd.TypeName  from CUS_SWRWGL_ITEM item  inner join JH_Auth_ZiDian zd on item.RWTYPE=zd.ID and zd.Class=2 
                                            where  ((RWUSER='{0}'  and RWWCQK is not null) or (SHUser='{1}' and RWFS is not null)) and item.ComId='{1}' order by item.CRDate desc", UserInfo.User.UserName, UserInfo.User.ComId);
            DataTable dt1 = new CUS_SWRWGL_ITEMB().GetDTByCommand(strSql1);
            msg.Result2 = dt1; 
        } 
        //获取任务代办列表
        public void GETCUS_SWRWGLDBLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string status = context.Request["status"] ?? "0";
            string strWhere = string.Format(" RWUSER='{0}' and item.ComId={1}", UserInfo.User.UserName, UserInfo.User.ComId);

            if (status != "")
            {
                if (status == "0")
                {

                    strWhere += string.Format(" and (RWWCQK is null or  RWWCQK='')");
                }
                else
                {
                    strWhere += string.Format(" and (RWWCQK is not null and RWWCQK!='')");
                }
            }
            if (P1 != "")//分类查询
            {
                strWhere += string.Format(" And zd.ID={0}", P1);
            }
            if (P2 != "")//内容查询
            {
                strWhere += string.Format(" And (item.RWContent like '%{0}%' or item.RWTitle like '%{0}%')", P2);
            }
            int page = 0;
            int.TryParse(context.Request.QueryString["p"] ?? "1", out page);
            page = page == 0 ? 1 : page;
            int total = 0;
            DataTable dt = new CUS_SWRWGLB().GetDataPager("CUS_SWRWGL_ITEM item  inner join JH_Auth_ZiDian zd on item.RWTYPE=zd.ID and zd.Class=2 ", "item.*,zd.TypeName", 8, page, " item.CRDate desc", strWhere, ref total);
            msg.Result = dt;
            msg.Result1 = Math.Ceiling(total * 1.0 / 8);
        }
        //获取任务列表
        public void GETCUS_SWRWGLLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strWhere = " item.ComId=" + UserInfo.User.ComId;
            strWhere += string.Format(" And item.CRUser='{0}' ", UserInfo.User.UserName);
            if (P1 != "")
            {
                strWhere += string.Format(" And item.RWTYPE={0}", P1);
            }
            if (P2 != "")//内容查询
            {
                strWhere += string.Format(" And (item.RWContent like '%{0}%' or item.RWTitle like '%{0}%')", P2);
            }
            string searType = context.Request["sType"] ?? "";
            if (searType != "")
            {
                strWhere += string.Format(" And item.Status={0}", searType);
            }
            string startDate = context.Request["strData"] ?? "";
            DateTime startTime = DateTime.Now;
            if (DateTime.TryParse(startDate, out startTime))
            {
                strWhere += string.Format(" And item.RWEDate>='{0}'", startDate);
            }
            string endDate = context.Request["endDate"] ?? "";
            DateTime endTime = DateTime.Now;
            if (DateTime.TryParse(endDate, out endTime))
            {
                strWhere += string.Format(" And item.RWEDate<='{0}'", endTime.AddDays(1).ToString("yyyy-MM-dd"));
            }
            if (searType == "0")
            {
                string strSql = string.Format("select item.*,zd.TypeName,branch.DeptName from CUS_SWRWGL_ITEM item inner join JH_Auth_Branch branch on item.RWBM=branch.DeptCode and item.ComId=branch.ComId  inner join JH_Auth_ZiDian zd on item.RWTYPE=zd.ID and zd.Class=2 Where {0} order by item.CRDate desc", strWhere);
                DataTable dt = new CUS_SWRWGL_ITEMB().GetDTByCommand(strSql);
                msg.Result = dt;
            }
            else
            {
                int page = 0;
                int.TryParse(context.Request.QueryString["p"] ?? "1", out page);
                page = page == 0 ? 1 : page;
                int total = 0;
                string tableName = string.Format("(select DISTINCT item.GroupTitle,item.GroupCode,item.CRUser from CUS_SWRWGL_ITEM item inner join JH_Auth_ZiDian zd on item.RWTYPE=zd.ID and zd.Class=2 where {0}) as newTab", strWhere);

                DataTable dt = new CUS_SWRWGLB().GetDataPager(tableName, "*", 8, page, " GroupCode desc", "", ref total);
                msg.Result = dt;
                msg.Result1 = Math.Ceiling(total * 1.0 / 8);
            }
            msg.Result2 = new CUS_SWRWGL_ITEMB().ExsSclarSql("SELECT TOP 1 SHUser from CUS_SWRWGL_ITEM WHERE ComId=" + UserInfo.User.ComId + " and CRUser='" + UserInfo.User.UserName + "' and SHUser is not NULL order by CRDate DESC");
        }
        //获取任务代办待审核列表
        public void GETCUS_SWRWGLDSHVIEWLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            string strWhere = string.Format(" Where GroupCode='{0}' And SHUser='{1}' and item.ComId={2} And Status=1 ", P1, UserInfo.User.UserName, UserInfo.User.ComId);

            string strSql = string.Format("select item.*,zd.TypeName from CUS_SWRWGL_ITEM item  inner join JH_Auth_ZiDian zd on item.RWTYPE=zd.ID and zd.Class=2 {0} order by item.CRDate desc", strWhere);
            DataTable dt = new SZHL_RWGL_ITEMB().GetDTByCommand(strSql);
            msg.Result = dt;
        }
        public void GETCUS_SWRWGLVIEWLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            string strWhere = string.Format(" Where GroupCode='{0}' and item.ComId={1}", P1, UserInfo.User.ComId);

            string strSql = string.Format("select item.*,zd.TypeName,branch.DeptName from CUS_SWRWGL_ITEM item  inner join JH_Auth_Branch branch on item.RWBM=branch.DeptCode inner join JH_Auth_ZiDian zd on item.RWTYPE=zd.ID and zd.Class=2 {0} order by item.CRDate desc", strWhere);
            DataTable dt = new SZHL_RWGL_ITEMB().GetDTByCommand(strSql);
            msg.Result = dt;
        }
        public void GETCUS_SWRWGLPJLIST_WX(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            string strWhere = string.Format(" Where GroupCode='{0}'  and item.ComId={1} and item.SHUser='{2}'", P1, UserInfo.User.ComId, UserInfo.User.UserName);

            string strSql = string.Format("select item.*,zd.TypeName from CUS_SWRWGL_ITEM item  inner join JH_Auth_ZiDian zd on item.RWTYPE=zd.ID and zd.Class=2 {0} order by item.CRDate desc", strWhere);
            DataTable dt = new SZHL_RWGL_ITEMB().GetDTByCommand(strSql);
            msg.Result = dt;
        }
        public void GETCUS_SWRWGLDBLIST_WX(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            string strWhere = string.Format(" Where item.ID='{0}' and item.ComId={1} and (item.RWUSER='{2}' or ','+item.RWXBUser+',' like '%,{2},%') ", P1, UserInfo.User.ComId, UserInfo.User.UserName);

            string strSql = string.Format("select item.*,zd.TypeName from CUS_SWRWGL_ITEM item  inner join JH_Auth_ZiDian zd on item.RWTYPE=zd.ID and zd.Class=2 {0} order by item.CRDate desc", strWhere);
            DataTable dt = new SZHL_RWGL_ITEMB().GetDTByCommand(strSql);
            msg.Result = dt;
        }
        public void GETCUS_SWRWGLDSHLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            string strWhere = string.Format(" Where SHUser='{0}' and item.ComId={1} And Status=1 ", UserInfo.User.UserName, UserInfo.User.ComId);
            string pfType = context.Request["pfType"] ?? "0";
            if (pfType == "0")
            {
                strWhere += " ANd RWFS is  null ";
            }
            else
            {
                strWhere += " ANd RWFS is not  null ";
            }
            if (P2 != "")//内容查询
            {
                strWhere += string.Format(" And (item.GroupTitle like '%{0}%' )", P2);
            }
            int page = 0;
            int.TryParse(context.Request.QueryString["p"] ?? "1", out page);
            page = page == 0 ? 1 : page;
            int total = 0;
            string tableName = string.Format("(select DISTINCT item.GroupTitle,item.GroupCode,item.CRUser,'' as DBcount from CUS_SWRWGL_ITEM item   {0}) as newTab", strWhere);
            DataTable dt = new CUS_SWRWGLB().GetDataPager(tableName, "*", 8, page, " GroupCode desc", "", ref total);
            msg.Result = dt;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["DBcount"] = new CUS_SWRWGL_ITEMB().strGetDBCount(dt.Rows[i]["GroupCode"].ToString());
            }

            msg.Result1 = Math.Ceiling(total * 1.0 / 8);
        }

        //获取任务信息
        public void GETCUS_SWRWGLMODEL(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = int.Parse(P1);
            msg.Result = new CUS_SWRWGLB().GetEntity(d => d.ID == Id);
            msg.Result1 = new CUS_SWRWGL_ITEMB().GetEntities(d => d.RWID == Id);
        }
        //设置完成情况
        public void SETCUS_SWRWGLWCQK(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strSql = string.Format("UPDATE CUS_SWRWGL_ITEM set RWWCQK='{0}' where ID={1} and ComId={2}", P1, P2, UserInfo.User.ComId);
            new CUS_SWRWGLB().ExsSql(strSql);

        }
        //完成情况评价
        public void SETCUS_SWRWGLPJWCQK(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string Id = context.Request["ID"] ?? "0";
            string strSql = string.Format("UPDATE CUS_SWRWGL_ITEM set DBPJ='{0}',RWFS={1} where ID in ({2}) and ComId={3}", P1, P2, Id, UserInfo.User.ComId);
            new CUS_SWRWGL_ITEMB().ExsSql(strSql);
        }
        //完成情况评分
        public void SETCUS_SWRWGLPFWCQK_WX(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string Id = context.Request["ID"] ?? "0";
            string strSql = string.Format("UPDATE CUS_SWRWGL_ITEM set RWFS={0} where ID in ({1}) and ComId={2}", P1, Id, UserInfo.User.ComId);
            new CUS_SWRWGL_ITEMB().ExsSql(strSql);
        }
        //完成情况评价
        public void SETCUS_SWRWGLPJWCQK_WX(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string Id = context.Request["ID"] ?? "0";
            string strSql = string.Format("UPDATE CUS_SWRWGL_ITEM set DBPJ='{0}' where ID in ({1}) and ComId={2}", P1, Id, UserInfo.User.ComId);
            new CUS_SWRWGL_ITEMB().ExsSql(strSql);
        }
        //提交任务到评价人
        public void SENDCUS_SWRWGLWCQK(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string title = context.Request["title"];
            string code = DateTime.Now.ToString("yyyyMMddHHmmss");
            string strSql = string.Format(" Update CUS_SWRWGL_ITEM set   Status=1,SHUser='{0}',GroupTitle='{1}',GroupCode='{2}' where ID in ({3}) And ComId={4}", P1, title, code, P2, UserInfo.User.ComId);
            strSql += string.Format(" Update CUS_SWRWGL_ITEM set  RWWCQK='未填写' where ID in ({0}) And ComId={1} and (RWWCQK is null or RWWCQK='')", P2, UserInfo.User.ComId);
            new CUS_SWRWGL_ITEMB().ExsSql(strSql);


            SZHL_TXSX TX = new SZHL_TXSX();
            TX.Date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            TX.APIName = "DBRW";
            TX.TXMode = "DBRW";
            TX.ComId = UserInfo.User.ComId;
            TX.FunName = "CUS_SWRWGLMSGB";
            TX.CRUserRealName = UserInfo.User.UserRealName;
            TX.MsgID = code;
            TX.CRUser = UserInfo.User.UserName;
            TX.TXContent = UserInfo.User.UserRealName + "发送了一组任务，等待您评价";
            TX.TXUser = P1;
            TXSX.TXSXAPI.AddALERT(TX); //时间为发送时间
        }
        //撤销任务
        public void DELCUS_SWRWGLITEM(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int[] Ids = P1.SplitTOInt(',');
            new CUS_SWRWGL_ITEMB().Delete(d => Ids.Contains(d.ID));
        }
        //获取任务详细
        public void GETCUS_SWRWGLITEM(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = int.Parse(P1);
            CUS_SWRWGL_ITEM item = new CUS_SWRWGL_ITEMB().GetEntity(d => d.ID == Id);
            if(item!=null){
            msg.Result = item;
            int TypeID = int.Parse(item.RWTYPE);
            msg.Result1 = new JH_Auth_ZiDianB().GetEntity(d => d.ID == TypeID && d.Class == 2).TypeName;
            }
        }
        //任务归档
        public void CUS_SWRWGLGD(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            if (!string.IsNullOrEmpty(P1))
            {
                string strSql = string.Format(" Update CUS_SWRWGL_ITEM set   Status=2 where GroupCode in ('{0}') and ComId={1}", P1.ToFormatLike(), UserInfo.User.ComId);
                new CUS_SWRWGL_ITEMB().ExsSql(strSql);
            }
        }
        public void GETCUS_SWRWGLGDLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strWhere = string.Format(" SHUser='{0}' and item.ComId={1} And Status=2", UserInfo.User.UserName, UserInfo.User.ComId);


            if (P2 != "")//内容查询
            {
                strWhere += string.Format(" And item.GroupTitle like '%{0}%'", P2);
            }
            int page = 0;
            int.TryParse(context.Request.QueryString["p"] ?? "1", out page);
            page = page == 0 ? 1 : page;
            int total = 0;
            string tableName = string.Format("(select DISTINCT item.GroupTitle,item.GroupCode,item.CRUser from CUS_SWRWGL_ITEM item inner join JH_Auth_ZiDian zd on item.RWTYPE=zd.ID and zd.Class=2 where {0}) as newTab", strWhere);
            DataTable dt = new CUS_SWRWGLB().GetDataPager(tableName, "*", 8, page, " GroupCode desc", "", ref total);
            msg.Result = dt;
            msg.Result1 = Math.Ceiling(total * 1.0 / 8);
        }
        public void CUS_DBRWMODIFY(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            CUS_SWRWGL_ITEM item = JsonConvert.DeserializeObject<CUS_SWRWGL_ITEM>(P1);
            if(item.ID!=0)
            {
                new CUS_SWRWGL_ITEMB().Update(item);
            }
            msg.Result = item;
        }

        #region 任务发送消息的接口
        public void CUS_SWRWGLMSG(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            SendCommonMsg(P1, "A");
        }
        public void CUS_SWRWGLMSGB(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            SendCommonMsg(P1, "B");
        }
        //发送消息方法

        public void SendCommonMsg(string P1, string type)
        {
            SZHL_TXSX TX = JsonConvert.DeserializeObject<SZHL_TXSX>(P1);
            Article ar0 = new Article();
            ar0.Title = TX.TXContent;
            ar0.Description = "";
            ar0.Url = TX.MsgID;
            List<Article> al = new List<Article>();
            al.Add(ar0);
            JH_Auth_UserB.UserInfo UserInfo = new JH_Auth_UserB.UserInfo();
            if (!string.IsNullOrEmpty(TX.TXUser))
            {
                try
                {
                    //发送PC消息
                    UserInfo = new JH_Auth_UserB().GetUserInfo(TX.ComId.Value, TX.CRUser);
                    new JH_Auth_User_CenterB().SendMsg(UserInfo, TX.TXMode, TX.TXContent, TX.MsgID, TX.TXUser, type, 0, TX.ISCS);
                }
                catch (Exception)
                {
                }

                //发送微信消息
                WXHelp wx = new WXHelp(UserInfo.QYinfo, TX.TXMode);
                wx.SendTH(al, TX.TXMode , type, TX.TXUser);
            }
        }
        #endregion
        #region 模板导入
        public void CUS_SWRWGIMPORTDBRW(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            try
            {
                HttpPostedFile _upfile = context.Request.Files["upFile"];
                if (_upfile == null)
                {
                    msg.ErrorMsg = "请选择要上传的文件 ";
                }
                else
                {
                    string fileName = _upfile.FileName;/*获取文件名： C:\Documents and Settings\Administrator\桌面\123.jpg*/
                    string suffix = fileName.Substring(fileName.LastIndexOf(".") + 1).ToLower();/*获取后缀名并转为小写： jpg*/
                    int bytes = _upfile.ContentLength;//获取文件的字节大小   
                    if (suffix == "xls" || suffix == "xlsx")
                    {
                        IWorkbook workbook = null;

                        Stream stream = _upfile.InputStream;

                        if (suffix == "xlsx") // 2007版本
                        {
                            workbook = new XSSFWorkbook(stream);
                        }
                        else if (suffix == "xls") // 2003版本
                        {
                            workbook = new HSSFWorkbook(stream);
                        }

                        //获取excel的第一个sheet
                        ISheet sheet = workbook.GetSheetAt(0);

                        //获取sheet的第二行
                        IRow headerRow = sheet.GetRow(1);
                        if (headerRow == null)
                        {
                            msg.ErrorMsg = "请添加要导入的内容";
                            return;
                        }


                        //一行最后一个方格的编号 即总的列数
                        int cellCount = headerRow.LastCellNum;
                        //最后一列的标号  即总的行数
                        // int rowCount = sheet.LastRowNum;
                        List<CUS_SWRWGL_ITEM> RWGLItem = new List<CUS_SWRWGL_ITEM>();

                        for (int i = (sheet.FirstRowNum + 1); i <= sheet.LastRowNum; i++)
                        {
                            IRow row = sheet.GetRow(i);
                            if ((row.GetCell(0) == null || row.GetCell(0).ToString() == "") && (row.GetCell(1) == null || row.GetCell(1).ToString() == "") && (row.GetCell(2) == null || row.GetCell(2).ToString() == ""))
                            {
                                continue;
                            }
                            if (row.GetCell(0) == null || row.GetCell(0).ToString() == "")
                            {
                                msg.ErrorMsg += (i + 1) + "行任务负责人不能为空";
                                return;
                            }
                            if (row.GetCell(1) == null || row.GetCell(1).ToString() == "")
                            {
                                msg.ErrorMsg += (i + 1) + "行要求完成时间不能为空";
                                return;
                            }
                            if (row.GetCell(2) == null || row.GetCell(2).ToString() == "")
                            {
                                msg.ErrorMsg += (i + 1) + "行督办内容不能为空";
                                return;
                            }
                            CUS_SWRWGL_ITEM item = new CUS_SWRWGL_ITEM();
                            for (int j = row.FirstCellNum; j < 3; j++)
                            {

                                if (row.GetCell(j) != null)
                                {
                                    string value = row.GetCell(j).ToString();
                                    switch (j)
                                    {
                                        case 0:
                                            List<JH_Auth_User> userlist = new JH_Auth_UserB().GetEntities(d => d.UserRealName == value && d.ComId == UserInfo.User.ComId).ToList();
                                            if (userlist.Count > 0)
                                            {
                                                item.RWUSER = userlist[0].UserName;
                                            }
                                            else
                                            {
                                                msg.ErrorMsg += value + "不是系统中的用户，";
                                            }
                                            break;
                                        case 1:
                                            DateTime EDate = DateTime.Now;
                                            if (!DateTime.TryParse(value, out EDate))
                                            {
                                                EDate = DateTime.Now;
                                            }
                                            item.RWEDate = EDate;
                                            break;
                                        case 2:
                                            item.RWContent = value;
                                            break;
                                    }
                                }
                            }
                            RWGLItem.Add(item);

                        }

                        msg.Result = RWGLItem;
                        workbook = null;
                        sheet = null;

                    }
                    else
                    {
                        msg.ErrorMsg = "请上传excel文件 ";
                    }

                }
            }
            catch (Exception ex)
            {
                msg.ErrorMsg = ex.ToString();
            }
        }
        #endregion


    }
}
