﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using FastReflectionLib;
using Newtonsoft.Json;
using System.Data;
using Newtonsoft.Json.Linq;
using Senparc.Weixin.QY.Entities;
using System.Net;
using System.IO;
using QjySaaSWeb.AppCode;
using QJY.Data;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System.Text.RegularExpressions;
using Senparc.Weixin;
using Senparc.Weixin.Entities;
using Senparc.Weixin.QY.AdvancedAPIs;

namespace QjySaaSWeb.API
{
    public class AuthManage : IWsService
    {
        public void ProcessRequest(HttpContext context, ref Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            MethodInfo methodInfo = typeof(AuthManage).GetMethod(msg.Action.ToUpper());
            AuthManage model = new AuthManage();
            methodInfo.FastInvoke(model, new object[] { context, msg, P1, P2, UserInfo });
        }

        #region 登录及密码
        public void MODIFYPWD(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            if (P1 == P2)
            {
                P1 = CommonHelp.GetMD5(P1);
                string userName = UserInfo.User.UserName;
                string uName = context.Request["username"] ?? "";
                if (!string.IsNullOrEmpty(uName))
                {
                    userName = uName;
                }
                new JH_Auth_UserB().UpdatePassWord(UserInfo.User.ComId.Value, userName, P1);
            }
            else
            {
                msg.ErrorMsg = "确认密码不一致";
            }
        }
        #endregion

        /// <summary>
        /// 获取角色列表用户
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="strUserName"></param>
        public void GETROLEUSER(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            DataTable dt = new JH_Auth_RoleB().GetDTByCommand(" select RoleCode,RoleName,RoleDec from JH_Auth_Role where RoleCode=" + P1);
            dt.Columns.Add("UserRoleList", Type.GetType("System.Object"));
            string strSQL = string.Format("select DISTINCT u.*,r.RoleCode,branch.DeptName from JH_Auth_User u inner join dbo.JH_Auth_UserRole ur on   u.Username=ur.UserName inner join JH_Auth_Role r on ur.RoleCode=r.RoleCode inner join JH_Auth_Branch branch on u.BranchCode=branch.DeptCode WHERE u.ComId={0}", UserInfo.User.ComId);
            DataTable dtUserRole = new JH_Auth_UserB().GetDTByCommand(strSQL);
            foreach (DataRow row in dt.Rows)
            {
                DataTable dtUserRole1 = dtUserRole.FilterTable(" RoleCode=" + P1);
                row["UserRoleList"] = dtUserRole1;
            }
            msg.Result = dt;
        }

        #region 部门管理


        /// <summary>
        /// 添加部门
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1">部门名称</param>
        /// <param name="P2">部门描述</param>
        /// <param name="strUserName"></param>
        public void ADDBRANCH(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            JH_Auth_Branch branch = new JH_Auth_Branch();
            branch = JsonConvert.DeserializeObject<JH_Auth_Branch>(P1);
            new JH_Auth_BranchB().AddBranch(UserInfo, branch, msg);
        }
        /// <summary>
        /// 获取部门信息
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="strUserName"></param>
        public void GETBRANCHBYCODE(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int code = int.Parse(P1);
            JH_Auth_Branch branch = new JH_Auth_BranchB().GetBMByDeptCode(UserInfo.QYinfo.ComId, code);
            msg.Result = branch;
            msg.Result1 = new JH_Auth_BranchB().GetBMByDeptCode(UserInfo.QYinfo.ComId, branch.DeptRoot);
        }
        /// <summary>
        /// 删除部门
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1">用户名</param>
        /// <param name="P2"></param>
        /// <param name="strUserName"></param>
        public void DELBRANCH(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            try
            {
                int deptCode = int.Parse(P1);
                JH_Auth_Branch branch = new JH_Auth_BranchB().GetEntity(d => d.DeptCode == deptCode);
                if (branch != null)
                {

                    if (new JH_Auth_UserB().GetEntities(d => d.BranchCode == deptCode && d.ComId == UserInfo.User.ComId).ToList().Count > 0)
                    {
                        msg.ErrorMsg = "本部门中存在用户，请先删除用户";
                        return;
                    }
                    if (new JH_Auth_BranchB().GetEntities(d => d.DeptRoot == branch.DeptCode).Count() > 0)
                    {
                        msg.ErrorMsg = "本部门中存在子部门，请先删除子部门";
                        return;
                    }
                    if (UserInfo.QYinfo.IsUseWX == "Y")
                    {
                        WXHelp bm = new WXHelp(UserInfo.QYinfo, "");
                        bm.WX_DelBranch(branch.WXBMCode.ToString());
                    }
                    if (!new JH_Auth_BranchB().Delete(d => d.DeptCode == deptCode))
                    {
                        msg.ErrorMsg = "删除部门失败";
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                new JH_Auth_LogB().InsertLog("DELBRANCH", ex.Message.ToString(), "", UserInfo.User.UserName, UserInfo.QYinfo.ComId);

            }

        }
        /// <summary>
        /// 获取部门列表（ztree使用）
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="strUserName"></param>
        public void GETALLBMUSERLISTNEW(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //判断是否强制加载所有部门数据
            string isAll = context.Request.QueryString["isAll"] ?? "";
            DataTable dtBMS = new DataTable();
            int deptRoot = -1;
            //获取有权限的部门Id
            string strTOPTree = "";
            string branchId = new JH_Auth_BranchB().GetBranchQX(UserInfo);
            if (branchId == "")
            {
                DataTable DT = new JH_Auth_BranchB().ISSUPADMIN(UserInfo.User.UserName);
                if (DT.Rows.Count > 0)
                {
                    branchId = DT.Rows[0]["DeptCode"].ToString();
                    strTOPTree = string.Format("{{id:'{0}',pId:'{1}',attr:'{2}',name:'{3}',leader:'{4}',isuse:'{5}',order:'{6}',{7},checked:{8}}},", DT.Rows[0]["DeptCode"].ToString(), DT.Rows[0]["DeptRoot"].ToString(), "Branch", DT.Rows[0]["DeptName"].ToString(), DT.Rows[0]["BranchLeader"].ToString(), DT.Rows[0]["Remark2"].ToString(), DT.Rows[0]["DeptShort"].ToString(), 1 == 1 ? "open:true" : "open:false", "false");
                }
            }
            string strUserTree = "[" + strTOPTree + new JH_Auth_BranchB().GetTopBranchTree(int.Parse(branchId), UserInfo.User.ComId.Value, P1, "").TrimEnd(',') + "]";

            msg.Result = strUserTree;
            msg.Result1 = UserInfo.User.BranchCode;
        }
        //设置部门可查看部门权限选择部门列表
        public void GETTOPBMLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //获取有权限的部门Id
            string branchId = new JH_Auth_BranchB().GetBranchQX(UserInfo);
            msg.Result = "[" + new JH_Auth_BranchB().GetTopBranchTree(-1, UserInfo.User.ComId.Value, P1, branchId) + "]";
        }
        //新分级显示获取部门数据
        public void GETALLBMNEW(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            DataTable dtBMS = new DataTable();
            //获取有权限的部门Id
            string branchId = new JH_Auth_BranchB().GetBranchQX(UserInfo);
            DataTable DT = new JH_Auth_BranchB().ISSUPADMIN(UserInfo.User.UserName);
            DT.Columns.Add("ChildBranch", Type.GetType("System.Object"));

            //string roleCode = new JH_Auth_UserRoleB().GetEntities(d => d.UserName == UserInfo.User.UserName);
            //int branchCode = new JH_Auth_BranchB().GetEntity(d => d.DeptCode == UserInfo.User.BranchCode).DeptCode;
            if (DT.Rows.Count > 0)
            {
                //dtBMS = new JH_Auth_BranchB().GetBranchList(-1, UserInfo.User.ComId.Value, branchId);
                DT.Rows[0]["ChildBranch"] = new JH_Auth_BranchB().GetBranchList(int.Parse(DT.Rows[0]["DeptCode"].ToString()), UserInfo.User.ComId.Value, branchId);
            }
            msg.Result = DT;
        }
        //机构管理添加人员加载的部门列表
        public void GETDLLBRANCHLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //获取有权限的部门Id
            string branchId = new JH_Auth_BranchB().GetBranchQX(UserInfo);
            List<JH_Auth_Branch> branchList = new List<JH_Auth_Branch>();
            if (!string.IsNullOrEmpty(branchId))
            {
                int deptCode = int.Parse(branchId);
                JH_Auth_Branch branch = new JH_Auth_BranchB().GetBMByDeptCode(UserInfo.QYinfo.ComId, deptCode);
                string strSql = string.Format(" SELECT * from JH_Auth_Branch  where ComId={0} and (Remark1 like '{1}%' OR DeptCode={2})", UserInfo.User.ComId, branch.DeptRoot + "-" + branch.DeptCode, branch.DeptCode); ;
                msg.Result = new JH_Auth_BranchB().GetDTByCommand(strSql);
            }
            else
            {
                branchList = new JH_Auth_BranchB().GetEntities(d => d.ComId == UserInfo.User.ComId && d.DeptRoot != -1).ToList();

                msg.Result = branchList;
            }

        }
        #endregion

        #region 企业信息
        /// <summary>
        /// 添加或编辑企业信息
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="strUserName"></param>
        public void EDITCOMPANY(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            JH_Auth_QY company = new JH_Auth_QY();
            company = JsonConvert.DeserializeObject<JH_Auth_QY>(P1);
            if (company.ComId != 0)
            {
                if (!new JH_Auth_QYB().Update(company))
                {
                    msg.ErrorMsg = "修改企业信息失败";
                }
            }
            else
            {
                JH_Auth_QY company1 = new JH_Auth_QYB().GetEntity(d => d.QYName == company.QYName);
                if (company1 != null)
                {
                    msg.ErrorMsg = "企业名称已存在";
                    return;
                }
                company.CRUser = UserInfo.User.UserName;
                company.CRDate = DateTime.Now;
                if (!new JH_Auth_QYB().Insert(company))
                {
                    msg.ErrorMsg = "添加企业信息失败";
                }
            }
        }

        /// <summary>
        /// 获取企业信息
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="strUserName"></param>
        public void GETCOMPANYINFO(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            msg.Result = UserInfo.QYinfo;
        }
        public void SAVECOMPANYQZ(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strSql = string.Format("UPDATE JH_Auth_QY set DXQZ='{0}' where ComId={1}", P1, UserInfo.User.ComId);
            new JH_Auth_QYB().ExsSql(strSql);
        }
        //更新企业是否启用微信 初始化放弃
        public void UPDATECOMPANYISUSEWX(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strSql = string.Format("UPDATE JH_Auth_QY set IsUseWX='{0}' where ComId={1}", P1, UserInfo.User.ComId);
            new JH_Auth_QYB().ExsSql(strSql);
        }
        //更新下次不再提示
        public void UPDATECOMPANYNOALERT(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            JH_Auth_QY qymodel = UserInfo.QYinfo;
            qymodel.SystemGGId = "Y";
            new JH_Auth_QYB().Update(qymodel);
        }
        #endregion

        #region 组织部门、人员

        /// <summary>
        /// 添加人员
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="strUserName"></param>
        public void ADDUSER(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            try
            {

                JH_Auth_User user = new JH_Auth_User();
                user = JsonConvert.DeserializeObject<JH_Auth_User>(P1);
                if (string.IsNullOrEmpty(user.UserName))
                {
                    msg.ErrorMsg = "用户名必填";
                    return;
                }
                if (string.IsNullOrEmpty(user.mobphone))
                {
                    msg.ErrorMsg = "手机号必填";
                    return;
                }
                Regex regexPhone = new Regex("^0?1[3|4|5|8|7][0-9]\\d{8}$");
                if (!regexPhone.IsMatch(user.mobphone))
                {
                    msg.ErrorMsg = "手机号填写不正确";
                    return;
                }
                Regex regexOrder = new Regex("^[0-9]*$");
                if (user.UserOrder != null && !regexOrder.IsMatch(user.UserOrder.ToString()))
                {
                    msg.ErrorMsg = "序号必须是数字";
                    return;
                }
                if (user.ID != 0)
                {
                    if (UserInfo.QYinfo.IsUseWX == "Y")
                    {
                        WXHelp wx = new WXHelp(UserInfo.QYinfo, "");
                        wx.WX_UpdateUser(user);
                    }
                    if (!new JH_Auth_UserB().Update(user))
                    {
                        msg.ErrorMsg = "修改用户失败";
                    }
                    else
                    {
                        string strSql = string.Format("INSERT into JH_Auth_UserRole (UserName,RoleCode,ComId) Values('{0}',{1},{2})", user.UserName, 1118, UserInfo.User.ComId);
                        new JH_Auth_RoleB().ExsSql(strSql);
                    }
                }
                else
                {
                    JH_Auth_User user1 = new JH_Auth_UserB().GetUserByUserName(UserInfo.QYinfo.ComId, user.UserName);
                    if (user1 != null)
                    {
                        msg.ErrorMsg = "用户已存在";
                        return;
                    }
                    user.UserPass = CommonHelp.GetMD5(user.UserPass);
                    user.ComId = UserInfo.User.ComId;
                    user.zhiwu = user.zhiwu == "" ? "员工" : user.zhiwu;
                    if (UserInfo.QYinfo.IsUseWX == "Y")
                    {
                        WXHelp wx = new WXHelp(UserInfo.QYinfo, "");
                        wx.WX_CreateUser(user);
                    }
                    user.CRDate = DateTime.Now;
                    user.CRUser = UserInfo.User.UserName;
                    if (!new JH_Auth_UserB().Insert(user))
                    {
                        msg.ErrorMsg = "添加用户失败";
                    }
                    else
                    {
                        string strSql = string.Format("INSERT into JH_Auth_UserRole (UserName,RoleCode,ComId) Values('{0}',{1},{2})", user.UserName, 1118, UserInfo.User.ComId);
                        new JH_Auth_RoleB().ExsSql(strSql);

                    }
                }
                msg.Result = user;
            }
            catch (Exception ex)
            {
                new JH_Auth_LogB().InsertLog("ADDUSER", ex.Message.ToString(), "", UserInfo.User.UserName, UserInfo.QYinfo.ComId);
                msg.ErrorMsg = "操作失败";
            }

        }




        /// <summary>
        /// 添加人员
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="strUserName"></param>
        public void UPDATEUSER(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            try
            {

                JH_Auth_User user = new JH_Auth_User();
                user = JsonConvert.DeserializeObject<JH_Auth_User>(P1);
                if (string.IsNullOrEmpty(user.UserName))
                {
                    msg.ErrorMsg = "用户名必填";
                    return;
                }
                if (string.IsNullOrEmpty(user.mobphone))
                {
                    msg.ErrorMsg = "手机号必填";
                    return;
                }
                Regex regexPhone = new Regex("^0?1[3|4|5|8|7][0-9]\\d{8}$");
                if (!regexPhone.IsMatch(user.mobphone))
                {
                    msg.ErrorMsg = "手机号填写不正确";
                    return;
                }
                if (!string.IsNullOrEmpty(P2))
                {
                    user.UserPass = CommonHelp.GetMD5(P2);
                }
                //if (user.UserPass == "AB8F53F05485F45A3031EEF048611F43")
                //{
                //    msg.ErrorMsg = "请更新初始密码";
                //    return;
                //}

                Regex regexOrder = new Regex("^[0-9]*$");
                if (user.UserOrder != null && !regexOrder.IsMatch(user.UserOrder.ToString()))
                {
                    msg.ErrorMsg = "序号必须是数字";
                    return;
                }
                if (user.ID != 0)
                {
                    if (UserInfo.QYinfo.IsUseWX == "Y")
                    {
                        WXHelp wx = new WXHelp(UserInfo.QYinfo, "");
                        wx.WX_UpdateUser(user);
                    }
                    if (!new JH_Auth_UserB().Update(user))
                    {
                        msg.ErrorMsg = "修改用户失败";
                    }
                    else
                    {
                        string strSql = "";
                        strSql += string.Format("INSERT into JH_Auth_UserRole (UserName,RoleCode,ComId) Values('{0}',{1},{2})", user.UserName, 1118, UserInfo.User.ComId);
                        new JH_Auth_RoleB().ExsSql(strSql);
                    }
                }

                msg.Result = user;
            }
            catch (Exception ex)
            {
                new JH_Auth_LogB().InsertLog("ADDUSER", ex.Message.ToString(), "", UserInfo.User.UserName, UserInfo.QYinfo.ComId);
                msg.ErrorMsg = "操作失败";
            }

        }
        //批量设置部门
        public void PLSETBRANCH(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string[] userNames = P1.Split(',');
            int branchCode = 0;
            int.TryParse(P2, out branchCode);
            List<JH_Auth_User> userList = new JH_Auth_UserB().GetEntities(d => d.ComId == UserInfo.User.ComId && userNames.Contains(d.UserName)).ToList();
            foreach (JH_Auth_User user in userList)
            {
                user.BranchCode = branchCode;
                if (UserInfo.QYinfo.IsUseWX == "Y")
                {
                    WXHelp wx = new WXHelp(UserInfo.QYinfo, "");
                    wx.WX_UpdateUser(user);
                }
                new JH_Auth_UserB().Update(user);
            }
        }
        /// <summary>
        /// 根据用户删除用户
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1">用户名</param>
        /// <param name="P2"></param>
        /// <param name="strUserName"></param>
        public void DELUSER(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            try
            {
                if (UserInfo.QYinfo.IsUseWX == "Y")
                {
                    WXHelp bm = new WXHelp(UserInfo.QYinfo, "");
                    bm.WX_DelUser(P1);
                }

                string strUser = P1.TrimEnd(',');
                if (strUser.Split(',').Length > 1)
                {
                    new JH_Auth_UserB().ExsSclarSql("DELETE  FROM JH_Auth_User WHERE USERNAME IN ('" + strUser.ToFormatLike(',') + "')");
                }
                else
                {
                    if (!new JH_Auth_UserB().Delete(d => d.ComId == UserInfo.QYinfo.ComId && d.UserName == P1))
                    {
                        msg.ErrorMsg = "删除失败";
                    }
                }

            }
            catch (Exception ex)
            {
                new JH_Auth_LogB().InsertLog("DELUSER", ex.Message.ToString(), "", UserInfo.User.UserName, UserInfo.QYinfo.ComId);

            }

        }
        /// <summary>
        /// 更改用户是否禁用的状态
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1">用户名</param>
        /// <param name="P2">状态</param>
        /// <param name="strUserName"></param>
        public void UPDATEUSERISUSE(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            try
            {
                JH_Auth_User UPUser = new JH_Auth_UserB().GetUserByUserName(UserInfo.QYinfo.ComId, P1);
                UPUser.IsUse = P2;

                if (UserInfo.QYinfo.IsUseWX == "Y")
                {
                    WXHelp bm = new WXHelp(UserInfo.QYinfo, "");
                    bm.WX_UpdateUser(UPUser);//为了更新微信用户状态
                }
                if (!new JH_Auth_UserB().Update(UPUser))
                {
                    msg.ErrorMsg = "更新失败";
                }
            }
            catch (Exception ex)
            {
                new JH_Auth_LogB().InsertLog("UPDATEUSERISUSE", ex.Message.ToString(), "", UserInfo.User.UserName, UserInfo.QYinfo.ComId);
            }
        }
        /// <summary>
        /// 更改用户是否禁用的状态
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1">用户名</param>
        /// <param name="P2">状态</param>
        /// <param name="strUserName"></param>
        public void UPDATEUSERGW(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            try
            {
                JH_Auth_User UPUser = new JH_Auth_UserB().GetUserByUserName(UserInfo.QYinfo.ComId, UserInfo.User.UserName);
                UPUser.UserGW = P1;

                if (!new JH_Auth_UserB().Update(UPUser))
                {
                    msg.ErrorMsg = "更新失败";
                }
                UserInfo.User = UPUser;
                msg.Result = UserInfo;
                int gwId = 0;
                int.TryParse(UserInfo.User.UserGW, out gwId);
                if (gwId > 0)
                {
                    msg.Result1 = new SZHL_PX_GWGLB().GetEntity(d => d.ID == gwId && d.ComId == UserInfo.User.ComId).GWName;
                }
            }
            catch (Exception ex)
            {
                new JH_Auth_LogB().InsertLog("UPDATEUSERGW", ex.Message.ToString(), "", UserInfo.User.UserName, UserInfo.QYinfo.ComId);
            }
        }


        /// <summary>
        /// 根据部门编号获取部门人员
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="strUserName"></param>
        public void GETUSERBYCODENEW(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int deptCode = 0;
            if (!int.TryParse(P1, out deptCode))
            {
                deptCode = 1;
            }
            DataTable dtUser = new JH_Auth_UserB().GetUserListbyBranch(deptCode, P2, UserInfo.QYinfo.ComId);
            msg.Result = dtUser;
        }


        public void MUIUPDATEUSER(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            JArray PASTEITEMS = (JArray)JsonConvert.DeserializeObject(P1);
            foreach (var item in PASTEITEMS)
            {
                int userid = int.Parse(item["ID"].ToString());
                JH_Auth_User user = new JH_Auth_UserB().GetEntity(d => d.ID == userid);
                user.mobphone = item["mobphone"].ToString();
                user.UserRealName = item["UserRealName"].ToString();
                user.zhiwu = item["zhiwu"].ToString();
                new JH_Auth_UserB().Update(user);

            }
        }
        /// <summary>
        /// 根据部门编号获取部门人员
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="strUserName"></param>
        public void GETUSERBYCODENEW_PAGE(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int deptCode = 0;
            int.TryParse(P1, out deptCode);
            JH_Auth_Branch branch = new JH_Auth_BranchB().GetBMByDeptCode(UserInfo.QYinfo.ComId, deptCode);
            if (branch == null) { msg.ErrorMsg = "数据异常"; }
            string strQXWhere = string.Format("And  ( u.branchCode={0} or b.Remark1 like '{1}%')", deptCode, (branch.Remark1 == "" ? "" : branch.Remark1 + "-") + branch.DeptCode);
            string branchqx = new JH_Auth_BranchB().GetBranchQX(UserInfo);
            if (branch.DeptRoot == -1 && !string.IsNullOrEmpty(branchqx))
            {
                strQXWhere = " And (";
                int i = 0;
                foreach (int dept in branchqx.SplitTOInt(','))
                {
                    JH_Auth_Branch branchQX = new JH_Auth_BranchB().GetBMByDeptCode(UserInfo.QYinfo.ComId, dept);
                    strQXWhere += string.Format((i == 0 ? "" : "And") + "  ( u.branchCode!={0} And b.Remark1 NOT like '{1}%')", dept, (branchQX.Remark1 == "" ? "" : branchQX.Remark1 + "-") + branchQX.DeptCode);
                    i++;
                }
                strQXWhere += ")";
            }
            string tableName = " JH_Auth_User u  inner join JH_Auth_Branch b on u.branchCode=b.DeptCode left join SZHL_PX_GWGL g on u.UserGW=g.ID";
            string tableColumn = " u.*,b.DeptName,b.DeptCode,g.GWName";
            string strWhere = string.Format("u.ComId={0}   {1}", UserInfo.User.ComId, strQXWhere);
            if (P2 != "")
            {
                strWhere += string.Format(" And (u.UserName like '%{0}%'  or u.UserRealName like '%{0}%'  or b.DeptName like '%{0}%' or u.mobphone like '%{0}%' ) ", P2);
            }
            int page = 0;
            int pagecount = 20;
            int.TryParse(context.Request["p"] ?? "0", out page);
            int.TryParse(context.Request.QueryString["pagecount"] ?? "20", out pagecount);//页数
            page = page == 0 ? 1 : page;

            int total = 0;
            DataTable dt = new SZHL_YCGLB().GetDataPager(tableName, tableColumn, pagecount, page, " b.DeptShort,ISNULL(u.UserOrder, 1000000) asc", strWhere, ref total);
            msg.Result = dt;
            msg.Result1 = Math.Ceiling(total * 1.0 / pagecount);
            msg.Result2 = total;
        }
        //导出员工
        public void EXPORTYG(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            GETUSERBYCODENEW_PAGE(context, msg, P1, P2, UserInfo);

            DataTable dt = msg.Result;

            string sqlCol = "ID,UserOrder|序号,DeptName|部门,RoomCode|房间号,UserName|账号,UserRealName|姓名,Sex|性别,mobphone|手机,QQ|QQ,weixinCard|微信,mailbox|邮箱,telphone|座机,zhiwu|职务,Usersign|职责,UserGW|岗位,IDCard|身份证,HomeAddress|家庭住址";
            DataTable dtResult = dt.DelTableCol(sqlCol);
            DataTable dtExtColumn = new JH_Auth_ExtendModeB().GetExtColumnAll(UserInfo.QYinfo.ComId, "YGGL");
            foreach (DataRow drExt in dtExtColumn.Rows)
            {
                dtResult.Columns.Add(drExt["TableFiledName"].ToString(), Type.GetType("System.String"));
            }

            if (dtExtColumn.Rows.Count > 0)
            {
                foreach (DataRow dr in dtResult.Rows)
                {
                    DataTable dtExtData = new JH_Auth_ExtendModeB().GetExtDataAll(UserInfo.QYinfo.ComId, "YGGL", dr["ID"].ToString());
                    foreach (DataRow drExtData in dtExtData.Rows)
                    {
                        dr[drExtData["TableFiledName"].ToString()] = drExtData["ExtendDataValue"].ToString();
                    }
                }
            }
            dtResult.Columns.Remove("ID");
            CommonHelp ch = new CommonHelp();
            msg.ErrorMsg = ch.ExportToExcel("员工", dtResult);
        }

        /// <summary>
        /// 根据部门编号获取可用人员
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="strUserName"></param>
        public void GETYUSERBYCODE(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int deptCode = 0;
            if (!int.TryParse(P1, out deptCode))
            {
                deptCode = 1;
            }
            DataTable dtUser = new JH_Auth_UserB().GetUserListbyBranchUse(deptCode, P2, UserInfo);
            msg.Result = dtUser;
        }
        //根据角色获取用户
        public void GETUSERBYROLECODE(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int roleCode = int.Parse(P1);
            msg.Result = new JH_Auth_UserRoleB().GetUserDTByRoleCode(roleCode, UserInfo.User.ComId.Value);

        }
        /// <summary>
        /// 获取前端需要的人员选择列表
        /// </summary>
        public void GETUSERJS(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            DataTable dtUsers = new JH_Auth_UserB().GetDTByCommand(" SELECT UserName,UserRealName,DeptCode,DeptName FROM JH_Auth_User INNER JOIN JH_Auth_Branch ON JH_Auth_User.ComId=JH_Auth_Branch.ComId AND JH_Auth_User.BranchCode=JH_Auth_Branch.DeptCode WHERE JH_Auth_User.IsUse='Y' And JH_Auth_Branch.ComId='" + UserInfo.User.ComId + "'");
            //获取选择用户需要的HTML和转化用户名需要的json数据
            msg.Result = dtUsers;
        }

        public void SETBRANCHLEADER(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strSql = string.Format("UPDATE JH_Auth_Branch set BranchLeader='{0}' where  DeptCode={1}", P1, P2);
            new JH_Auth_BranchB().ExsSql(strSql);
        }
        public void SETUSERLEADER(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strSql = string.Format("UPDATE JH_Auth_User set UserLeader='{0}' where  username='{1}' and ComID={2}", P1, P2, UserInfo.User.ComId);
            new JH_Auth_BranchB().ExsSql(strSql);
        }
        #endregion

        #region 获取当前人员下属人员的接口
        public void GETCHILDRENUSER(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            List<JH_Auth_User> dtUser = new List<JH_Auth_User>();
            List<JH_Auth_Branch> branchList = new JH_Auth_BranchB().GetEntities(d => d.ComId == UserInfo.User.ComId && d.BranchLeader == UserInfo.User.UserName).ToList();
            foreach (JH_Auth_Branch branch in branchList)
            {
                dtUser = new JH_Auth_UserB().GetChildrenUser(UserInfo.User.ComId.Value, branch.DeptRoot, branch.DeptCode, UserInfo.User.UserName);
            }
            List<JH_Auth_User> userList = new JH_Auth_UserB().GetEntities(d => d.ComId == UserInfo.User.ComId && d.UserLeader == UserInfo.User.UserName).ToList();
            List<JH_Auth_User> resultUser = dtUser.Concat(userList).ToList();
            msg.Result1 = resultUser;

        }
        #endregion


        #region 角色管理
        public void EDITROLE(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            JH_Auth_Role role = new JH_Auth_Role();
            role = JsonConvert.DeserializeObject<JH_Auth_Role>(P1);

            if (role.RoleCode != 0)
            {
                if (role.RoleCode == 0 && P2 == "")
                {
                    msg.ErrorMsg = "管理员至少有一人！！！";
                    return;
                }
                if (!new JH_Auth_RoleB().Update(role))
                {
                    msg.ErrorMsg = "修改职务失败";
                }

            }
            else
            {
                if (string.IsNullOrEmpty(role.ComId.ToString()))
                {
                    JH_Auth_Role user1 = new JH_Auth_RoleB().GetEntity(d => d.RoleName == role.RoleName && d.ComId == UserInfo.User.ComId && d.IsUse == "Y");
                    if (user1 != null)
                    {
                        msg.ErrorMsg = "职务已存在";
                        return;
                    }
                    role.isSysRole = "N";
                    role.ComId = UserInfo.User.ComId;
                    if (!new JH_Auth_RoleB().Insert(role))
                    {
                        msg.ErrorMsg = "添加职务失败";
                    }
                }

            }
            if (msg.ErrorMsg == "")
            {
                new JH_Auth_UserRoleB().Delete(d => d.RoleCode == role.RoleCode);
            }
            if (P2 != "")
            {
                try
                {
                    foreach (string name in P2.Split(','))
                    {
                        string strSQL = string.Format("INSERT INTO  [JH_Auth_UserRole] ([UserName]  ,[RoleCode],ComID) VALUES  ('{0}'  ,'{1}','{2}')", name, role.RoleCode, UserInfo.User.ComId);
                        new JH_Auth_UserRoleB().ExsSql(strSQL);
                    }
                }
                catch (Exception ex)
                {
                    msg.ErrorMsg = ex.Message;
                }
            }
            msg.Result = role;
        }

        /// <summary>
        /// 获取角色列表
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="strUserName"></param>
        public void GETROLE(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strWhere = " where r.PRoleCode<>-1 and r.RoleCode!=0 and ( r.ComId=0 or  r.ComId=" + UserInfo.User.ComId + ")";
            //获取当前部门不能查看的角色
            string roleIds = new JH_Auth_RoleB().GetRoleQX(UserInfo);
            if (!string.IsNullOrEmpty(roleIds))
            {
                strWhere += " And r.RoleCode not in (" + roleIds + ")";
            }


            if (P1 != "")
            {
                int Id = int.Parse(P1);
                msg.Result1 = new JH_Auth_BranchB().GetBMByDeptCode(UserInfo.QYinfo.ComId, Id);
            }
            DataTable dt = new JH_Auth_RoleB().GetDTByCommand(@" select r.RoleCode,r.RoleName,r.isSysRole,r.RoleQX,r.IsHasQX,COUNT(distinct u.UserName) userCount from JH_Auth_Role r left join JH_Auth_UserRole ur on r.RoleCode=ur.RoleCode and ur.ComId=" + UserInfo.User.ComId + @"     
                                                               left join JH_Auth_User u on ur.UserName=u.UserName  and u.ComId=" + UserInfo.User.ComId + strWhere + " group by r.RoleCode,r.RoleName,r.isSysRole,r.RoleQX,r.IsHasQX");

            msg.Result = dt;
            msg.Result2 = UserInfo.QYinfo.ComId.ToString();

        }

        public void DELROLE(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int ID = int.Parse(P1);
            JH_Auth_Role role = new JH_Auth_RoleB().GetEntity(d => d.RoleCode == ID);
            if (role != null && role.ComId != 0 && role.RoleCode != 2)
            {
                new JH_Auth_RoleB().delRole(ID, UserInfo.User.ComId.Value);
            }
            else
            {
                msg.ErrorMsg = "此职务不能删除";
            }
        }
        public void GETROLEBYCODE(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = int.Parse(P1);
            int ComId = UserInfo.User.ComId.Value;
            if (Id == 0)
            {
                ComId = 0;
            }
            msg.Result = new JH_Auth_RoleB().GetEntity(d => d.RoleCode == Id && d.ComId == ComId);
            int roleCode = int.Parse(P1);
            msg.Result1 = new JH_Auth_UserRoleB().GetDTByCommand("SELECT DISTINCT u.UserName,userrole.RoleCode from JH_Auth_User u inner join   JH_Auth_UserRole  userrole on u.username=userrole.username where userrole.RoleCode=" + roleCode);
        }

        /// <summary>
        /// 获取用户权限
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETROLEFUN(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            if (!string.IsNullOrEmpty(P1) && UserInfo.UserRoleCode != "" && UserInfo.User.isSupAdmin != "Y")
            {
                msg.Result = new JH_Auth_RoleB().GetModelFun(UserInfo.User.ComId.Value, UserInfo.UserRoleCode, P1);
            }
            else
            {

                msg.Result = new JH_Auth_RoleB().GetModelFun(UserInfo.User.ComId.Value, "0", P1);
            }
        }
        //删除角色人员
        public void DELROLEUSER(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int roleCode = 0;
            int.TryParse(P1, out roleCode);
            new JH_Auth_UserRoleB().Delete(d => d.RoleCode == roleCode && d.UserName == P2 && d.ComId == UserInfo.User.ComId);
        }

        #endregion



        #region 数据权限接口

        /// <summary>
        /// 统一删除MODEL数据
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void DELFORMDATA(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //根据FormCode和ID删除数据

            try
            {
                string strFormCode = context.Request.QueryString["FormCode"] as string;
                string strFormID = context.Request.QueryString["FormID"] as string;
                JH_Auth_FormBase FormBaseData = new JH_Auth_FormBaseB().GetEntity(d => d.FormCode == strFormCode);
                FormHelp FormHelp = new FormHelp();
                foreach (string ID in strFormID.Split(','))
                {
                    FormHelp.DelForm(int.Parse(ID), FormBaseData.FormTable);
                }
            }
            catch (Exception)
            {
                msg.ErrorMsg = "error";
            }

        }


        /// <summary>
        /// 判断用户是否有编辑数据或者删除数据得操作权限
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void ISHASDATAQX(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //根据FormCode和ID删除数据

            try
            {
                string strModelCode = P1;
                int DataID = int.Parse(P2);
                string strIsHasDataQX = new JH_Auth_QY_ModelB().isHasDataQX(strModelCode, DataID, UserInfo);
                msg.Result = strIsHasDataQX;
            }
            catch (Exception)
            {
                msg.ErrorMsg = "error";
            }

        }
        /// <summary>
        /// 判断用户是否有数据得查看权限
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void ISHASDATAREADQX(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //根据FormCode和ID删除数据

            try
            {
                string strModelCode = P1;
                int DataID = int.Parse(P2);
                string strIsHasDataQX = new JH_Auth_QY_ModelB().ISHASDATAREADQX(strModelCode, DataID, UserInfo);
                msg.Result = strIsHasDataQX;
            }
            catch (Exception)
            {
                msg.ErrorMsg = "error";
            }

        }


        #endregion

        #region 消息中心
        public void GETXXZXIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strWhere = string.Format(" ComId={0} and UserTO='{1}' and isRead=0", UserInfo.User.ComId, UserInfo.User.UserName);
            string strSQL = string.Format(@"SELECT  MsgType,MsgContent,CRDate,UserFrom,isRead,ID,MsgLink from JH_Auth_User_Center Where  {0}  order by CRDate desc", strWhere);
            DataTable dt = new JH_Auth_User_CenterB().GetDTByCommand(strSQL);
            msg.Result = dt;
            msg.Result1 = new JH_Auth_User_CenterB().ExsSclarSql("SELECT count(0) from  JH_Auth_User_Center Where  " + strWhere);
        }
        //抄送给我的消息
        public void GETXXZXABOUTLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strWhere = string.Format("  isCS='Y'  And  ComId={0} and UserTO='{1}' and isRead=0 ", UserInfo.User.ComId, UserInfo.User.UserName);
            if (P1 != "")
            {
                strWhere += string.Format(" And MsgType='{0}'", P1);
            }
            string strSQL = string.Format(@"SELECT top 8 MsgType,MsgContent,CRDate,UserFrom,isRead,ID,MsgLink,MsgModeID from JH_Auth_User_Center Where {0}order by CRDate desc", strWhere);
            DataTable dt = new JH_Auth_User_CenterB().GetDTByCommand(strSQL);
            msg.Result = dt;
            msg.Result1 = new JH_Auth_User_CenterB().ExsSclarSql("SELECT count(0) from  JH_Auth_User_Center Where  " + strWhere);
        }
        //抄送给我的消息
        public void GETXXZXABOUTTYPE(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strWhere = string.Format("  isCS='Y'  And  ComId={0} and UserTO='{1}' and isRead=0 ", UserInfo.User.ComId, UserInfo.User.UserName);
            string strSQL = string.Format(@"SELECT DISTINCT MsgType from JH_Auth_User_Center Where {0} ", strWhere);
            DataTable dt = new JH_Auth_User_CenterB().GetDTByCommand(strSQL);
            msg.Result = dt;

        }
        /// <summary>
        /// 获取用户消息列表
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1">消息类型</param>
        /// <param name="P2">消息内容模糊查询</param>
        /// <param name="strUserName"></param>
        public void GETXXZXIST_PAGE(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strWhere = string.Format(" ComId={0}  And UserTO='{1}'", UserInfo.User.ComId, UserInfo.User.UserName);
            if (P1 != "")
            {
                strWhere += string.Format(" and isRead={0} ", P1);
            }
            if (P2 != "")
            {
                strWhere += string.Format(" and MsgContent like  '%{0}%'", P2);

            }
            string msgType = context.Request.QueryString["msgType"] ?? "";
            if (msgType != "")
            {
                strWhere += string.Format(" and MsgType ='{0}'", msgType);
            }
            string msgTypes = context.Request.QueryString["msgTypes"] ?? "";
            if (msgTypes != "")
            {
                msgTypes = System.Web.HttpUtility.UrlDecode(msgTypes);
                strWhere += string.Format(" and MsgModeID in ('{0}')", msgTypes.Replace(",", "','"));
            }

            int page = 0;
            int pagecount = 8;
            int.TryParse(context.Request.QueryString["pagecount"] ?? "8", out pagecount);
            int.TryParse(context.Request.QueryString["p"] ?? "0", out page);
            page = page == 0 ? 1 : page;
            int total = 0;
            DataTable dt = new JH_Auth_User_CenterB().GetDataPager("JH_Auth_User_Center", "MsgType,MsgModeID,MsgContent,CRDate,UserFrom,isRead,ID,wxLink,MsgLink ", pagecount, page, "isRead asc,CRDate desc", strWhere, ref total);
            msg.Result = dt;
            //  msg.Result1 = Math.Ceiling(total * 1.0 / pagecount);
            msg.Result1 = total;

        }

        //是否有未读消息
        public void HASREADMSG(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string SQL = string.Format("select top 1 1 from JH_Auth_User_Center where ComId={0} and UserTO='{1}' and isRead=0 ", UserInfo.User.ComId, UserInfo.User.UserName);
            object ss2 = new JH_Auth_User_CenterB().ExsSclarSql(SQL);
            if (ss2 != null)
            {
                msg.Result = "1";  //1:标记有未读消息
            }
        }
        /// <summary>
        /// 更新读取状态
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void UPDTEREADSTATES(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            try
            {
                string strSql = "";
                string status = context.Request["s"] ?? "1";

                strSql = string.Format("UPDATE JH_Auth_User_Center set isRead='{0}' where ID in ({1}) ", status, P1);

                new JH_Auth_User_CenterB().ExsSql(strSql);
            }
            catch (Exception ex)
            {
                msg.ErrorMsg = ex.Message;
            }
        }
        /// <summary>
        /// 获取消息中心类型
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETUSERCENTERTYPE(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strWhere = string.Empty;
            if (P1 != "")
            {
                string msgTypes = System.Web.HttpUtility.UrlDecode(P1);
                strWhere += string.Format(" and MsgModeID in ('{0}')", msgTypes.Replace(",", "','"));
            }
            string strSql = string.Format("SELECT  DISTINCT MsgType from JH_Auth_User_Center where ComId={0} and  userTo='{1}' " + strWhere, UserInfo.User.ComId, UserInfo.User.UserName);
            msg.Result = new JH_Auth_User_CenterB().GetDTByCommand(strSql);
        }
        //获取详细详情
        public void GETXXZXMODEL(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = int.Parse(P1);
            JH_Auth_User_Center userCenter = new JH_Auth_User_CenterB().GetEntity(d => d.ID == Id && d.ComId == UserInfo.User.ComId);
            msg.Result = userCenter;
        }

        //删除消息中心消息
        public void DELXXZX(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            try
            {
                if (P1 != "")
                {
                    string strSql = string.Format("Delete JH_Auth_User_Center where ID in ({0}) ", P1);
                    new JH_Auth_User_CenterB().ExsSql(strSql);
                }
            }
            catch (Exception ex)
            {
                msg.ErrorMsg = ex.Message;
            }
        }
        public void GETXXZXMODELINFO(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strSql = string.Format(@"SELECT DISTINCT MsgType,MsgModeID,SUM( case when isRead=0 then 1 else 0 END) num ,MAX(CRDate) CRDate 
                                      from JH_Auth_User_Center where ComId={0}  and UserTO='{1}' group by MsgType,MsgModeID order by CRDate DESC", UserInfo.User.ComId, UserInfo.User.UserName);
            DataTable dt = new JH_Auth_User_CenterB().GetDTByCommand(strSql);
            dt.Columns.Add("NewXX", Type.GetType("System.Object"));
            foreach (DataRow row in dt.Rows)
            {
                string strSql2 = "SELECT top 1 * from JH_Auth_User_Center where ComId=" + UserInfo.User.ComId + "  and   UserTO='" + UserInfo.User.UserName + "' and MsgType='" + row["MsgType"] + "' ";
                if (row["num"].ToString() != "0")
                {
                    row["NewXX"] = new JH_Auth_User_CenterB().GetDTByCommand(strSql2 + " and isRead=0 order by CRDate DESC");
                }
                else
                {
                    row["NewXX"] = new JH_Auth_User_CenterB().GetDTByCommand(strSql2 + " and isRead=1 order by CRDate DESC");
                }
            }
            msg.Result = dt.OrderBy(" num desc");
        }
        #endregion

        #region 前台首页获取数据
        public void GETUSERBYUSERNAME(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //如果获取当前用户信息，直接返回，否则按用户名查找
            if (P1 =="")
            {
                msg.Result = UserInfo;
                int gwId = 0;
                int.TryParse(UserInfo.User.UserGW, out gwId);
                if (gwId > 0)
                {
                    SZHL_PX_GWGL gwgl = new SZHL_PX_GWGLB().GetEntity(d => d.ID == gwId && d.ComId == UserInfo.User.ComId);
                    if (gwgl != null)
                    {
                        msg.Result1 = gwgl;
                    }
                }

            }
            else
            {
                UserInfo = new JH_Auth_UserB().GetUserInfo(UserInfo.User.ComId.Value, P1);
                msg.Result = UserInfo;
                int gwId = 0;
                int.TryParse(UserInfo.User.UserGW, out gwId);
                if (gwId > 0)
                {
                    msg.Result1 = new SZHL_PX_GWGLB().GetEntity(d => d.ID == gwId && d.ComId == UserInfo.User.ComId);
                }

            }
            msg.Result2 = new JH_Auth_BranchB().GetDTByCommand("SELECT TOP 1 [ComId],[DeptCode],[DeptName] FROM [QJY_PXKS].[dbo].[JH_Auth_Branch] where ComId='10312'  and  ','+BranchLeader+',' like '%,186185183,%'").Rows.Count;
            JH_Auth_Branch tempdata = new JH_Auth_BranchB().GETDW(UserInfo.BranchInfo);
            msg.Result3 = tempdata;
        }


        /// <summary>
        /// 获取企业应用相关数据
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        //public void GETCOMPANYAUTH(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        //{
        //    //获取用户有权限的菜单
        //    DataTable dt = new JH_Auth_ModelB().GETMenuList(UserInfo, P1);
        //    msg.Result = dt;
        //    //获取快捷方式的菜单
        //    DataRow[] dr = dt.Select(" IsKJFS=1 ");
        //    if (dr.Count() > 0)
        //    {
        //        DataTable dtKJFS = dr.CopyToDataTable();
        //        dtKJFS.Columns.Add("issel");
        //        List<string> UserKJFS = new JH_Auth_UserCustomDataB().GetEntities(d => d.ComId == UserInfo.User.ComId && d.UserName == UserInfo.User.UserName && d.DataType == "PCKJFS" && d.DataContent1 == "Y").Select(d => d.DataContent).ToList();
        //        foreach (DataRow row in dtKJFS.Rows)
        //        {
        //            row["issel"] = UserKJFS.Contains(row["ModelCode"].ToString());
        //        }

        //        msg.Result1 = dtKJFS;
        //    }

        //}
        //获取用的快捷方式
        public void GETUSERKJFS(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //获取设置的快捷方式
            msg.Result = new JH_Auth_UserCustomDataB().GetEntities(d => d.ComId == UserInfo.User.ComId && d.UserName == UserInfo.User.UserName && d.DataContent1 == "Y" && d.DataType == "PCKJFS");
        }
        #endregion

        #region 管理员首次登陆判断是否已添加应用 初始化放弃
        public void GETQYMODEL(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            if (UserInfo.User.isSupAdmin == "Y")
            {
                msg.Result = UserInfo.QYinfo;
            }
        }
        public void GETQYINITSTATUS(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            msg.Result = UserInfo.QYinfo.SystemGGId;
        }
        #endregion

        #region 后台首页
        public void GETQYTJMODELLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            DataTable dt = new JH_Auth_WXPJB().GetDTByCommand("SELECT * from JH_Auth_WXPJ");
            dt.Columns.Add("Model", Type.GetType("System.Object"));
            DataTable dtModel = new JH_Auth_QY_ModelB().GetDTByCommand("SELECT model.*,case when qymodel.id is not NULL then 1 else 0 end issel from   JH_Auth_Model model left  join JH_Auth_QY_Model qymodel on  model.ID=qymodel.ModelID  and qymodel.ComId=" + UserInfo.User.ComId);
            List<JH_Auth_Model> modeList = new JH_Auth_ModelB().GetEntities(d => d.ModelStatus == "0").ToList();
            foreach (DataRow row in dt.Rows)
            {
                string tjId = row["TJID"].ToString();
                row["Model"] = dtModel.FilterTable("TJId='" + tjId + "'");
            }
            msg.Result = dt;
        }
        #endregion


        #region 首次初始化加载应用及添加操作
        //首次初始化第一步数据加载 初始化放弃
        public void GETQYMODELLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            DataTable dt = new JH_Auth_WXPJB().GetDTByCommand("SELECT  DISTINCT ModelType from JH_Auth_Model  where ModelStatus=0 and ( ComId=0 or ComId=" + UserInfo.User.ComId + ")");
            dt.Columns.Add("Model", Type.GetType("System.Object"));
            DataTable dtModel = new JH_Auth_QY_ModelB().GetDTByCommand("SELECT model.*, ISNULL(qymodel.Status,0)  issel from  JH_Auth_Model model left  join JH_Auth_QY_Model qymodel on  model.ID=qymodel.ModelID  and qymodel.ComId=" + UserInfo.User.ComId + " WHERE (model.ComId=0 or  model.ComId=" + UserInfo.User.ComId + ")");
            foreach (DataRow row in dt.Rows)
            {
                string modelType = row["ModelType"].ToString();
                row["Model"] = dtModel.FilterTable("ModelType='" + modelType + "'");
            }
            msg.Result = dt;
            msg.Result1 = new JH_Auth_QY_ModelB().GetEntities(d => d.ComId == UserInfo.User.ComId).Count();
            msg.Result2 = UserInfo.User.ComId;
            msg.Result3 = UserInfo.QYinfo.IsUseWX;
            msg.Result4 = UserInfo.QYinfo.SystemGGId;
        }
        //初始化系统菜单数据 初始化放弃
        public void INITSYSTEMDATA(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            try
            {
                string[] modelIds = P1.Split(',');
                JH_Auth_QY_Model qymodel = new JH_Auth_QY_Model();
                foreach (string strModel in modelIds)
                {
                    int modelId = 0;
                    int.TryParse(strModel.Split(':')[0], out modelId);
                    qymodel = new JH_Auth_QY_ModelB().GetEntity(d => d.ComId == UserInfo.User.ComId && d.ModelID == modelId);
                    if (qymodel != null)
                    {
                        qymodel.Status = strModel.Split(':')[1];
                        new JH_Auth_QY_ModelB().Update(qymodel);
                    }
                    else
                    {
                        qymodel = new JH_Auth_QY_Model();
                        qymodel.ComId = UserInfo.User.ComId;
                        qymodel.ModelID = modelId;
                        qymodel.CRDate = DateTime.Now;
                        qymodel.QYModelCode = new JH_Auth_ModelB().GetEntity(d => d.ID == modelId).ModelCode;
                        qymodel.CRUser = UserInfo.User.UserName;
                        qymodel.Status = strModel.Split(':')[1];
                        qymodel.AgentId = "";
                        new JH_Auth_QY_ModelB().Insert(qymodel);
                    }
                }
                //给超级管理员添加二级菜单权限
                string strSql = string.Format(@"if  not exists(select * from JH_Auth_RoleFun where ComId={0} and RoleCode=0) INSERT INTO JH_Auth_RoleFun(ComId,RoleCode,FunCode,ActionCode)SELECT {0},RoleCode,FunCode,ActionCode from JH_Auth_RoleFun where ComId=0", UserInfo.User.ComId);
                new JH_Auth_RoleFunB().ExsSql(strSql);

            }
            catch (Exception ex)
            {
                msg.ErrorMsg = ex.Message;
            }

        }
        //获取应用套件
        public void GETTJDATA(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //查询套件
            DataTable dt = new JH_Auth_WXPJB().GetDTByCommand(@"SELECT distinct pj.* from JH_Auth_WXPJ pj inner join JH_Auth_Model  model on  pj.TJID=model.TJId 
                                                                    inner join JH_Auth_QY_Model qymodel on model.ID=qymodel.ModelID  where qymodel.ComId=" + UserInfo.User.ComId + " and model.TJId is not NULL and qymodel.Status=1");
            dt.Columns.Add("Model", Type.GetType("System.Object"));
            DataTable dtModel = new JH_Auth_QY_ModelB().GetDTByCommand("SELECT model.*,qymodel.AgentId from JH_Auth_Model model inner join JH_Auth_QY_Model qymodel on model.ID=qymodel.ModelID where qymodel.ComId=" + UserInfo.User.ComId + " and model.TJId is not NULL and qymodel.Status=1  and (model.ComId=0 or model.ComId=" + UserInfo.User.ComId + ")");
            foreach (DataRow row in dt.Rows)
            {
                string tjId = row["TJID"].ToString();
                row["Model"] = dtModel.FilterTable("TJId='" + tjId + "'");
            }
            msg.Result = dt;
        }
        //获取微信安装app列表 初始化放弃
        public void GETMOBILEAZAPP(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //查询套件
            DataTable dt = new JH_Auth_WXPJB().GetDTByCommand(@"SELECT distinct pj.* from JH_Auth_WXPJ pj inner join JH_Auth_Model  model on  pj.TJID=model.TJId 
                                                                    inner join JH_Auth_QY_Model qymodel on model.ID=qymodel.ModelID  where qymodel.ComId=" + UserInfo.User.ComId + " and model.TJId is not NULL and qymodel.Status=1 order by pj.TJName ASC");
            dt.Columns.Add("Model", Type.GetType("System.Object"));
            string strSql = string.Format(@"SELECT model.*,qymodel.AgentId from JH_Auth_Model model inner join 
                                            JH_Auth_QY_Model qymodel on model.ID=qymodel.ModelID where
                                            qymodel.ComId={0} and model.TJId is not NULL and qymodel.Status=1
                                            and model.WXUrl!='' and model.WXUrl is not NULL  and (model.ComId=0 or model.ComId={0})", UserInfo.User.ComId);
            DataTable dtModel = new JH_Auth_QY_ModelB().GetDTByCommand(strSql);
            foreach (DataRow row in dt.Rows)
            {
                string tjId = row["TJID"].ToString();
                row["Model"] = dtModel.FilterTable("TJId='" + tjId + "'").OrderBy(" ORDERID asc");
            }
            msg.Result = dt;
        }
        //微信获取菜单
        public void GETMOBILETJDATA(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //查询套件
            DataTable dt = new JH_Auth_WXPJB().GetDTByCommand(@"SELECT distinct pj.* from JH_Auth_WXPJ pj inner join JH_Auth_Model  model on  pj.TJID=model.TJId 
                                                                inner join JH_Auth_QY_Model qymodel on model.ID=qymodel.ModelID
                                                                inner join JH_Auth_CommonUrl commonurl on model.ModelCode=commonurl.ModelCode
                                                                where qymodel.ComId=" + UserInfo.User.ComId + " and model.TJId is not NULL And qymodel.Status=1 and commonurl.Type=''");
            dt.Columns.Add("Model", Type.GetType("System.Object"));
            DataTable dtModel = new JH_Auth_QY_ModelB().GetDTByCommand("SELECT model.*,qymodel.AgentId,commonurl.Url from JH_Auth_Model model inner join JH_Auth_QY_Model qymodel on model.ID=qymodel.ModelID   inner join JH_Auth_CommonUrl commonurl on model.ModelCode=commonurl.ModelCode where qymodel.ComId=" + UserInfo.User.ComId + " and model.TJId is not NULL And qymodel.Status=1 and commonurl.Type=''");
            foreach (DataRow row in dt.Rows)
            {
                string tjId = row["TJID"].ToString();
                row["Model"] = dtModel.FilterTable("TJId='" + tjId + "'");
            }
            msg.Result = dt;
        }
        #endregion
        #region 字典管理
        public void GETZIDIANLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strSql = string.Format("SELECT * from JH_Auth_ZiDian where  Class={0} and ComId={1} and Remark=0 ", P1, UserInfo.User.ComId);
            if (P2 != "")//内容查询
            {
                strSql += string.Format(" And TypeName like '%{0}%'", P2);
            }
            msg.Result = new JH_Auth_QY_ModelB().GetDTByCommand(strSql);

        }
        public void GETZIDIANZYLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strSql = string.Format("SELECT zd.ID,zd.TypeName,COUNT(gwkc.ID) gwCount from JH_Auth_ZiDian zd LEFT JOIN SZHL_PX_GWKC gwkc on zd.ID=gwkc.GWPXYQ and gwkc.GWId={0} where  zd.Class=21  and zd.Remark=0  and zd.ComId={1}  GROUP by zd.Id,zd.TypeName ", P1, UserInfo.User.ComId);
            if (P2 != "")//内容查询
            {
                strSql += string.Format(" And TypeName like '%{0}%'", P2);
            }
            msg.Result = new JH_Auth_QY_ModelB().GetDTByCommand(strSql);
        }
        public void GETZIDIANSLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            DataTable dt = new DataTable();
            if (P1 != "")
            {
                string[] strs = P1.Split(',');
                if (strs.Length > 1)
                {
                    dt.Columns.Add("Class", Type.GetType("System.String"));
                    dt.Columns.Add("Item", Type.GetType("System.Object"));

                    foreach (string str in strs)
                    {
                        DataRow dr = dt.NewRow();
                        dr["Class"] = str;

                        string strSql = string.Format("SELECT * from JH_Auth_ZiDian where  Class={0} and ComId={1} and Remark=0", str, UserInfo.User.ComId);
                        dr["Item"] = new JH_Auth_QY_ModelB().GetDTByCommand(strSql);

                        dt.Rows.Add(dr);
                    }
                }
                else
                {
                    string strSql = string.Format("SELECT * from JH_Auth_ZiDian where  Class={0} and ComId={1} and Remark=0 ", P1, UserInfo.User.ComId);
                    dt = new JH_Auth_QY_ModelB().GetDTByCommand(strSql);
                }
            }
            else if (P2 != "")
            {
                string strSql = string.Format("SELECT * from JH_Auth_ZiDian where  ID={0} and ComId={1} ", P2, UserInfo.User.ComId);
                dt = new JH_Auth_QY_ModelB().GetDTByCommand(strSql);
            }
            msg.Result = dt;
        }
        //获取CRM分类设置列表
        public void GETCRMZIDIANLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int[] classid = new int[] { 10, 11, 12, 13, 14, 15, 16, 17 };
            List<JH_Auth_ZiDian> ZiDianList = new JH_Auth_ZiDianB().GetEntities(d => classid.Contains(d.Class.Value) && (d.ComId == 0 || d.ComId == UserInfo.User.ComId) && d.Remark == "0").ToList();
            DataTable dt = new DataTable();
            dt.Columns.Add("Class");
            dt.Columns.Add("ZiDianDataList", Type.GetType("System.Object"));
            for (int i = 10; i < 18; i++)
            {
                DataRow row = dt.NewRow();
                row["Class"] = i;
                row["ZiDianDataList"] = ZiDianList.Where(d => d.Class == i).ToList();
                dt.Rows.Add(row);
            }
            msg.Result = dt;
        }
        public void DELTYPEBYID(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = int.Parse(P1);
            JH_Auth_ZiDian zidian = new JH_Auth_ZiDianB().GetEntity(d => d.ID == Id);
            if (zidian != null)
            {

                if (zidian.ComId == 0)
                {
                    msg.ErrorMsg = "此类型不能删除";
                }
                else if (zidian.Remark == "0")
                {
                    zidian.Remark = "1";
                    new JH_Auth_ZiDianB().Update(zidian);
                }
            }
        }
        public void SAVETYPEMODEL(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            JH_Auth_ZiDian zidian = JsonConvert.DeserializeObject<JH_Auth_ZiDian>(P1);
            if (zidian.TypeName.Length > 10)
            {
                msg.ErrorMsg = "分类名称建议不超过10个字!";
                return;
            }

            if (zidian.ID == 0)
            {
                zidian.ComId = UserInfo.User.ComId;
                JH_Auth_ZiDian zidiannew = new JH_Auth_ZiDianB().GetEntity(d => d.TypeName == zidian.TypeName && d.Class == zidian.Class && d.ComId == zidian.ComId && d.Remark != "1");
                if (zidiannew != null)
                {
                    msg.ErrorMsg = "此分类已存在";
                    return;
                }

                zidian.Remark = "0";
                zidian.CRDate = DateTime.Now;
                zidian.CRUser = UserInfo.User.UserName;
                new JH_Auth_ZiDianB().Insert(zidian);


            }
            else
            {
                JH_Auth_ZiDian zidiannew = new JH_Auth_ZiDianB().GetEntity(d => d.TypeName == zidian.TypeName && d.Class == zidian.Class && d.ComId == zidian.ComId && d.Remark != "1" && d.ID != zidian.ID);
                if (zidiannew != null)
                {
                    msg.ErrorMsg = "此分类已存在";
                    return;
                }
                new JH_Auth_ZiDianB().Update(zidian);
            }
            msg.Result = zidian;
        }
        #endregion

        #region 微信使用
        /// <summary>
        /// 获取微信选人列表
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETWXUSER(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            List<WXUserBR> list = new List<WXUserBR>();
            //获取所有部门
            var listALL = new JH_Auth_BranchB().GetEntities(p => p.ComId == UserInfo.User.ComId).ToList();

            //第一级显示的部门
            var list0 = new List<JH_Auth_Branch>();

            //获取顶级部门信息，加载第二级部门信息列表
            var dcbm = new JH_Auth_BranchB().GetEntity(p => p.ComId == UserInfo.User.ComId && p.DeptRoot == -1);
            list0 = listALL.Where(p => p.DeptRoot == dcbm.DeptCode).OrderBy(p => p.DeptShort).ToList();
            //获取用户所在部门没有权限看到的部门Ids
            string branchId = new JH_Auth_BranchB().GetBranchQX(UserInfo);
            //如果有不能看到的部门Ids,排除用户不能看到的部门
            if (!string.IsNullOrEmpty(branchId))
            {
                int[] noqxBranch = branchId.SplitTOInt(',');
                list0 = list0.Where(d => !noqxBranch.Contains(d.DeptCode)).ToList();
            }

            //var list0 = listALL.Where(p => p.DeptCode == dept).OrderBy(p => p.DeptShort).ToList();
            //循环要显示的第一级部门信息加载部门以下的部门及部门人员信息
            foreach (var v in list0)
            {
                WXUserBR wx = new WXUserBR();
                wx.DeptCode = v.DeptCode;
                wx.DeptName = v.DeptName;
                var users = new JH_Auth_UserB().GetEntities(d => d.BranchCode == v.DeptCode && d.IsUse == "Y").ToList().Select(d => new { d.ID, d.UserName, d.UserRealName, d.telphone, d.mobphone, d.zhiwu });
                wx.DeptUser = users;
                wx.DeptUserNum = users.Count();
                GetNextWxUser(wx, listALL);
                list.Add(wx);
            }

            msg.Result = list;

            DataTable dtUser = new JH_Auth_UserB().GetUserListbyBranchUse(dcbm.DeptCode, "", UserInfo);

            msg.Result1 = dtUser.DelTableCol("ID,UserName,UserRealName,mobphone,telphone,zhiwu");
            msg.Result2 = UserInfo.QYinfo.QYCode;
            msg.Result3 = new JH_Auth_UserB().GetEntities(d => d.BranchCode == dcbm.DeptCode && d.IsUse == "Y").ToList().Select(d => new { d.ID, d.UserName, d.UserRealName, d.telphone, d.mobphone, d.zhiwu });
        }
        /// <summary>
        /// 获取部门下的列表
        /// </summary>
        /// <param name="wx"></param>
        /// <param name="listALL"></param>
        public void GetNextWxUser(WXUserBR wx, List<JH_Auth_Branch> listALL)
        {
            var list = (from p in listALL
                        where p.DeptRoot == wx.DeptCode
                        orderby p.DeptShort
                        select new WXUserBR { DeptCode = p.DeptCode, DeptName = p.DeptName }).ToList();

            wx.SubDept = list;
            foreach (var v in list)
            {
                var users = new JH_Auth_UserB().GetEntities(d => d.BranchCode == v.DeptCode && d.IsUse == "Y").ToList().Select(d => new { d.ID, d.UserName, d.UserRealName, d.telphone, d.mobphone, d.zhiwu });
                v.DeptUser = users;
                v.DeptUserNum = users.Count();
                wx.DeptUserNum = wx.DeptUserNum + users.Count();
                GetNextWxUser(v, listALL);
            }
        }

        /// <summary>
        /// 初始化微信
        /// </summary>
        public void WXINIT(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            DataTable dtUsers = new JH_Auth_UserB().GetDTByCommand(" SELECT * FROM JH_Auth_User where ComId='" + UserInfo.User.ComId + "'");
            //获取选择用户需要的HTML和转化用户名需要的json数据
            msg.Result = dtUsers;

            var url = new JH_Auth_CommonUrlB().GetEntity(p => p.ModelCode == P1 && p.Type == P2);
            if (url != null)
            {
                msg.Result1 = url.Url;
            }
        }

        public void GETUSERINFO(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            msg.Result = UserInfo;
        }

        /// <summary>
        /// 同步通讯录
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void TBTXL(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            try
            {
                int bmcount = 0;
                int rycount = 0;
                P1 = "123456";
                var list = new JH_Auth_QY_ModelB().GetEntities(p => p.ComId == UserInfo.User.ComId && p.AgentId != "");
                if (list.Count() > 0)
                {
                    WXHelp wx = new WXHelp(UserInfo.QYinfo, "");
                    #region 更新部门
                    Senparc.Weixin.QY.AdvancedAPIs.MailList.GetDepartmentListResult bmlist = wx.WX_GetBranchList("");
                    foreach (var wxbm in bmlist.department)
                    {
                        var bm = new JH_Auth_BranchB().GetEntity(p => p.ComId == UserInfo.User.ComId && p.WXBMCode == wxbm.id);
                        if (bm == null)
                        {
                            #region 新增部门
                            JH_Auth_Branch jab = new JH_Auth_Branch();
                            jab.WXBMCode = wxbm.id;
                            jab.ComId = UserInfo.User.ComId;
                            jab.DeptName = wxbm.name;
                            jab.DeptDesc = wxbm.name;
                            jab.DeptShort = wxbm.order;

                            if (wxbm.parentid == 0)//如果是跟部门,设置其跟部门为-1
                            {
                                jab.DeptRoot = -1;
                            }
                            else
                            {
                                var bm1 = new JH_Auth_BranchB().GetEntity(p => p.ComId == UserInfo.User.ComId && p.WXBMCode == wxbm.parentid);
                                jab.DeptRoot = bm1.DeptCode;
                                jab.Remark1 = new JH_Auth_BranchB().GetBranchNo(UserInfo.User.ComId.Value, jab.DeptRoot);
                            }


                            new JH_Auth_BranchB().Insert(jab);

                            bmcount = bmcount + 1;
                            #endregion
                        }
                        else
                        {
                            //同步部门时放弃更新现有部门
                            #region 更新部门
                            //bm.WXBMCode = wxbm.id;
                            //bm.ComId = UserInfo.User.ComId;
                            //bm.DeptName = wxbm.name;
                            //bm.DeptShort = wxbm.order;
                            //if (wxbm.parentid == 0) { bm.DeptRoot = -1; }
                            //else
                            //{
                            //    var bm1 = new JH_Auth_BranchB().GetEntity(p => p.ComId == UserInfo.User.ComId && p.WXBMCode == wxbm.parentid);
                            //    bm.DeptRoot = bm1.DeptCode;
                            //    bm.Remark1 = new JH_Auth_BranchB().GetBranchNo(UserInfo.User.ComId.Value, bm.DeptRoot);
                            //}

                            //new JH_Auth_BranchB().Update(bm);
                            #endregion
                        }
                    }
                    //同步部门时放弃更新现有部门
                    //var bms = new JH_Auth_BranchB().GetEntities(p => p.ComId == UserInfo.User.ComId);
                    //foreach (var l in bms)
                    //{
                    //    if (bmlist.department.Where(p => p.id == l.WXBMCode) == null)
                    //    {
                    //        new JH_Auth_BranchB().Delete(l);
                    //    }
                    //}
                    #endregion

                    #region 更新人员
                    Senparc.Weixin.QY.AdvancedAPIs.MailList.GetDepartmentMemberInfoResult yg = wx.WX_GetDepartmentMemberInfo(1);

                    foreach (var u in yg.userlist)
                    {
                        var user = new JH_Auth_UserB().GetUserByUserName(UserInfo.QYinfo.ComId, u.userid);
                        if (user == null)
                        {
                            #region 新增人员
                            JH_Auth_User jau = new JH_Auth_User();
                            jau.ComId = UserInfo.User.ComId;
                            jau.UserName = u.userid;
                            jau.UserPass = CommonHelp.GetMD5(P1);
                            jau.UserRealName = u.name;
                            jau.Sex = u.gender == 1 ? "男" : "女";
                            if (u.department.Length > 0)
                            {
                                int id = u.department[0];
                                var bm1 = new JH_Auth_BranchB().GetEntity(p => p.ComId == UserInfo.User.ComId && p.WXBMCode == id);
                                jau.BranchCode = bm1.DeptCode;
                            }
                            jau.mailbox = u.email;
                            jau.mobphone = u.mobile;
                            jau.weixinnum = u.weixinid;
                            jau.zhiwu = string.IsNullOrEmpty(u.position) ? "员工" : u.position;
                            jau.IsUse = "Y";

                            if (u.status == 1 || u.status == 4)
                            {
                                jau.isgz = u.status.ToString();
                            }
                            jau.txurl = u.avatar;

                            new JH_Auth_UserB().Insert(jau);

                            rycount = rycount + 1;
                            #endregion

                            //为所有人增加普通员工的权限
                            JH_Auth_Role rdefault = new JH_Auth_RoleB().GetEntity(p => p.ComId == UserInfo.User.ComId && p.isSysRole == "Y" && p.RoleName == "员工");//找到默认角色
                            if (rdefault != null)
                            {
                                JH_Auth_UserRole jaurdefault = new JH_Auth_UserRole();
                                jaurdefault.ComId = UserInfo.User.ComId;
                                jaurdefault.RoleCode = rdefault.RoleCode;
                                jaurdefault.UserName = jau.UserName;
                                new JH_Auth_UserRoleB().Insert(jaurdefault);
                            }


                        }
                        else
                        {
                            //同步人员时放弃更新现有人员
                            #region 更新人员
                            //user.UserRealName = u.name;
                            //if (u.department.Length > 0)
                            //{
                            //    int id = u.department[0];
                            //    var bm1 = new JH_Auth_BranchB().GetEntity(p => p.ComId == UserInfo.User.ComId && p.WXBMCode == id);
                            //    user.BranchCode = bm1.DeptCode;
                            //}
                            //user.mailbox = u.email;
                            //user.mobphone = u.mobile;
                            //user.weixinnum = u.weixinid;
                            //user.zhiwu = string.IsNullOrEmpty(u.position) ? "员工" : u.position;
                            //user.Sex = u.gender == 1 ? "男" : "女";
                            //if (u.status == 1 || u.status == 4)
                            //{
                            //    user.IsUse = "Y";
                            //    user.isgz = u.status.ToString();
                            //}
                            ////else if (u.status == 2)
                            ////{
                            ////    user.IsUse = "N";
                            ////}
                            //user.txurl = u.avatar;

                            //new JH_Auth_UserB().Update(user);
                            #endregion
                        }

                        #region 更新角色(职务)
                        if (!string.IsNullOrEmpty(u.position))
                        {
                            var r = new JH_Auth_RoleB().GetEntity(p => p.ComId == UserInfo.User.ComId && p.RoleName == u.position);

                            if (r == null)
                            {
                                JH_Auth_Role jar = new JH_Auth_Role();
                                jar.ComId = UserInfo.User.ComId;
                                jar.RoleName = u.position;
                                jar.RoleDec = u.position;
                                jar.PRoleCode = 0;
                                jar.isSysRole = "N";
                                jar.IsUse = "Y";
                                jar.leve = 0;
                                jar.DisplayOrder = 0;

                                new JH_Auth_RoleB().Insert(jar);

                                JH_Auth_UserRole jaur = new JH_Auth_UserRole();
                                jaur.ComId = UserInfo.User.ComId;
                                jaur.RoleCode = jar.RoleCode;
                                jaur.UserName = u.userid;
                                new JH_Auth_UserRoleB().Insert(jaur);


                            }
                            else
                            {
                                //同步人员时放弃更新现有职务
                                //var ur = new JH_Auth_UserRoleB().GetEntity(p => p.ComId == UserInfo.User.ComId && p.UserName == u.userid && p.RoleCode == r.RoleCode);
                                //if (ur == null)
                                //{
                                //    JH_Auth_UserRole jaur = new JH_Auth_UserRole();
                                //    jaur.ComId = UserInfo.User.ComId;
                                //    jaur.RoleCode = r.RoleCode;
                                //    jaur.UserName = u.userid;

                                //    new JH_Auth_UserRoleB().Insert(jaur);
                                //}
                            }
                        }
                        #endregion
                    }
                    //同步人员时放弃删除不在企业号的人员
                    //var ygs = new JH_Auth_UserB().GetEntities(p => p.ComId == UserInfo.User.ComId);
                    //foreach (var l in ygs)
                    //{
                    //    if (yg.userlist.Where(p => p.userid == l.UserName) == null)
                    //    {
                    //        new JH_Auth_UserB().Delete(l);
                    //    }
                    //}
                    #endregion

                    #region 更新标签
                    //Senparc.Weixin.QY.AdvancedAPIs.MailList.GetTagListResult tags = wx.WX_GetTagList();
                    //foreach (var t in tags.taglist)
                    //{
                    //    int tid = Int32.Parse(t.tagid);

                    //    string us = string.Empty;
                    //    Senparc.Weixin.QY.AdvancedAPIs.MailList.GetTagMemberResult bqry = wx.WX_GetTagMember(tid);

                    //    foreach (var u in bqry.userlist)
                    //    {
                    //        if (string.IsNullOrEmpty(us))
                    //        {
                    //            us = u.userid;
                    //        }
                    //        else
                    //        {
                    //            us = us + "," + u.userid;
                    //        }
                    //    }

                    //    var ts = new JH_Auth_UserCustomDataB().GetEntity(p => p.ComId == UserInfo.User.ComId && p.WXBQCode == tid);
                    //    if (ts == null)
                    //    {
                    //        JH_Auth_UserCustomData jar = new JH_Auth_UserCustomData();
                    //        jar.ComId = UserInfo.User.ComId;
                    //        jar.WXBQCode = tid;
                    //        jar.DataType = "USERGROUP";
                    //        jar.DataContent = t.tagname;
                    //        jar.DataContent1 = us;

                    //        new JH_Auth_UserCustomDataB().Insert(jar);
                    //    }
                    //    else
                    //    {
                    //        ts.DataContent = t.tagname;
                    //        ts.DataContent1 = us;
                    //        new JH_Auth_UserCustomDataB().Update(ts);
                    //    }
                    //}

                    #endregion

                    msg.Result1 = bmcount;
                    msg.Result2 = rycount;
                }
                else
                {
                    msg.ErrorMsg = "请至少安装一个应用！";
                }

            }
            catch (Exception ex)
            {
                msg.ErrorMsg = ex.ToString();
            }
        }
        /// <summary>
        /// 是否有托管应用
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void ISTGYY(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            var list = new JH_Auth_QY_ModelB().GetEntities(p => p.ComId == UserInfo.User.ComId && p.AgentId != "");
            msg.Result = list.Count();
        }

        #endregion

        #region 导入通讯录

        /// <summary>
        /// 导入通讯录
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void IMPORTTXL(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            try
            {
                HttpPostedFile _upfile = context.Request.Files["upFile"];
                if (_upfile == null)
                {
                    msg.ErrorMsg = "请选择要上传的文件 ";
                }
                else
                {
                    string fileName = _upfile.FileName;/*获取文件名： C:\Documents and Settings\Administrator\桌面\123.jpg*/
                    string suffix = fileName.Substring(fileName.LastIndexOf(".") + 1).ToLower();/*获取后缀名并转为小写： jpg*/
                    int bytes = _upfile.ContentLength;//获取文件的字节大小   
                    if (suffix == "xls" || suffix == "xlsx")
                    {
                        int bmcount = 0;
                        int rycount = 0;

                        IWorkbook workbook = null;

                        Stream stream = _upfile.InputStream;

                        if (suffix == "xlsx") // 2007版本
                        {
                            workbook = new XSSFWorkbook(stream);
                        }
                        else if (suffix == "xls") // 2003版本
                        {
                            workbook = new HSSFWorkbook(stream);
                        }

                        //获取excel的第一个sheet
                        ISheet sheet = workbook.GetSheetAt(0);

                        //获取sheet的第二行
                        IRow headerRow = sheet.GetRow(1);

                        //一行最后一个方格的编号 即总的列数
                        int cellCount = headerRow.LastCellNum;
                        //最后一列的标号  即总的行数
                        int rowCount = sheet.LastRowNum;

                        WXHelp wx = new WXHelp(UserInfo.QYinfo, "");
                        if (wx.isUseWX == "Y")
                        {
                            #region 使用微信导入excel
                            var list = new JH_Auth_QY_ModelB().GetEntities(p => p.ComId == UserInfo.User.ComId && p.AgentId != "");
                            if (list.Count() > 0)
                            {
                                for (int i = (sheet.FirstRowNum + 2); i <= sheet.LastRowNum; i++)
                                {
                                    JH_Auth_User jau = new JH_Auth_User();
                                    jau.ComId = UserInfo.User.ComId;

                                    IRow row = sheet.GetRow(i);

                                    for (int j = row.FirstCellNum; j < cellCount; j++)
                                    {
                                        if (row.GetCell(j) != null)
                                        {
                                            string value = row.GetCell(j).ToString().Trim();
                                            switch (j)
                                            {
                                                case 0: jau.UserRealName = value; break;
                                                case 1: jau.UserName = value; break;
                                                case 2: jau.Sex = value; break;
                                                case 3: jau.weixinnum = value; break;
                                                case 4: jau.mobphone = value; break;
                                                case 5: jau.mailbox = value; break;
                                                case 6:
                                                    if (value != "")
                                                    {
                                                        string[] bs = value.Split('/');

                                                        int pid = new JH_Auth_BranchB().GetEntity(p => p.ComId == UserInfo.User.ComId && p.WXBMCode == 1).DeptCode;
                                                        for (int m = 0; m < bs.Length; m++)
                                                        {
                                                            string name = bs[m];
                                                            var bm = new JH_Auth_BranchB().GetEntity(p => p.ComId == UserInfo.User.ComId && p.DeptName == name);
                                                            if (bm == null)
                                                            {
                                                                var bms = new JH_Auth_BranchB().GetEntities(p => p.ComId == UserInfo.User.ComId && p.DeptRoot == pid);

                                                                JH_Auth_Branch jab = new JH_Auth_Branch();
                                                                //jab.WXBMCode = l.id;
                                                                jab.ComId = UserInfo.User.ComId;
                                                                jab.DeptName = bs[m];
                                                                jab.DeptDesc = bs[m];
                                                                if (bms.Count() > 0)
                                                                {
                                                                    jab.DeptShort = bms.Max(p => p.DeptShort) + 1;
                                                                }
                                                                else
                                                                {
                                                                    jab.DeptShort = 1;
                                                                }
                                                                jab.Remark1 = new JH_Auth_BranchB().GetBranchNo(UserInfo.QYinfo.ComId, pid);
                                                                jab.DeptRoot = pid;
                                                                try
                                                                {
                                                                    jab.WXBMCode = wx.WX_CreateBranch(jab);

                                                                    new JH_Auth_BranchB().Insert(jab);
                                                                    pid = jab.DeptCode;

                                                                    bmcount = bmcount + 1;
                                                                }
                                                                catch
                                                                {

                                                                }
                                                            }
                                                            else
                                                            {
                                                                pid = bm.DeptCode;
                                                            }
                                                        }
                                                        jau.BranchCode = pid;

                                                    }
                                                    break;
                                                case 7: jau.zhiwu = value; break;
                                            }
                                        }
                                    }
                                    try
                                    {
                                        var user = wx.WX_GetUser(jau.UserName);
                                        if (user == null)
                                        {
                                            wx.WX_CreateUser(jau);
                                            rycount = rycount + 1;
                                        }
                                        else if (string.IsNullOrEmpty(user.userid))
                                        {
                                            wx.WX_CreateUser(jau);
                                            rycount = rycount + 1;
                                        }
                                        else
                                        {
                                            wx.WX_UpdateUser(jau);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        new QjySaaSWeb.AppCode.JH_Auth_LogB().Insert(new QJY.Data.JH_Auth_Log() { CRDate = DateTime.Now, LogContent = ex.ToString() });
                                    }
                                }


                                workbook = null;
                                sheet = null;
                                //同步通讯录
                                TBTXL(context, msg, "", "", UserInfo);

                            }
                            else
                            {
                                msg.ErrorMsg = "请至少安装一个应用！";
                            }
                            #endregion
                        }
                        else if (wx.isUseWX == "N")
                        {
                            #region 没有使用微信导入excel
                            for (int i = (sheet.FirstRowNum + 2); i <= sheet.LastRowNum; i++)
                            {
                                JH_Auth_User jau = new JH_Auth_User();
                                jau.ComId = UserInfo.User.ComId;

                                IRow row = sheet.GetRow(i);

                                for (int j = row.FirstCellNum; j < cellCount; j++)
                                {
                                    if (row.GetCell(j) != null)
                                    {
                                        string value = row.GetCell(j).ToString().Trim();
                                        switch (j)
                                        {
                                            case 0: jau.UserRealName = value; break;
                                            case 1: jau.UserName = value; break;
                                            case 2: jau.Sex = value; break;
                                            case 3: jau.weixinnum = value; break;
                                            case 4: jau.mobphone = value; break;
                                            case 5: jau.mailbox = value; break;
                                            case 6:
                                                if (value != "")
                                                {
                                                    string[] bs = value.Split('/');

                                                    int pid = new JH_Auth_BranchB().GetEntity(p => p.ComId == UserInfo.User.ComId && p.DeptRoot == -1).DeptCode;
                                                    for (int m = 0; m < bs.Length; m++)
                                                    {
                                                        string name = bs[m];
                                                        var bm = new JH_Auth_BranchB().GetEntity(p => p.ComId == UserInfo.User.ComId && p.DeptName == name);
                                                        if (bm == null)
                                                        {
                                                            var bms = new JH_Auth_BranchB().GetEntities(p => p.ComId == UserInfo.User.ComId && p.DeptRoot == pid);

                                                            JH_Auth_Branch jab = new JH_Auth_Branch();
                                                            //jab.WXBMCode = l.id;
                                                            jab.ComId = UserInfo.User.ComId;
                                                            jab.DeptName = bs[m];
                                                            jab.DeptDesc = bs[m];
                                                            if (bms.Count() > 0)
                                                            {
                                                                jab.DeptShort = bms.Max(p => p.DeptShort) + 1;
                                                            }
                                                            else
                                                            {
                                                                jab.DeptShort = 1;
                                                            }
                                                            jab.Remark1 = new JH_Auth_BranchB().GetBranchNo(UserInfo.User.ComId.Value, pid);
                                                            jab.DeptRoot = pid;
                                                            try
                                                            {
                                                                new JH_Auth_BranchB().Insert(jab);
                                                                pid = jab.DeptCode;

                                                                bmcount = bmcount + 1;
                                                            }
                                                            catch
                                                            {

                                                            }
                                                        }
                                                        else
                                                        {
                                                            pid = bm.DeptCode;
                                                        }
                                                    }
                                                    jau.BranchCode = pid;

                                                }
                                                break;
                                            case 7: jau.zhiwu = value; break;
                                        }
                                    }
                                }
                                try
                                {

                                    var user = new JH_Auth_UserB().GetUserByUserName(jau.ComId.Value, jau.UserName);
                                    if (user == null)
                                    {
                                        jau.UserPass = CommonHelp.GetMD5("123456");
                                        jau.IsUse = "Y";
                                        new JH_Auth_UserB().Insert(jau);

                                        rycount = rycount + 1;

                                        //为所有人增加普通员工的权限
                                        JH_Auth_Role rdefault = new JH_Auth_RoleB().GetEntity(p => p.ComId == UserInfo.User.ComId && p.isSysRole == "Y");//找到默认角色
                                        if (rdefault != null)
                                        {
                                            JH_Auth_UserRole jaurdefault = new JH_Auth_UserRole();
                                            jaurdefault.ComId = UserInfo.User.ComId;
                                            jaurdefault.RoleCode = rdefault.RoleCode;
                                            jaurdefault.UserName = jau.UserName;
                                            new JH_Auth_UserRoleB().Insert(jaurdefault);
                                        }

                                    }
                                    else
                                    {
                                        user.BranchCode = jau.BranchCode;
                                        user.mailbox = jau.mailbox;
                                        user.Sex = jau.Sex;
                                        user.UserRealName = jau.UserRealName;
                                        user.zhiwu = string.IsNullOrEmpty(jau.zhiwu) ? "员工" : jau.zhiwu;
                                        user.weixinnum = jau.weixinnum;
                                        user.mobphone = jau.mobphone;
                                        new JH_Auth_UserB().Update(user);
                                    }
                                    string value = jau.zhiwu;
                                    if (!string.IsNullOrEmpty(value))
                                    {
                                        var r = new JH_Auth_RoleB().GetEntity(p => p.ComId == UserInfo.User.ComId && p.RoleName == value);
                                        var rdefault = new JH_Auth_RoleB().GetEntity(p => p.ComId == UserInfo.User.ComId && p.isSysRole == "Y");//找到默认角色

                                        if (r == null)
                                        {
                                            JH_Auth_Role jar = new JH_Auth_Role();
                                            jar.ComId = UserInfo.User.ComId;
                                            jar.RoleName = value;
                                            jar.RoleDec = value;
                                            jar.PRoleCode = 0;
                                            jar.isSysRole = "N";
                                            jar.IsUse = "Y";
                                            jar.leve = 0;
                                            jar.DisplayOrder = 0;

                                            new JH_Auth_RoleB().Insert(jar);

                                            JH_Auth_UserRole jaur = new JH_Auth_UserRole();
                                            jaur.ComId = UserInfo.User.ComId;
                                            jaur.RoleCode = jar.RoleCode;
                                            jaur.UserName = jau.UserName;

                                            new JH_Auth_UserRoleB().Insert(jaur);



                                        }
                                        else
                                        {
                                            var ur = new JH_Auth_UserRoleB().GetEntity(p => p.ComId == UserInfo.User.ComId && p.UserName == jau.UserName);
                                            if (ur == null)
                                            {
                                                JH_Auth_UserRole jaur = new JH_Auth_UserRole();
                                                jaur.ComId = UserInfo.User.ComId;
                                                jaur.RoleCode = r.RoleCode;
                                                jaur.UserName = jau.UserName;

                                                new JH_Auth_UserRoleB().Insert(jaur);
                                            }
                                            else
                                            {
                                                ur.RoleCode = r.RoleCode;
                                                new JH_Auth_UserRoleB().Update(ur);
                                            }
                                        }
                                    }
                                }
                                catch
                                {
                                }
                            }

                            workbook = null;
                            sheet = null;

                            #endregion
                        }

                        msg.Result1 = bmcount;
                        msg.Result2 = rycount;
                    }
                    else
                    {
                        msg.ErrorMsg = "请上传excel文件 ";
                    }
                }
            }
            catch (Exception ex)
            {
                msg.ErrorMsg = ex.ToString();
            }

        }

        #endregion

        #region excel转换为table

        /// <summary>
        /// excel转换为table
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void EXCELTOTABLE(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            try
            {
                DataTable dt = new DataTable();
                HttpPostedFile _upfile = context.Request.Files["upFile"];
                string headrow = context.Request["headrow"] ?? "0";//头部开始行下标
                if (_upfile == null)
                {
                    msg.ErrorMsg = "请选择要上传的文件 ";
                }
                else
                {
                    string fileName = _upfile.FileName;/*获取文件名： C:\Documents and Settings\Administrator\桌面\123.jpg*/
                    string suffix = fileName.Substring(fileName.LastIndexOf(".") + 1).ToLower();/*获取后缀名并转为小写： jpg*/
                    int bytes = _upfile.ContentLength;//获取文件的字节大小   
                    if (suffix == "xls" || suffix == "xlsx")
                    {
                        IWorkbook workbook = null;

                        Stream stream = _upfile.InputStream;

                        if (suffix == "xlsx") // 2007版本
                        {
                            workbook = new XSSFWorkbook(stream);
                        }
                        else if (suffix == "xls") // 2003版本
                        {
                            workbook = new HSSFWorkbook(stream);
                        }

                        //获取excel的第一个sheet
                        ISheet sheet = workbook.GetSheetAt(0);

                        //获取sheet的第一行
                        IRow headerRow = sheet.GetRow(int.Parse(headrow));

                        //一行最后一个方格的编号 即总的列数
                        int cellCount = headerRow.LastCellNum;
                        //最后一列的标号  即总的行数
                        int rowCount = sheet.LastRowNum;
                        if (rowCount <= int.Parse(headrow))
                        {
                            msg.ErrorMsg = "文件中无数据! ";
                        }
                        else
                        {
                            List<CommonHelp.IMPORTYZ> yz = new List<CommonHelp.IMPORTYZ>();
                            CommonHelp ch = new CommonHelp();
                            yz = ch.GetTable(P1, UserInfo.QYinfo.ComId);//获取验证字段

                            string str1 = string.Empty;//验证字段是否包含列名
                            //列名
                            for (int i = 0; i < cellCount; i++)
                            {
                                string strlm = headerRow.GetCell(i).ToString().Trim();
                                if (yz.Count > 0)
                                {
                                    #region 字段是否包含列名验证
                                    var l = yz.Where(p => p.Name == strlm);//验证字段是否包含列名
                                    if (l.Count() == 0)
                                    {
                                        if (string.IsNullOrEmpty(str1))
                                        {
                                            str1 = "文件中的【" + strlm + "】";
                                        }
                                        else
                                        {
                                            str1 = str1 + "、【" + strlm + "】";
                                        }

                                        strlm = strlm + "<span style='color:red;'>不会导入</span>";
                                    }
                                    #endregion
                                }
                                dt.Columns.Add(strlm);//添加列名
                            }

                            if (!string.IsNullOrEmpty(str1))
                            {
                                str1 = str1 + " 不属于当前导入的字段!<br>";
                            }

                            dt.Columns.Add("status", Type.GetType("System.String"));

                            string str2 = string.Empty;//验证必填字段是否存在

                            #region 必填字段在文件中存不存在验证
                            foreach (var v in yz.Where(p => p.IsNull == 1))
                            {
                                if (!dt.Columns.Contains(v.Name))
                                {
                                    if (string.IsNullOrEmpty(str2))
                                    {
                                        str2 = "当前导入的必填字段：【" + v.Name + "】";
                                    }
                                    else
                                    {
                                        str2 = str2 + "、【" + v.Name + "】";
                                    }
                                }
                            }

                            if (!string.IsNullOrEmpty(str2))
                            {
                                str2 = str2 + " 在文件中不存在!<br>";
                            }
                            #endregion

                            string str3 = string.Empty;//验证必填字段是否有值
                            string str4 = string.Empty;//验证字段是否重复
                            string str5 = string.Empty;//验证字段是否存在

                            for (int i = (sheet.FirstRowNum + int.Parse(headrow) + 1); i <= sheet.LastRowNum; i++)
                            {
                                string str31 = string.Empty;
                                string str41 = string.Empty;
                                string str42 = string.Empty;
                                string str51 = string.Empty;

                                DataRow dr = dt.NewRow();
                                bool bl = false;
                                IRow row = sheet.GetRow(i);

                                dr["status"] = "0";

                                for (int j = row.FirstCellNum; j < cellCount; j++)
                                {
                                    string strsj = row.GetCell(j) != null ? row.GetCell(j).ToString().Trim() : "";
                                    if (strsj != "")
                                    {
                                        bl = true;
                                    }

                                    foreach (var v in yz.Where(p => p.Name == headerRow.GetCell(j).ToString().Trim()))
                                    {
                                        if (strsj == "")
                                        {
                                            #region 必填字段验证
                                            if (v.IsNull == 1)
                                            {
                                                //strsj = "<span style='color:red;'>必填</span>";

                                                if (string.IsNullOrEmpty(str31))
                                                {
                                                    str31 = "第" + (i + 1) + "行的必填字段：【" + v.Name + "】";
                                                }
                                                else
                                                {
                                                    str31 = str31 + "、【" + v.Name + "】";
                                                }
                                                dr["status"] = "2";
                                            }
                                            #endregion
                                        }
                                        else
                                        {
                                            #region 长度验证
                                            if (v.Length != 0)
                                            {
                                                if (Encoding.Default.GetBytes(strsj).Length > v.Length)
                                                {
                                                    strsj = strsj + "<span style='color:red;'>长度不能超过" + v.Length + "</span>";
                                                    dr["status"] = "2";
                                                }
                                            }
                                            #endregion
                                            #region 重复验证
                                            if (!string.IsNullOrEmpty(v.IsRepeat))
                                            {
                                                #region 与现有数据比较是否重复
                                                string[] strRS = v.IsRepeat.Split('|');

                                                var cf = new JH_Auth_UserB().GetDTByCommand("select * from " + strRS[0] + " where " + strRS[1] + "= '" + strsj + "' and ComId='" + UserInfo.QYinfo.ComId + "'");
                                                if (cf.Rows.Count > 0)
                                                {
                                                    if (string.IsNullOrEmpty(str41))
                                                    {
                                                        str41 = "第" + (i + 1) + "行的字段：【" + v.Name + "】" + strsj;
                                                    }
                                                    else
                                                    {
                                                        str41 = str41 + "、【" + v.Name + "】:" + strsj;
                                                    }
                                                    dr["status"] = "2";
                                                }
                                                #endregion
                                                #region 与Excel中数据比较是否重复
                                                DataRow[] drs = dt.Select(headerRow.GetCell(j).ToString().Trim() + "='" + strsj + "'");
                                                if (drs.Length > 0)
                                                {
                                                    if (string.IsNullOrEmpty(str42))
                                                    {
                                                        str42 = "第" + (i + 1) + "行的字段：【" + v.Name + "】" + strsj;
                                                    }
                                                    else
                                                    {
                                                        str42 = str42 + "、【" + v.Name + "】" + strsj;
                                                    }
                                                    dr["status"] = "2";
                                                }
                                                #endregion
                                            }
                                            #endregion
                                            #region 存在验证
                                            if (!string.IsNullOrEmpty(v.IsExist))
                                            {
                                                string[] strES = v.IsExist.Split('|');

                                                var cz = new JH_Auth_UserB().GetDTByCommand("select * from " + strES[0] + " where " + strES[1] + "= '" + strsj + "' and ComId='" + UserInfo.QYinfo.ComId + "'");
                                                if (cz.Rows.Count == 0)
                                                {
                                                    if (string.IsNullOrEmpty(str51))
                                                    {
                                                        str51 = "第" + (i + 1) + "行的字段：【" + v.Name + "】" + strsj;
                                                    }
                                                    else
                                                    {
                                                        str51 = str51 + "、【" + v.Name + "】" + strsj;
                                                    }
                                                    dr["status"] = "2";
                                                }
                                            }
                                            #endregion
                                        }
                                    }

                                    dr[j] = strsj;
                                }
                                if (!string.IsNullOrEmpty(str31))
                                {
                                    str31 = str31 + " 不能为空！<br>";
                                    str3 = str3 + str31;
                                }
                                if (!string.IsNullOrEmpty(str41))
                                {
                                    str41 = str41 + " 已经存在！<br>";
                                    str4 = str4 + str41;
                                }
                                if (!string.IsNullOrEmpty(str42))
                                {
                                    str42 = str42 + " 在文件中已经存在！<br>";
                                    str4 = str4 + str42;
                                }
                                if (!string.IsNullOrEmpty(str51))
                                {
                                    str51 = str51 + " 不存在！<br>";
                                    str5 = str5 + str51;
                                }

                                if (bl)
                                {
                                    dt.Rows.Add(dr);
                                }
                            }
                            if (string.IsNullOrEmpty(str2) && string.IsNullOrEmpty(str3) && string.IsNullOrEmpty(str4) && string.IsNullOrEmpty(str5))
                            {
                                msg.Result = dt;
                            }

                            msg.Result1 = str1 + str2 + str3 + str4 + str5;
                        }

                        sheet = null;
                        workbook = null;
                    }
                    else
                    {
                        msg.ErrorMsg = "请上传excel文件 ";
                    }
                }
            }
            catch (Exception ex)
            {
                //msg.ErrorMsg = ex.ToString();
                msg.ErrorMsg = "导入失败！";
            }
        }

        #endregion

        #region 导出模板excel

        /// <summary>
        /// 导出模板excel
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void EXPORTTOEXCEL(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            try
            {
                List<CommonHelp.IMPORTYZ> yz = new List<CommonHelp.IMPORTYZ>();
                CommonHelp ch = new CommonHelp();
                yz = ch.GetTable(P1, UserInfo.QYinfo.ComId);//获取字段
                if (yz.Count > 0)
                {
                    HSSFWorkbook workbook = new HSSFWorkbook();
                    ISheet sheet = workbook.CreateSheet("Sheet1");

                    ICellStyle HeadercellStyle = workbook.CreateCellStyle();
                    HeadercellStyle.BorderBottom = BorderStyle.Thin;
                    HeadercellStyle.BorderLeft = BorderStyle.Thin;
                    HeadercellStyle.BorderRight = BorderStyle.Thin;
                    HeadercellStyle.BorderTop = BorderStyle.Thin;
                    HeadercellStyle.Alignment = HorizontalAlignment.Center;
                    HeadercellStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.SkyBlue.Index;
                    HeadercellStyle.FillPattern = FillPattern.SolidForeground;
                    HeadercellStyle.FillBackgroundColor = NPOI.HSSF.Util.HSSFColor.SkyBlue.Index;

                    //字体
                    NPOI.SS.UserModel.IFont headerfont = workbook.CreateFont();
                    headerfont.Boldweight = (short)FontBoldWeight.Bold;
                    headerfont.FontHeightInPoints = 12;
                    HeadercellStyle.SetFont(headerfont);


                    //用column name 作为列名
                    int icolIndex = 0;
                    IRow headerRow = sheet.CreateRow(0);
                    foreach (var l in yz)
                    {
                        ICell cell = headerRow.CreateCell(icolIndex);
                        cell.SetCellValue(l.Name);
                        cell.CellStyle = HeadercellStyle;
                        icolIndex++;
                    }

                    ICellStyle cellStyle = workbook.CreateCellStyle();

                    //为避免日期格式被Excel自动替换，所以设定 format 为 『@』 表示一率当成text來看
                    cellStyle.DataFormat = HSSFDataFormat.GetBuiltinFormat("@");
                    cellStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
                    cellStyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
                    cellStyle.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
                    cellStyle.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;


                    NPOI.SS.UserModel.IFont cellfont = workbook.CreateFont();
                    cellfont.Boldweight = (short)FontBoldWeight.Normal;
                    cellStyle.SetFont(cellfont);

                    string strDataJson = ch.GetExcelData(P1);
                    if (strDataJson != "")
                    {
                        string[] strs = strDataJson.Split(',');

                        //建立内容行

                        int iCellIndex = 0;

                        IRow DataRow = sheet.CreateRow(1);
                        for (int i = 0; i < strs.Length; i++)
                        {

                            ICell cell = DataRow.CreateCell(iCellIndex);
                            cell.SetCellValue(strs[i]);
                            cell.CellStyle = cellStyle;
                            iCellIndex++;
                        }
                    }

                    //自适应列宽度
                    for (int i = 0; i < icolIndex; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }

                    using (MemoryStream ms = new MemoryStream())
                    {
                        workbook.Write(ms);

                        HttpContext curContext = HttpContext.Current;

                        string strName = string.Empty;
                        if (P1 == "KHGL")
                        {
                            strName = "客户";
                        }
                        else if (P1 == "KHLXR")
                        {
                            strName = "客户联系人";
                        }

                        // 设置编码和附件格式
                        curContext.Response.ContentType = "application/vnd.ms-excel";
                        curContext.Response.ContentEncoding = Encoding.UTF8;
                        curContext.Response.Charset = "";
                        curContext.Response.AppendHeader("Content-Disposition",
                            "attachment;filename=" + HttpUtility.UrlEncode("CRM_" + strName + "_模板文件.xls", Encoding.UTF8));

                        curContext.Response.BinaryWrite(ms.GetBuffer());
                        curContext.Response.End();

                        workbook = null;
                        ms.Close();
                        ms.Dispose();
                    }
                }

            }
            catch
            {
                msg.ErrorMsg = "导入失败！";
            }
        }

        /// <summary>
        /// 下载模板excel
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void DOWNLOADEXCEL(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            try
            {
                string strName = string.Empty;
                if (P1 == "KHGL")
                {
                    strName = "CRM_客户_导入模板.xls";
                }
                else if (P1 == "KHLXR")
                {
                    strName = "CRM_客户联系人_导入模板.xls";
                }
                else if (P1 == "HTGL")
                {
                    strName = "CRM_合同_导入模板.xls";
                }
                HttpContext curContext = HttpContext.Current;
                string headrow = context.Request["headrow"] ?? "0";//头部开始行下标
                string path = curContext.Server.MapPath(@"/ViewV5/base/" + strName);
                FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read);
                string suffix = path.Substring(path.LastIndexOf(".") + 1).ToLower();

                IWorkbook workbook = null;

                if (suffix == "xlsx") // 2007版本
                {
                    workbook = new XSSFWorkbook(file);
                }
                else if (suffix == "xls") // 2003版本
                {
                    workbook = new HSSFWorkbook(file);
                }
                ISheet sheet = workbook.GetSheetAt(0);

                IRow headerRow = sheet.GetRow(int.Parse(headrow));
                IRow oneRow = sheet.GetRow(int.Parse(headrow) + 1);

                int icolIndex = headerRow.Cells.Count;

                DataTable dtExtColumn = new JH_Auth_ExtendModeB().GetExtColumnAll(UserInfo.QYinfo.ComId, P1);
                foreach (DataRow drExt in dtExtColumn.Rows)
                {
                    ICell cell = headerRow.CreateCell(icolIndex);
                    cell.SetCellValue(drExt["TableFiledName"].ToString());
                    cell.CellStyle = headerRow.Cells[icolIndex - 1].CellStyle;

                    ICell onecell = oneRow.CreateCell(icolIndex);
                    onecell.SetCellValue("");
                    onecell.CellStyle = oneRow.Cells[icolIndex - 1].CellStyle;

                    icolIndex++;
                }

                //自适应列宽度
                for (int i = 0; i < icolIndex; i++)
                {
                    sheet.AutoSizeColumn(i);
                }

                if (P1 == "KHGL")
                {
                    //表头样式
                    ICellStyle HeadercellStyle = workbook.CreateCellStyle();
                    HeadercellStyle.BorderBottom = BorderStyle.Thin;
                    HeadercellStyle.BorderLeft = BorderStyle.Thin;
                    HeadercellStyle.BorderRight = BorderStyle.Thin;
                    HeadercellStyle.BorderTop = BorderStyle.Thin;
                    HeadercellStyle.Alignment = HorizontalAlignment.Center;

                    //字体
                    NPOI.SS.UserModel.IFont headerfont = workbook.CreateFont();
                    headerfont.Boldweight = (short)FontBoldWeight.Bold;
                    headerfont.FontHeightInPoints = 12;
                    HeadercellStyle.SetFont(headerfont);

                    //单元格样式
                    ICellStyle cellStyle = workbook.CreateCellStyle();

                    //为避免日期格式被Excel自动替换，所以设定 format 为 『@』 表示一率当成text來看
                    cellStyle.DataFormat = HSSFDataFormat.GetBuiltinFormat("@");
                    cellStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
                    cellStyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
                    cellStyle.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
                    cellStyle.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;


                    NPOI.SS.UserModel.IFont cellfont = workbook.CreateFont();
                    cellfont.Boldweight = (short)FontBoldWeight.Normal;
                    headerfont.FontHeightInPoints = 10;
                    cellStyle.SetFont(cellfont);

                    for (int i = 10; i < 15; i++)
                    {
                        string strZTName = string.Empty;
                        if (i == 10) { strZTName = "客户类型"; }
                        if (i == 11) { strZTName = "跟进状态"; }
                        if (i == 12) { strZTName = "客户来源"; }
                        if (i == 13) { strZTName = "所属行业"; }
                        if (i == 14) { strZTName = "人员规模"; }
                        ISheet sheet1 = workbook.CreateSheet(strZTName);
                        IRow headerRow1 = sheet1.CreateRow(0);
                        ICell cell1 = headerRow1.CreateCell(0);
                        cell1.SetCellValue(strZTName);
                        cell1.CellStyle = HeadercellStyle;

                        int rowindex1 = 1;

                        foreach (var l in new JH_Auth_ZiDianB().GetEntities(p => p.ComId == UserInfo.QYinfo.ComId && p.Class == i))
                        {
                            IRow DataRow = sheet1.CreateRow(rowindex1);
                            ICell cell = DataRow.CreateCell(0);
                            cell.SetCellValue(l.TypeName);
                            cell.CellStyle = cellStyle;
                            rowindex1++;
                        }

                        sheet1.AutoSizeColumn(0);
                    }
                }
                if (P1 == "HTGL")
                {
                    //表头样式
                    ICellStyle HeadercellStyle = workbook.CreateCellStyle();
                    HeadercellStyle.BorderBottom = BorderStyle.Thin;
                    HeadercellStyle.BorderLeft = BorderStyle.Thin;
                    HeadercellStyle.BorderRight = BorderStyle.Thin;
                    HeadercellStyle.BorderTop = BorderStyle.Thin;
                    HeadercellStyle.Alignment = HorizontalAlignment.Center;

                    //字体
                    NPOI.SS.UserModel.IFont headerfont = workbook.CreateFont();
                    headerfont.Boldweight = (short)FontBoldWeight.Bold;
                    headerfont.FontHeightInPoints = 12;
                    HeadercellStyle.SetFont(headerfont);

                    //单元格样式
                    ICellStyle cellStyle = workbook.CreateCellStyle();

                    //为避免日期格式被Excel自动替换，所以设定 format 为 『@』 表示一率当成text來看
                    cellStyle.DataFormat = HSSFDataFormat.GetBuiltinFormat("@");
                    cellStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
                    cellStyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
                    cellStyle.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
                    cellStyle.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;


                    NPOI.SS.UserModel.IFont cellfont = workbook.CreateFont();
                    cellfont.Boldweight = (short)FontBoldWeight.Normal;
                    headerfont.FontHeightInPoints = 10;
                    cellStyle.SetFont(cellfont);

                    for (int i = 16; i < 18; i++)
                    {
                        string strZTName = string.Empty;
                        if (i == 16) { strZTName = "合同类型"; }
                        if (i == 17) { strZTName = "付款方式"; }
                        ISheet sheet1 = workbook.CreateSheet(strZTName);
                        IRow headerRow1 = sheet1.CreateRow(0);
                        ICell cell1 = headerRow1.CreateCell(0);
                        cell1.SetCellValue(strZTName);
                        cell1.CellStyle = HeadercellStyle;

                        int rowindex1 = 1;

                        foreach (var l in new JH_Auth_ZiDianB().GetEntities(p => p.ComId == UserInfo.QYinfo.ComId && p.Class == i))
                        {
                            IRow DataRow = sheet1.CreateRow(rowindex1);
                            ICell cell = DataRow.CreateCell(0);
                            cell.SetCellValue(l.TypeName);
                            cell.CellStyle = cellStyle;
                            rowindex1++;
                        }

                        sheet1.AutoSizeColumn(0);
                    }
                }

                using (MemoryStream ms = new MemoryStream())
                {
                    workbook.Write(ms);

                    //HttpContext curContext = HttpContext.Current;

                    // 设置编码和附件格式
                    curContext.Response.ContentType = "application/vnd.ms-excel";
                    curContext.Response.ContentEncoding = Encoding.UTF8;
                    curContext.Response.Charset = "";
                    curContext.Response.AppendHeader("Content-Disposition",
                        "attachment;filename=" + HttpUtility.UrlEncode(strName, Encoding.UTF8));

                    curContext.Response.BinaryWrite(ms.GetBuffer());
                    curContext.Response.End();

                    workbook = null;
                    ms.Close();
                    ms.Dispose();
                }
            }
            catch
            {
                msg.ErrorMsg = "下载失败！";
            }
        }

        #endregion

        #region 菜单应用
        //添加菜单应用及接口
        public void ADDFUNCTION(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            JH_Auth_Function function = JsonConvert.DeserializeObject<JH_Auth_Function>(P1);
            if (function.ID == 0)
            {
                if (new JH_Auth_FunctionB().GetEntities(d => d.ComId == UserInfo.User.ComId && d.PageName == function.PageName && d.PageUrl == function.PageUrl).Count() > 0)
                {
                    msg.ErrorMsg = "此页面已存在";
                }
                else
                {
                    function.CRUser = UserInfo.User.UserName;
                    function.CRDate = DateTime.Now;
                    new JH_Auth_FunctionB().Insert(function);
                }
            }
            else
            {
                new JH_Auth_FunctionB().Update(function);
            }

        }
        //获取应用菜单及接口
        public void GETFUNCTION(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            string strSql = string.Format("select JH_Auth_Model.* from JH_Auth_QY_Model INNER JOIN JH_Auth_Model on JH_Auth_QY_Model.ModelID=JH_Auth_Model.ID  WHERE JH_Auth_QY_Model.ComId={0} and JH_Auth_QY_Model.Status=1", UserInfo.User.ComId);
            DataTable dt = new JH_Auth_ModelB().GetDTByCommand(strSql);
            dt.Columns.Add("FunData", Type.GetType("System.Object"));
            DataTable dtRoleFun = new JH_Auth_RoleFunB().GetDTByCommand(@"SELECT DISTINCT fun.*,rolefun.ActionCode RoleFun,rolefun.FunCode 
                                                                            from JH_Auth_Function fun left join JH_Auth_RoleFun rolefun on fun.ID=rolefun.FunCode and rolefun.ComId=" + UserInfo.User.ComId + "and rolefun.RoleCode=" + P1 + " Where  fun.ComId=0 or fun.ComId=" + UserInfo.User.ComId);
            int roleId = 0;
            int.TryParse(P1, out roleId);
            JH_Auth_Role roleModel = new JH_Auth_RoleB().GetEntity(d => d.RoleCode == roleId && d.ComId == UserInfo.User.ComId);
            string isinit = "N";//是否需要默认加载权限
            if (roleModel.isSysRole == "Y")
            {
                DataRow[] roleFun = dtRoleFun.Select(" RoleFun is not null");
                isinit = roleFun.Count() > 0 ? "N" : "Y";//>0已分配过权限，==0未分配权限
            }

            foreach (DataRow row in dt.Rows)
            {
                int modelId = int.Parse(row["ID"].ToString());
                row["FunData"] = dtRoleFun.FilterTable(" ModelID =" + modelId);
            }
            msg.Result = dt;
            msg.Result1 = isinit;
        }
        //添加角色接口权限
        public void ADDROLEFUN(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int roleCode = int.Parse(P1);
            //删除之前设置的接口权限
            new JH_Auth_RoleFunB().Delete(d => d.ComId == UserInfo.User.ComId && d.RoleCode == roleCode);
            //添加要设置的接口权限
            List<JH_Auth_RoleFun> roleFunList = JsonConvert.DeserializeObject<List<JH_Auth_RoleFun>>(P2);
            foreach (JH_Auth_RoleFun fun in roleFunList)
            {
                fun.ComId = UserInfo.User.ComId;
                new JH_Auth_RoleFunB().Insert(fun);
            }
        }
        //获取应用二级菜单
        public void GETFUNCTIONDATE(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int modelId = int.Parse(P1);
            msg.Result = new JH_Auth_FunctionB().GetEntities(d => d.ModelID == modelId);
        }
        public void DELFUNDATA(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = int.Parse(P1);
            new JH_Auth_FunctionB().Delete(d => d.ID == Id);
        }

        //获取二级菜单详细
        public void GETFUNCTIONMODEL(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = int.Parse(P1);
            msg.Result = new JH_Auth_FunctionB().GetEntity(d => d.ID == Id);
        }  //初始化系统菜单数据

        #endregion

        #region 用户自定义分组
        //获取自定义列表
        public void GETUSERGROUP(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            msg.Result = new JH_Auth_UserCustomDataB().GetEntities(d => d.UserName == UserInfo.User.UserName && d.ComId == UserInfo.User.ComId && d.DataType == P1);
            if (P1 == "DXGL")
            {
                string sql = string.Format("SELECT Distinct DataContent1 FROM JH_Auth_UserCustomData WHERE dataType='DXGL' AND UserName = '{0}' AND ComId={1} GROUP BY DataContent1 ORDER BY DataContent1 DESC", UserInfo.User.UserName, UserInfo.User.ComId);
                msg.Result1 = new JH_Auth_UserCustomDataB().GetDTByCommand(sql);
            }
        }
        //添加自定义组
        public void ADDUSERGROUP(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            JH_Auth_UserCustomData customData = new JH_Auth_UserCustomData();
            customData.ComId = UserInfo.User.ComId;
            customData.CRDate = DateTime.Now;
            customData.CRUser = UserInfo.User.UserName;
            customData.DataContent = P1;
            customData.DataContent1 = P2.Trim();
            customData.DataType = context.Request["DataType"];
            customData.UserName = UserInfo.User.UserName;
            new JH_Auth_UserCustomDataB().Insert(customData);
            msg.Result = customData;
        }
        //根据组id获取用户列表
        public void GETUSERLISTBYGROUP(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = int.Parse(P1);
            JH_Auth_UserCustomData customData = new JH_Auth_UserCustomDataB().GetEntity(d => d.ID == Id);
            string[] usernames = customData.DataContent1.Split(',');
            msg.Result = new JH_Auth_UserB().GetEntities(d => usernames.Contains(d.UserName) && d.ComId == UserInfo.User.ComId);

        }
        //删除用户自定义分组 ，短信模板
        public void DELUSERGROUP(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = int.Parse(P1);
            new JH_Auth_UserCustomDataB().Delete(d => d.ID == Id);
        }
        //删除通讯录分类分组
        public void DELUSERGROUPTXL(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = int.Parse(P1);
            DataTable dt = new JH_Auth_UserCustomDataB().GetDTByCommand("SELECT * from SZHL_TXL where TagName=" + Id + " and ComId=" + UserInfo.User.ComId);
            if (dt.Rows.Count == 0)
            {
                new JH_Auth_UserCustomDataB().Delete(d => d.ID == Id);
            }
            else
            {
                msg.ErrorMsg = "请先删除此分类下的人员信息";
            }
        }


        #endregion

        #region 获取是否可以录入快递角色
        public void GETKDROLE(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            JH_Auth_ZiDian sfkd = new JH_Auth_ZiDianB().GetEntity(d => d.ComId == UserInfo.User.ComId && d.TypeNO == "KDGL");
            if (sfkd != null)
            {
                string[] userNames = sfkd.Remark.Split(',');
                if (userNames.Contains(UserInfo.User.UserName))
                {
                    msg.Result = "Y";
                }
                else
                {
                    msg.Result = "N";
                }
            }
            else
            {
                msg.Result = "N";
            }
        }
        #endregion
        #region 获取是否可以录入菜品角色
        public void GETCPROLE(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            JH_Auth_ZiDian sccd = new JH_Auth_ZiDianB().GetEntity(d => d.ComId == UserInfo.User.ComId && d.TypeNO == "DCGL");
            JH_Auth_ZiDian jdcd = new JH_Auth_ZiDianB().GetEntity(d => d.ComId == UserInfo.User.ComId && d.TypeNO == "DCGLA");

            if (sccd != null)
            {
                string[] userNames = sccd.Remark.Split(',');
                if (userNames.Contains(UserInfo.User.UserName))
                {
                    msg.Result = "Y";
                }
                else
                {
                    msg.Result = "N";
                }
            }
            else
            {
                msg.Result = "N";
            }
            if (jdcd != null)
            {
                string[] userNames = jdcd.Remark.Split(',');
                if (userNames.Contains(UserInfo.User.UserName))
                {
                    msg.Result1 = "Y";
                }
                else
                {
                    msg.Result1 = "N";
                }
            }
            else
            {
                msg.Result1 = "N";
            }
        }
        #endregion

        #region 系统日志
        public void GEXTRZ(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //string strWhere = string.Format(" ComId={0}  And CRUser='{1}'", UserInfo.User.ComId, UserInfo.User.UserName);
            string strWhere = string.Format(" ComId={0} ", UserInfo.User.ComId);
            if (P1 != "")
            {
                strWhere += string.Format(" and LogContent like  '%{0}%'", P1);
            }
            if (P2 != "")
            {
                strWhere += string.Format(" and LogType IN ('{0}')", P2.Replace(",", "','"));  //多个类型逗号隔开传过来
            }
            int page = 0;
            int pagecount = 8;
            int.TryParse(context.Request.QueryString["p"] ?? "0", out page);
            int.TryParse(context.Request.QueryString["pagecount"] ?? "8", out pagecount);//页数
            page = page == 0 ? 1 : page;
            int total = 0;
            DataTable dt = new JH_Auth_LogB().GetDataPager("JH_Auth_Log", "* ", pagecount, page, " CRDate desc ", strWhere, ref total);
            msg.Result = dt;
            msg.Result1 = total;

        }
        #endregion


        #region 自定义设置（上传菜单人员，收发快递人员）
        public void ADDZDYSZSCCD(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            // JH_Auth_ZiDian zdysz = JsonConvert.DeserializeObject<JH_Auth_ZiDian>(P1);


            JH_Auth_ZiDian zdysz1 = new JH_Auth_ZiDianB().GetEntity(d => d.ComId == UserInfo.User.ComId && d.TypeNO == "DCGL");
            if (P1 == "")
            {
                msg.ErrorMsg = "请选择上传菜单人员";
                return;
            }
            if (zdysz1 == null)
            {
                JH_Auth_ZiDian zdysz = new JH_Auth_ZiDian();
                zdysz.Remark = P1;
                zdysz.ComId = UserInfo.User.ComId;
                zdysz.CRUser = UserInfo.User.UserName;
                zdysz.CRDate = DateTime.Now;
                zdysz.TypeNO = "DCGL";
                zdysz.TypeName = "上传菜单人员";
                new JH_Auth_ZiDianB().Insert(zdysz);
            }
            else
            {
                zdysz1.Remark = P1;
                new JH_Auth_ZiDianB().Update(zdysz1);
            }

        }
        public void ADDZDYSZSFKD(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //JH_Auth_ZiDian zdysz = JsonConvert.DeserializeObject<JH_Auth_ZiDian>(P1);


            JH_Auth_ZiDian zdysz1 = new JH_Auth_ZiDianB().GetEntity(d => d.ComId == UserInfo.User.ComId && d.TypeNO == "KDGL");
            if (P1 == "")
            {
                msg.ErrorMsg = "请选择收发快递人员";
                return;
            }
            if (zdysz1 == null)
            {
                JH_Auth_ZiDian zdysz = new JH_Auth_ZiDian();
                zdysz.Remark = P1;
                zdysz.ComId = UserInfo.User.ComId;
                zdysz.CRUser = UserInfo.User.UserName;
                zdysz.CRDate = DateTime.Now;
                zdysz.TypeNO = "KDGL";
                zdysz.TypeName = "收发快递人员";
                new JH_Auth_ZiDianB().Insert(zdysz);
            }
            else
            {
                zdysz1.Remark = P1;
                new JH_Auth_ZiDianB().Update(zdysz1);
            }
        }
        public void ADDZDYSZJDRY(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //JH_Auth_ZiDian zdysz = JsonConvert.DeserializeObject<JH_Auth_ZiDian>(P1);


            JH_Auth_ZiDian zdysz1 = new JH_Auth_ZiDianB().GetEntity(d => d.ComId == UserInfo.User.ComId && d.TypeNO == "DCGLA");
            if (P1 == "")
            {
                msg.ErrorMsg = "请选择菜单接单人员";
                return;
            }
            if (zdysz1 == null)
            {
                JH_Auth_ZiDian zdysz = new JH_Auth_ZiDian();
                zdysz.Remark = P1;
                zdysz.ComId = UserInfo.User.ComId;
                zdysz.CRUser = UserInfo.User.UserName;
                zdysz.CRDate = DateTime.Now;
                zdysz.TypeNO = "DCGLA";
                zdysz.TypeName = "接单人员";
                new JH_Auth_ZiDianB().Insert(zdysz);
            }
            else
            {
                zdysz1.Remark = P1;
                new JH_Auth_ZiDianB().Update(zdysz1);
            }
        }
        public void GETZDYSZCONTENT(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            JH_Auth_ZiDian jdry = new JH_Auth_ZiDianB().GetEntity(d => d.ComId == UserInfo.User.ComId && d.TypeNO == "DCGLA");
            JH_Auth_ZiDian sccd = new JH_Auth_ZiDianB().GetEntity(d => d.ComId == UserInfo.User.ComId && d.TypeNO == "DCGL");
            JH_Auth_ZiDian sfkd = new JH_Auth_ZiDianB().GetEntity(d => d.ComId == UserInfo.User.ComId && d.TypeNO == "KDGL");
            if (sccd != null)
            {
                msg.Result = sccd.Remark;
            }
            if (sfkd != null)
            {
                msg.Result1 = sfkd.Remark;
            }
            if (jdry != null)
            {
                msg.Result2 = jdry.Remark;
            }
        }
        #endregion



        #region 订单管理
        /// <summary>
        /// 添加订单
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void ADDDDGL(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            SZHL_DDGL dcgl = JsonConvert.DeserializeObject<SZHL_DDGL>(P1);

            if (!dcgl.OrderMoney.HasValue || dcgl.OrderMoney < 100)
            {
                msg.ErrorMsg = "订单金额错误";
            }
            else
            {
                dcgl.OrderID = DateTime.Now.ToString("yyyyMMddHHmmssfff");
                dcgl.Status = "未支付";
                dcgl.ComId = UserInfo.QYinfo.ComId;
                dcgl.CRDate = DateTime.Now;
                dcgl.CRUser = UserInfo.User.UserName;

                new SZHL_DDGLB().Insert(dcgl);

                msg.Result = dcgl.OrderID;

            }


        }
        public void GETSZMX(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strWhere = " cc.ComId=" + UserInfo.User.ComId;
            if (P1 != "")
            {
                strWhere += string.Format(" and cc.Memo like '%{0}%'", P1);
            }
            if (P2 != "")
            {
                if (P2 == "1") //收入
                {
                    strWhere += string.Format(" and cc.Change>0 ");
                }
                else if (P2 == "2") //支出
                {
                    strWhere += string.Format(" and cc.Change<0 ");
                }
            }
            int page = 0;
            int pagecount = 8;
            int.TryParse(context.Request.QueryString["p"] ?? "1", out page);
            int.TryParse(context.Request.QueryString["pagecount"] ?? "8", out pagecount);//页数
            page = page == 0 ? 1 : page;
            int total = 0;
            DataTable dt = new SZHL_CCXJB().GetDataPager(" SZHL_DDGL_ITEM cc", "cc.*", pagecount, page, " cc.CRDate desc", strWhere, ref total);

            msg.Result = dt;
            msg.Result1 = total;

        }
        #endregion


        #region  获取评论
        public void GETCOMENT(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            msg.Result = new JH_Auth_TLB().GetTL(P1, P2);
        }
        #endregion

        #region  删除评论
        public void DELCOMENT(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            var tl = new JH_Auth_TLB().GetEntities(" ID ='" + P1 + "'").FirstOrDefault();
            if (tl != null)
            {
                new JH_Auth_TLB().Delete(tl);
            }
        }
        #endregion

        #region 添加评论
        /// <summary>
        /// 添加评论
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="strParamData"></param>
        /// <param name="strUserName"></param>
        public void ADDCOMENT(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strMsgType = context.Request["MsgType"] ?? "";
            string strMsgLYID = context.Request["MsgLYID"] ?? "";
            string strPoints = context.Request["Points"] ?? "0";
            string strfjID = context.Request["fjID"] ?? "";
            JH_Auth_TL Model = new JH_Auth_TL();
            Model.CRDate = DateTime.Now;
            Model.CRUser = UserInfo.User.UserName;
            Model.CRUserName = UserInfo.User.UserRealName;
            Model.MSGContent = P1;
            Model.ComId = UserInfo.User.ComId;
            Model.MSGTLYID = strMsgLYID;
            Model.MSGType = strMsgType;
            Model.MSGisHasFiles = strfjID;
            int record = 0;
            int.TryParse(strPoints, out record);
            Model.Points = record;
            new JH_Auth_TLB().Insert(Model);
            if (Model.MSGType == "GZBG" || Model.MSGType == "RWGL" || Model.MSGType == "TSSQ")
            {
                int modelId = int.Parse(Model.MSGTLYID);
                string CRUser = "";
                string Content = UserInfo.User.UserRealName + "评论了您的";
                string modelName = "";
                switch (Model.MSGType)
                {
                    case "GZBG":
                        CRUser = new SZHL_GZBGB().GetEntity(d => d.ID == modelId).CRUser;
                        Content += "工作报告";
                        break;
                    case "RWGL":
                        CRUser = new SZHL_RWGLB().GetEntity(d => d.ID == modelId).CRUser;
                        Content += "任务管理";
                        break;
                    case "TSSQ":
                        CRUser = new SZHL_TSSQB().GetEntity(d => d.ID == modelId).CRUser;
                        Content += "话题";
                        break;
                }

                if (CRUser != UserInfo.User.UserName)
                {
                    SZHL_TXSX CSTX = new SZHL_TXSX();
                    CSTX.Date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    CSTX.APIName = "XTGL";
                    CSTX.ComId = UserInfo.User.ComId;
                    CSTX.FunName = "SENDPLMSG";
                    CSTX.CRUserRealName = UserInfo.User.UserRealName;
                    CSTX.MsgID = modelId.ToString();

                    CSTX.TXContent = Content;
                    CSTX.ISCS = "N";
                    CSTX.TXUser = CRUser;
                    CSTX.TXMode = Model.MSGType;
                    CSTX.CRUser = UserInfo.User.UserName;

                    TXSX.TXSXAPI.AddALERT(CSTX); //时间为发送时间 
                }
            }

            msg.Result = Model;
            if (Model.MSGisHasFiles == "")
                Model.MSGisHasFiles = "0";
            msg.Result1 = new FT_FileB().GetEntities(" ID in (" + Model.MSGisHasFiles + ")");
        }
        public void SENDPLMSG(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            SZHL_TXSX TX = JsonConvert.DeserializeObject<SZHL_TXSX>(P1);
            Article ar0 = new Article();
            ar0.Title = TX.TXContent;
            ar0.Description = "";
            ar0.Url = TX.MsgID;
            List<Article> al = new List<Article>();
            al.Add(ar0);
            if (!string.IsNullOrEmpty(TX.TXUser))
            {
                try
                {
                    //发送PC消息
                    UserInfo = new JH_Auth_UserB().GetUserInfo(TX.ComId.Value, TX.CRUser);
                    WXHelp wx = new WXHelp(UserInfo.QYinfo, TX.TXMode);
                    wx.SendTH(al, TX.TXMode, "A", TX.TXUser);
                    new JH_Auth_User_CenterB().SendMsg(UserInfo, TX.TXMode, TX.TXContent, TX.MsgID, TX.TXUser);
                }
                catch (Exception)
                {
                }
                //发送微信消息

            }
        }
        #endregion

        /// <summary>
        /// 上传文件
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void UPLOADFILE(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            try
            {
                int fid = 3;
                if (!string.IsNullOrEmpty(P1))
                {
                    fid = Int32.Parse(P1);
                }
                List<FT_File> ls = new List<FT_File>();
                //HttpPostedFile uploadFile = context.Request.Files["upFile"];
                for (int i = 0; i < context.Request.Files.Count; i++)
                {
                    HttpPostedFile uploadFile = context.Request.Files[i];
                    string originalName = uploadFile.FileName;

                    string[] temp = uploadFile.FileName.Split('.');

                    //保存图片

                    string filename = System.Guid.NewGuid() + "." + temp[temp.Length - 1].ToLower();

                    string URL = UserInfo.QYinfo.FileServerUrl + "fileupload?qycode=" + UserInfo.QYinfo.QYCode;
                    string md5 = SaveFile(URL, filename, uploadFile);
                    FT_File newfile = new FT_File();
                    newfile.ComId = UserInfo.User.ComId;
                    newfile.Name = uploadFile.FileName.Substring(0, uploadFile.FileName.LastIndexOf('.'));
                    newfile.FileMD5 = md5.Replace("\"", "");
                    newfile.FileSize = uploadFile.InputStream.Length.ToString();
                    newfile.FileVersin = 0;
                    newfile.CRDate = DateTime.Now;
                    newfile.CRUser = UserInfo.User.UserName;
                    newfile.UPDDate = DateTime.Now;
                    newfile.UPUser = UserInfo.User.UserName;
                    newfile.FileExtendName = temp[temp.Length - 1].ToLower();
                    newfile.FolderID = fid;
                    if (new List<string>() { "txt", "html", "doc", "mp4", "flv", "ogg", "jpg", "gif", "png", "bmp", "jpeg" }.Contains(newfile.FileExtendName.ToLower()))
                    {
                        newfile.ISYL = "Y";
                    }

                    new FT_FileB().Insert(newfile);

                    if (new List<string>() { "pdf", "doc", "docx", "ppt", "pptx", "xls", "xlsx" }.Contains(newfile.FileExtendName.ToLower()))
                    {
                        new FileHelp().CoverOffice(newfile.FileMD5, newfile.Name, newfile.FileExtendName.ToLower(), UserInfo);
                    }
                    int filesize = 0;
                    int.TryParse(newfile.FileSize, out filesize);
                    new FT_FileB().AddSpace(UserInfo.User.ComId.Value, filesize);
                    //msg.Result = newfile;
                    ls.Add(newfile);
                }
                msg.Result = ls;
            }
            catch (Exception e)
            {
                msg.ErrorMsg = "上传图片";
            }
        }

        /// <summary>
        /// 上传文件（文档中心）
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void UPLOADFILES(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            try
            {
                HttpPostedFile uploadFile = context.Request.Files["upFile"];
                string originalName = uploadFile.FileName;

                string[] temp = uploadFile.FileName.Split('.');

                //保存图片

                string filename = System.Guid.NewGuid() + "." + temp[temp.Length - 1].ToLower();

                string URL = UserInfo.QYinfo.FileServerUrl + "fileupload?qycode=" + UserInfo.QYinfo.QYCode;
                string md5 = SaveFile(URL, filename, uploadFile);
                string json = "[{filename:'" + uploadFile.FileName + "',md5:" + md5 + ",filesize:'" + uploadFile.InputStream.Length.ToString() + "'}]";

                QYWDManage qywd = new QYWDManage();
                qywd.ADDFILE(context, msg, json, P1, UserInfo);

            }
            catch (Exception e)
            {
                msg.ErrorMsg = "上传图片";
            }
        }

        /// <summary>
        /// 上传文件到服务器
        /// </summary>
        /// <param name="uploadUrl"></param>
        /// <param name="fileName"></param>
        /// <param name="uploadFile"></param>
        /// <returns></returns>
        public string SaveFile(string uploadUrl, string fileName, HttpPostedFile uploadFile)
        {
            try
            {
                string result = "";
                string boundary = "----------" + DateTime.Now.Ticks.ToString("x");
                HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(uploadUrl);
                webrequest.ContentType = "multipart/form-data; boundary=" + boundary;
                webrequest.Method = "POST";
                StringBuilder sb = new StringBuilder();
                sb.Append("--");
                sb.Append(boundary);
                sb.Append("\r\n");
                sb.Append("Content-Disposition: form-data; name=\"file");
                sb.Append("\"; filename=\"" + fileName + "\"");
                sb.Append("\"");
                sb.Append("\r\n");
                sb.Append("Content-Type: application/octet-stream");
                sb.Append("\r\n");
                sb.Append("\r\n");
                string postHeader = sb.ToString();
                byte[] postHeaderBytes = Encoding.UTF8.GetBytes(postHeader);
                byte[] boundaryBytes = Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");
                webrequest.ContentLength = uploadFile.InputStream.Length + postHeaderBytes.Length + boundaryBytes.Length;
                Stream requestStream = webrequest.GetRequestStream();
                requestStream.Write(postHeaderBytes, 0, postHeaderBytes.Length);
                byte[] buffer = new Byte[(int)uploadFile.InputStream.Length]; //声明文件长度的二进制类型
                uploadFile.InputStream.Read(buffer, 0, buffer.Length); //将文件转成二进制
                requestStream.Write(buffer, 0, buffer.Length); //赋值二进制数据 
                requestStream.Write(boundaryBytes, 0, boundaryBytes.Length);
                webrequest.UserAgent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)";
                WebResponse responce = webrequest.GetResponse();
                requestStream.Close();
                using (Stream s = responce.GetResponseStream())
                {
                    using (StreamReader sr = new StreamReader(s))
                    {
                        result = sr.ReadToEnd();
                    }
                }
                responce.Close();


                return result;
            }
            catch (Exception ex)
            {

            }

            return "";
        }
        //微信获取APP设置列表 初始化放弃
        public void GETMOBILEAPPTJDATA(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            //获取有权限的角色APP
            DataTable dtModel = new JH_Auth_ModelB().GETMenuList(UserInfo, P1);
            DataView dv = new DataView(dtModel);
            //获取套件
            DataTable dtTJ = dv.ToTable(true, new string[] { "ModelType", "TJId" }).OrderBy("ModelType asc");
            dtTJ.Columns.Add("Model", Type.GetType("System.Object"));
            foreach (DataRow row in dtTJ.Rows)
            {
                string tjId = row["TJID"].ToString();
                row["Model"] = dtModel.FilterTable("TJId='" + tjId + "'");
            }
            msg.Result = dtTJ;
        }
        //设置手机APP首页显示应用
        public void SETAPPINDEX(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string type = context.Request["type"] ?? "APPINDEX";//默认为APP首页显示菜单，传值为PC首页的快捷方式按钮
            //判断是否存在菜单的数据，存在只更新状态，不存在添加
            JH_Auth_UserCustomData customData = new JH_Auth_UserCustomDataB().GetEntity(d => d.UserName == UserInfo.User.UserName && d.DataType == type && d.ComId == UserInfo.User.ComId && d.DataContent == P1);
            string status = context.Request["Status"];
            string modelName = context.Request["name"] ?? "";
            if (customData != null)
            {
                customData.DataContent1 = status;
                new JH_Auth_UserCustomDataB().Update(customData);
            }
            else
            {
                customData = new JH_Auth_UserCustomData();
                customData.ComId = UserInfo.User.ComId;
                customData.UserName = UserInfo.User.UserName;
                customData.CRDate = DateTime.Now;
                customData.CRUser = UserInfo.User.UserName;
                customData.DataContent = P1;
                customData.Remark = modelName;
                customData.DataContent1 = "Y";
                customData.DataType = type;
                new JH_Auth_UserCustomDataB().Insert(customData);
            }
            msg.Result = customData;

        }
        #region 设置部门人员的查看权限
        public void SETBRANCHQX(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            int deptCode = int.Parse(P2);
            JH_Auth_Branch branch = new JH_Auth_BranchB().GetEntity(d => d.ComId == UserInfo.User.ComId && d.DeptCode == deptCode);
            branch.TXLQX = P1;
            branch.IsHasQX = context.Request["qx"] ?? "N";
            new JH_Auth_BranchB().Update(branch);

        }
        #endregion
        #region 设置部门人员的查看角色
        public void SETROLEQX(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            int roleCode = int.Parse(P2);
            JH_Auth_Role role = new JH_Auth_RoleB().GetEntity(d => d.RoleCode == roleCode);
            role.RoleQX = P1;
            role.IsHasQX = context.Request["qx"] ?? "N";
            new JH_Auth_RoleB().Update(role);
            msg.Result = role;
        }
        #endregion

        #region 是否安装通讯录
        public void GETISAZTXL(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            msg.Result = new JH_Auth_QY_ModelB().GetEntity(d => d.ComId == UserInfo.User.ComId && d.ModelID == 17).AgentId;
        }
        #endregion

        #region 获取已发送短信数及容量使用情况
        public void GETDXANDSPACE(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            decimal DXCost = decimal.Parse(System.Configuration.ConfigurationManager.AppSettings["DXCost"]);
            //已发送短信总数量
            msg.Result = new SZHL_DXGLB().GetEntities(d => d.ComId == UserInfo.User.ComId.Value).Count();
            msg.Result1 = (int)(UserInfo.QYinfo.AccountMoney.Value / DXCost);
            msg.Result2 = UserInfo.QYinfo.QySpace / 10000000000;
            string strSql = string.Format("SELECT isnull(sum(CAST( FileSize  as DECIMAL(18,2))),0) from  FT_File where ComId=" + UserInfo.User.ComId);
            object obj = new FT_FileB().ExsSclarSql(strSql);
            decimal Size = 0;
            string fileSize = obj.ToString();
            if (fileSize.Length < 4)
            {
                Size = decimal.Parse(fileSize);
                msg.Result4 = "kb";
            }
            if (fileSize.Length >= 4 && fileSize.Length <= 8)
            {
                Size = Math.Round(decimal.Parse(fileSize) / 10000, 2);
                msg.Result4 = "M";
            }
            if (fileSize.Length > 8)
            {
                Size = Math.Round(decimal.Parse(fileSize) / 100000000, 2);
                msg.Result4 = "G";
            }
            msg.Result3 = Size;
        }
        #endregion

        #region  扩展字段
        public void GETEXTENDFIELD(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strWhere = "";
            string pdid = context.Request["PDID"] ?? "0";
            if (P1 == "LCSP")
            {
                if (P2 != "")
                {
                    int id = Int32.Parse(P2);
                    var lc = new SZHL_LCSPB().GetEntity(p => p.ID == id && p.ComId == UserInfo.User.ComId);
                    strWhere = " and PDID='" + lc.LeiBie + "'";
                }
                else
                {
                    strWhere = " and PDID='" + pdid + "'";
                }
            }
            DataTable dt = new JH_Auth_ExtendModeB().GetDTByCommand(string.Format("select * from JH_Auth_ExtendMode where ComId='{0}' and TableName='{1}' " + strWhere, UserInfo.User.ComId, P1));
            msg.Result = dt;
        }
        public void ADDEXTENDFIELD(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            JH_Auth_ExtendMode extMode = JsonConvert.DeserializeObject<JH_Auth_ExtendMode>(P1);
            if (extMode.ID == 0) //add
            {
                extMode.ComId = UserInfo.User.ComId;
                extMode.CRUser = UserInfo.User.UserName;
                extMode.CRDate = DateTime.Now;
                new JH_Auth_ExtendModeB().Insert(extMode);
            }
            else //edit
            {
                new JH_Auth_ExtendModeB().Update(extMode);
            }

            msg.Result = extMode;

        }
        public void DELEXTENDFIELD(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            new JH_Auth_ExtendModeB().Delete(p => p.ID.ToString() == P1);
        }
        public void GETEXTDATA(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            if (P1 == "YGGL")
            {
                JH_Auth_User user = new JH_Auth_UserB().GetEntity(d => d.UserName == P2 && d.ComId == UserInfo.User.ComId);
                if (user != null)
                {
                    P2 = user.ID.ToString();
                }
                else
                {
                    P2 = "";
                }
            }
            string strWhere = "";
            string pdid = context.Request["PDID"] ?? "0";
            if (P1 == "LCSP")
            {
                if (P2 != "")
                {
                    int id = Int32.Parse(P2);
                    var lc = new SZHL_LCSPB().GetEntity(p => p.ID == id && p.ComId == UserInfo.User.ComId);
                    strWhere = " and PDID='" + lc.LeiBie + "'";
                }
                else
                {
                    strWhere = " and PDID='" + pdid + "'";
                }
            }
            DataTable dt = new JH_Auth_ExtendModeB().GetDTByCommand(string.Format("select j.ComId, j.ID, j.TableName, j.TableFiledColumn, j.TableFiledName, j.TableFileType, j.DefaultOption, j.DefaultValue, j.IsRequire, d.ExtendModeID, d.ID AS ExtID, d.DataID, d.ExtendDataValue from [dbo].[JH_Auth_ExtendMode] j left join JH_Auth_ExtendData d on j.ComId=d.ComId and j.ID=d.ExtendModeID and d.DataID='{2}' where j.ComId='{0}' and j.TableName='{1}' " + strWhere, UserInfo.User.ComId, P1, P2));

            msg.Result = dt;
        }
        public void UPDATEEXTDATA(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int dataid = Int32.Parse(P2);
            string ExtData = context.Request.Params["ExtData"];
            if (!string.IsNullOrEmpty(ExtData))
            {
                List<ExtDataModel> ext = JsonConvert.DeserializeObject<List<ExtDataModel>>(ExtData);
                if (ext.Count > 0)
                {
                    foreach (var v in ext)
                    {
                        var extModel = new JH_Auth_ExtendDataB().GetEntity(p => p.ID == v.ExtID);
                        if (extModel == null)
                        {
                            JH_Auth_ExtendData jext = new JH_Auth_ExtendData();
                            jext.ComId = v.ComId;
                            jext.DataID = jext.DataID == null ? dataid : jext.DataID;
                            jext.TableName = v.TableName;
                            jext.ExtendModeID = v.ID;
                            jext.ExtendDataValue = v.ExtendDataValue;
                            jext.CRUser = UserInfo.User.UserName;
                            jext.CRDate = DateTime.Now;

                            new JH_Auth_ExtendDataB().Insert(jext);
                        }
                        else
                        {
                            extModel.ExtendDataValue = v.ExtendDataValue;
                            new JH_Auth_ExtendDataB().Update(extModel);
                        }
                    }
                }

            }

        }


        #endregion

        #region 获取注册用户
        /// <summary>
        /// 获取注册用户
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETQYUSER(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strWhere = " 1=1 ";
            if (P1 != "")
            {
                strWhere += string.Format(" And ( QYName like '%{0}%' )", P1);
            }
            int page = 0;
            int pagecount = 8;
            int.TryParse(context.Request.QueryString["p"] ?? "1", out page);
            int.TryParse(context.Request.QueryString["pagecount"] ?? "8", out pagecount);//页数
            page = page == 0 ? 1 : page;
            int recordCount = 0;
            DataTable dt = new JH_Auth_QYB().GetDataPager(" JH_Auth_QY ", " * ", pagecount, page, " CRDate desc ", strWhere, ref recordCount);
            msg.Result = dt;
            msg.Result2 = UserInfo.User.ID;
            //msg.Result1 = Math.Ceiling(recordCount * 1.0 / 10);
            msg.Result1 = recordCount;
        }
        #endregion

        #region 判断默认密码是否为默认密码123456
        public void ISDEFAULTPASS(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string password = "123456";
            password = CommonHelp.GetMD5(password);
            if (UserInfo.User.UserPass == password)
            {
                msg.ErrorMsg = "modify";
            }
        }
        #endregion

        #region 获取企业授权信息
        public void GETAUTHINFO(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            JH_Auth_QY_TJ jaqt = new JH_Auth_QY_TJB().GetEntities(p => p.ComId == UserInfo.QYinfo.ComId).FirstOrDefault();
            if (jaqt != null)
            {
                JH_Auth_WXPJ jaw = new JH_Auth_WXPJB().GetEntity(p => p.TJID == jaqt.TJID);
                if (jaw != null)
                {
                    string SuiteToken = ThirdPartyAuthApi.GetSuiteToken(jaw.TJID, jaw.TJSecret, jaw.Ticket).suite_access_token;

                    var url = string.Format("https://qyapi.weixin.qq.com/cgi-bin/service/get_auth_info?suite_access_token={0}", SuiteToken);

                    var data = new
                    {
                        suite_id = jaqt.TJID,
                        auth_corpid = jaqt.CorpID,
                        permanent_code = jaqt.PermanentCode
                    };

                    QjySaaSWeb.API.GetAuthInfoResult gair = Senparc.Weixin.CommonAPIs.CommonJsonSend.Send<QjySaaSWeb.API.GetAuthInfoResult>(null, url, data, CommonJsonSendType.POST);
                    msg.Result = gair;
                }
            }
        }
        #endregion

        #region 设置应用信息
        public void SETAPPINFO(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            var jaqts = new JH_Auth_QY_TJB().GetEntities(p => p.ComId == UserInfo.QYinfo.ComId);
            foreach (var jaqt in jaqts)
            {
                if (jaqt != null)
                {
                    JH_Auth_WXPJ jaw = new JH_Auth_WXPJB().GetEntity(p => p.TJID == jaqt.TJID);
                    if (jaw != null)
                    {
                        string SuiteToken = ThirdPartyAuthApi.GetSuiteToken(jaw.TJID, jaw.TJSecret, jaw.Ticket).suite_access_token;

                        var url = string.Format("https://qyapi.weixin.qq.com/cgi-bin/service/get_auth_info?suite_access_token={0}", SuiteToken);

                        var data = new
                        {
                            suite_id = jaqt.TJID,
                            auth_corpid = jaqt.CorpID,
                            permanent_code = jaqt.PermanentCode
                        };

                        QjySaaSWeb.API.GetAuthInfoResult gair = Senparc.Weixin.CommonAPIs.CommonJsonSend.Send<QjySaaSWeb.API.GetAuthInfoResult>(null, url, data, CommonJsonSendType.POST);

                        foreach (var yy in gair.auth_info.agent)
                        {
                            var qm = new JH_Auth_QY_ModelB().GetEntities(p => p.ComId == UserInfo.User.ComId && p.AgentId == yy.agentid).FirstOrDefault();
                            if (qm != null)
                            {
                                var ml = new JH_Auth_ModelB().GetEntities(p => p.ComId == UserInfo.User.ComId && p.ID == qm.ModelID).FirstOrDefault();
                                if (ml != null && ml.AppType == "1")
                                {
                                    WXHelp wx = new WXHelp(UserInfo.QYinfo, qm.QYModelCode);

                                    //Senparc.Weixin.QY.AdvancedAPIs.App.SetAppPostData appdata=new Senparc.Weixin.QY.AdvancedAPIs.App.SetAppPostData();
                                    //appdata.agentid = yy.agentid;
                                    //appdata.isreportenter = 1;
                                    //wx.SetAPPinfo(appdata);

                                    var url1 = string.Format("https://qyapi.weixin.qq.com/cgi-bin/agent/set?access_token={0}", wx.GetToken());
                                    var data1 = new
                                    {
                                        agentid = yy.agentid,
                                        isreportenter = 1
                                    };

                                    QyJsonResult ret = Senparc.Weixin.CommonAPIs.CommonJsonSend.Send<QyJsonResult>(null, url1, data1, CommonJsonSendType.POST);
                                }
                            }
                        }
                    }
                }
            }
        }
        #endregion

        #region MyRegion
        /// <summary>
        /// 根据部门编号获取可用人员
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="strUserName"></param>
        public void GETYUSERBYCODEPAGE(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int deptCode = 0;
            if (!int.TryParse(P1, out deptCode))
            {
                deptCode = 1;
            }
            JH_Auth_Branch branch = new JH_Auth_BranchB().GetBMByDeptCode(UserInfo.User.ComId.Value, deptCode);
            string strQXWhere = string.Format(" And ( u.branchCode={0} or b.Remark1 like '{1}%')", deptCode, (branch.Remark1 == "" ? "" : branch.Remark1 + "-") + branch.DeptCode);
            string branchqx = new JH_Auth_BranchB().GetBranchQX(UserInfo);
            if (branch.DeptRoot == -1 && !string.IsNullOrEmpty(branchqx))
            {
                strQXWhere = " And (";
                int i = 0;
                foreach (int dept in branchqx.SplitTOInt(','))
                {
                    JH_Auth_Branch branchQX = new JH_Auth_BranchB().GetBMByDeptCode(UserInfo.QYinfo.ComId, dept);
                    strQXWhere += string.Format((i == 0 ? "" : "And") + "  ( u.branchCode!={0} and b.Remark1 NOT like '{1}%')", dept, (branchQX.Remark1 == "" ? "" : branchQX.Remark1 + "-") + branchQX.DeptCode);
                    i++;
                }
                strQXWhere += ")";
            }
            string strwhere = " u.IsUse='Y' and u.ComId=" + UserInfo.User.ComId;
            strwhere += string.Format(" and (u.isSupAdmin is NULL  or u.isSupAdmin!='Y') {0}", strQXWhere);

            if (P2 != "")
            {
                strwhere += string.Format(" And (u.UserName like '%{0}%'  or u.UserRealName like '%{0}%'  or b.DeptName like '%{0}%' or u.mobphone like '%{0}%')", P2);
            }
            int page = 0;
            int.TryParse(context.Request.QueryString["p"] ?? "1", out page);
            page = page == 0 ? 1 : page;
            int recordCount = 0;
            int pageSize = 0;
            int.TryParse(context.Request.QueryString["pageSize"] ?? "1", out pageSize);
            pageSize = pageSize == 0 ? 20 : pageSize;
            DataTable dtUser = new JH_Auth_UserB().GetDataPager(" JH_Auth_User u  inner join JH_Auth_Branch b on u.branchCode=b.DeptCode ", "u.*,b.DeptName,b.DeptCode", pageSize, page, "b.DeptShort,ISNULL(u.UserOrder, 1000000) asc", strwhere, ref recordCount);

            msg.Result = dtUser;
            msg.Result1 = recordCount;
        }
        #endregion

        #region 人力资源列表
        /// <summary>
        /// 人力资源列表
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETRLZYLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string userName = UserInfo.User.UserName;
            string strWhere = " 1=1 and cc.ComId=" + UserInfo.User.ComId;

            int month = DateTime.Now.Month;
            string strTime = new DateTime(DateTime.Now.Year, month, 1).ToShortDateString();
            string endTime = new DateTime(DateTime.Now.Year, month + 1, 1).ToShortDateString();


            if (P1 != "")
            {
                int page = 0;
                int pagecount = 8;
                int.TryParse(context.Request.QueryString["p"] ?? "1", out page);
                int.TryParse(context.Request.QueryString["pagecount"] ?? "8", out pagecount);//页数
                page = page == 0 ? 1 : page;
                int total = 0;
                DataTable dt = new DataTable();
                switch (P1)
                {
                    case "1": //迟到
                        {
                            strWhere += string.Format(" And cc.KQDate BETWEEN '{0}' and '{1}'", strTime, endTime);
                            strWhere += " And cc.Type ='0' and cc.Status='1' and cc.KQUser='" + userName + "'";
                            dt = new SZHL_CCXJB().GetDataPager("SZHL_KQJL cc", " cc.* ", pagecount, page, " cc.KQDate desc", strWhere, ref total);
                        }
                        break;
                    case "2": //早退
                        {
                            strWhere += string.Format(" And cc.KQDate BETWEEN '{0}' and '{1}'", strTime, endTime);
                            strWhere += " And cc.Type ='1' and Status='2' and cc.KQUser='" + userName + "'";
                            dt = new SZHL_CCXJB().GetDataPager("SZHL_KQJL cc", " cc.* ", pagecount, page, " cc.KQDate desc", strWhere, ref total);
                        }
                        break;
                    case "3":  //请假
                        {
                            strWhere += string.Format(" And cc.CRDate BETWEEN '{0}' and '{1}' and cc.CRUser='{2}'", strTime, endTime, userName);

                            var intProD = new Yan_WF_PIB().GetYSHUserPI(userName, UserInfo.User.ComId.Value, "CCXJ").Select(d => d.ID.ToString()).ToList();
                            if (intProD.Count > 0)
                            {
                                strWhere += " And intProcessStanceid in (" + (intProD.ListTOString(',') == "" ? "0" : intProD.ListTOString(',')) + ")";

                            }
                            else
                            {
                                strWhere += " And 1=0";
                            }

                            dt = new SZHL_CCXJB().GetDataPager("SZHL_CCXJ cc left join JH_Auth_ZiDian zd on cc.LeiBie=zd.ID", " cc.*,zd.TypeName,dbo.fn_PDStatus(cc.intProcessStanceid) AS StateName ", pagecount, page, " cc.CRDate desc", strWhere, ref total);

                            if (dt.Rows.Count > 0)
                            {
                                dt.Columns.Add("FileList", Type.GetType("System.Object"));
                                foreach (DataRow dr in dt.Rows)
                                {
                                    if (dr["Files"] != null && dr["Files"].ToString() != "")
                                    {
                                        dr["FileList"] = new FT_FileB().GetEntities(" ID in (" + dr["Files"].ToString() + ")");
                                    }
                                }
                            }
                        }
                        break;
                }



                msg.Result = dt;
                msg.Result1 = total;
            }
        }
        #endregion

        #region 应用管理v5
        //应用管理
        public void GETAPPLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string sql = string.Format(@"SELECT * FROM JH_Auth_Model WHERE (ComId=0 or ComId={0} ) AND ISNULL(PModelCode,'') != 'RLZY' AND ISNULL(PModelCode,'') !='CRM' ", UserInfo.User.ComId);
            DataTable dt = new JH_Auth_ModelB().GetDTByCommand(sql);

            msg.Result = dt;
        }
        #endregion
        #region 应用管理v5
        //应用管理
        public void ISSUPADMIN(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strUserName = P1;
            string sql = string.Format(@"SELECT TOP 1 * FROM [QJY_PXKS].[dbo].[JH_Auth_Branch] where ComId = '10312'  and  ','+BranchLeader+',' like '%,{0},%' ORDER BY ID", UserInfo.User.UserName);
            DataTable dt = new JH_Auth_BranchB().GetDTByCommand(sql);
            msg.Result = dt;
        }
        #endregion



        public void SETGW(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            //获取有权限的角色APP
            string strUsers = P1.TrimEnd(',');
            string strGW = P2;
            string strSQL = "UPDATE JH_Auth_User  SET UserGW='" + strGW + "' WHERE UserName IN ('" + strUsers.ToFormatLike(',') + "') ";
            new JH_Auth_ExtendModeB().ExsSclarSql(strSQL);
        }


    }

    public class WXUserBR
    {
        public int DeptCode { get; set; }
        public string DeptName { get; set; }
        public dynamic DeptUser { get; set; }
        public int DeptUserNum { get; set; }
        public List<WXUserBR> SubDept { get; set; }
    }

    public class ExtDataModel
    {
        public int ComId { get; set; }
        public int ID { get; set; }
        public string TableName { get; set; }
        public string TableFiledColumn { get; set; }
        public string TableFiledName { get; set; }
        public string TableFileType { get; set; }
        public string DefaultOption { get; set; }
        public string DefaultValue { get; set; }
        public string IsRequire { get; set; }
        public int? ExtendModeID { get; set; }
        public int? ExtID { get; set; }
        public int? DataID { get; set; }
        public string ExtendDataValue { get; set; }
    }

}